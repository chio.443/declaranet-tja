# Declaranet 2021

_Sistema de declaración de situación patrimonial y de intereses para servidores públicos del Poder Ejecutivo del Estado de Guanajuato_

## Comenzando 🚀

_Estas instrucciones te permiten obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_¿Qué necesita para instalar el software y cómo instalarlo?_

**Requisitos del sistema**


[Apache 2.4](https://httpd.apache.org/download.cgi)

[PHP 7.4](https://www.php.net/downloads.php)


**Aplicaciones**

[Composer](https://getcomposer.org/)

[Node.js 10.24.1](https://nodejs.org/download/release/v10.24.1/)

[Git](https://git-scm.com/downloads)


**Base de datos**

[PostgreSQL 12](https://www.postgresql.org/download/)



### Instalación 🔧

_Después de clonar el repositorio, se deben ejecutar una serie de comandos tanto en el frontend como en el backend._

**FRONTEND**


_Ir a la carpeta contenedora del proyecto declaranetcompleto2019\declaranet_vue_

_Instalar vue globalmente ejecutando los sigueintes comandos:_

```
npm install -g @vue/cli
```

```
npm install --save-dev
```

**BACKEND**

_Ir a la carpeta contenedora del proyecto declaranetcompleto2019\declaranet_laravel y ejecutar el sigueinte comando:_

```
composer update
```

**Base de datos**

_Creamos una nueva base de datos con el nombre de **declaranet**_



## Configuracion⚙️

_La base de datos está configurada, dentro de la carpeta declaranet_laravel / config / database.php buscamos las líneas correspondientes al nombre de la base de datos, el usuario y la contraseña_
```
pgsql -

'database' => env('DB_DATABASE', 'declaranet'),
'username' => env('DB_USERNAME', 'root'),
'password' => env('DB_PASSWORD', ''),
```
_Se puede configurar los datos en un archivo de environment, en tu proyecto local, para eso buscamos el archivo.env_

**Base de datos**

_Para iniciar la base de datos, ir a la carpeta contenedora del proyecto /declaranetcompleto2019/declaranet_laravel y ejecutamos el siguiente comando_

```
php artisan migrate:fresh --seed
```
_Este comando crearía las tablas de la base de datos así como los catálogos necesarios para el funcionamiento de la aplicación._

**FRONTEND**

_Ir a la carpeta contenedora del proyecto declaranetcompleto2019\declaranet_vue


_Configurar el archivo main.js dentro de la carpeta /declaranet_vue/src/main.js y el archivo /declaranet_vue/public/conf.json pra agregar las rutas correspondientes al dominio y google analytics_

_main.js_
```
 var apisUrl = "dominio.com/declaranet/api/";
```

```
Vue.use(VueAnalytics, {
    id: ["UA-000000000-0"],
    router,
});

```
_conf.json_
```
{
  "ruta": "https://declaranet.com"
}
```

_Ejecutar el proyecto_
```
npm run dev
```
_Compilar el proyecto_

```
npm run build
```
_Esto genera los archivos correspondientes al frontend dentro de la carpeta de public de laravel_


**BACKEND**

_Ir a la carpeta contenedora del proyecto /declaranetcompleto2019/declaranet_laravel

```
php artisan serve
```

## Construido con 🛠️

_Las herramientas utilizadas en la elaboración de este proyecto son:_

* [Vue](https://vuejs.org/) - El framework para el frontend
* [Laravel](https://laravel.com/) - El framework para el backend
