import Vue from "vue";
import VueStorage from "vuestorage";
import "./pollyfills";
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import VueNotify from "vue-notifyjs";
import store from "./store.js";
import VeeValidate, {
  Validator
} from "vee-validate";
import AppModal from "./components/Modal.component.vue";
import Rating from "./components/Rating.component.vue";
import es from "vee-validate/dist/locale/es";
import VueSweetalert2 from "vue-sweetalert2";
import VueProgressBar from "vue-progressbar";
import lang from "element-ui/lib/locale/lang/es";
import locale from "element-ui/lib/locale";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import VueNumeric from "vue-numeric";
/* import DatePicker from 'vue2-datepicker' */
import VueAnalytics from "vue-analytics";
import vuetify from "./plugins/vuetify";
import ThemifyIcon from "vue-themify-icons";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import axios from 'axios';
//import ruta from "../static/conf.js";
// const ruta = () => import("../static/conf.js");

import {
  Rate
} from "element-ui";

import VueJWT from "vuejs-jwt";
/* import VueMoment from 'vue-moment' */

let optionsjwt = {
  signKey: "my-secret-fucking-key",
  keyName: "app-token-declaranet"
};

Vue.use(VueJWT, optionsjwt);

export const eventBus = new Vue();

const config_validate = {
  aria: true,
  classes: true,
  delay: 0,
  dictionary: null,
  errorBagName: "errors", // change if property conflicts
  events: "input|blur",
  fieldsBagName: "veeFields",
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: "validations", // the nested key under which the validation messages will be located
  inject: true,
  locale: "es",
  validity: false,
  useConstraintAttrs: true
};

import {
  library,
  dom
} from "@fortawesome/fontawesome-svg-core";
import {
  fas
} from "@fortawesome/free-solid-svg-icons";
import {
  fab
} from "@fortawesome/free-brands-svg-icons";
import {
  far
} from "@fortawesome/free-regular-svg-icons";
import {
  FontAwesomeIcon
} from "@fortawesome/vue-fontawesome";

// Plugins
import GlobalComponents from "./gloablComponents";
import GlobalDirectives from "./globalDirectives";
import SideBar from "./components/UIComponents/SidebarPlugin";

// router setup
import routes from "./routes/routes";

import {
  mix
} from "./mixin";
import {
  mixCat
} from "./mixinCatalogs";
// library imports
import "./filters";
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./assets/sass/paper-dashboard.scss";
import "./assets/sass/element_variables.scss";
import "./assets/sass/demo.scss";
import "./assets/css/global.css";

import Currency from "./components/UIComponents/CurrencyNumber.vue";
//import SpinnerPlugin from './spinner.js'
/*import {
  BreedingRhombusSpinner
} from 'epic-spinners'
*/
dom.watch();
library.add(fas, fab, far);

/* Vue.component('date-picker', DatePicker) */
Vue.component("currency", Currency);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.mixin(mix);
Vue.mixin(mixCat);
Vue.component("app-modal", AppModal);
Vue.component("rating", Rating);
Vue.use(VueResource);
Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(GlobalDirectives);
Vue.use(GlobalComponents);
Vue.use(VueNotify);
Vue.use(VueNumeric);
Vue.use(SideBar);
Vue.use(Rate);
Vue.use(VueStorage);
Vue.component(ThemifyIcon);
/* require('moment/locale/es')

Vue.use(VueMoment) */
/*
Vue.use(SpinnerPlugin)
Vue.component('breeding-rhombus-spinner', BreedingRhombusSpinner)
*/
Vue.use(VueProgressBar, {
  color: "rgb(249, 185, 0)",
  failedColor: "red",
  height: "4px"
});

// Vue.use(VeeValidate)
Validator.localize("es", es);

const isCurp = (curp, param = true) => {
  var re = /^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$/;

  var validado = curp.match(re);

  if (!validado) {
    return false;
  }
  return true; //Validado
}


const isRfc = (rfc, param = true) => {
  const re = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/;

  var validado = rfc.match(re);

  if (!validado) {
    return false;
  }
  return true;
}


Validator.extend("rfc", {
  getMessage: field =>
    "El formato de la RFC no es válido, verifica la información.",
  validate: isRfc
});

Validator.extend("curp", {
  getMessage: field =>
    "El formato de la CURP no es válido, verifica la información.",
  validate: isCurp
});

Validator.extend("uppercase", {
  getMessage: field =>
    "Este campo requiere utilizar solo mayusculas y números.",
  validate: value => {
    if (value.match(/^[A-Z0-9]*$/)) {
      return true;
    } else {
      return false;
    }
  }
});

const dictionary = {
  es: {
    messages: {
      _default: () => "El campo no es válido",
      required: () => "El campo es obligatorio",
      numeric: () => "El campo debe ser número",
    }
  }
};
// Override and merge the dictionaries
Validator.localize(es);
//Validator.localize('es', dictionary.es);

Vue.use(VeeValidate, config_validate);
locale.use(lang);

var ruta = "";

// ruta = require("../static/conf.json");

ruta = "//sistemas.tjagto.gob.mx/app_declaranet";

if (window.location.hostname == 'localhost') {
  Vue.http.options.root = '//localhost:8000/api/';
  Vue.prototype.$rootUrl = '//localhost:8000';
} else {
  Vue.http.options.root = ruta + '/index.php/api/';
  Vue.prototype.$rootUrl = ruta;
}

var TotalLoads = 0;
var TotalLoadsDone = 0;

Vue.http.interceptors.push(function (request) {
  const token = this.$jwt.getToken();
  const auth = "Bearer " + (token ? token : "");
  request.headers.set("Authorization", auth);

  //console.log(request.headers);

  this.$Progress.start();
  //this.$Spinner.show()
  TotalLoads++;

  return function (response) {
    TotalLoadsDone++;
    if (TotalLoadsDone == TotalLoads) {
      // console.log('!!!');
      this.$Progress.finish();
      // this.$Spinner.hide()
      // this.$Spinner.hide();
      TotalLoads = 0;
      TotalLoadsDone = 0;
    }
  };
});

window.$ = window.jQuery = require("jquery");

///

const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "active"
});

//
Vue.use(VueAnalytics, {
  id: "UA-140095545-1",
  router
});

router.beforeEach((to, from, next) => {
  /*
    var token = Vue.prototype.$jwt.hasToken();
    var jwt = '';
    if (token) {
      jwt = Vue.prototype.$jwt.decode();
    }
  */
  var ip_id = localStorage.getItem("app-token-declaranet");

  if (to.path !== "/login" && to.path !== "/busqueda" && !to.path.includes("/recuperapass") && !ip_id) {
    next("/login");
  } else {
    next();
  }
});

new Vue({
  router,
  vuetify,
  store,
  el: "#app",
  render: h => h(App)
});
