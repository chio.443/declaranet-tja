export const mix = {
  data() {
    return {
      ip_id: "",
      modalComponent: "",
      modalTitle: "",
      modalData: "",
      modalSize: "",
      modalScroll: false,
      modalFooterComp: "",
      tableButtons: [],
      excelTitle: ""
    };
  },
  stored: {
    checkedlog: {
      type: JSON,
      key: function () {
        return "log";
      },
      default: function () {
        return [];
      }
    }
  },
  watch: {
    checkedlog: function () {
      if (this.$router) {
        for (let j = 0; j < this.$router.options.routes.length; ++j) {
          var ruta = this.$router.options.routes[j];
          if ((ruta.path = "/busqueda")) {
            return "";
          }
        }
        this.$router.push("/login");
      }

      // console.log(this.checkedlog);
    }
  },

  methods: {

    OK(msg) {
      this.$swal({
        title: "OK",
        html: msg,
        type: "success"
      });
    },
    StepOk(msg) {
      this.$swal({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        type: "success",
        title: msg
      });
    },
    StepInfo(msg) {
      this.$swal({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        type: "info",
        title: msg
      });
    },
    StepError(error, time = 0) {
      this.onReady();
      this.$Progress.fail();
      let e = new Array();
      if (error.status && error.status == 401) {
        // console.log('unauthorized');
        // location.href = '#/login';
        this.$router.push("/login");
        return;
      } else if (error.data && error.data.errors) {
        for (let x in error.data.errors) {
          e.push(error.data.errors[x]);
          // console.log(error.data.errors[x]);
        }
      } else if (error.data && error.data.message) {
        e.push(error.data.message);
      } else {
        e.push(error);
      }
      var close = true;
      if (time > 0) {
        close = false;
      }
      this.$swal({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        type: "error",
        title: "Error",
        showCloseButton: close,
        timer: time,
        html: e.join("<br>")
      });
    },
    StepW(error) {
      this.onReady();
      this.$Progress.fail();
      let e = new Array();
      if (error.status && error.status == 401) {
        // console.log('unauthorized');
        this.$router.push("/login");
        return;
      } else if (error.data && error.data.errors) {
        for (let x in error.data.errors) {
          e.push(error.data.errors[x]);
          // console.log(error.data.errors[x]);
        }
      } else if (error.data && error.data.message) {
        e.push(error.data.message);
      } else {
        e.push(error);
      }
      this.$swal({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        type: "warning",
        title: "Verifica",
        showCloseButton: close,
        timer: 5000,
        html: e.join("<br>")
      });
      if (this.$el.querySelector("[aria-invalid=true]")) {
        this.$el.querySelector("[aria-invalid=true]").scrollIntoView({
          block: "center",
          behavior: "smooth"
        });
      }
    },
    getToken() {
      // Return token from cookies or local storage
      let token = this.$jwt.getToken();
      if (token) {
        return token;
      } else {
        return null;
      }
    },
    hasToken() {
      // Return true / false - check if a JWT token is stored in cookies or local storage
      let token = this.$jwt.hasToken();
      return token;
    },
    Err(error) {
      this.onReady();
      this.$Progress.fail();
      let e = new Array();
      if (error.status && error.status == 401) {
        // console.log('unauthorized');
        this.$router.push("/login");
        return;
      } else if (error.data && error.data.errors) {
        for (let x in error.data.errors) {
          e.push(error.data.errors[x]);
          // console.log(error.data.errors[x]);
        }
      } else if (error.data && error.data.message) {
        e.push(error.data.message);
      } else {
        e.push(error);
      }
      this.$swal({
        title: "Error",
        html: e.join("<br>"),
        type: "error"
      });
    },
    Ask(msg, fn) {
      this.$swal({
        title: "Confirme",
        html: msg,
        type: "question",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "OK",
        cancelButtonText: "Cancelar"
      }).then(result => {
        if (result.value) {
          fn();
        }
      });
    },
    /* const { value: text } = await Swal.fire({
      input: 'textarea',
      inputLabel: 'Message',
      inputPlaceholder: 'Type your message here...',
      inputAttributes: {
        'aria-label': 'Type your message here'
      },
      showCancelButton: true
    })

    if (text) {
      Swal.fire(text)
    } */
    AskMessage(title, msg, okbtn = 'OK', fn) {
      this.$swal({
        title: title,
        width: 650,
        html: '<div class="text-left pt-3">' + msg + '</div>',
        // type: "question",
        input: 'textarea',
        inputLabel: 'Observaciones',
        inputPlaceholder: 'Escribe tus observaciones aquí...',
        inputAttributes: {
          'aria-label': 'Escribe tus observaciones aquí'
        },
        inputValidator: (value) => {
          if (!value) {
            return 'Debe escribir una observación'
          }
        },
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: okbtn,
        cancelButtonText: "Cancelar",
      }).then(result => {
        if (result.value) {
          fn(result.value);
        }
      });
    },
    Ask2(msg, fn, fn2) {
      this.$swal({
        title: "Confirme",
        html: msg,
        type: "question",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#F4B468",
        confirmButtonText: "Filtrar",
        cancelButtonText: "Trae Todo"
      }).then(result => {
        if (result.value) {
          fn();
        } else {
          fn2();
        }
      });
    },
    Modal(selector) {
      $(selector ? selector : ".modal")
        .modal("show")
        .on("hidden.bs.modal", () => {
          this.modalComponent = "";
          this.modalFooterComp = "";
          /* $('.summernote').summernote('destroy'); */
        });
    },
    getId() {
      let ip_id = this.getKey("uid");
      if (ip_id) {
        return ip_id;
      }
      return false;
    },
    getKey(campo) {
      let token = this.$jwt.decode();
      if (token) {
        return token[campo];
      }
    },
    /* getToken() {
      return localStorage.getItem('app-token-declaranet')
    },*/
    setSession(declarante) {
      localStorage.setItem("app-token-declaranet", declarante.token);
    },
    makeDataTable(selector) {
      $(selector).DataTable({
        retrieve: true,
        dom: "Bfrtip",
        ordering: false,
        // responsive: true,
        /* order: [
          [0, 'desc']
        ], */
        // buttons: ['selectAll', 'selectNone', 'excelHtml5'],
        buttons: this.tableButtons,
        oClasses: {
          sFilterInput: "form-control",
          sLengthSelect: "form-control"
        },
        pageLength: 50,
        autoWidth: false,
        // 'searching': false,
        language: {
          sProcessing: "Procesando...",
          sLengthMenu: "Mostrar _MENU_ registros",
          sZeroRecords: "No se encontraron resultados",
          sEmptyTable: "Ningún registro encontrado",
          sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          sInfoEmpty: "0 registros",
          sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
          sInfoPostFix: "",
          sSearch: "Filtrar:",
          sUrl: "",
          sInfoThousands: ",",
          sLoadingRecords: "Cargando...",
          oPaginate: {
            sFirst: "Primero",
            sLast: "Último",
            sNext: "Siguiente",
            sPrevious: "Anterior"
          },
          oAria: {
            sSortAscending: ": Activar para ordenar la columna de manera ascendente",
            sSortDescending: ": Activar para ordenar la columna de manera descendente"
          }
        }
      });
    },
    makeDataInfo(selector) {
      $(selector).DataTable({
        retrieve: true,
        dom: "Bfrtip",
        // buttons: ['selectAll', 'selectNone', 'excelHtml5'],
        buttons: [{
          text: '<i class="fa fa-lg fa-file-excel-o"></i>',
          extend: "excel",
          className: "btn btn-success float-right shadow-sm ml-3 mr-3 mb-2 excel-button",
          title: this.excelTitle
        }],
        paging: false,
        autoWidth: false,
        ordering: false,
        info: false,
        searching: false
      });
    },
    getFormData(obj) {
      const formData = new FormData();
      Object.keys(obj).forEach(key => {
        //   console.log(key + ' => ' + obj[key]);
        if (Array.isArray(obj[key])) {
          obj[key].forEach((item, i) => {
            if (item instanceof File) {
              // console.log('FILE');
              formData.append(key + "[" + i + "]", item);
              // }else if(typeof item == 'object'){
              //     // console.log('OBJ');
              //     formData.append(key + '[' + i +']', JSON.stringify(item) );
            } else {
              // console.log('PLAIN');
              formData.append(key + "[" + i + "]", item);
            }
          });
        } else {
          formData.append(key, obj[key]);
        }
      });
      return formData;
    },
    loadButton(obj) {
      const icon = $(obj)
        .find("i")
        .attr("class");
      $(obj).html(
        '<i class = "fa fa-spinner fa-spin" action = "' +
        icon +
        '"></i> ' +
        $(obj).text()
      );
      $(obj).attr("disabled", "disabled");
      $(obj).addClass("disabled");
    },
    onReady() {
      const icon = $(".fa-spin").attr("action");
      $(".fa-spin")
        .parents("button")
        .removeAttr("disabled")
        .removeClass("disabled");
      $(".fa-spin")
        .parents("a")
        .removeAttr("disabled")
        .removeClass("disabled");
      if (icon)
        $(".fa-spin")
        .parent()
        .html(
          '<i class = "' +
          icon +
          '"></i> ' +
          $(".fa-spin")
          .parent()
          .text()
        );
      $(".fa-spin").remove();
    },
    compareArrays(o, n) {
      var temp = [];
      var dif = [];

      var oldkeys = $.map(o, function (value, key) {
        return "" + value;
      });
      var newkeys = $.map(n, function (value, key) {
        return "" + value;
      });

      for (var i = 0; i < oldkeys.length; i++) {
        //In a and not in b
        if (!newkeys.includes(oldkeys[i])) {
          temp.push(oldkeys[i]);
        }
      }
      for (var i = 0; i < newkeys.length; i++) {
        //in b and not in a
        if (!oldkeys.includes(newkeys[i])) {
          dif.push(newkeys[i]);
        }
      }

      console.log(temp);
      console.log(dif);

      if (temp.length > 0) {
        //Diferentes
        return true;
      } else {
        //Iguales
        return false;
      }
    },
    jsonEqual(o, n) {
      return (
        JSON.stringify(o, this.replacer) === JSON.stringify(n, this.replacer)
      );
    },
    replacer(key, value) {
      if (typeof value === "number") {
        return value.toString();
      }
      if (typeof value === "boolean") {
        return value ? "1" : "0";
      }
      return value;
    },
    curpValida(curp) {
      var re = /^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$/,
        // var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
        validado = curp.match(re);

      if (!validado) {
        return false;
      }
      // var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
      //   validado = curp.match(re);
      // if (curp === "OICH840707MJCRLL07") return true;

      // if (!validado)
      //   //Coincide con el formato general?
      //   return false;

      // //Validar que coincida el dígito verificador
      // function digitoVerificador(curp17) {
      //   //Fuente https://consultas.curp.gob.mx/CurpSP/
      //   var diccionario = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
      //     lngSuma = 0.0,
      //     lngDigito = 0.0;
      //   for (var i = 0; i < 17; i++)
      //     lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
      //   lngDigito = 10 - (lngSuma % 10);
      //   if (lngDigito == 10) return 0;
      //   return lngDigito;
      // }

      // if (validado[2] != digitoVerificador(validado[1])) return false;

      return true; //Validado
    },
    rfcValido(rfc, aceptarGenerico = true) {
      const re = /^[A-Z,Ñ,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$/;
      // const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
      var validado = rfc.match(re);

      if (!validado) {
        return false;
      }
      // const re = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
      // var validado = rfc.match(re);

      // if (!validado)
      //   //Coincide con el formato general del regex?
      //   return false;

      // //Separar el dígito verificador del resto del RFC
      // const digitoVerificador = validado.pop(),
      //   rfcSinDigito = validado.slice(1).join(""),
      //   len = rfcSinDigito.length,
      //   //Obtener el digito esperado
      //   diccionario = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
      //   indice = len + 1;
      // var suma, digitoEsperado;

      // if (len == 12) suma = 0;
      // else suma = 481; //Ajuste para persona moral

      // for (var i = 0; i < len; i++)
      //   suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
      // digitoEsperado = 11 - (suma % 11);
      // if (digitoEsperado == 11) digitoEsperado = 0;
      // else if (digitoEsperado == 10) digitoEsperado = "A";

      // //El dígito verificador coincide con el esperado?
      // // o es un RFC Genérico (ventas a público general)?
      // // Si es un rfc válido MALC7211227T4
      // if (rfc == "IAPF870205HX4" || rfc == "RALY900508T93" || rfc == "MALC7211227T4" || rfc == "LEFJ8506304R2" || rfc == "LOLF7012036U0") return true;
      // else if (
      //   digitoVerificador != digitoEsperado &&
      //   (!aceptarGenerico ||
      //     rfcSinDigito + digitoVerificador != "XAXX010101000")
      // )
      //   return false;
      // else if (
      //   !aceptarGenerico &&
      //   rfcSinDigito + digitoVerificador == "XEXX010101000"
      // )
      //   return false;

      return true;
    },
    checkPermisos(v) {
      let p = this.getKey('permisos')
      // console.log(v, p, p.includes(v));
      if (!p.includes(v)) {
        this.$router.push('/admin/tablero')
      }
    },
    group1k(s) {
      let s1 = s.toString()
      if (s1.length > 3) {
        if (s1.length == 6) {
          s1 = s1.split('').reverse().join('').replace(/\d{3}/g, '$&,').split('').reverse().join('')
          return s1.substring(1, s1.length);
        } else {
          return s1.split('').reverse().join('').replace(/\d{3}/g, '$&,').split('').reverse().join('');
        }

      } else
        return s1
    },
    /* validateRfc(validaciones, form) {

      var rfcVal = [];
      Object.keys(validaciones).forEach((item) => {
        rfcVal.push(new Promise((resolve, reject) => {
          if (this.rfcValido(form[item])) {
            validaciones[item] = false;
            return resolve(true);
          } else {
            validaciones[item] = true;
            return resolve(false);
          }
        }));
      });

      var validall = this.$validator.validateAll();
      return Promise.all([validall, ...rfcVal]);
    }, */

  },

};
