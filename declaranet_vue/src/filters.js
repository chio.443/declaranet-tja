import Vue from 'vue'
import moment from 'moment'

// Filters
Vue.filter('dateFormat', (value, onlyDate = true) => {
  if (value) {
    if (onlyDate)
      return moment(String(value)).format('DD-MM-YYYY');
    else
      return moment(String(value)).format('DD-MM-YYYY HH:mm');
  }
  return ' '
})

Vue.filter('timeFormat', (value) => {
  if (value) {
    return moment(String(value)).format('HH:mm');
  }
  return ' '
})

Vue.filter('year', (value) => {
  if (value) {
    return moment(String(value)).year().toString();
  }
  return ' '
})

Vue.filter('text', (value, max) => {
  if (value) {
    let len = max ? max : 100;
    return value.substr(0, len) + (value.length > len ? "..." : "");
  }
  return ' '
})

Vue.filter('string', (value) => {
  if (value) {
    return value + ''
  }
  return ' '
})

Vue.filter('uppercase', (value) => {
  if (value) {
    return value.toUpperCase();
  }
  return ' '
})

Vue.filter('lowercase', (value) => {
  if (value) {
    return value.toLowerCase();
  }
  return ' '
})

Vue.filter('currency', (value) => {
  if (value) {
    let val = (value / 1).toFixed(2).replace(',', '.')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
  return ' -'
})
