import Vue from "vue";
import Vuex from "vuex";
/* import VueResource from "vue-resource"
Vue.use(VueResource) */
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

const state = {
  entidades: [],
  paises: [],
  monedas: [],
  beneficiarios: [],
  tipos_instituciones: [],
  tipos_beneficios: [],
  tipos_apoyos: [],
  tipos_fideicomisos: [],
  tipos_fideicomisos_part: [],
  tipos_participaciones: [],
  nivel_gobierno: [],
  sectores_industria: [],
  tipos_representacion: [],
  tipos_vehiculos: [],
  tipos_inmuebles: [],
  tipos_mueble: [],
  titular_adeudo: [],
  tipos_adeudos: [],
  adquisiciones: [],
  relaciones: [],
  transmisores: [],
  pagos: [],
  valores_inmueble: [],
  bajas_inmueble: [],
  inversiones: [],
  inv_bancarias: [],
  inv_fondos: [],
  inv_organizaciones: [],
  inv_monedas: [],
  inv_seguros: [],
  inv_valores: [],
  inv_afores: [],
};
const actions = {
  getEntidades({
    commit,
    state
  }, data) {
    commit("setEntidades", data);
  },
  getPaises({
    commit,
    state
  }, data) {
    commit("setPaises", data);
  },
  getTipoInstitucion({
    commit,
    state
  }, data) {
    commit("setTipoInstitucion", data);
  },
  getTipoApoyos({
    commit,
    state
  }, data) {
    commit("setTipoApoyos", data);
  },
  getNivelGobierno({
    commit,
    state
  }, data) {
    commit("setNivelGobierno", data);
  },
  getSectoresIndustria({
    commit,
    state
  }, data) {
    commit("setSectoresIndustria", data);
  },
  getTipoRepresentacion({
    commit,
    state
  }, data) {
    commit("setTipoRepresentacion", data);
  },
  getTipoBeneficio({
    commit,
    state
  }, data) {
    commit("setTipoBeneficio", data);
  },
  getMonedas({
    commit,
    state
  }, data) {
    commit("setMonedas", data);
  },
  getTipoFideicomiso({
    commit,
    state
  }, data) {
    commit("setTipoFideicomiso", data);
  },
  getTipoFideicomisoPart({
    commit,
    state
  }, data) {
    commit("setTipoFideicomisoPart", data);
  },
  getTipoParticipaciones({
    commit,
    state
  }, data) {
    commit("setTipoParticipaciones", data);
  },
  getBeneficiario({
    commit,
    state
  }, data) {
    commit("setBeneficiario", data);
  },
  getTipoVehiculo({
    commit,
    state
  }, data) {
    commit("setTipoVehiculo", data);
  },
  getTipoInmueble({
    commit,
    state
  }, data) {
    commit("setTipoInmueble", data);
  },
  getTitularAdeudo({
    commit,
    state
  }, data) {
    commit("setTitularAdeudo", data);
  },
  getTipoAdeudo({
    commit,
    state
  }, data) {
    commit("setTipoAdeudo", data);
  },
  getAdquisiciones({
    commit,
    state
  }, data) {
    commit("setAdquisiciones", data);
  },
  getRelaciones({
    commit,
    state
  }, data) {
    commit("setRelaciones", data);
  },
  getPagos({
    commit,
    state
  }, data) {
    commit("setPagos", data);
  },
  getValorInmueble({
    commit,
    state
  }, data) {
    commit("setValorInmueble", data);
  },
  getBajaInmueble({
    commit,
    state
  }, data) {
    commit("setBajaInmueble", data);
  },
  getTransmisor({
    commit,
    state
  }, data) {
    commit("setTransmisor", data);
  },
  getMueble({
    commit,
    state
  }, data) {
    commit("setMueble", data);
  },
  getInversiones({
    commit,
    state
  }, data) {
    commit("setInversiones", data);
  },
  getInvBancaria({
    commit,
    state
  }, data) {
    commit("setInvBancaria", data);
  },
  getInvFondos({
    commit,
    state
  }, data) {
    commit("setInvFondos", data);
  },
  getInvOrganizaciones({
    commit,
    state
  }, data) {
    commit("setInvOrganizaciones", data);
  },
  getInvMoneda({
    commit,
    state
  }, data) {
    commit("setInvMoneda", data);
  },
  getInvSeguros({
    commit,
    state
  }, data) {
    commit("setInvSeguros", data);
  },
  getInvValores({
    commit,
    state
  }, data) {
    commit("setInvValores", data);
  },
  getInvAfores({
    commit,
    state
  }, data) {
    commit("setInvAfores", data);
  },
};

const mutations = {
  setEntidades(state, catalogData) {
    state.entidades = catalogData;
  },
  setPaises(state, catalogData) {
    state.paises = catalogData;
  },
  setTipoInstitucion(state, catalogData) {
    state.tipos_instituciones = catalogData;
  },
  setTipoApoyos(state, catalogData) {
    state.tipos_apoyos = catalogData;
  },
  setNivelGobierno(state, catalogData) {
    state.nivel_gobierno = catalogData;
  },
  setSectoresIndustria(state, catalogData) {
    state.sectores_industria = catalogData;
  },
  setTipoRepresentacion(state, catalogData) {
    state.tipos_representacion = catalogData;
  },
  setTipoBeneficio(state, catalogData) {
    state.tipos_beneficios = catalogData;
  },
  setMonedas(state, catalogData) {
    state.monedas = catalogData;
  },
  setTipoFideicomiso(state, catalogData) {
    state.tipos_fideicomisos = catalogData;
  },
  setTipoFideicomisoPart(state, catalogData) {
    state.tipos_fideicomisos_part = catalogData;
  },
  setTipoParticipaciones(state, catalogData) {
    state.tipos_participaciones = catalogData;
  },
  setBeneficiario(state, catalogData) {
    state.beneficiarios = catalogData;
  },
  setTipoVehiculo(state, catalogData) {
    state.tipos_vehiculos = catalogData;
  },
  setTipoInmueble(state, catalogData) {
    state.tipos_inmuebles = catalogData;
  },
  setTitularAdeudo(state, catalogData) {
    state.titular_adeudo = catalogData;
  },
  setTipoAdeudo(state, catalogData) {
    state.tipos_adeudos = catalogData;
  },
  setAdquisiciones(state, catalogData) {
    state.adquisiciones = catalogData;
  },
  setRelaciones(state, catalogData) {
    state.relaciones = catalogData;
  },
  setPagos(state, catalogData) {
    state.pagos = catalogData;
  },
  setValorInmueble(state, catalogData) {
    state.valores_inmueble = catalogData;
  },
  setBajaInmueble(state, catalogData) {
    state.bajas_inmueble = catalogData;
  },
  setTransmisor(state, catalogData) {
    state.transmisores = catalogData;
  },
  setMueble(state, catalogData) {
    state.tipos_mueble = catalogData;
  },
  setInversiones(state, catalogData) {
    state.inversiones = catalogData;
  },
  setInvBancaria(state, catalogData) {
    state.inv_bancarias = catalogData;
  },
  setInvFondos(state, catalogData) {
    state.inv_fondos = catalogData;
  },
  setInvOrganizaciones(state, catalogData) {
    state.inv_organizaciones = catalogData;
  },
  setInvMoneda(state, catalogData) {
    state.inv_monedas = catalogData;
  },
  setInvSeguros(state, catalogData) {
    state.inv_seguros = catalogData;
  },
  setInvValores(state, catalogData) {
    state.inv_valores = catalogData;
  },
  setInvAfores(state, catalogData) {
    state.inv_afores = catalogData;
  },
};

const getters = {
  getMexico: state => {
    return state.paises.find(todo => todo.id === 150);
  }
};

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
  /* strict: debug */
});
