/* eslint-disable eqeqeq */
/* eslint-disable semi */
/* eslint-disable quotes */
import {
  mapState,
  mapActions
} from "vuex";
import moment from 'moment';
export const mixCat = {
  computed: {
    ...mapState(["entidades", "paises", "tipos_instituciones", "tipos_apoyos", "nivel_gobierno", "sectores_industria", "tipos_representacion", "tipos_beneficios", "monedas", "tipos_fideicomisos", "tipos_fideicomisos_part", "tipos_participaciones", "beneficiarios", "tipos_inmuebles", "tipos_vehiculos", "titular_adeudo", "tipos_adeudos", "adquisiciones", "relaciones", "pagos", "valores_inmueble", "bajas_inmueble", "transmisores", "tipos_mueble", "inversiones", "inv_bancarias", "inv_fondos", "inv_organizaciones", "inv_monedas", "inv_seguros", "inv_valores", "inv_afores"])
  },
  methods: {
    ...mapActions(["getEntidades", "getPaises", "getTipoInstitucion", "getTipoApoyos", "getNivelGobierno", "getSectoresIndustria", "getTipoRepresentacion", "getTipoBeneficio", "getMonedas", "getTipoFideicomiso", "getTipoFideicomisoPart", "getTipoParticipaciones", "getBeneficiario", "getTipoVehiculo", "getTipoInmueble", "getTitularAdeudo", "getTipoAdeudo", "getAdquisiciones", "getRelaciones", "getPagos", "getValorInmueble", "getBajaInmueble", "getTransmisor", "getMueble", "getInversiones", "getInvBancaria", "getInvFondos", "getInvOrganizaciones", "getInvMoneda", "getInvSeguros", "getInvValores", "getInvAfores"]),
    getEntidadesCatalog() {
      this.$http.get("entidadFed").then(
        response => {
          // this.$store.dispatch("getEntidades", response.data.entidadfed);
          this.getEntidades(response.data.entidadfed);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getPaisesCatalog() {
      this.$http.get("paises").then(
        response => {
          // this.$store.dispatch("getEntidades", response.data.entidadfed);
          this.getPaises(response.data.pais);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoInsCatalog() {
      this.$http.get("cat_tipo_institucion").then(
        response => {
          this.getTipoInstitucion(response.data.institucion);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoApoyoCatalog() {
      this.$http.get("cat_tipo_apoyo").then(
        response => {
          this.getTipoApoyos(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getNivelGobiernoCatalog() {
      this.$http.get("nivelGobierno").then(
        response => {
          this.getNivelGobierno(response.data.nivelGobierno);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getSectoresCatalog() {
      this.$http.get("cat_sector_industria").then(response => {
          this.getSectoresIndustria(response.data.sectores);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoRepCatalog() {
      this.$http.get("cat_tipo_representacion").then(response => {
          this.getTipoRepresentacion(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoBeneficiosCatalog() {
      this.$http.get("cat_tipo_beneficio").then(response => {
          this.getTipoBeneficio(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getMonedasCatalog() {
      this.$http.get("monedas").then(response => {
          this.getMonedas(response.data.monedas);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoFideicomisoCatalog() {
      this.$http.get("cat_tipo_fideicomiso").then(response => {
          this.getTipoFideicomiso(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoFideicomisoPartCatalog() {
      this.$http.get("cat_tipo_fideicomiso_part").then(response => {
          this.getTipoFideicomisoPart(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoParticipacionesCatalog() {
      this.$http.get("tipoparticipacion").then(response => {
          this.getTipoParticipaciones(response.data.tiposp);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getBeneficiarioCatalog() {
      this.$http.get("cat_beneficiario").then(response => {
          this.getBeneficiario(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoVehiculoCatalog() {
      this.$http.get("cat_tipo_bien_mueble").then(response => {
          this.getTipoVehiculo(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoInmuebleCatalog() {
      this.$http.get("cat_tipo_bien").then(response => {
          this.getTipoInmueble(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTitularAdeudoCatalog() {
      this.$http.get("cat_titular_adeudo").then(response => {
          this.getTitularAdeudo(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTipoAdeudoCatalog() {
      this.$http.get("cat_tipo_adeudo").then(response => {
          this.getTipoAdeudo(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getAdquisicionesCatalog() {
      this.$http.get("cat_forma_adquisicion").then(response => {
          this.getAdquisiciones(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getRelacionesCatalog() {
      this.$http.get("cat_relacion_persona_adquirio").then(response => {
          this.getRelaciones(response.data.relaciones);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getPagosCatalog() {
      this.$http.get("cat_forma_pago").then(response => {
          this.getPagos(response.data.pagos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getValorInmuebleCatalog() {
      this.$http.get("cat_valor_inmueble").then(response => {
          this.getValorInmueble(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getBajaInmuebleCatalog() {
      this.$http.get("cat_baja_inmueble").then(response => {
          this.getBajaInmueble(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getTransmisoresCatalog() {
      this.$http.get("cat_relacion_transmisor").then(response => {
          this.getTransmisor(response.data.response);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getMueblesCatalog() {
      this.$http.get("cat_tipo_bien_mueble_nr").then(response => {
          this.getMueble(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    /* Inverciones */
    getInversionesCatalog() {
      this.$http.get("cat_tipo_inversion").then(response => {
          this.getInversiones(response.data.tipos);
        },
        error => {
          this.Err(error);
        }
      );
    },

    getInvBancariaCatalog() {
      this.$http.get("cat_bancaria").then(
        response => {
          this.getInvBancaria(response.data.bancarias);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvFondosCatalog() {
      this.$http.get("cat_fondo_inversion").then(
        response => {
          this.getInvFondos(response.data.fondos);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvOrgCatalog() {
      this.$http.get("cat_organizacionespym").then(
        response => {
          this.getInvOrganizaciones(response.data.organizaciones);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvMonedaCatalog() {
      this.$http.get("cat_tipo_moneda_metales").then(
        response => {
          this.getInvMoneda(response.data.metales);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvSegurosCatalog() {
      this.$http.get("cat_seguros").then(
        response => {
          this.getInvSeguros(response.data.seguros);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvValoresCatalog() {
      this.$http.get("cat_tipo_valores").then(
        response => {
          this.getInvValores(response.data.valores);
        },
        error => {
          this.Err(error);
        }
      );
    },
    getInvAforesCatalog() {

      this.$http.get("cat_afores").then(
        response => {
          this.getInvAfores(response.data.afores);
        },
        error => {
          this.Err(error);
        }
      );
    },



    checkCatalogs() {
      if (!(this.tipos_apoyos.length > 0)) {
        this.getTipoApoyoCatalog();
      }
      if (!(this.nivel_gobierno.length > 0)) {
        this.getNivelGobiernoCatalog();
      }
      if (!(this.entidades.length > 0)) {
        this.getEntidadesCatalog();
      }
      if (!(this.paises.length > 0)) {
        this.getPaisesCatalog();
      }
      if (!(this.tipos_instituciones.length > 0)) {
        this.getTipoInsCatalog();
      }
      if (!(this.sectores_industria.length > 0)) {
        this.getSectoresCatalog();
      }
      if (!(this.tipos_representacion.length > 0)) {
        this.getTipoRepCatalog();
      }
      if (!(this.tipos_beneficios.length > 0)) {
        this.getTipoBeneficiosCatalog();
      }
      if (!(this.monedas.length > 0)) {
        this.getMonedasCatalog();
      }
      if (!(this.tipos_fideicomisos.length > 0)) {
        this.getTipoFideicomisoCatalog();
      }
      if (!(this.tipos_fideicomisos_part.length > 0)) {
        this.getTipoFideicomisoPartCatalog();
      }
      if (!(this.tipos_participaciones.length > 0)) {
        this.getTipoParticipacionesCatalog();
      }
      if (!(this.beneficiarios.length > 0)) {
        this.getBeneficiarioCatalog();
      }
      if (!(this.tipos_vehiculos.length > 0)) {
        this.getTipoVehiculoCatalog();
      }
      if (!(this.tipos_inmuebles.length > 0)) {
        this.getTipoInmuebleCatalog();
      }
      if (!(this.titular_adeudo.length > 0)) {
        this.getTitularAdeudoCatalog();
      }
      if (!(this.tipos_adeudos.length > 0)) {
        this.getTipoAdeudoCatalog();
      }
      if (!(this.adquisiciones.length > 0)) {
        this.getAdquisicionesCatalog();
      }
      if (!(this.relaciones.length > 0)) {
        this.getRelacionesCatalog();
      }
      if (!(this.pagos.length > 0)) {
        this.getPagosCatalog();
      }
      if (!(this.valores_inmueble.length > 0)) {
        this.getValorInmuebleCatalog();
      }
      if (!(this.bajas_inmueble.length > 0)) {
        this.getBajaInmuebleCatalog();
      }
      if (!(this.transmisores.length > 0)) {
        this.getTransmisoresCatalog();
      }
      if (!(this.tipos_mueble.length > 0)) {
        this.getMueblesCatalog();
      }
      if (!(this.inversiones.length > 0)) {
        this.getInversionesCatalog();
      }

      if (!(this.inv_bancarias.length > 0)) {
        this.getInvBancariaCatalog();
      }
      if (!(this.inv_fondos.length > 0)) {
        this.getInvFondosCatalog();
      }
      if (!(this.inv_organizaciones.length > 0)) {
        this.getInvOrgCatalog();
      }
      if (!(this.inv_monedas.length > 0)) {
        this.getInvMonedaCatalog();
      }
      if (!(this.inv_seguros.length > 0)) {
        this.getInvSegurosCatalog();
      }
      if (!(this.inv_valores.length > 0)) {
        this.getInvValoresCatalog();
      }
      if (!(this.inv_afores.length > 0)) {
        this.getInvAforesCatalog();
      }
    },
    /*************************************
     * ***********************************
     * ***********************************
     * FILTROS DE DATOS EN CATALOGOS
     *************************************
     **************************************/
    filterAnio(date) {
      return moment(date, 'YYYY-MM-DD').format('YYYY');
    },
    filterDeclarante(index) {
      var resp = " ";
      if (index == '1') {
        resp = "Declarante";
      } else if (index == '2') {
        resp = "Pareja";
      } else {
        resp = "Dependiente Económico";
      }
      return resp;
    },
    filterBeneficiario(id) {
      var resp = " ";
      if (this.beneficiarios) {
        this.beneficiarios.forEach(function (item) {
          if (item.id == id) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterCatalog(id, list) {
      var resp = "-";
      if (list) {
        list.forEach(function (item) {
          if (item.id == id) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterNivel(id) {
      var resp = " ";
      if (this.nivel_gobierno) {
        this.nivel_gobierno.forEach(function (item) {
          if (item.id == id) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterApoyo(id) {
      var resp = " ";
      if (this.tipos_apoyos) {
        this.tipos_apoyos.forEach(function (item) {
          if (item.id == id) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterSector(index) {
      var resp = " ";
      if (this.sectores_industria) {
        this.sectores_industria.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterBool(index) {
      var resp = "No";
      if (index == 1 || index == "1") {
        resp = "Si";
        return resp;
      }
      return resp;
    },
    filterTipoPersona(index) {
      var resp = " ";
      if (index == '1') {
        resp = "Persona física";
      } else if (index == '2') {
        resp = "Persona moral";
      }
      return resp;
    },
    filterTipoRepresentacion(index) {
      var resp = " ";
      if (this.tipos_representacion) {
        this.tipos_representacion.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterTipoRepresentante(index) {
      var resp = " ";
      if (index == '1') {
        resp = "Representante";
      } else if (index == '2') {
        resp = "Representado";
      }
      return resp;
    },
    filterTipoMoneda(index) {
      var resp = " ";
      if (this.monedas) {
        this.monedas.forEach(function (item) {
          if (item.id == index) {
            resp = item.moneda;
            return resp;
          }
        });
      }
      return resp;
    },
    filterForma(index) {
      var resp = " ";
      if (index == '1') {
        resp = "Monetario";
      } else if (index == '2') {
        resp = "Especie";
      }
      return resp;
    },
    filterTipoBeneficio(index) {
      var resp = " ";
      if (this.tipos_beneficios) {
        this.tipos_beneficios.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterTipofideicomiso(index) {
      var resp = " ";
      if (this.tipos_fideicomisos) {
        this.tipos_fideicomisos.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterTipoFideicomisoPart(index) {
      var resp = " ";
      if (this.tipos_fideicomisos_part) {
        this.tipos_fideicomisos_part.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterTipoParticipacion(index) {
      var resp = " ";
      if (this.tipos_participaciones) {
        this.tipos_participaciones.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterTipoBien(item) {
      if (item == '1') {
        return "Inmueble";
      } else {
        return "Vehiculo";
      }
    },
    filterInmueble(index) {
      var resp = " ";
      if (this.tipos_inmuebles) {
        this.tipos_inmuebles.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },
    filterMueble(index) {
      var resp = " ";
      if (this.tipos_vehiculos) {
        this.tipos_vehiculos.forEach(function (item) {
          if (item.id == index) {
            resp = item.valor;
            return resp;
          }
        });
      }
      return resp;
    },

    getError(fieldName) {
      return this.errors.first(fieldName);
    },
    isNumber(n) {
      // console.log(n);
      n.target.value = n.target.value.replace(/[^\d.-]/g, '');
    },
    filterSignoDos(val = "97") {
      var signo = "$";
      this.monedas.forEach(function (item) {
        if (item.id == val) {
          signo = item.signo;
        }
      });
      return signo;
    },
    /*
    getMunicipio(index) {
        var resp = "";
        if (this.municipios) {
          this.municipios.forEach(function (item) {
            if (item.id == index) {
              resp = item.nom_mun;
              return resp;
            }
          });
        }
        return resp;
      },
    filterLocalidad(index) {
      var resp = "";
      if (this.localidades) {
        this.localidades.forEach(function (item) {
          if (item.id == index) {
            resp = item.nom_loc;
            return resp;
          }
        });
      }
      return resp;
    }, */
    scrolling() {
      this.$el.querySelector("[aria-invalid=true]").scrollIntoView({
        block: "center",
        behavior: "smooth"
      });
    },
    /*
    validate() {
        if (this.add_item) {
          this.validaciones = {
            rfc_parte: false
          };
          if (!this.rfcValido(this.form_item.rfc_parte)) {
            this.validaciones.rfc_parte = true;
            return false;
          }
        }
        return this.$validator.validateAll();
      },

    */

  }
};
