import {
  mix
} from './mixin'

export default (function returnMenu() {

  let grupo = 0
  let rol = 0
  let stats = 0


  if (stats == 1) {

    if (grupo == 3) { ///si es el g
      declaracion = [{
          name: 'Información Personal',
          path: '/forms/infpersonal',
          icon: 'fas fa-user-cog fa-xs'
        },
        {
          name: 'Intereses',
          path: '/forms/intereses',
          icon: 'fas fa-file-invoice-dollar fa-xs'
        },
        {
          name: 'Ingresos',
          path: '/forms/ingresos',
          icon: 'fas fa-dollar-sign fa-xs'
        }
      ];
    } else {
      declaracion = [{
          name: 'Información Personal',
          path: '/forms/infpersonal',
          icon: 'fas fa-user-cog fa-xs'
        },
        {
          name: 'Intereses',
          path: '/forms/intereses',
          icon: 'fas fa-file-invoice-dollar fa-xs'
        },
        {
          name: 'Ingresos',
          path: '/forms/ingresos',
          icon: 'fas fa-dollar-sign fa-xs'
        },
        {
          name: 'Activos',
          path: '/forms/activos',
          icon: 'fas fa-hand-holding-usd fa-xs'
        },
        {
          name: 'Pasivos',
          path: '/forms/pasivos',
          icon: 'fas fa-money-bill fa-xs'
        },
        {
          name: 'Fiscal',
          path: '/forms/fiscal',
          icon: 'fas fa-balance-scale fa-xs'
        },
        {
          name: 'Resumen',
          path: '/forms/resumen',
          icon: 'fas fa-file-invoice fa-xs'
        },
        {
          name: 'Acuse',
          path: '/forms/enviada',
          icon: 'fas fa-file-invoice fa-xs'
        },
        {
          name: 'Guía de llenado',
          path: '/public/Guía llenado Declaración Patrimonial.pdf',
          icon: 'fas fa-file-invoice fa-xs'
        }

      ];
    }

    declaraciones = [{
      name: 'Declaración 2019',
      path: '/forms/infpersonal',
      icon: 'fas fa-angle-right fa-xs'
    }];
  } else {
    //si ya la envio
    declaracion = [{
      name: 'Enviada',
      path: '/forms/enviada',
      icon: 'fas fa-user-cog fa-xs'
    }];
    fiscal = [{
      name: 'Acuse de Declaración Fiscal',
      path: '/forms/fiscal',
      icon: 'fas fa-balance-scale',
      permiso: [1],
    }];
    declaraciones = [{
      name: 'Declaración 2019',
      path: '/forms/enviada',
      icon: 'fas fa-angle-right fa-xs'
    }]

  }
  if (rol == 1) { //si es administrador, mostrar menú completo
    menu = [{
        name: 'Declaraciones',
        icon: 'ti-clipboard',
        children: [{
            name: 'Declaraciones',
            path: '#',
            icon: 'fas fa-clipboard-list fa-xs'
          },
          {
            name: 'Verificación',
            path: '#',
            icon: 'fas fa-check-double fa-xs'
          }
        ]
      },
      {
        name: 'Padrón',
        icon: 'ti-agenda',
        collapsed: true,
        children: [{
            name: 'Altas de personal',
            path: '/admin/altapersonal',
            icon: 'fas fa-user-plus fa-xs'

          },
          {
            name: 'Movimientos',
            path: '/forms/movimientos',
            icon: 'fas fa-user-edit fa-xs'
          },
          {
            name: 'Plantilla',
            path: '/forms/plantilla',
            icon: 'fas fa-file-excel fa-xs'
          }
        ]
      },
      {
        name: 'Procedimientos',
        icon: 'ti-write',
        collapsed: true,
        children: [{
            name: 'Casos omisos',
            path: '#',
            icon: 'fas fa-eye-slash fa-xs'

          },
          {
            name: 'Procedimientos',
            path: '#',
            icon: 'fas fa-tasks fa-xs'
          }
        ]
      }, {
        name: 'Configuración',
        icon: 'ti-settings',
        collapsed: true,
        children: [{
          name: 'Usuarios',
          path: '/forms/usuarios',
          icon: 'fas fa-users-cog fa-xs'
        }, {
          name: 'Permisos',
          path: '/forms/permisos',
          icon: 'fas fa-key fa-xs'
        }]
      }, {
        name: 'Reportes',
        icon: 'ti-printer',
        children: [{
            name: 'Declaraciones',
            path: '#',
            icon: 'fas fa-clipboard-list fa-xs'
          },
          {
            name: 'Movimientos',
            path: '#',
            icon: 'fas fa-arrows-alt fa-xs'
          },
          {
            name: 'Cumplimiento',
            path: '#',
            icon: 'fas fa-tasks fa-xs'
          }
        ]
      }
    ]

  } else {
    menu = [{
        name: 'Mis declaraciones',
        icon: 'ti-layout-list-thumb',
        children: declaraciones
      },
      {
        name: 'Declaraciones',
        icon: 'ti-clipboard',
        children: declaracion
      },
      {
        name: 'Declaración Fiscal',
        icon: 'ti-file',
        collapsed: false,
        children: fiscal
      }
    ];

  }


  return menu;

}())
