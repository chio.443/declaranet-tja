import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
import InicioLayout from '../components/Dashboard/Layout/InicioLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Pages

import Login from 'src/components/Dashboard/Views/Pages/Login.vue'
import Busqueda from 'src/components/Dashboard/Views/Pages/Busqueda.vue'
import CambiaPass from 'src/components/Dashboard/Views/Pages/CambiaPass.vue'
import RecuperaPass from 'src/components/Dashboard/Views/Pages/RecuperaPass.vue'
import Notificaciones from 'src/components/Dashboard/Views/Pages/Notificaciones.vue'

// Formularios Declaranet
import InfPersonal from 'src/components/Dashboard/Views/Forms/InfPersonal.vue'
import Intereses from 'src/components/Dashboard/Views/Forms/Intereses.vue'
import Ingresos from 'src/components/Dashboard/Views/Forms/Ingresos.vue'
import Pasivos from 'src/components/Dashboard/Views/Forms/Pasivos.vue'
import Fiscal from 'src/components/Dashboard/Views/Forms/Fiscal.vue'
import Aclaraciones from 'src/components/Dashboard/Views/Forms/Aclaraciones.vue'
import Activos from 'src/components/Dashboard/Views/Forms/Activos.vue'
import Historico from 'src/components/Dashboard/Views/Forms/Historico.vue'


// Movimientos
import Plantilla from 'src/components/Dashboard/Views/Forms/Plantilla.vue'
import PlantillaIngresos from 'src/components/Dashboard/Views/Forms/PlantillaIngresos.vue'
import PlantillaBajas from 'src/components/Dashboard/Views/Forms/PlantillaBajas.vue'
import Movimientos from 'src/components/Dashboard/Views/Forms/Movimientos.vue'
import AltaPersonal from 'src/components/Dashboard/Views/Forms/Altas.vue'


import Observaciones from 'src/components/Dashboard/Views/Forms/Observaciones.vue'
import Resumen from 'src/components/Dashboard/Views/Forms/Resumen.vue'

import Enviada from 'src/components/Dashboard/Views/Forms/Enviada.vue'

// Formularios Configuración
import Usuarios from 'src/components/Dashboard/Views/Forms/Usuarios.vue'
import Permisos from 'src/components/Dashboard/Views/Forms/Permisos.vue'

// Reportes
import Declaraciones from 'src/components/Dashboard/Views/Reportes/Declaraciones.vue'
import Omisos from 'src/components/Dashboard/Views/Reportes/Omisos.vue'
import Declaraciones2 from 'src/components/Dashboard/Views/Reportes/Declaraciones2.vue'
import Cumplimiento from 'src/components/Dashboard/Views/Reportes/Cumplimiento.vue'
import Cumplimiento2 from 'src/components/Dashboard/Views/Reportes/Cumplimiento2.vue'
import Tablero from 'src/components/Dashboard/Views/Reportes/Tablero.vue'
import Tablero2 from 'src/components/Dashboard/Views/Reportes/Tablero2.vue'

import TableroDec from 'src/components/Dashboard/Views/Reportes/TableroDec.vue'
import ListDeclaraciones from 'src/components/Dashboard/Views/Reportes/ListDeclaraciones.vue'
import HistoricoDeclaraciones from 'src/components/Dashboard/Views/Reportes/HistoricoDeclaraciones.vue'
import RepDeclaraciones from 'src/components/Dashboard/Views/Reportes/RepDeclaraciones.vue'
/*Verificacion*/
import Verificacion from 'src/components/Dashboard/Views/Reportes/Verificacion.vue'


import ReporteDeclaraciones from 'src/components/Dashboard/Views/Reportes/ListDeclaraciones2.vue'
import ReporteDeclaraciones2 from 'src/components/Dashboard/Views/Reportes/RepDeclaraciones2.vue'
import ReporteConflictoInteres from 'src/components/Dashboard/Views/Reportes/RepConflictoInteres.vue'

import ReporteBajasDefuncion from 'src/components/Dashboard/Views/Reportes/RepBajasDefuncion.vue'
import ReporteAccesosBalanza from 'src/components/Dashboard/Views/Reportes/BitacoraB.vue'

import RMovimientos from 'src/components/Dashboard/Views/Reportes/Movimientos.vue'
//Alertas
import BitacoraC from 'src/components/Dashboard/Views/Alertas/BitacoraC.vue'
import Modificadas from 'src/components/Dashboard/Views/Alertas/Modificadas.vue'

let formsMenu = {
  path: '/forms',
  component: DashboardLayout,
  redirect: '/forms/infPersonal',
  children: [{
      path: 'infPersonal',
      name: 'Información Personal',
      component: InfPersonal
    },
    {
      path: 'intereses',
      name: 'Intereses',
      component: Intereses
    },
    {
      path: 'ingresos',
      name: 'Ingresos',
      component: Ingresos
    },
    {
      path: 'activos',
      name: 'Activos',
      component: Activos
    },
    {
      path: 'pasivos',
      name: 'Pasivos',
      component: Pasivos
    },
    {
      path: 'fiscal',
      name: 'Fiscal',
      component: Fiscal
    },
    {
      path: 'aclaraciones',
      name: 'Aclaraciones',
      component: Aclaraciones
    },
    {
      path: 'resumen',
      name: 'Resumen',
      component: Resumen
    },
    {
      path: 'observaciones',
      name: 'Observaciones',
      component: Observaciones
    },
    {
      path: 'enviada',
      name: 'Enviada',
      component: Enviada
    },
    {
      path: 'tablero',
      name: 'Tablero Inicial',
      component: TableroDec
    },
    {
      path: 'historico',
      name: 'Histórico',
      component: Historico
    }
  ]
}
let adminMenu = {
  path: '/admin',
  component: DashboardLayout,
  redirect: '/admin/plantilla',
  children: [{
      path: 'plantilla',
      name: 'Plantilla',
      component: Plantilla
    },
    {
      path: 'plantilla_ingresos',
      name: 'Plantilla de Ingresos',
      component: PlantillaIngresos
    },
    {
      path: 'plantilla_bajas',
      name: 'Plantilla para bajas',
      component: PlantillaBajas
    },
    {
      path: 'movimientos',
      name: 'Movimientos',
      component: Movimientos
    },
    {
      path: 'usuarios',
      name: 'Usuarios',
      component: Usuarios
    },
    {
      path: 'permisos',
      name: 'Permisos',
      component: Permisos
    },
    {
      path: 'altapersonal',
      name: 'Alta Personal',
      component: AltaPersonal
    },
    {
      path: 'tablero',
      name: 'Tablero',
      component: Tablero
    },
    {
      path: 'notificaciones',
      name: 'Notificaciones',
      component: Notificaciones
    },
    {
      path: 'tablero2',
      name: 'Tablero2',
      component: Tablero2
    }
  ]
}

let reportesMenu = {
  path: '/reportes',
  component: DashboardLayout,
  redirect: '/reportes/declaraciones',
  children: [{
      path: 'declaraciones',
      name: 'Declaraciones',
      component: Declaraciones
    },
    {
      path: 'omisos',
      name: 'Omisos',
      component: Omisos
    },
    {
      path: 'declaraciones2',
      name: 'Declaraciones2',
      component: Declaraciones2
    },
    {
      path: 'cumplimiento',
      name: 'Cumplimiento',
      component: Cumplimiento
    },
    {
      path: 'cumplimiento2',
      name: 'Cumplimiento2',
      component: Cumplimiento2
    },
    {
      path: 'ldeclaraciones',
      name: 'Lista de Declaraciones',
      component: ListDeclaraciones
    },
    {
      path: 'hdecl',
      name: 'Historico de Declaraciones',
      component: HistoricoDeclaraciones
    },
    /*Ruta para para verificación*/
    {
      path: 'verificacion',
      name: 'Verificación',
      component: Verificacion
    },
    {
      path: 'RepDeclaraciones',
      name: 'Reporte de Declaraciones',
      component: RepDeclaraciones
    },
    {
      path: 'movimientos',
      name: 'Reporte de Movimientos',
      component: RMovimientos
    },
    {
      path: 'reporteDeclaraciones',
      name: 'Reporte Declaraciones',
      component: ReporteDeclaraciones
    },
    {
      path: 'reporteDeclaraciones2',
      name: 'No de Declaraciones',
      component: ReporteDeclaraciones2
    },
    {
      path: 'reporteConflictoInt',
      name: 'Reporte Conflicto de Interés ',
      component: ReporteConflictoInteres
    },
    {
      path: 'reporteBajasDefuncion',
      name: 'Reporte Bajas por defunción ',
      component: ReporteBajasDefuncion
    }, 
    {
      path: 'reporteAccesosBalanza',
      name: 'Reporte Accesos a Balanza ',
      component: ReporteAccesosBalanza
    }
  ]
}
let alertas = {
  path: '/alertas',
  component: DashboardLayout,
  redirect: '/alertas/bitacora',
  children: [{
      path: '/alertas/bitacora',
      name: 'Bitácora de Consulta',
      component: BitacoraC
    },
    {
      path: '/alertas/modificadas',
      name: 'Declaraciones Modificadas',
      component: Modificadas
    }
  ]
}


let loginPage = {
  path: '/login',
  name: 'Login',
  component: Login
}

let busqueda = {
  path: '/busqueda',
  name: 'Busqueda',
  component: Busqueda
}

let cambiaPass = {
  path: '/update',
  name: 'Update',
  component: CambiaPass
}

let recuperaPass = {
  path: '/recuperapass/:user?/:upass?',
  name: 'Recupera',
  component: RecuperaPass,
  props: true
}


const routes = [
  formsMenu,
  loginPage,
  adminMenu,
  reportesMenu,
  cambiaPass,
  recuperaPass,
  busqueda,
  alertas,
  {
    path: '*',
    component: Login
  }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
