import AppSpinner from './components/UIComponents/Spinner.component.vue';

const SpinnerPlugin = {
  install(Vue, options) {
    let Spinner = {
      $vm: null,
      init(vm) {
        this.$vm = vm
      },
      show() {
        this.$vm.ACTIVE = true;
      },
      hide() {
        this.$vm.ACTIVE = false;
      },
    }

    const VueSpinnerEventBus = new Vue({
      data: {
        ACTIVE: false
      }
    })
    const inBrowser = typeof window !== 'undefined'

    if (inBrowser) {
      window.VueSpinnerEventBus = VueSpinnerEventBus
      Spinner.init(VueSpinnerEventBus)
    }

    Vue.component("app-spinner", AppSpinner);

    Vue.prototype.$Spinner = Spinner
  }
};

export default SpinnerPlugin;
