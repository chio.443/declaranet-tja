<?php

use Illuminate\Http\Request;

/*Login*/

Route::get('/login/{usuario}/{pass}', 'LoginController@getLogin');
Route::post('/updatePass/{pass}', 'LoginController@updatePass')->middleware('token');
Route::get('/recupera_pass/{rfc}', 'LoginController@recupera_pass2');
Route::get('/recupera_check_user/{rfc}/{pass}', 'LoginController@recupera_check_user');
Route::get('/cron_recupera2', 'LoginController@cron_recupera2');
Route::get('/cron_envia2/{rfc}/{pass}/{enviado}', 'LoginController@cron_envia2');
Route::get('/cron_recupera_omisos', 'LoginController@cron_recupera_omisos');

/* Ruta Bienes Inmuebles */
Route::get('bienes_inmuebles', 'BienesInmueblesController@index')->middleware('token');
Route::get('bienes_inmuebles/{id}', 'BienesInmueblesController@item')->middleware('token');
Route::post('bienes_inmuebles', 'BienesInmueblesController@store')->middleware('token');
Route::delete('bienes_inmuebles/{id}', 'BienesInmueblesController@delete')->middleware('token');

/* Ruta Bienes Intangibles */
Route::get('bienes_intangibles', 'BienesIntangiblesController@index')->middleware('token');
Route::get('bienes_intangibles/{id}', 'BienesIntangiblesController@item')->middleware('token');
Route::post('bienes_intangibles', 'BienesIntangiblesController@store')->middleware('token');
Route::delete('bienes_intangibles/{id}', 'BienesIntangiblesController@delete')->middleware('token');

/* Ruta Bienes Muebles No Registrados */
Route::get('bienes_muebles_no_registrados', 'BienesMueblesNoRegistrablesController@index')->middleware('token');
Route::get('bienes_muebles_no_registrados/{id}', 'BienesMueblesNoRegistrablesController@item')->middleware('token');
Route::post('bienes_muebles_no_registrados', 'BienesMueblesNoRegistrablesController@store')->middleware('token');
Route::delete('bienes_muebles_no_registrados/{id}', 'BienesMueblesNoRegistrablesController@delete')->middleware('token');

/* Ruta Bienes Muebles Registrados */
Route::get('bienes_muebles_registrados', 'BienesMueblesRegistrablesController@index')->middleware('token');
Route::get('bienes_muebles_registrados/{id}', 'BienesMueblesRegistrablesController@item')->middleware('token');
Route::post('bienes_muebles_registrados', 'BienesMueblesRegistrablesController@store')->middleware('token');
Route::delete('bienes_muebles_registrados/{id}', 'BienesMueblesRegistrablesController@delete')->middleware('token');

/* Ruta Efectivo Metales */
Route::get('efectivo_metales', 'EfectivoMetalesController@index')->middleware('token');
Route::get('efectivo_metales/{id}', 'EfectivoMetalesController@item')->middleware('token');
Route::post('efectivo_metales', 'EfectivoMetalesController@store')->middleware('token');
Route::delete('efectivo_metales/{id}', 'EfectivoMetalesController@delete')->middleware('token');

/* Ruta Inversiones Cuentas Valores */
Route::get('inversiones_cuentas_valores', 'InversionesCuentasValoresController@index')->middleware('token');
Route::get('inversiones_cuentas_valores/{id}', 'InversionesCuentasValoresController@item')->middleware('token');
Route::post('inversiones_cuentas_valores', 'InversionesCuentasValoresController@store')->middleware('token');
Route::delete('inversiones_cuentas_valores/{id}', 'InversionesCuentasValoresController@delete')->middleware('token');
/* Ruta Empresas*/
Route::get('empresas', 'EmpresasController@index')->middleware('token');
Route::get('empresas/{id}', 'EmpresasController@item')->middleware('token');
Route::post('empresas', 'EmpresasController@store')->middleware('token');
Route::delete('empresas/{id}', 'EmpresasController@delete')->middleware('token');
/* Ruta Membresias*/
Route::get('membresias', 'MembresiasController@index')->middleware('token');
Route::get('membresias/{id}', 'MembresiasController@item')->middleware('token');
Route::post('membresias', 'MembresiasController@store')->middleware('token');
Route::delete('membresias/{id}', 'MembresiasController@delete')->middleware('token');
Route::get('getPersonas', 'MembresiasController@getPersonas')->middleware('token');
Route::get('getNaturalezas', 'MembresiasController@getNaturalezas')->middleware('token');
Route::get('getColaboraciones', 'MembresiasController@getColaboraciones')->middleware('token');

/* Ruta Apoyos*/
Route::get('apoyos', 'ApoyosBeneficiosPublicosController@index')->middleware('token');
Route::get('apoyos/{id}', 'ApoyosBeneficiosPublicosController@item')->middleware('token');
Route::post('apoyos', 'ApoyosBeneficiosPublicosController@store')->middleware('token');
Route::delete('apoyos/{id}', 'ApoyosBeneficiosPublicosController@delete')->middleware('token');
/* Ruta Apoyos*/
Route::get('representacion', 'RepresentacionActivaController@index')->middleware('token');
Route::get('representacion/{id}', 'RepresentacionActivaController@item')->middleware('token');
Route::post('representacion', 'RepresentacionActivaController@store')->middleware('token');
Route::delete('representacion/{id}', 'RepresentacionActivaController@delete')->middleware('token');
/* Ruta Sueldos y salarios publicos*/
Route::get('sueldos_publicos', 'SueldosSalariosPublicosController@index')->middleware('token');
Route::get('sueldos_publicos/{id}', 'SueldosSalariosPublicosController@item')->middleware('token');
Route::post('sueldos_publicos', 'SueldosSalariosPublicosController@store')->middleware('token');
Route::delete('sueldos_publicos/{id}', 'SueldosSalariosPublicosController@delete')->middleware('token');
/* Ruta Sueldos y salarios otros*/
Route::get('sueldos_otros', 'SueldosSalariosOtrosEmpleosController@index')->middleware('token');
Route::get('sueldos_otros/{id}', 'SueldosSalariosOtrosEmpleosController@item')->middleware('token');
Route::get('sueldos_otros_rfc', 'SueldosSalariosOtrosEmpleosController@rfcingresos')->middleware('token');
Route::post('sueldos_otros', 'SueldosSalariosOtrosEmpleosController@store')->middleware('token');
Route::delete('sueldos_otros/{id}', 'SueldosSalariosOtrosEmpleosController@delete')->middleware('token');
/* Ruta Actividades Empresariales*/
Route::get('actividades_empresariales', 'ActividadEmpresarialController@index')->middleware('token');
Route::get('actividades_empresariales/{id}', 'ActividadEmpresarialController@item')->middleware('token');
Route::post('actividades_empresariales', 'ActividadEmpresarialController@store')->middleware('token');
Route::delete('actividades_empresariales/{id}', 'ActividadEmpresarialController@delete')->middleware('token');
/* Ruta Actividades Profecionales*/
Route::get('actividades_profecionales', 'ActividadProfesionalController@index')->middleware('token');
Route::get('actividades_profecionales/{id}', 'ActividadProfesionalController@item')->middleware('token');
Route::post('actividades_profecionales', 'ActividadProfesionalController@store')->middleware('token');
Route::delete('actividades_profecionales/{id}', 'ActividadProfesionalController@delete')->middleware('token');
/* Ruta Actividades Economicas*/
Route::get('actividades_economicas', 'ActividadEconomicaMenorController@index')->middleware('token');
Route::get('actividades_economicas/{id}', 'ActividadEconomicaMenorController@item')->middleware('token');
Route::post('actividades_economicas', 'ActividadEconomicaMenorController@store')->middleware('token');
Route::delete('actividades_economicas/{id}', 'ActividadEconomicaMenorController@delete')->middleware('token');
/* Ruta Arrendamiento*/
Route::get('arrendamientos', 'ArrendamientoController@index')->middleware('token');
Route::get('arrendamientos/{id}', 'ArrendamientoController@item')->middleware('token');
Route::post('arrendamientos', 'ArrendamientoController@store')->middleware('token');
Route::delete('arrendamientos/{id}', 'ArrendamientoController@delete')->middleware('token');
/* Ruta Intereses*/
Route::get('intereses', 'InteresesController@index')->middleware('token');
Route::get('intereses/{id}', 'InteresesController@item')->middleware('token');
Route::post('intereses', 'InteresesController@store')->middleware('token');
Route::delete('intereses/{id}', 'InteresesController@delete')->middleware('token');
/* Ruta Premios*/
Route::get('premios', 'PremiosController@index')->middleware('token');
Route::get('premios/{id}', 'PremiosController@item')->middleware('token');
Route::post('premios', 'PremiosController@store')->middleware('token');
Route::delete('premios/{id}', 'PremiosController@delete')->middleware('token');
/* Ruta Enajenaciones*/
Route::get('enajenaciones', 'EnajenacionesController@index')->middleware('token');
Route::get('enajenaciones/{id}', 'EnajenacionesController@item')->middleware('token');
Route::post('enajenaciones', 'EnajenacionesController@store')->middleware('token');
Route::delete('enajenaciones/{id}', 'EnajenacionesController@delete')->middleware('token');
/* Ruta Otros Ingresos*/
Route::get('otros_ingresos', 'OtrosIngresosController@index')->middleware('token');
Route::get('otros_ingresos/{id}', 'OtrosIngresosController@item')->middleware('token');
Route::post('otros_ingresos', 'OtrosIngresosController@store')->middleware('token');
Route::delete('otros_ingresos/{id}', 'OtrosIngresosController@delete')->middleware('token');
/* Ruta Fideicomisos*/
Route::get('fideicomisos', 'FideicomisosController@index')->middleware('token');
Route::get('fideicomisos/{id}', 'FideicomisosController@item')->middleware('token');
Route::post('fideicomisos', 'FideicomisosController@store')->middleware('token');
Route::delete('fideicomisos/{id}', 'FideicomisosController@delete')->middleware('token');
/* Ruta Cuentas por Cobrar*/
Route::get('cuentas_por_cobrar', 'CuentasPorCobrarController@index')->middleware('token');
Route::get('cuentas_por_cobrar/{id}', 'CuentasPorCobrarController@item')->middleware('token');
Route::post('cuentas_por_cobrar', 'CuentasPorCobrarController@store')->middleware('token');
Route::delete('cuentas_por_cobrar/{id}', 'CuentasPorCobrarController@delete')->middleware('token');
/* Ruta Uso de Bienes*/
Route::get('uso_beneficios', 'UsoBeneficiosController@index')->middleware('token');
Route::get('uso_beneficios/{id}', 'UsoBeneficiosController@item')->middleware('token');
Route::post('uso_beneficios', 'UsoBeneficiosController@store')->middleware('token');
Route::delete('uso_beneficios/{id}', 'UsoBeneficiosController@delete')->middleware('token');


/*
|--------------------------------------------------------------------------
| CATALOGOS
|--------------------------------------------------------------------------
|
 */
/* Catalogo Forma Adquisicion Controller */
Route::get('cat_forma_adquisicion', 'CatFormaAdquisicionController@index')->middleware('token');
Route::post('cat_forma_adquisicion', 'CatFormaAdquisicionController@store')->middleware('token');
Route::delete('cat_forma_adquisicion/{id}', 'CatFormaAdquisicionController@delete')->middleware('token');
/* Catalogo Forma Pagos Controller */
Route::get('cat_forma_pago', 'CatFormaPagoController@index')->middleware('token');
Route::post('cat_forma_pago', 'CatFormaPagoController@store')->middleware('token');
Route::delete('cat_forma_pago/{id}', 'CatFormaPagoController@delete')->middleware('token');
/* Catalogo Bancaria Controller */
Route::get('cat_bancaria', 'CatBancariaController@index')->middleware('token');
Route::post('cat_bancaria', 'CatBancariaController@store')->middleware('token');
Route::delete('cat_bancaria/{id}', 'CatBancariaController@delete')->middleware('token');
/* Catalogo Bancaria Controller */
Route::get('cat_fondo_inversion', 'CatFondoInversionController@index')->middleware('token');
Route::post('cat_fondo_inversion', 'CatFondoInversionController@store')->middleware('token');
Route::delete('cat_fondo_inversion/{id}', 'CatFondoInversionController@delete')->middleware('token');
/* Catalogo Organizaciones privadas y mercantiles */
Route::get('cat_organizacionespym', 'CatOrganizacionespymController@index')->middleware('token');
Route::post('cat_organizacionespym', 'CatOrganizacionespymController@store')->middleware('token');
Route::delete('cat_organizacionespym/{id}', 'CatOrganizacionespymController@delete')->middleware('token');
/* Catalogo tipo de monedas y / o metales */
Route::get('cat_tipo_moneda_metales', 'CatTipoMonedaMetalesController@index')->middleware('token');
Route::post('cat_tipo_moneda_metales', 'CatTipoMonedaMetalesController@store')->middleware('token');

Route::get('cat_tipo_instrumentos', 'CatTipoInstrumentoController@index')->middleware('token');
Route::get('cat_tipo_bien_enajenacion', 'CatTipoBienEnajenacionController@index')->middleware('token');

Route::delete('cat_tipo_moneda_metales/{id}', 'CatTipoMonedaMetalesController@delete')->middleware('token');

Route::get('cat_tipo_instrumentos', 'CatTipoInstrumentoController@index')->middleware('token');
Route::get('cat_tipo_bien_enajenacion', 'CatTipoBienEnajenacionController@index')->middleware('token');
/* Catalogo de Seguros */
Route::get('cat_seguros', 'CatSegurosController@index')->middleware('token');
Route::post('cat_seguros', 'CatSegurosController@store')->middleware('token');
Route::delete('cat_seguros/{id}', 'CatSegurosController@delete')->middleware('token');
/* Catalogo Tipos de valores bursátiles */
Route::get('cat_tipo_valores', 'CatTipoValoresController@index')->middleware('token');
Route::post('cat_tipo_valores', 'CatTipoValoresController@store')->middleware('token');
Route::delete('cat_tipo_valores/{id}', 'CatTipoValoresController@delete')->middleware('token');
/* Catalogo Tipos de valores bursátiles */
Route::get('cat_afores', 'CatAforesController@index')->middleware('token');
Route::post('cat_afores', 'CatAforesController@store')->middleware('token');
Route::delete('cat_afores/{id}', 'CatAforesController@delete')->middleware('token');
/* Catalogo Identificacion Bien */
Route::get('cat_identificacion_bien', 'CatIdentificacionBienController@index')->middleware('token');
Route::post('cat_identificacion_bien', 'CatIdentificacionBienController@store')->middleware('token');
Route::delete('cat_identificacion_bien/{id}', 'CatIdentificacionBienController@delete')->middleware('token');
/* Relacion Transmisor */
Route::get('cat_relacion_transmisor', 'CatRelacionTransmisorController@index')->middleware('token');
/* Catalogo Relacion Persona Adquirio*/
Route::get('cat_relacion_persona_adquirio', 'CatRelacionPersonaAdquirioController@index')->middleware('token');
Route::post('cat_relacion_persona_adquirio', 'CatRelacionPersonaAdquirioController@store')->middleware('token');
Route::delete('cat_relacion_persona_adquirio/{id}', 'CatRelacionPersonaAdquirioController@delete')->middleware('token');
/* Catalogo Setor Industria*/
Route::get('cat_sector_industria', 'CatSectorIndustriaController@index')->middleware('token');
Route::post('cat_sector_industria', 'CatSectorIndustriaController@store')->middleware('token');
Route::delete('cat_sector_industria/{id}', 'CatSectorIndustriaController@delete')->middleware('token');
/* Catalogo Tipo Beneficio*/
Route::get('cat_tipo_beneficio', 'CatTipoBeneficioController@index')->middleware('token');
Route::post('cat_tipo_beneficio', 'CatTipoBeneficioController@store')->middleware('token');
Route::delete('cat_tipo_beneficio/{id}', 'CatTipoBeneficioController@delete')->middleware('token');

/* Valor Inmueble */
Route::get('cat_valor_inmueble', 'CatValorInmuebleController@index')->middleware('token');
Route::get('cat_baja_inmueble', 'CatBajaInmuebleController@index')->middleware('token');
/* Catalogo Tipo Bien*/

Route::get('cat_tipo_bien', 'CatTipoBienController@index')->middleware('token');
Route::post('cat_tipo_bien', 'CatTipoBienController@store')->middleware('token');
Route::delete('cat_tipo_bien/{id}', 'CatTipoBienController@delete')->middleware('token');
/* Catalogo Tipo Bien Mueble*/
Route::get('cat_tipo_bien_mueble', 'CatTipoBienMuebleController@index')->middleware('token');
Route::post('cat_tipo_bien_mueble', 'CatTipoBienMuebleController@store')->middleware('token');
Route::delete('cat_tipo_bien_mueble/{id}', 'CatTipoBienMuebleController@delete')->middleware('token');
/* Catalogo Tipo Bien Mueble NR*/
Route::get('cat_tipo_bien_mueble_nr', 'CatTipoBienMuebleNrController@index')->middleware('token');
Route::post('cat_tipo_bien_mueble_nr', 'CatTipoBienMuebleNrController@store')->middleware('token');
Route::delete('cat_tipo_bien_mueble_nr/{id}', 'CatTipoBienMuebleNrController@delete')->middleware('token');
/* Catalogo Tipo Especifico Inversion*/
Route::get('cat_tipo_especifico_inversion', 'CatTipoEspecificoInversionController@index')->middleware('token');
Route::post('cat_tipo_especifico_inversion', 'CatTipoEspecificoInversionController@store')->middleware('token');
Route::delete('cat_tipo_especifico_inversion/{id}', 'CatTipoEspecificoInversionController@delete')->middleware('token');
/* Catalogo Tipo Inversion*/
Route::get('cat_tipo_inversion', 'CatTipoInversionController@index')->middleware('token');
Route::post('cat_tipo_inversion', 'CatTipoInversionController@store')->middleware('token');
Route::delete('cat_tipo_inversion/{id}', 'CatTipoInversionController@delete')->middleware('token');
/* Catalogo Tipo Metal*/
Route::get('cat_tipo_metal', 'CatTipoMetalController@index')->middleware('token');
Route::post('cat_tipo_metal', 'CatTipoMetalController@store')->middleware('token');
Route::delete('cat_tipo_metal/{id}', 'CatTipoMetalController@delete')->middleware('token');
/* Catalogo Tipo Operacion*/
Route::get('cat_tipo_operacion', 'CatTipoOperacionController@index')->middleware('token');
Route::post('cat_tipo_operacion', 'CatTipoOperacionController@store')->middleware('token');
Route::delete('cat_tipo_operacion/{id}', 'CatTipoOperacionController@delete')->middleware('token');
/* Catalogo Tipo Operacion*/
Route::get('cat_tipo_operacion_inversion', 'CatTipoOperacionInversionController@index')->middleware('token');
Route::post('cat_tipo_operacion_inversion', 'CatTipoOperacionInversionController@store')->middleware('token');
Route::delete('cat_tipo_operacion_inversion/{id}', 'CatTipoOperacionInversionController@delete')->middleware('token');
/* Catalogo Tipo Operacion*/
Route::get('cat_tipo_operacion_interes', 'CatTipoOperacionInteresController@index')->middleware('token');
Route::post('cat_tipo_operacion_interes', 'CatTipoOperacionInteresController@store')->middleware('token');
Route::delete('cat_tipo_operacion_interes/{id}', 'CatTipoOperacionInteresController@delete')->middleware('token');
/* Catalogo Tipo Sociedades*/
Route::get('cat_tipo_sociedad', 'CatTipoSociedadController@index')->middleware('token');
Route::post('cat_tipo_sociedad', 'CatTipoSociedadController@store')->middleware('token');
Route::delete('cat_tipo_sociedad/{id}', 'CatTipoSociedadController@delete')->middleware('token');
/* Catalogo Frecuencia anual*/
Route::get('cat_frecuencia_anual', 'CatFrecuenciaAnualController@index')->middleware('token');
Route::post('cat_frecuencia_anual', 'CatFrecuenciaAnualController@store')->middleware('token');
Route::delete('cat_frecuencia_anual/{id}', 'CatFrecuenciaAnualController@delete')->middleware('token');
/* Catalogo Titular*/
Route::get('cat_titular', 'CatTitularController@index')->middleware('token');
Route::post('cat_titular', 'CatTitularController@store')->middleware('token');
Route::delete('cat_titular/{id}', 'CatTitularController@delete')->middleware('token');
/* Catalogo Titular del Adeudo*/
Route::get('cat_titular_adeudo', 'CatTitularAdeudoController@index')->middleware('token');
/* Catalogo Unidad Medida Plazo*/
Route::get('cat_unidad_medida_plazo', 'CatUnidadMedidaPlazoController@index')->middleware('token');
Route::post('cat_unidad_medida_plazo', 'CatUnidadMedidaPlazoController@store')->middleware('token');
Route::delete('cat_unidad_medida_plazo/{id}', 'CatUnidadMedidaPlazoController@delete')->middleware('token');
/*Catalogo Tipo Apoyo*/
Route::resource('/cat_tipo_apoyo', 'CatTipoApoyoController')->middleware('token');
/*Catalogo Beneficiario*/
Route::resource('/cat_beneficiario', 'CatBeneficiarioController')->middleware('token');
/* Catalogo Tipo Representacion*/
/* Route::get('cat_tipo_representacion', 'CatTipoRepresentacionController@index')->middleware('token');
Route::post('cat_tipo_representacion', 'CatTipoRepresentacionController@store')->middleware('token');
Route::delete('cat_tipo_representacion/{id}', 'CatTipoRepresentacionController@delete')->middleware('token'); */
Route::resource('/cat_tipo_representacion', 'CatTipoRepresentacionController')->middleware('token');
/*Informacion Personal*/

Route::get('infpersonal/rfc/{id}', 'InfPersonalController@filtrorfc')->middleware('token');
Route::get('infpersonal/altas/{id}', 'InfPersonalController@filtrorfcaltas')->middleware('token');
Route::get('infpersonal/usuarios/{id}', 'InfPersonalController@item')->middleware('token');
Route::get('infpersonal/limite/{id}/{desde}/{hasta}', 'InfPersonalController@item2');
Route::resource('/infpersonal', 'InfPersonalController')->middleware('token');
Route::post('infpersonal/resetpass', 'InfPersonalController@resetpass')->middleware('token');

/*Estado Civil*/
Route::resource('/edocivil', 'EstadoCivilController')->middleware('token');
/*Regimen Matrimonial*/
Route::resource('/regmatrimonial', 'RegimenMatController')->middleware('token');
/*Grado de Escolaridad - Catalogo*/
Route::resource('/gradoesc', 'GradoEscolaridadController')->middleware('token');
/*Estatus Carrera- Catalogo*/
Route::resource('/estatuscarr', 'EstatusCarreraController')->middleware('token');
/*Docto Obtenido- Catalogo*/
Route::resource('/doctoObt', 'DocumentoObtController')->middleware('token');
/*Datos Curriculares Catalogo*/
Route::resource('/datosCurric', 'DatosCurricController')->middleware('token');
/*Periodos Cursados*/
Route::resource('/periodosC', 'CatPeriodosCursadosController')->middleware('token');
/*Nivel Gobierno- Catalogo*/
Route::resource('/nivelGobierno', 'NivelGobiernoController')->middleware('token');
/*Poder Juridico- Catalogo*/
Route::resource('/poderJuridico', 'PoderJuridicoController')->middleware('token');
/*Encargo Actual*/
Route::resource('/encargoAct', 'EncargoActualController')->middleware('token');
Route::get('encargo/{id}', 'EncargoActualController@encargo')->middleware('token');
/*Ambitos - Catalogos*/
Route::resource('/ambitos', 'AmbitosController')->middleware('token');
/*Experiencia Laboral*/
Route::resource('/expLaboral', 'ExperienciaLaboralController')->middleware('token');
/*Funciones Principales*/
Route::resource('/funcPrinc', 'FuncionesPrincipalesController')->middleware('token');
/*Tipo Relacion*/
Route::resource('/tipoRelac', 'TipoRelacionController')->middleware('token');
/*Dependientes Economicos*/
Route::resource('/dependientesEc', 'DependientesEconController')->middleware('token');
/*Paises*/
Route::resource('/paises', 'CatPaisController')->middleware('token');
/*Entidad Federativa*/
Route::resource('/entidadFed', 'CatEntidadFedController')->middleware('token');
/*Municipios*/
Route::get('municipios/municipio/{id}', 'CatMunicipioController@municipio')->middleware('token');
Route::resource('/municipios', 'CatMunicipioController')->middleware('token');
/*Localidad*/
Route::get('localidades/localidad/{id}', 'CatLocalidadController@localidad')->middleware('token');
Route::resource('/localidades', 'CatLocalidadController')->middleware('token');
Route::resource('/observaciones', 'ObservacionesController')->middleware('token');
/*Resumen*/

Route::get('getResumen/{id}', 'ResumenController@generaResumen')->middleware('token');
Route::get('getResumen2/{id}', 'ResumenController@generaResumen2');
//bitacora

Route::get('bitacora/{finicio}/{ffin}', 'BitacoraConsultaController@getBitacora');
//actualizar fiscales
Route::get('bitacoraFiscales', 'BitacoraDeclaracionController@actualizaFiscal');
//actualizar campos nuevos
Route::get('bitacoraNuevos', 'BitacoraDeclaracionController@actualizaNuevosCampos');

Route::get('getResumen/pdf/{id}', 'ResumenController@export_pdf');
Route::get('pdf/{id}', 'PdfController@index');

// Bitacora Borrecciones
Route::post('bitacoracorreccion_consulta', 'BitacoraCorreccionesController@item')->middleware('token');
Route::post('bitacoracorreccion', 'BitacoraCorreccionesController@store')->middleware('token');

/*Monedas*/
Route::resource('/monedas', 'CatMonedaController')->middleware('token');
/*Importar excel*/
Route::post('/Excel', 'ExcelController@ImportPlantilla')->middleware('token');
Route::get('/Excel/download/{id}', 'ExcelController@download')->middleware('token');
/*Importar excel*/
Route::post('/ExcelIngresos', 'ExcelController@ImportPlantillaIngresos')->middleware('token');
Route::post('/ExcelBajas', 'ExcelController@ImportPlantillaBajas')->middleware('token');



/*Importar pdf fiscal*/
Route::post('/fiscal', 'FiscalController@ImportPdf')->middleware('token');
Route::get('fiscal/{id}', 'FiscalController@item');
Route::get('hasfiscal/{id}', 'FiscalController@hasfiscal')->middleware('token');
Route::delete('fiscal/{id}', 'FiscalController@delete')->middleware('token');
Route::get('oblfiscal/{id}', 'FiscalController@obligado')->middleware('token');

/*Importar pdf aclaraciones*/
Route::post('aclaracion', 'AclaracionesController@ImportPdf')->middleware('token');
Route::post('solicitud', 'AclaracionesController@store')->middleware('token');
Route::post('changesolicitud', 'AclaracionesController@changeSolicitud')->middleware('token');
Route::get('aclaracion/{id}', 'AclaracionesController@item')->middleware('token');
Route::get('solicitudes', 'AclaracionesController@getsolicitudes')->middleware('token');
Route::get('solicitud/{id}', 'AclaracionesController@solicitudes')->middleware('token');
Route::delete('aclaracion/{id}', 'AclaracionesController@delete')->middleware('token');

/*Catalogos Reportes*/
Route::get('tiposDeclaracion', 'ReportesController@tipoDeclaracion')->middleware('token');
Route::get('getPeriodos', 'ReportesController@periodos')->middleware('token');
Route::post('getDeclarantesAct', 'ReportesController@declarantesActivos')->middleware('token');
Route::post('getDeclarantesBj', 'ReportesController@declarantesBajas')->middleware('token');
Route::post('getDeclarantes', 'ReportesController@declarantes')->middleware('token');
Route::post('getDeclarantesT', 'ReportesController@declarantesTodos')->middleware('token');
Route::post('getOmisos', 'OmisosController@getOmisos')->middleware('token');
Route::post('getOmisos2', 'OmisosController@getOmisos2')->middleware('token');
Route::get('getConflictos', 'ReportesController@getServidoresConflictos')->middleware('token');
Route::get('getBajasDefuncion', 'ReportesController@getBajasDefuncion')->middleware('token');


Route::post('repdeclarantes', 'ReportesController3@repUsuarioTipoDec')->middleware('token');
Route::post('repcumplimiento', 'ReportesController3@cumplimiento')->middleware('token');
Route::post('repndeclaraciones', 'ReportesController3@repNdec')->middleware('token');

Route::post('repdeclarantesF', 'ReportesController3@repUsuarioTipoDecF')->middleware('token');

//Reporte de Cumplimiento
Route::post('cumplimiento', 'CumplimientoController@cumplimiento')->middleware('token');
Route::post('cumplimientoAct', 'CumplimientoController@cumplimientoAct')->middleware('token');
Route::post('cumplimientoBj', 'CumplimientoController@cumplimientoBj')->middleware('token');
//Reporte de Cumplimiento2
Route::post('cumplimiento2', 'CumplimientoController2@cumplimiento')->middleware('token');
Route::post('cumplimientoAct2', 'CumplimientoController2@cumplimientoAct')->middleware('token');
Route::post('cumplimientoBj2', 'CumplimientoController2@cumplimientoBj')->middleware('token');

/*Catalogos Reportes2---------------------------------*/
Route::post('getDeclarantes2', 'ReportesController2@declarantes')->middleware('token');
//////Reportes del Tablero----------------------------------

Route::get('tableroGlobales', 'TableroController@globales')->middleware('token');
Route::get('semana', 'TableroController@getUltimos7')->middleware('token');

Route::get('tableroGlobales2', 'TableroController2@globales')->middleware('token');
Route::get('semana2', 'TableroController2@getUltimos7')->middleware('token');


/* Ruta RepresentacionPasiva */
Route::get('representacion_pasiva', 'RepresentacionPasivaController@index')->middleware('token');
Route::get('representacion_pasiva/{id}', 'RepresentacionPasivaController@item')->middleware('token');
Route::post('representacion_pasiva', 'RepresentacionPasivaController@store')->middleware('token');
Route::delete('representacion_pasiva/{id}', 'RepresentacionPasivaController@delete')->middleware('token');

/*Rutas Socios Comerciales */
Route::get('socios_comerciales', 'SocioComercialController@index')->middleware('token');
Route::get('socios_comerciales/{id}', 'SocioComercialController@item')->middleware('token');
Route::post('socios_comerciales', 'SocioComercialController@store')->middleware('token');
Route::delete('socios_comerciales/{id}', 'SocioComercialController@delete')->middleware('token');

/*Rutas Clientes Principales */
Route::get('clientes_principales', 'ClientesPrincipalesController@index')->middleware('token');
Route::get('clientes_principales/{id}', 'ClientesPrincipalesController@item')->middleware('token');
Route::post('clientes_principales', 'ClientesPrincipalesController@store')->middleware('token');
Route::delete('clientes_principales/{id}', 'ClientesPrincipalesController@delete')->middleware('token');

/* Ruta otras partes */
Route::get('otras_partes', 'OtrasPartesController@index')->middleware('token');
Route::get('otras_partes/{id}', 'OtrasPartesController@item')->middleware('token');
Route::post('otras_partes', 'OtrasPartesController@store')->middleware('token');
Route::delete('otras_partes/{id}', 'OtrasPartesController@delete')->middleware('token');

/*Ruta beneficios gratuitos*/
Route::get('beneficios_privados', 'BeneficiosPrivadosController@index')->middleware('token');
Route::get('beneficios_privados/{id}', 'BeneficiosPrivadosController@item')->middleware('token');
Route::post('beneficios_privados', 'BeneficiosPrivadosController@store')->middleware('token');
Route::delete('beneficios_privados/{id}', 'BeneficiosPrivadosController@delete')->middleware('token');

Route::resource('/localidades', 'CatLocalidadController')->middleware('token');
/*Tipos de Relacion de dependientes economicos*/
Route::resource('/tipoRelacDep', 'CatTipoRelacionDepController')->middleware('token');
/*Monedas*/
Route::resource('/monedas', 'CatMonedaController')->middleware('token');
/*Tipos de Instituciones*/
Route::resource('/cat_tipo_institucion', 'CatTipoInstitucionController')->middleware('token');
/*Tipos de fideicomisos*/
Route::resource('/cat_tipo_fideicomiso', 'CatTipoFideicomisosController')->middleware('token');
Route::resource('/cat_tipo_fideicomiso_part', 'CatTipoFideicomisosPartController')->middleware('token');
/*Tipos de Naturalezas*/
Route::resource('/cat_naturaleza_membresia', 'CatNaturalezaMembresiaController')->middleware('token');

/*Tipos de Ente Publico*/
Route::get('cat/dec', 'CatEntePublicoController@entes')->middleware('token');
Route::resource('/cat_ente_publico', 'CatEntePublicoController')->middleware('token');
Route::get('cat_ente_publico_ingresos', 'CatEntePublicoController@entesingresos')->middleware('token');


/*Tipos de Actividad o Servicio*/
Route::resource('/cat_tipo_actividad', 'CatTipoActividadServicioController')->middleware('token');
/* Ruta Deudas */
Route::get('deudas', 'DeudasController@index')->middleware('token');
Route::get('deudas/{id}', 'DeudasController@item')->middleware('token');
Route::post('deudas', 'DeudasController@store')->middleware('token');
Route::delete('deudas/{id}', 'DeudasController@delete')->middleware('token');

Route::resource('/cat_tipo_acreedores', 'CatTipoAcreedorController')->middleware('token');
Route::resource('/cat_tipo_adeudo', 'CatTipoAdeudoController')->middleware('token');
Route::resource('/cat_tipo_obligaciones', 'CatTipoObligacionesController')->middleware('token');

/* Ruta Otras obligaciones */
Route::get('otrasObligaciones', 'OtrasObligacionesController@index')->middleware('token');
Route::get('otrasObligaciones/{id}', 'OtrasObligacionesController@item')->middleware('token');
Route::post('otrasObligaciones', 'OtrasObligacionesController@store')->middleware('token');
Route::delete('otrasObligaciones/{id}', 'OtrasObligacionesController@delete')->middleware('token');

/* Ruta Usuarios*/
Route::get('usuarios', 'UsuariosController@index')->middleware('token');
Route::get('getTipoUsuarios', 'UsuariosController@getTipoUsuarios')->middleware('token');
//Route::get('usuarios/{id}', 'UsuariosController@item')->middleware('token');
Route::post('usuarios', 'UsuariosController@store')->middleware('token');
Route::delete('usuarios/{id}', 'UsuariosController@delete')->middleware('token');

/*Ruta Movimientos*/
// Catalogo Movimientos
Route::get('cat_movimientos', 'CatTipoMovimientoController@index')->middleware('token');

Route::get('movimientos', 'MovimientosController@lista')->middleware('token');
Route::get('servidores', 'MovimientosController@index')->middleware('token');
Route::get('servidores/correcciones', 'MovimientosController@getCorrecciones')->middleware('token');
Route::post('servidores', 'MovimientosController@store')->middleware('token');
Route::post('aplicaCambio', 'MovimientosController@aplicaCorrecciones')->middleware('token');
Route::post('rechazar', 'MovimientosController@rechazaCorrecciones')->middleware('token');
Route::get('historial', 'MovimientosController@getHCorrecciones')->middleware('token');
Route::get('movimientos/{tipo}/{dependencia}/{fechaInicio}/{fechaFin}', 'MovimientosController@getMovimientos')->middleware('token');
Route::get('servidoresinactivos', 'MovimientosController@servidoresinactivos')->middleware('token');
Route::post('usuario/reactivar', 'MovimientosController@reactivarUsuario')->middleware('token');
Route::post('cancelarbaja', 'MovimientosController@cancelarbaja')->middleware('token');

/* Ruta Permisos*/
Route::get('permisos', 'PermisosController@index')->middleware('token');
Route::get('permisos/catalogo', 'PermisosController@permisos')->middleware('token');
Route::get('permisos/{id}', 'PermisosController@item')->middleware('token');
Route::post('permisos', 'PermisosController@store')->middleware('token');
Route::delete('permisos/{id}', 'PermisosController@delete')->middleware('token');
Route::get('savedeclaracion/{id}', 'DeclaracionController@index')->middleware('token');
Route::get('guardaDec/{id}', 'DeclaracionController@insertDec');

Route::get('getModificadas', 'DeclaracionController@getModificadas');

Route::post('ldeclaraciones', 'DeclaracionesReporte@dec')->middleware('token');

Route::post('hdeclaraciones', 'HistoricoDeclaracionesController@dec')->middleware('token');
Route::get('hdeclaraciones/{id}/{desde}/{hasta}', 'HistoricoDeclaracionesController@dec2');


Route::get('getServidores', 'VerificacionController@getServidores')->middleware('token');

Route::get('historicolst/{id}', 'HistoricoDeclaracionesController@historico')->middleware('token');

Route::get('generaleslst/{id}', 'VerificacionController@getGenerales')->middleware('token');

Route::post('ldeclaracionesrep', 'DeclaracionesReporte@decRep')->middleware('token');

Route::post('rdeclaraciones', 'DeclaracionesReporte@repDec')->middleware('token');
Route::post('rdeclaraciones2', 'DeclaracionesReporte@repDec2')->middleware('token');

// Route::get('ldeclaraciones/{tipo}/{entidad}/{periodo}', 'DeclaracionesReporte@dec')->middleware('token');

Route::get('declaracion/reactiva/{idDecl}', 'DeclaracionesReporte@reactivar')->middleware('token')->middleware('token');


Route::get('getDeclaracion/pdf/{id}/{idus}', 'DeclaracionesReporte@descargaDec');

Route::get('getDeclaracion1/pdf/{id}/{idus}', 'DeclaracionesReporte@descargaDec1');

Route::get('getDeclaracionPublica/pdf/{id}', 'DeclaracionesReporte@descargaDecPublica');

Route::get('getDeclaracion2/pdf/{id}/{idus}', 'DeclaracionesReporte@descargaDec2');

Route::get('getDeclaracionComp/pdf/{id}/{idus}', 'DeclaracionesReporte@descargaDecComp');

// Route::get('getDeclaracion1/pdf/{id}/{idus}', 'DeclaracionesReporte@descargaDec1')->middleware('token');

Route::get('getAcuse/{id}', 'AcuseHistoricoController@index');
Route::get('getAcuseAdmin/{id}', 'AcuseHistoricoController@getAcuseAdmin');


//Solicitudes de transferencia de personal
Route::post('solicitaT', 'SolicitudTransferenciaController@nuevaTransferencia')->middleware('token');
Route::get('getSolicitudes', 'SolicitudTransferenciaController@getSolicitudes')->middleware('token');
Route::delete('solicitudes/{id}', 'SolicitudTransferenciaController@eliminaSolicitud')->middleware('token');
Route::get('solicitudes/rechazar/{id}', 'SolicitudTransferenciaController@rechazar')->middleware('token');
Route::get('solicitudes/aprobar/{id}', 'SolicitudTransferenciaController@aprobar')->middleware('token');
Route::post('reabaja', 'SolicitudTransferenciaController@reabaja')->middleware('token');
///Consulta
Route::get('busqueda/{cadena}', 'ConsultaController@filtro');
Route::get('declaraciones/{id}', 'ConsultaController@getDeclaraciones');
Route::get('getDeclaracion/busqueda/pdf/{id}', 'ConsultaController@getDeclaracion'); //->middleware('token');

///bitacora
Route::get('bitacoraDeclaracion/{id}', 'BitacoraDeclaracionController@item')->middleware('token');
Route::get('bitacoraDeclaracion', 'BitacoraDeclaracionController@guardaBitacora');

//historial de declaraciones del declarante

Route::resource('/historico', 'HistoricoController')->middleware('token');

/////intereses, nueva version

Route::resource('/prcomodato', 'PrestamoComodatoController')->middleware('token');
Route::resource('/empresasasc', 'EmpresasAsociacionesController')->middleware('token');
Route::get('/tipoparticipacion', 'EmpresasAsociacionesController@tipoParticipacion')->middleware('token');

/*Toma de Decisiones*/
Route::resource('/toma_decisiones', 'TomaDecisionesController')->middleware('token');

/*Actualizar id de ente*/
Route::get('actualizaEnte', 'BitacoraDeclaracionController@actualizaEnteD');

//notificaciones
Route::resource('/notificaciones', 'NotificacionController')->middleware('token');

/*Otro Encargo*/
Route::resource('/otroEncargo', 'OtroEncargoController')->middleware('token');


Route::get('/set_tipo_dec_', 'CronController@setTipoDec');
Route::post('/tipo_dec_movimineto', 'UsuarioTipoDecController@movimiento')->middleware('token');
Route::get('/tipo_dec_transferencia', 'UsuarioTipoDecController@transferencia')->middleware('token');
Route::post('/tipo_dec_declaracion', 'UsuarioTipoDecController@declaracion')->middleware('token');
Route::post('/reactivardec', 'UsuarioTipoDecController@reactivardec')->middleware('token');
Route::post('/tipo_dec_nuevo', 'UsuarioTipoDecController@nuevoUsuario')->middleware('token');
Route::post('/tipo_dec_agregar', 'UsuarioTipoDecController@agregar')->middleware('token');
Route::delete('tipo_dec_delete/{id}', 'UsuarioTipoDecController@delete')->middleware('token');
// http://localhost:8000/api/set_tipo_dec_
// https://declaranet.strc.guanajuato.gob.mx/index.php/api/set_tipo_dec_
