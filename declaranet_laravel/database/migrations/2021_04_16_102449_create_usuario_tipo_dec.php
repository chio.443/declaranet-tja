<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTipoDec extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_tipo_dec', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_dec_id');
            $table->integer('ip_id');
            $table->integer('ente_publico_id');
            $table->integer('declaracion_id');
            $table->date('fecha_posesion');
            $table->date('fecha_termino');
            $table->date('fecha_declaracion');
            $table->string('periodo');
            $table->boolean('revision');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_tipo_dec');
    }
}