<?php

//exit();
require_once('DBConn.php');
require_once('class.phpmailer.php');

//////////////////////// DATA /////////////////////////////////////////////
$fecha = date("Y-m-d");

$fp = fopen("contador.json", "r");
$data = fgets($fp);
fclose($fp);
$data = json_decode($data);
$fecha2 = $data->fecha;
$contador = 0;

if (!$data) {
    if ($fecha2 != $fecha) {
        $json_ = array('contador' => 0, 'fecha' => $fecha, 'error' => false);
        $fp = fopen('contador.json', 'w');
        fwrite($fp, json_encode($json_));
        fclose($fp);
    }
} else {
    if ($data->error) {
        exit();
    }
    $contador = $data->contador;
}

if ($contador <= 1000) {
    $remitente = "declaranet@guanajuato.gob.mx";
    $pwd = 'Declaranet2019';
} elseif ($contador > 1000 && $contador <= 2000) {
    $remitente = "declaranet1@guanajuato.gob.mx";
    $pwd = 'Temporal2020a';
} elseif ($contador > 2000 && $contador <= 3000) {
    $remitente = "declaranet2@guanajuato.gob.mx";
    $pwd = 'Temporal2020a';
} elseif ($contador > 3000 && $contador <= 4000) {
    $remitente = "declaranet3@guanajuato.gob.mx";
    $pwd = 'Temporal2020a';
} else {
    exit();
}

//////////////////////// MAIL /////////////////////////////////////////////
DEFINE('MAIL_ADD', $remitente);
DEFINE('MAIL_PWD', $pwd);

DEFINE('MAIL_HOST', 'smtp.gmail.com');
DEFINE('MAIL_PORT', 587);
DEFINE('MAIL_SECURE', 'tls');
DEFINE('MAIL_USER', MAIL_ADD);

//////////////////////// SEND /////////////////////////////////////////////

echo "FROM " . MAIL_ADD . "...<hr>";

$url = 'https://declaranet.strc.guanajuato.gob.mx/api/cron_recupera2'; // path to your JSON file
$data = file_get_contents($url); // put the contents of the fisle into a variable
$users = json_decode($data); // decode the JSON feed
// print_r($users);
foreach ($users->users as $d) {
    // print_r($d);
    if ($d->email) {
        $mail = new PHPMailer();
        $mail->Charset = "UTF-8";
        //$mail->Encoding = "16bit";
        $mail->AddAddress($d->email);
        $mail->From = "constanciastrc@guanajuato.gob.mx";
        $mail->FromName = utf8_decode('Declaranet');
        $mail->addReplyTo('constanciastrc@guanajuato.gob.mx', 'notificaciones strc');
        $mail->Subject = utf8_decode('Recuperar Contraseña');

        $body = '<div style="background:#fff;padding:10px;margin-top:10px;font-size:14px">
                    <h2 style="margin:0;font-family:Arial,Helvetica,sans-serif;color:#000;font-size:22px">Hola, ' . $d->nombre . '!</h2>
                    <p style="font-family:Arial,Helvetica,sans-serif;color:#000">
                    Declaranet te envía un mensaje. Por favor, lee con atención.
                    </p>
                    <p>
                    Recibimos una solicitación de cambio de contraseña. Para confirmar tu nueva contraseña haz click en el siguiente link:<br>
                        <a href="https://declaranet.strc.guanajuato.gob.mx/#/recuperapass/' . $d->rfc . '/' . $d->pass . '" target="_blank" >
                            https://declaranet.strc.guanajuato.gob.mx/#/recuperapass/' . $d->rfc . '/' . $d->pass . '
                        </a>
                    </p>
                    <p>
                        Por favor, ignora este mensaje en el caso que no nos hayas enviado un cambio de contraseña de tu cuenta.
                    </p>
                    <p>
                        Saludos,
                    </p>
                    <p>
                        El Equipo de Declaranet!
                    </p>
                </div>';

        $mail->Body = (BodyMail(("Contraseña Declaranet"), (''), $body));

        $mail->IsHTML(true);
        $mail->IsSMTP();
        $mail->SMTPDebug = 1;  //---->Esta linea es para hacer debug y ver los errores que se generan en el envio del mail.
        //        $mail->Host = 'ssl://smtp.gmail.com';
        $mail->SMTPSecure = MAIL_SECURE;
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PWD;
        $mail->SMTPOptions = array('ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
        //$sended = $mail->Send();
        $error = false;
        if ($sended = $mail->Send()) {
            $url = 'https://declaranet.strc.guanajuato.gob.mx/api/cron_envia2/' . $d->rfc . '/' . $d->pass; // path to your JSON file
            $actualizar = file_get_contents($url); //
        } else {
            if ($contador < 1000) {
                $contador = 1001;
                $remitente = "declaranet1@guanajuato.gob.mx";
                $pwd = 'Temporal2020a';
            } elseif ($contador < 2000) {
                $contador = 2001;
                $remitente = "declaranet2@guanajuato.gob.mx";
                $pwd = 'Temporal2020a';
            } elseif ($contador < 3000) {
                $contador = 3001;
                $remitente = "declaranet3@guanajuato.gob.mx";
                $pwd = 'Temporal2020a';
            } elseif ($contador < 4000) {

                $json_ = array('contador' => 0, 'fecha' => $fecha, 'error' => true);
                $fp = fopen('contador.json', 'w');
                fwrite($fp, json_encode($json_));
                fclose($fp);
                break;
            }
        }
        $json_ = array('contador' => $contador++, 'fecha' => $fecha, 'error' => false);
        $fp = fopen('contador.json', 'w');
        fwrite($fp, json_encode($json_));
        fclose($fp);
    }
}



function BodyMail($subject, $name, $text)
{
    return ("<table style = 'border: 2px outset #084773; border-collapse: collapse; font-size: 9pt; '>
                <tr>
                    <td style = 'border: 2px outset #084773; padding: 5px; text-align: center; '>
                        <p><b>Secretaría de la Transparencia y Rendición de Cuentas </b></p>
                        <p><b>Gobierno del Estado de Guanajuato</b></p>
                    </td>
                </tr>
                <tr>
                    <td width = '700' style = 'border: 2px outset #084773; padding: 5px;'><center>" . $subject . "</center></td>
                </tr>
                <tr>
                    <td width = '700' style = 'border: 2px outset #084773; padding: 5px;'>"
        . ($name ? "<p><b>Estimado(a): " . $name . "</b></p>" : "")
        . "<p>" . $text . "</p>
                    </td>
                </tr>
                <tr>
                    <td style = 'color: red'><center><b>Favor de no responder este mensaje</b></center></td>
                </tr>
         </table>");
}