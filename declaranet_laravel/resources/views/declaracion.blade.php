<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<img src="encabezado.jpg" height="100" width="325">
<p style="text-align: justify;font-size: 12px;">
TITULAR DEL ÓRGANO INTERNO DE CONTROL 
</p>
<p style="text-align: justify;font-size: 12px;">
  TRIBUNAL DE JUSTICIA ADMINISTRATIVA DEL ESTADO DE GUANAJUATO 
</p>
<p style="text-align: justify;font-size: 12px;">
BAJO PROTESTA DE DECIR VERDAD, PRESENTO A USTED MI DECLARACIÓN DE SITUACIÓN PATRIMONIAL Y DE INTERESES, CONFORME A LO DISPUESTO EN LA LEGISLACIÓN Y DEMÁS NORMATIVIDAD APLICABLE A LA MATERIA.
</p>
<mark style="color: black !important;">
  <p style="text-align: justify;font-size: 12px; font-weight: bold;">
LOS DATOS DE TERCEROS SIEMPRE Y CUANDO SEAN PERSONAS FÍSICAS, Y LOS DATOS RESALTADOS NO SERÁN PUBLICOS.
</p>
</mark>

 <table class="table table-striped" >
  <thead style="text-align: left;font-size: 15px;">
    <tr>
      <td colspan="2">
        <h3>Declaración patrimonial y de intereses</h3>
      </td>
       <td align="center">
        @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
          INICIAL
        @else
            @if( $datosDeclaracion['tipo_declaracion_id'] == 2)
              ANUAL
            @else
                CONCLUSIÓN
            @endif
        @endif
        <br>
        <strong style="font-size: 10px;">Fecha de presentación:</strong>
        <strong style="font-size: 10px;">{{ $fechaPresenta }}</strong>

      </td>
    </tr>
  </thead>

  <tbody>

      <tr>
        <td class="tsecciones" style="font-size:50px;text-align: left;color:navy;font-weight: 900; " colspan="3"><br>DATOS PERSONALES</td>
      </tr>
      <tr>
       <td style="text-align: left; font-weight: bold;">Nombre:</td>
        <td style="text-align: left; font-weight: bold;">País de Nacimiento: </td>
        <td style="text-align: left; font-weight: bold;">Entidad de Nacimiento: </td>
      </tr>
      <tr>
       <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['nombres'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; "> CURP: <br></td>
        <td style="text-align: left; font-weight: bold;"> RFC: <br></td>
        <td style="text-align: left; font-weight: bold;"> Estado Civil: <br></td>
      </tr>

      <tr>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; ">Correo Personal: <br></td>
        <td style="text-align: left; font-weight: bold;">Correo Laboral: <br></td>
        <td style="text-align: left; font-weight: bold;">Teléfono Particular: <br></td>
      </tr>

      <tr>
        <td style="text-align: left; "><mark>********************</mark><br></td>
        <td style="text-align: left; ">{{ $dpersonales['ce_laboral'] }}<br></td>
        <td style="text-align: left; "><mark>********************</mark><br></td>
      </tr>
       <tr>
      <td style="text-align: left; font-weight: bold;" colspan="3"><br>Domicilio</td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold;">Municipio: <br></td>
        <td style="text-align: left; font-weight: bold;">Entidad: <br></td>
        <td style="text-align: left; font-weight: bold;">Localidad: <br></td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
      </tr>
       <tr>
        <td style="text-align: left; font-weight: bold;">Colonia: <br></td>
        <td style="text-align: left; font-weight: bold;">Tipo Vialidad: <br></td>
        <td style="text-align: left; font-weight: bold;">Calle: <br></td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
      </tr>

       <tr>
        <td style="text-align: left; font-weight: bold;">No. Ext: <br></td>
        <td style="text-align: left; font-weight: bold;">No. Int: <br></td>
        <td style="text-align: left; font-weight: bold;">CP: <br></td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark><br></td>
      </tr>
      <tr>
        <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3"><br>DATOS CURRICULARES</td>
      </tr>

      @foreach($dcurrc as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold; ">Grado de Escolaridad:</td>
        <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['grado'] }}</td>
        <td style="text-align: left;text-transform: uppercase; " colspan="2">{{ $dato['institucion'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; ">Carrera</td>

        <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['carrera'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; ">Entidad</td>
        <td style="text-align: left; font-weight: bold; ">Periodos</td>
        <td style="text-align: left; font-weight: bold; ">Periodos Cursados </td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['entidad'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['periodos'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['periodosc'] }}</td>
      </tr>
       <tr>
         <td style="text-align: left; font-weight: bold; ">Estatus</td>
        <td style="text-align: left; font-weight: bold; ">Documento Obtenido</td>
        <td style="text-align: left; font-weight: bold; ">Cédula</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['estatus'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['doctoOb'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['cedula'] }}</td>
      </tr>
      @endforeach
      <tr>
        <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3"><br>EXPERIENCIA LABORAL</td>
      </tr>

      @foreach($experiencia as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold;">Ámbito:</td>
        <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase;">{{ $dato['ambito'] }}</td>
        <td style="text-align: left;text-transform: uppercase;" colspan="2">{{ $dato['nombreInst'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left;  font-weight: bold;">Unidad Administrativa</td>
        <td style="text-align: left; font-weight: bold;" colspan="2">Sector</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['area'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['sector'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left;  font-weight: bold;">Jerarquía</td>
        <td style="text-align: left;  font-weight: bold;">Cargo</td>
        <td style="text-align: left;  font-weight: bold;">Fecha Ingreso</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['jerarquia'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['cargo'] }}</td>
        <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['fingreso'] }}</td>
      </tr>
       <tr>
         <td style="text-align: left; font-weight: bold;">Fecha Salida</td>

        <td style="text-align: left;text-transform: uppercase; " colspan="2">{{ $dato['fsalida'] }}</td>
      </tr>
      <tr>
      <td style="text-align: left; font-weight: bold;" colspan="3"><br>Domicilio</td>
      </tr>
      <tr>
        <td style="text-align: left;font-weight: bold; ">Municipio: <br></td>
        <td style="text-align: left;font-weight: bold; ">Entidad: <br></td>
        <td style="text-align: left;font-weight: bold; ">Localidad: <br></td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['municipio'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['entidad'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['localidad'] }}<br></td>
      </tr>
       <tr>
        <td style="text-align: left;font-weight: bold; ">Colonia:<br></td>
        <td style="text-align: left;font-weight: bold; ">Tipo Vialidad: <br></td>
        <td style="text-align: left; font-weight: bold;">Calle: <br></td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['colonia'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tvialidad'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['calle'] }}<br></td>
      </tr>

       <tr>
        <td style="text-align: left; font-weight: bold;">No. Ext:<br></td>
        <td style="text-align: left; font-weight: bold;">No. Int:<br></td>
        <td style="text-align: left; font-weight: bold;">CP: <br></td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['numext'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['numint'] }}<br></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['cp'] }}<br></td>
      </tr>
      @endforeach



       <tr>
        <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3">
          <br>CÓNYUGE, CONCUBINA, CONCUBINARIO Y/O DEPENDIENTES ECONÓMICOS</td>
      </tr>

      @foreach($dependientes as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold; ">Nombre</td>
        <td style="text-align: left; font-weight: bold; ">Relación</td>
        <td style="text-align: left; font-weight: bold; ">CURP</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase;  "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase;  "><mark>********************</mark><br></td>
        <td style="text-align: left;text-transform: uppercase;  "><mark>********************</mark><br></td>
      </tr>
      @endforeach

      <tr>
      <br>
      @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</td>
      @else
          <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DATOS DEL ENCARGO ACTUAL</td>
      @endif


      </tr>
      <tr>
        <td style="text-align: left;font-weight: bold; " colspan="3">Dependencia:</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; " colspan="3">{{ $dencargo['dependencia'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold;">Área</td>
        <td style="text-align: left; font-weight: bold;">Cargo</td>
        @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
          <td style="text-align: left; font-weight: bold;">Fecha de conclusión del empleo, cargo o comisión. </td>
        @else
          <td style="text-align: left; font-weight: bold;">Toma Posesión</td>
        @endif
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['area'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['cargo'] }}</td>
        @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
         <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['baja'] }}</td>
        @else
          <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['ftomaposesion'] }}</td>
        @endif

      </tr>
      <tr>
      <br>
      <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>INTERESES</td>
      </tr>
      <tr>
        <td style="text-align: left;font-weight: bold; " colspan="3">Empresas, Sociedades y Asociaciones</td>
      </tr>
      @if(count($empresas)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($empresas as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['grado'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['naturaleza'] }}</td>
        </tr>
      @endforeach


      <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Participación en Toma de Decisiones</td>
      </tr>
      @if(count($tomaDecisiones)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($tomaDecisiones as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['nombre'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['puesto'] }}</td>
        </tr>
      @endforeach

      <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Beneficios Públicos</td>
      </tr>
      @if(count($beneficiosPubl)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($beneficiosPubl as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['programa'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['institucion'] }}</td>
        </tr>
      @endforeach

      <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Representación Activa</td>
      </tr>
      @if(count($reprActiva)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($reprActiva as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['nombre'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc'] }}</td>
        </tr>
      @endforeach


      <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Clientes Principales</td>
      </tr>
      @if(count($clientesPrinc)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($clientesPrinc as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['nombre'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc'] }}</td>
        </tr>
      @endforeach

        <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Beneficios Privados</td>
      </tr>
      @if(count($beneficiosPriv)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($beneficiosPriv as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['nombre'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc'] }}</td>
        </tr>
      @endforeach

      <tr>
          <td style="text-align: left;font-weight: bold; " colspan="3">Fideicomisos</td>
      </tr>
      @if(count($fideicomisos)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
      @endif
      @foreach($fideicomisos as $dato)
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3" >{{ $dato['nombre'] }}</td>
        </tr>
        <tr>
          <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc'] }}</td>
        </tr>
      @endforeach

       <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>INGRESOS</td>
      </tr>
       @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
             <tr>
                <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos (situación actual)</td>
              </tr>
              @foreach($spublicos as $dato)
              <tr>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" >{{ $dato['nombre'] }}</td>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach
               <tr>
                <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
              </tr>

              @foreach($otross as $dato)
              <tr>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach
              <tr>

                <td style=  "text-align: left; font-weight: bold; padding-left: 15px"><br>TOTAL</br></td>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $totalingresos['total'] }}</td>

              </tr>
             <!--  <tr>
                <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos (Año inmediato anterior)</td>
              </tr>

              @foreach($spublicosant as $dato)
              <tr>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" >{{ $dato['nombre'] }}</td>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach
               <tr>
                <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
              </tr>

              @foreach($otrossant as $dato)
              <tr>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach -->


        @else
               <tr>
                <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos</td>
              </tr>

              @foreach($spublicosant as $dato)
              <tr>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" >{{ $dato['nombre'] }}</td>
                <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach
               <tr>
                <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
              </tr>

              @foreach($otrossant as $dato)
              <tr>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['ingreso'] }}</td>
              </tr>
              @endforeach

              <tr>

                <td style=  "text-align: left; font-weight: bold; padding-left: 15px"><br>TOTAL</br></td>

                <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $totalingresos['total'] }}</td>
              </tr>
        @endif

      <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>ACTIVOS</td>
      </tr>
      <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3">BIENES INMUEBLES</td>
      </tr>

      @foreach($binmuebles as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold;text-transform: uppercase; " colspan="3">{{ $dato['tipoOperacion'] }}</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['naturaleza'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
      </tr>
      @endforeach

       <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>BIENES MUEBLES</td>
      </tr>

      @foreach($bmuebles as $dato)
      <tr>
        <td style="text-align: left;  padding-left: 15px; font-weight: bold;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>

        <td style="text-align: left; padding-left: 15px;text-transform: uppercase; ">{{ $dato['tipoBien'] }}</td>

        <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
      </tr>
      @endforeach
      <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;"><br>BIENES MUEBLES NO REGISTRABLES</td>
      </tr>

      @foreach($bmueblesnr as $dato)
      <tr>
        <td style="text-align: left;  padding-left: 15px; font-weight: bold;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>

        <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['tipoBien'] }}</td>

        <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
      </tr>
      @endforeach

       <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3">INVERSIONES</td>
      </tr>

      @foreach($inversiones as $dato)
       <tr>
         <td style="text-align: left; font-weight: bold; ">Tipo de Operacion</td>
        <td style="text-align: left; font-weight: bold; ">Tipo de Inversión</td>
        <td style="text-align: left; font-weight: bold; ">Titular</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoOp'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoInv'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
       <tr>
         <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
        <td style="text-align: left; font-weight: bold;" >Cuenta</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; "  colspan="2">{{ $dato['nombre_institucion'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
      <tr>
         <td style="text-align: left; font-weight: bold; ">Monto Original</td>
        <td style="text-align: left; font-weight: bold; ">Pais</td>
        <td style="text-align: left; font-weight: bold; ">Entidad</td>
      </tr>
       <tr>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['pais'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['entidad'] }}</td>
      </tr>
      @endforeach

      <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>PASIVOS</td>
      </tr>
        @if(count($pasivos)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
        @endif

      @foreach($pasivos as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold; ">Tipo de Operación:</td>
        <td style="text-align: left; font-weight: bold; ">Tipo Adeudo:</td>
        <td style="text-align: left; font-weight: bold; ">Identificador:</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoAdeudo'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; ">Fecha Adeudo:</td>
        <td style="text-align: left; font-weight: bold; ">Monto Original:</td>
        <td style="text-align: left; font-weight: bold; ">Saldo Pendiente:</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['fadeudo'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['original'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
      @endforeach


      <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>PRÉSTAMO O COMODATO POR TERCEROS</td>
      </tr>
        @if(count($prestamo)== 0)
        <tr>
          <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
        @endif

      @foreach($prestamo as $dato)
      <tr>
        <td style="text-align: left; font-weight: bold; ">Tipo:</td>
        <td style="text-align: left; font-weight: bold; ">Tipo de Bien:</td>
        <td style="text-align: left; font-weight: bold; ">Detalle:</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipo'] }}</td>
        <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoBien'] }}</td>
        <td style="text-align: left;text-transform: uppercase; "><mark>********************</mark></td>
      </tr>
      @endforeach


       <tr>
        <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DECLARACIÓN FISCAL</td>
      </tr>
      <tr>
        <td style="text-align: left;text-transform: uppercase; " colspan="3">{{ $resFis }}</td>
      </tr>
  </tbody>
</table>


<style type="text/css">
  tbody{
    font-size: 12px !important;
  }
  table {
  border: 1px solid black;
}
.tsecciones{
    font-size: 15px !important;
  }
  mark {
  background-color: #000000;
  color: #000000;
  /*font-size: 15px !important;*/

}
</style>


