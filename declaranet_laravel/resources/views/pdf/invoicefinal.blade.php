<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Declara TJA Guanajuato - Acuse de recibo</title>
    <style>
            .clearfix:after {
              content: "";
              display: table;
              clear: both;
            }
            html { margin-bottom: 0px}

            a {
              color: #5D6975;
              text-decoration: underline;
            }

            body {
              position: relative;
              width: 21cm;
              height: 29.7cm;
              margin: 0 auto;
              color: #001028;
              background: #FFFFFF;
              font-family: Arial, sans-serif;
              font-size: 12px;
              font-family: Arial;
            }

            header {
              padding: 10px 0;
              margin-bottom: 30px;
            }

            #logo {
              text-align: center;
              margin-bottom: 10px;
            }

            #logo img {
              width: 90px;
            }
            .encabezado{
                text-align: right;
                width: 90%;
                  font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
            }
            h1 {
                              width: 90%;

              border-top: 1px solid  #5D6975;
              border-bottom: 1px solid  #5D6975;
              color: #FFFFFF;
              font-size: 2.4em;
              line-height: 1.4em;
              font-weight: normal;
              text-align: center;
              margin: 0 0 0 0;
              background-color: #477fbc;
                border-radius: 15px 15px 15px 15px;
                -moz-border-radius: 15px 15px 15px 15px;
                -webkit-border-radius: 15px 15px 15px 15px;
                border: 0px solid #000000;
                /*
                              background: url(dimension.png);
                              */

            }

            #project {
              float: left;
            }

            #project span {
              color: #5D6975;
              text-align: right;
              width: 52px;
              margin-right: 10px;
              display: inline-block;
              font-size: 0.8em;
            }

            #company {
              float: right;
              text-align: right;
            }

            #project div,
            #company div {
              white-space: nowrap;
            }

            table {
              width: 90%;
              border-collapse: collapse;
              border-spacing: 0;
              margin-bottom: 20px;
            }

            table tr:nth-child(2n-1) td {
              background: #F5F5F5;
            }

            table th,
            table td {
              /*text-align: center;*/
            }

            table th {
              padding: 5px 20px;
              color: #5D6975;
              border-bottom: 1px solid #C1CED9;
              white-space: nowrap;
              font-weight: normal;
            }

            table .service,
            table .desc {
              text-align: left;
            }

            table td {
              padding: 20px;
             /* text-align: right;*/
            }

            table td.service,
            table td.desc {
              vertical-align: top;
            }

            table td.unit,
            table td.qty,
            table td.total {
              font-size: 1.2em;
            }

            table td.grand {
              border-top: 1px solid #5D6975;
            }

            #notices .notice {
              color: #5D6975;
              font-size: 1.2em;
            }

            footer {
              color: #5D6975;
              width: 90%;
              height: 30px;
              position: absolute;
              bottom: 0;
              border-top: 1px solid #C1CED9;
              padding: 8px 0;
              text-align: center;
            }
            .inicio{
                position: relative;
                top:5px;
                width: 90%;

                text-align:left;


                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
               /* line-height: 20px;*/
                 line-height: 1.8;
            }
            .justificado{
                                position: relative;
                top:15px;

                width: 90%;
                 background: #F5F5F5;
                text-align:justify;
                text-justify:auto;

                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
               /* line-height: 20px;*/
                 line-height: 1.8;
            }
            .firma{
                position: relative;
                top:15px;
                width: 90%;

                text-align:center;


                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
               /* line-height: 20px;*/
                 line-height: 1.8;
            }
    </style>
  </head>
  <body>
    <main>
        <div align="center"><img src="logogto.png"></div>
        <br>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>Declara TJA Guanajuato <br>Acuse de recibo</h1>
          <p class="encabezado">Tribunal de Justicia Administrativa del Estado de Guanajuato</p>
          <p class="encabezado">Silao de la Victoria, Gto. {{$fechaDec}}</p>
        </div>
      </div>
        <p class="inicio">Estimado/a<br>
            <strong>{{$declarante}}</strong>
        </p>
        <p class="justificado">
          Le comunico la recepción de su información patrimonial y de intereses a través del Sistema de 
          Declaración Patrimonial relativa a la {{$Tipo_DecText}}, de conformidad con lo dispuesto en los 
          artículos 81, 122, 124, fracción II, y 132 de la Constitución Política para el Estado de Guanajuato; 
          Transitorio Tercero de la Ley General de Responsabilidades Administrativas; 1, 2, 28, 
          fracción V, y 50, fracción VII, de la Ley Orgánica del Tribunal de Justicia Administrativa del Estado
          de Guanajuato; 7, fracción I, 10, fracciones XII y XIII, 26, 27, 53, 54, fracción I, y 56 de la 
          Ley del Sistema Estatal Anticorrupción de Guanajuato; 3, fracción VIII, 26, 27, 28, 29, 30, 31, 32, 
          33, fracción I, 34, 35, 36, 37, 38, 39, 40, 41, 42 y 49, fracción IV, de la Ley de Responsabilidades
          Administrativas para el Estado de Guanajuato; y el Acuerdo por el que se modifican los Anexos
          Primero y Segundo del Acuerdo por el que el Comité Coordinador del Sistema Nacional Anticorrupción
          emite el formato de declaraciones: de situación patrimonial y de intereses; y expide las normas 
          e instructivo para su llenado y presentación.</p>
                <p class="justificado">

          Conviene subrayar que, en el supuesto de que existan incongruencias en su declaración de
          situación patrimonial, se le notificará personalmente el contenido de estas, para que, 
          dentro del plazo establecido en el requerimiento, formule las aclaraciones pertinentes.
            </p>

            <p class="justificado">

              Sin embargo, lo anterior no es impedimento para que Usted informe las aclaraciones que estime convenientes de 
              manera previa al citado requerimiento.
  
          </p>
                    <p class="justificado">

          Para cualquier duda respecto a su declaración, puede contactarnos personalmente de lunes a
           viernes de 9:00 a 15:00 horas en las oficinas del Órgano Interno de Control ubicadas en Ejido El 
           Capulín Parcela 76 Z-6 P-1/1, sin número, código postal 36297, en el Municipio de Silao de la
            Victoria, Guanajuato; vía telefónica al número 4726909800 extensión 1411; y vía electrónica a
             través de las cuentas de correo electrónico: sojedac@guanajuato.gob.mx y mvazquezgue@guanajuato.gob.mx.<

        </p>

        <p class="firma">
            <strong>{{$director}}</strong>
            <br>
            Titular del Órgano Interno de Control
        </p>
          <p class="justificado">

Manifiesto que toda la información proporcionada en mi declaración patrimonial y de intereses correspondiente al
año {{$anio}} es verídica.        </p>
        <br><br>
        <p class="firma">
            <span>____________________________________________________</span>
            <br>
            <strong>{{$declarante}}</strong>
            <br>
            Nombre y Firma
        </p>


          <h4>Fecha de impresión: {{$fechaActual}} a las {{$hora}}</h4>

  </body>
</html>