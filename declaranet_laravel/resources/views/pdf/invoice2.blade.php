<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> Declara TJA Guanajuato- Acuse de recibo</title>
    <style>
            .clearfix:after {
              content: "";
              display: table;
              clear: both;
            }

            a {
              color: #5D6975;
              text-decoration: underline;
            }

            body {
              position: relative;
              width: 21cm;  
              height: 29.7cm; 
              margin: 0 auto; 
              color: #001028;
              background: #FFFFFF; 
              font-family: Arial, sans-serif; 
              font-size: 12px; 
              font-family: Arial;
            }

            header {
              padding: 10px 0;
              margin-bottom: 30px;
            }

            #logo {
              text-align: center;
              margin-bottom: 10px;
            }

            #logo img {
              width: 90px;
            }
            .encabezado{
                text-align: right;
                width: 90%;
                  font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
            }
            h1 {
                              width: 90%;

              border-top: 1px solid  #5D6975;
              border-bottom: 1px solid  #5D6975;
              color: #FFFFFF;
              font-size: 2.4em;
              line-height: 1.4em;
              font-weight: normal;
              text-align: center;
              margin: 0 0 0 0;
              background-color: #477fbc;
                border-radius: 15px 15px 15px 15px;
                -moz-border-radius: 15px 15px 15px 15px;
                -webkit-border-radius: 15px 15px 15px 15px;
                border: 0px solid #000000;       
                /*
                              background: url(dimension.png);
                              */

            }

            #project {
              float: left;
            }

            #project span {
              color: #5D6975;
              text-align: right;
              width: 52px;
              margin-right: 10px;
              display: inline-block;
              font-size: 0.8em;
            }

            #company {
              float: right;
              text-align: right;
            }

            #project div,
            #company div {
              white-space: nowrap;        
            }

            table {
              width: 90%;
              border-collapse: collapse;
              border-spacing: 0;
              margin-bottom: 20px;
            }

            table tr:nth-child(2n-1) td {
              background: #F5F5F5;
            }

            table th,
            table td {
              /*text-align: center;*/
            }

            table th {
              padding: 5px 20px;
              color: #5D6975;
              border-bottom: 1px solid #C1CED9;
              white-space: nowrap;        
              font-weight: normal;
            }

            table .service,
            table .desc {
              text-align: left;
            }

            table td {
              padding: 20px;
             /* text-align: right;*/
            }

            table td.service,
            table td.desc {
              vertical-align: top;
            }

            table td.unit,
            table td.qty,
            table td.total {
              font-size: 1.2em;
            }

            table td.grand {
              border-top: 1px solid #5D6975;
            }

            #notices .notice {
              color: #5D6975;
              font-size: 1.2em;
            }

            footer {
              color: #5D6975;
              width: 90%;
              height: 30px;
              position: absolute;
              bottom: 0;
              border-top: 1px solid #C1CED9;
              padding: 8px 0;
              text-align: center;
            }
            .inicio{
                position: relative;
                top:20px;
                width: 90%;
                
                text-align:left;  
              
  
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;  
               /* line-height: 20px;*/
                 line-height: 1.8;       
            }
            .justificado{
                                position: relative;
                top:20px;

                width: 90%;
                 background: #F5F5F5;
                text-align:justify;  
                text-justify:auto;    
  
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;  
               /* line-height: 20px;*/
                 line-height: 1.8;
            }
            .firma{
                position: relative;
                top:20px;
                width: 90%;
                
                text-align:center;  
              
  
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;  
               /* line-height: 20px;*/
                 line-height: 1.8;                
            }
    </style>
  </head>
  <body>
    <main>
        <div align="center"><img src="logogto.png"></div>
        <br>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>Declara TJA Guanajuato <br>Acuse de recibo</h1>
          <p class="encabezado">Tribunal de Justicia Administrativa del Estado de Guanajuato</p>
          <p class="encabezado">Silao de la Victoria, Gto. {{$fechaActual}}</p>
          <p class="encabezado">{{$hora}}.</p>          
        </div>
      </div>
        <p class="inicio">Estimado/a<br>
            <strong>{{$declarante}}</strong>
        </p>
        <p class="justificado">
            Le comunico la recepción de su información patrimonial a través del Sistema Declaranet Guanajuato
            relativa a la declaración patrimonial de modificación del año 2018, de conformidad con lo dispuesto en
            los artículos 32, 33 fracción II y 34 de la Ley de Responsabilidades Administrativas para el Estado de
            Guanajuato, así como 18, fracción I del Reglamento Interior de la Secretaría de la Transparencia y
            Rendición de Cuentas.</p>
                <p class="justificado">

            Asimismo, no omito señalarle que en el supuesto de que existan incongruencias en su declaración de
            situación patrimonial, se le notificará personalmente el contenido de las mismas, para que, dentro del
            plazo de treinta días hábiles contados a partir de la recepción de la notificación, formule las
            aclaraciones pertinentes.
            </p>
            <p class="justificado">
              Para cualquier duda respecto a su declaración, estamos a sus órdenes en el siguiente número
              telefónico 473 73 5 13 13, en un horario de lunes a viernes de las 8:30 a las 16:00 horas.
            </p>
        <p class="firma">
            <strong>{{$director}}</strong>
            <br>
            Director de Enlace, Información y Organización de Archivos
        </p>
       
  </body>
</html>