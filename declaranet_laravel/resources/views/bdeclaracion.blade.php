<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<table style="width: 100%; border: 10px;">    
  <tbody>
      <tr>    
         <td style="text-align: center;" colspan="2">
          TIPO DECLARACIÓN: MODIFICACIÓN PATRIMONIAL 2019
        </td> 
      </tr>
      <tr>    
         <td style="text-align: right;" colspan="2">
          <br>
          FECHA DE LA DECLARACIÓN: {{$fechaDeclaracion}}
        </td> 
      </tr>
      <tr>        
        <td style="text-align: left;font-weight: bold; " colspan="2">
          <br><br><br>
          DATOS GENERALES DEL SERVIDOR PÚBLICO
        </td> 
      </tr>
      <tr>
       <td style="text-align: left; ">NOMBRE:</td>     
       <td style="text-align: left; ">{{ strtoupper($dpersonales['nombres'] )}}</td>      
      </tr>
       <tr>        
      <td style="text-align: left; font-weight: bold;" colspan="2">
        <br> <br><br>
          DATOS DEL PUESTO O ENCARGO ACTUAL DEL SERVIDOR PÚBLICO
      </td> 
      </tr>
      <tr>        
        <td style="text-align: left;">NOMBRE DEL ENCARGO O PUESTO: <br></td>
        <td style="text-align: left; ">{{ strtoupper($dencargo['cargo'] )}}</td> 
      </tr>
      <tr>        
        <td style="text-align: left;">DEPENDENCIA O ENTIDAD <br></td>
        <td style="text-align: left; " colspan="2">{{ strtoupper($dencargo['dependencia'] )}}</td> 
      </tr>
      <tr>        
        <td style="text-align: left;">DOMICILIO: <br></td>
        <td style="text-align: left; ">{{ strtoupper($dencargo['domicilio'] )}}</td> 
      </tr>

      <tr>        
        <td style="text-align: left;font-weight: bold; " colspan="2">
           <br><br><br>
          DATOS CURRICULARES DEL SERVIDOR PÚBLICO
        </td> 
      </tr>
      <tr>        
        <td style="text-align: left;font-weight: bold; " colspan="2">
          ESCOLARIDAD
        </td> 
      </tr>
      <tr>        
        <td style="text-align: left;font-weight: bold; border-bottom: 1px solid black; " colspan="2">
          GRADO MÁXIMO DE ESTUDIOS
        </td> 
      </tr>

      <tr>  
        <td  colspan="2">
        <table style="width: 100%; border: 10px;">   
          <tr class="border_bottom">        
            <td style="text-align: left; ">NIVEL</td> 
            <td style="text-align: left; ">CARRERA</td> 
            <td style="text-align: left; ">INSTITUCIÓN</td> 
            <td style="text-align: left; ">ESTATUS</td> 
          </tr>

          @foreach($dcurrc as $dato)
            <tr>        
              <td style="text-align: left; ">{{ strtoupper($dato['grado'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['carrera'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['institucion'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['estatus'] )}}</td> 
            </tr>
          @endforeach
        </table>
      </td>
      </tr>
       
      
      <tr>        
        <td style="text-align: left;font-weight: bold; border-bottom: 1px solid black;" colspan="2"><br><br>EXPERIENCIA LABORAL</td> 
      </tr>      
       <tr>  
        <td  colspan="2">
        <table style="width: 100%; border: 10px;">   
         <tr class="border_bottom">        
            <td>SECTOR</td> 
            <td>INSTITUCIóN</td> 
            <td>UNIDAD ADMINISTRATIVA</td> 
            <td>PUESTO</td> 
            <td>JERARQUÍA</td> 
            <td>INGRESO</td> 
            <td>EGRESO</td> 
          </tr>
          @foreach($experiencia as $dato)
            <tr>        
              <td style="text-align: left; ">{{ strtoupper($dato['sector'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['nombreInst'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['area'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['cargo'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['jerarquia'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['fingreso'] )}}</td> 
              <td style="text-align: left; ">{{ strtoupper($dato['fsalida'] )}}</td> 
            </tr>
          @endforeach
        </table>
      <br><br><br>
      </td>
      </tr>       
      <tr>
        <td style="text-align: left; font-weight: bold; padding-left: 20px !important;" colspan="2">
           1. DATOS PATRIMONIALES. 
        </td> 
      </tr>
      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          INGRESOS ANUALES NETOS
        </td> 
      </tr>
      <tr>
        <td style="text-align: left;">
          SUELDOS Y SALARIOS PÚBLICOS
        </td> 
         <td style="text-align: left;">
          {{$spublicos['ingreso']}}
        </td> 
      </tr>
      <tr>
        <td style="text-align: left;">
          OTROS SUELDOS Y SALARIOS
        </td> 
         <td style="text-align: left;">          
          {{$otross['otros']}}
          <br><br>
        </td> 
      </tr>

      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          BIENES INMUEBLES
        </td> 
      </tr>
      @foreach($binmuebles as $dato)
      <tr>        
        <td style="text-align: left;" colspan="2">
          {{ strtoupper($dato['tipoOperacion'] )}}: {{strtoupper($dato['naturaleza'])}}, {{ strtoupper($dato['padquisicion'] )}}
        </td> 
      </tr>  
      @endforeach
      @if(count($binmuebles) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif
      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          <br>
          BIENES MUEBLES
        </td> 
      </tr>
      @foreach($bmuebles as $dato)
      <tr>        
        <td style="text-align: left;" colspan="2">
          {{ strtoupper($dato['tipoOperacion'] )}}: {{strtoupper($dato['tipoBien'])}}, {{ strtoupper($dato['padquisicion'] )}}
        </td> 
      </tr>  
      @endforeach
      @if(count($bmuebles) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif
      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          <br>
          BIENES MUEBLES NO REGISTRABLES
        </td> 
      </tr>
      @foreach($bmueblesnr as $dato)
      <tr>        
        <td style="text-align: left;" colspan="2">
          {{ strtoupper($dato['tipoOperacion'] )}}: {{strtoupper($dato['tipoBien'])}}, {{ strtoupper($dato['padquisicion'] )}}
        </td> 
      </tr>  
      @endforeach
      @if(count($bmueblesnr) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif
      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          <br>
          INVERSIONES
        </td> 
      </tr>
      @foreach($inversiones as $dato)
      <tr>        
        <td style="text-align: left;" colspan="2">
          {{ strtoupper($dato['tipoOp'] )}}: {{strtoupper($dato['tipoInv'])}}, {{ strtoupper($dato['monto_original'] )}}
        </td> 
      </tr>  
      @endforeach
      @if(count($inversiones) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif
      <tr>
        <td style="text-align: left; font-weight: bold; border-bottom: 1px solid black;" colspan="2">
          <br>
          PASIVOS
        </td> 
      </tr>
    @foreach($pasivos as $dato)
      <tr>        
        <td colspan="2" style="text-align: left;">
          {{ strtoupper($dato['tipoOperacion'] )}}: {{strtoupper($dato['tipoAdeudo'])}}, {{ strtoupper($dato['saldo_pendiente'] )}}
        </td> 
      </tr>  
      @endforeach
      @if(count($pasivos) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif
      
      <tr>
        <td style="text-align: left; font-weight: bold; padding-left: 20px !important; border-bottom: 1px solid black;" colspan="2">
          <br><br>
          2. DECLARACIÓN DE POSIBLE CONFLICTO DE INTERESES.
        </td> 
      </tr>
     @foreach($intereses as $dato)
      <tr>        
        <td style="text-align: left;">{{ strtoupper($dato['tipo'] )}}: {{strtoupper($dato['tipoOperacion'])}}, {{ strtoupper($dato['titular'] )}}
        </td colspan="2"> 
      </tr>  
      @endforeach
      @if(count($intereses) == 0)
        <tr>        
        <td style="text-align: left;">(NINGUNO)</td> 
      </tr>        
      @endif



       <tr>
        <td style="text-align: left; font-weight: bold; padding-left: 20px !important; border-bottom: 1px solid black;" colspan="2">
          <br><br>
           3. DECLARACIÓN FISCAL.
        </td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ strtoupper($resFis )}}</td> 
      </tr>       
  </tbody>
</table>


<style type="text/css">
  tbody{
    font-size: 13px !important;
  }
  table {
  border: 1px solid black;
}

tr.border_bottom td {
  border-bottom:1pt solid black;
  text-align: left;
}

</style>


