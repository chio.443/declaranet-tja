<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<img src="encabezado.jpg" height="100" width="325">
<p style="text-align:justify;font-size:12px;">
    C. DIRECTORA DE ENLACE E INFORMACIÓN DE LA SECRETARÍA DE LA TRANSPARENCIA Y RENDICIÓN DE CUENTAS,BAJO PROTESTA DE DECIR VERDAD, PRESENTO A USTED MI DECLARACIÓN DE SITUACIÓN PATRIMONIAL Y DE INTERESES, CONFORME A LO DISPUESTO EN LA LEY DE RESPONSABILIDADES ADMINISTRATIVAS PARA EL ESTADO DE GUANAJUATO, LEY GENERAL DE SISTEMA NACIONAL ANTICORRUPCIÓN Y LA NORMATIVIDAD APLICABLES.
</p>

<p style="text-align:justify;font-size:12px; font-weight:bold;">
    LOS DATOS DE TERCEROS SIEMPRE Y CUANDO SEAN PERSONAS FÍSICAS, Y LOS DATOS RESALTADOS NO SERÁN PUBLICOS.
</p>


<table class="table">
    <thead style="text-align: left;font-size: 15px;">
        <tr>
            <td colspan="2">
                <h3>Declaración patrimonial y de intereses</h3>
            </td>
            <td align="center">
                @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
                INICIAL
                @else
                @if( $datosDeclaracion['tipo_declaracion_id'] == 2)
                ANUAL
                @else
                CONCLUSIÓN
                @endif
                @endif
                <br>
                <strong style="font-size: 10px;">Fecha de presentación:</strong>
                <strong style="font-size: 10px;">{{ $fechaPresenta }}</strong>

            </td>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td class="tsecciones" style="font-size:50px;text-align: left;color:navy;font-weight: 900; " colspan="3"><br>DATOS PERSONALES</td>
        </tr>
        <tr style="height: 0px">
            <td style="height: 0px" colspan="3"></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre:</td>
            <td style="text-align: left; font-weight: bold;">País de Nacimiento: </td>
            <td style="text-align: left; font-weight: bold;">Entidad de Nacimiento: </td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['nombres'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['paisnac'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['entnac'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; "> CURP: <br></td>
            <td style="text-align: left; font-weight: bold;"> RFC: <br></td>
            <td style="text-align: left; font-weight: bold;"> Estado Civil: <br></td>
        </tr>

        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['curp'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['rfc'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['edoCivil'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Correo Personal: <br></td>
            <td style="text-align: left; font-weight: bold;">Correo Laboral: <br></td>
            <td style="text-align: left; font-weight: bold;">Teléfono Particular: <br></td>
        </tr>

        <tr>
            <td style="text-align: left; ">{{ $dpersonales['ce_personal'] }}<br></td>
            <td style="text-align: left; ">{{ $dpersonales['ce_laboral'] }}<br></td>
            <td style="text-align: left; ">{{ $dpersonales['tel_particular'] }}<br></td>
        </tr>
        <tr>
            <td class="tsecciones" style="font-size:50px;text-align: left;color:navy;font-weight: 900; " colspan="3"><br>Domicilio</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Municipio: <br></td>
            <td style="text-align: left; font-weight: bold;">Entidad: <br></td>
            <td style="text-align: left; font-weight: bold;">Localidad: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['municipio'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['entidad'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['localidad'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Colonia: <br></td>
            <td style="text-align: left; font-weight: bold;">Tipo Vialidad: <br></td>
            <td style="text-align: left; font-weight: bold;">Calle: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['colonia'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['tvialidad'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['calle'] }}<br></td>
        </tr>

        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">No. Ext: <br></td>
            <td style="text-align: left; font-weight: bold;">No. Int: <br></td>
            <td style="text-align: left; font-weight: bold;">CP: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['numext'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['numint'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dpersonales['cp'] }}<br></td>
        </tr>
        <tr>
            <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3"><br>DATOS CURRICULARES</td>
        </tr>

        @foreach($dcurrc as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Grado de Escolaridad:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['grado'] }}</td>
            <td style="text-align: left;text-transform: uppercase; " colspan="2">{{ $dato['institucion'] }}</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold; ">Carrera</td>

            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['carrera'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Entidad</td>
            <td style="text-align: left; font-weight: bold; ">Periodos</td>
            <td style="text-align: left; font-weight: bold; ">Periodos Cursados </td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['entidad'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['periodos'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['periodosc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Estatus</td>
            <td style="text-align: left; font-weight: bold; ">Documento Obtenido</td>
            <td style="text-align: left; font-weight: bold; ">Cédula</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['estatus'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['doctoOb'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['cedula'] }}</td>
        </tr>
        @endforeach
        <tr>
            <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3"><br>EXPERIENCIA LABORAL</td>
        </tr>

        @foreach($experiencia as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Ámbito:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;">{{ $dato['ambito'] }}</td>
            <td style="text-align: left;text-transform: uppercase;" colspan="2">{{ $dato['nombreInst'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;  font-weight: bold;">Unidad Administrativa</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Sector</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['area'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['sector'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;  font-weight: bold;">Jerarquí­a</td>
            <td style="text-align: left;  font-weight: bold;">Cargo</td>
            <td style="text-align: left;  font-weight: bold;">Fecha Ingreso</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['jerarquia'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['cargo'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['fingreso'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Fecha Salida</td>

            <td style="text-align: left;text-transform: uppercase; " colspan="2">{{ $dato['fsalida'] }}</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3"><br>Domicilio</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; ">Municipio: <br></td>
            <td style="text-align: left;font-weight: bold; ">Entidad: <br></td>
            <td style="text-align: left;font-weight: bold; ">Localidad: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['municipio'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['entidad'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['localidad'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; ">Colonia:<br></td>
            <td style="text-align: left;font-weight: bold; ">Tipo Vialidad: <br></td>
            <td style="text-align: left; font-weight: bold;">Calle: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['colonia'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tvialidad'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['calle'] }}<br></td>
        </tr>

        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">No. Ext:<br></td>
            <td style="text-align: left; font-weight: bold;">No. Int:<br></td>
            <td style="text-align: left; font-weight: bold;">CP: <br></td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['numext'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['numint'] }}<br></td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['cp'] }}<br></td>
        </tr>
        @endforeach



        <tr>
            <td class="tsecciones" style="text-align: left;color:navy;font-weight: 900; " colspan="3">
                <br>CÓNYUGE, CONCUBINA, CONCUBINARIO Y/O DEPENDIENTES ECONÓMICOS</td>
        </tr>

        @foreach($dependientes as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Nombre</td>
            <td style="text-align: left; font-weight: bold; ">Relación</td>
            <td style="text-align: left; font-weight: bold; ">CURP</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['tipo_r'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['curp'] }}</td>
        </tr>
        @endforeach

        <tr>
            <br>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</td>
            @else
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DATOS DEL ENCARGO ACTUAL</td>
            @endif


        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Dependencia:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; " colspan="3">{{ $dencargo['dependencia'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Área</td>
            <td style="text-align: left; font-weight: bold;">Cargo</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align: left; font-weight: bold;">Fecha de conclusión del empleo, cargo o comisión. </td>
            @else
            <td style="text-align: left; font-weight: bold;">Toma Posesión</td>
            @endif
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['area'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['cargo'] }}</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['baja'] }}</td>
            @else
            <td style="text-align: left;text-transform: uppercase; ">{{ $dencargo['ftomaposesion'] }}</td>
            @endif

        </tr>
        <tr>
            <br>
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>INTERESES</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Empresas, Sociedades y Asociaciones</td>
        </tr>
        @if(count($empresas)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($empresas as $dato)
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre de la empresa, sociedad o asociación:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Porcentaje de participación: </td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;">{{ $dato['grado'] }}</td>
            <td style="text-align: left;text-transform: uppercase;" colspan="2">{{ $dato['porcentaje_participacion'] }}% </td>
        </tr>
        <tr class="titulostr">
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left; font-weight: bold;">Tipo de participación:<br></td>
            <td style="text-align: left; font-weight: bold;">¿Recibe Remuneración?:<br></td>
            <td style="text-align: left; font-weight: bold;">Monto Mensual Neto: <br></td>
            @else
            <td style="text-align: left; font-weight: bold;">Tipo de participación</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">¿Recibe Remuneración?</td>
            @endif
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['participacion'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">Sí­</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['participacion'] }}</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">No</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Lugar donde se úbica:</td>
        </tr>
        @if($dato['domicilio_pais_id'] == 150)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">En México</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['domicilio_entidad_federativa_id'] }}</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">En el extranjero</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['domicilio_pais_id'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['naturaleza'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach


        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Participación en Toma de Decisiones</td>
        </tr>
        @if(count($tomaDecisiones)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($tomaDecisiones as $dato)
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Tipo de institución:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['tipo_institucion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Nombre de la institución: </td>
            <td style="text-align: left; font-weight: bold; ">RFC</td>
            <td style="text-align: left; font-weight: bold; ">Puesto / Rol</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['rfc'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['puesto'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left; font-weight: bold;">Fecha de inicio de participación:<br></td>
            <td style="text-align: left; font-weight: bold;">¿Recibe Remuneración?:<br></td>
            <td style="text-align: left; font-weight: bold;">Monto Mensual Neto: <br></td>
            @else
            <td style="text-align: left; font-weight: bold;">Fecha de inicio de participación:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">¿Recibe Remuneración?:</td>
            @endif
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['fecha'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">Sí­</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['fecha'] }}</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">No</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Lugar donde se úbica:</td>
        </tr>
        @if($dato['domicilio_pais_id'] == 150)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">En México</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['domicilio_entidad_federativa_id'] }}</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">En el extranjero</td>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['domicilio_pais_id'] }}</td>
        </tr>
        @endif

        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>

        @endforeach

        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Beneficios Públicos</td>
        </tr>
        @if(count($beneficiosPubl)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($beneficiosPubl as $dato)
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Beneficiario de programa:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['beneficiario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Nombre del programa:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['programa'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Institución que otorga el apoyo: </td>
            <td style="text-align: left; font-weight: bold; ">Nivel u orden de gobierno</td>
            <td style="text-align: left; font-weight: bold; ">Tipo de Apoyo</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['institucion'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nivel_gobierno'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['tipo_apoyo_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;  font-weight: bold;">Forma de recepción del apoyo</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Monto aproximado del apoyo mensual</td>
        </tr>
        <tr>
            @if($dato['forma'] == 1)
            <td style="text-align: left;text-transform: uppercase;  ">Monetario</td>
            @else
            <td style="text-align: left;text-transform: uppercase;  ">Especie</td>
            @endif
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['monto'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Representación Activa</td>
        </tr>
        @if(count($reprActiva)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($reprActiva as $dato)
        @if($dato['representante'] == 1)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['representante'] == 2)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Dependiente Económico</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Tipo de Representación:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Fecha de inicio de la representación:</td>
        </tr>
        <tr>
            @if($dato['tipo_representacion_id'] == 1)
            <td style="text-align: left;text-transform: uppercase;">Representante</td>
            @else
            <td style="text-align: left;text-transform: uppercase;">Representado</td>
            @endif
            <td style="text-align: left;text-transform: uppercase;" colspan="2">{{ $dato['fecha'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Representante / Representado:</td>
        </tr>
        <tr>
            @if($dato['representado'] == 1)
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="3">Persona fí­sica</td>
            @else
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre de la parte representada:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left; font-weight: bold;">¿Recibe remuneración por su representación?:<br></td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Monto Mensual Neto: <br></td>
            @else
            <td style="text-align: left; font-weight: bold;" colspan="3">¿Recibe remuneración por su representación?</td>
            @endif
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align: left;text-transform: uppercase;  ">Sí­</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">No</td>
            @endif
        </tr>
        <tr class="titulostr">
            @if($dato['pais_id'] == 1)
            <td style="text-align: left; font-weight: bold;">Paí­s donde se localiza:<br></td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Entidad Federativa: <br></td>
            @else
            <td style="text-align: left; font-weight: bold;" colspan="3">Paí­s donde se localiza:</td>
            @endif
        </tr>
        <tr>
            @if($dato['pais_id'] == 150)
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['pais'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['entidad_federativa_id'] }}</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['pais'] }}</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Clientes Principales</td>
        </tr>
        @if(count($clientesPrinc)== 0)
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">¿Realiza alguna actividad lucrativa independiente al empleo, cargo o comisión?</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($clientesPrinc as $dato)
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">¿Realiza alguna actividad lucrativa independiente al empleo, cargo o comisión?</td>
        </tr>
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">Sí</td>
        </tr>
        @if($dato['propietario'] == 1)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['propietario'] == 2)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Dependiente Económico</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre de la empresa o servicio que proporciona:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Cliente principal</td>
        </tr>
        <tr>
            @if($dato['tipo_persona'] == 1)
            <td style="text-align: left; font-weight: bold;" colspan="3">Persona fí­sica</td>
            @else
            <td style="text-align: left; font-weight: bold;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Monto aproximado del beneficio o ganancia mensual que obtiene del cliente principal:</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['monto'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['pais_id'] == 1)
            <td style="text-align: left; font-weight: bold;">Paí­s donde se localiza:<br></td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Entidad Federativa: <br></td>
            @else
            <td style="text-align: left; font-weight: bold;" colspan="3">Paí­s donde se localiza:</td>
            @endif
        </tr>
        <tr>
            @if($dato['pais_id'] == 150)
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['pais'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['entidad_federativa_id'] }}</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['pais'] }}</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Beneficios Privados</td>
        </tr>
        @if(count($beneficiosPriv)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($beneficiosPriv as $dato)
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Otorgante</td>
        </tr>
        <tr>
            @if($dato['otorgante'] == 1)
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona fí­sica</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Nombre o razón social del otorgante</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['nombre'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">RFC</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Forma de recepción del beneficio</td>
        </tr>
        <tr>
            @if($dato['otorgante'] == 1)
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Monetario</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Especie</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Monto mensual aproximado del beneficio:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Tipo de moneda:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['monto'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['tipo_moneda'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Sector productivo al que pertenece</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Fideicomisos</td>
        </tr>
        @if(count($fideicomisos)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($fideicomisos as $dato)
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Participación en fideicomisos</td>
        </tr>
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Tipo de fideicomiso:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">Tipo de participación:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['tipo_fideicomiso_id'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['tipo_participacion_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">RFC del fideicomisos</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['rfc_fideicomiso'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Fideicomitente</td>
        </tr>
        <tr>
            @if($dato['fideicomitente'] == 1)
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona fí­sica</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre o razón social del fideicomitente:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre o razón social del fiduciario:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre_fiduciario'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['rfc_fiduciario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Fideicomisario</td>
        </tr>
        <tr>
            @if($dato['fideicomisario'] == 1)
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona fí­sica</td>
            @else
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;">Nombre o razón social del fideicomisario:</td>
            <td style="text-align: left; font-weight: bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase;  ">{{ $dato['nombre_fideicomisario'] }}</td>
            <td style="text-align: left;text-transform: uppercase;  " colspan="2">{{ $dato['rfc_fideicomisario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Sector productivo al que pertenece</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left;font-weight: bold; " colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach



        <tr>
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>INGRESOS</td>
        </tr>
        @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos (situación actual)</td>
        </tr>
        @foreach($spublicos as $dato)
        <tr>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
        </tr>

        @foreach($otross as $dato)
        <tr>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach

        <tr>

            <td style="text-align: left; font-weight: bold; padding-left: 15px"><br>TOTAL</br></td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $totalingresos['total'] }}</td>

        </tr>

        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos (Año inmediato anterior)</td>
        </tr>

        @foreach($spublicosant as $dato)
        <tr>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
        </tr>

        @foreach($otrossant as $dato)
        <tr>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr>

            <td style="text-align: left; font-weight: bold; padding-left: 15px"><br>TOTAL</br></td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $totalingresosant['total'] }}</td>

        </tr>


        @else
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; " colspan="3">Sueldos y salarios públicos</td>
        </tr>

        @foreach($spublicosant as $dato)
        <tr>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align: left;  padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;" colspan="3"><br>Otros sueldos y salarios</td>
        </tr>

        @foreach($otrossant as $dato)
        <tr>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $dato['nombre'] }}</td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr>

            <td style="text-align: left; font-weight: bold; padding-left: 15px"><br>TOTAL</br></td>

            <td style="text-align: left; padding-left: 15px; text-transform: uppercase;">{{ $totalingresosant['total'] }}</td>

        </tr>
        @endif



        <tr>
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>ACTIVOS</td>
        </tr>
        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3">BIENES INMUEBLES</td>
        </tr>

        @foreach($binmuebles as $dato)
        <tr>
            <td style="text-align: left; font-weight: bold;text-transform: uppercase; " colspan="3">{{ $dato['tipoOperacion'] }}</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['naturaleza'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['domicilio'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>BIENES MUEBLES</td>
        </tr>

        @foreach($bmuebles as $dato)
        <tr>
            <td style="text-align: left;  padding-left: 15px; font-weight: bold;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>

            <td style="text-align: left; padding-left: 15px;text-transform: uppercase; " colspan="2">{{ $dato['tipoBien'] }}</td>

            <!-- <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td> -->
        </tr>
        <tr>
            <td style="text-align: left;  padding-left: 15px; font-weight: bold;text-transform: uppercase; ">{{ $dato['marca'] }}</td>

            <td style="text-align: left; padding-left: 15px;text-transform: uppercase; ">{{ $dato['modelo'] }}</td>

            <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>BIENES MUEBLES NO REGISTRABLES</td>
        </tr>

        @foreach($bmueblesnr as $dato)
        <tr>
            <td style="text-align: left;  padding-left: 15px; font-weight: bold;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>

            <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['tipoBien'] }}</td>

            <td style="text-align: left;  padding-left: 15px;text-transform: uppercase; ">{{ $dato['padquisicion'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3">INVERSIONES</td>
        </tr>

        @foreach($inversiones as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Tipo de Operacion</td>
            <td style="text-align: left; font-weight: bold; ">Tipo de Inversión</td>
            <td style="text-align: left; font-weight: bold; ">Titular</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoOp'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoInv'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['titular'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold;" colspan="2">Institución</td>
            <td style="text-align: left; font-weight: bold;">Cuenta</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; " colspan="2">{{ $dato['nombre_institucion'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['numero_cuenta'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Monto Original</td>
            <td style="text-align: left; font-weight: bold; ">Pais</td>
            <td style="text-align: left; font-weight: bold; ">Entidad</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['monto_original'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['pais'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['entidad'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>PASIVOS</td>
        </tr>
        @if(count($pasivos)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;">NO</td>
        </tr>
        @endif

        @foreach($pasivos as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Tipo de Operación:</td>
            <td style="text-align: left; font-weight: bold; ">Tipo Adeudo:</td>
            <td style="text-align: left; font-weight: bold; ">Identificador:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoOperacion'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoAdeudo'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['identificador'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Fecha Adeudo:</td>
            <td style="text-align: left; font-weight: bold; ">Monto Original:</td>
            <td style="text-align: left; font-weight: bold; ">Saldo Pendiente:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['fadeudo'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['original'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['pendiente'] }}</td>
        </tr>
        @endforeach


        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900; " colspan="3"><br>PRÉSTAMO O COMODATO POR TERCEROS</td>
        </tr>
        @if(count($prestamo)== 0)
        <tr>
            <td style="text-align: left; font-weight: bold;" colspan="3">NO</td>
        </tr>
        @endif

        @foreach($prestamo as $dato)
        <tr class="titulostr">
            <td style="text-align: left; font-weight: bold; ">Tipo:</td>
            <td style="text-align: left; font-weight: bold; ">Tipo de Bien:</td>
            <td style="text-align: left; font-weight: bold; ">Detalle:</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipo'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['tipoBien'] }}</td>
            <td style="text-align: left;text-transform: uppercase; ">{{ $dato['desc'] }}</td>
        </tr>
        @endforeach


        <tr class="titulostr">
            <td class="tsecciones" style="text-align: left;color:navy; font-weight: 900;" colspan="3"><br>DECLARACIÓN FISCAL</td>
        </tr>
        <tr>
            <td style="text-align: left;text-transform: uppercase; " colspan="3">{{ $resFis }}</td>
        </tr>
    </tbody>
</table>


<style type="text/css">
    tbody {
        font-size: 12px !important;
    }

    table {
        border: 1px solid black;
    }

    .tsecciones {
        font-size: 15px !important;
    }

    mark {
        background-color: gray;
        color: black;
        /*font-size: 15px !important;*/

    }

    /* .tsecciones  {
    background-color:#eeeeee !important;
} */
    .titulostr {
        background-color: #eeeeee !important;
    }
</style>
