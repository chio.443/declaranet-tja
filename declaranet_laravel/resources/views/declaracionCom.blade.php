<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<img src="encabezado.jpg" height="100" width="325">
<p style="text-align: justify;font-size: 12px;">
  TITULAR DEL ÓRGANO INTERNO DE CONTROL 
</p>

<p style="text-align: justify;font-size: 12px;">
  TRIBUNAL DE JUSTICIA ADMINISTRATIVA DEL ESTADO DE GUANAJUATO 
</p>
<p style="text-align: justify;font-size: 12px;">
BAJO PROTESTA DE DECIR VERDAD, PRESENTO A USTED MI DECLARACIÓN DE SITUACIÓN PATRIMONIAL Y DE INTERESES, CONFORME A LO DISPUESTO EN LA LEGISLACIÓN Y DEMÁS NORMATIVIDAD APLICABLE A LA MATERIA.
</p>

  <p style="text-align: justify;font-size: 12px; font-weight: bold;">
LOS DATOS DE TERCEROS SIEMPRE Y CUANDO SEAN PERSONAS FÍSICAS, Y LOS DATOS RESALTADOS NO SERÁN PUBLICOS.
</p>



<table class="table">
    <thead style="text-align:left;font-size:15px;">
        <tr>
            <td colspan="2">
                <h3>Declaración patrimonial y de intereses</h3>
            </td>
            <td align="center">
                @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
                INICIAL
                @else
                @if( $datosDeclaracion['tipo_declaracion_id'] == 2)
                MODIFICACIÓN
                @else
                CONCLUSIÓN
                @endif
                @endif
                <br>
                <strong style="font-size:10px;">Fecha de presentación:</strong>
                <strong style="font-size:10px;">{{ $fechaPresenta }}</strong>

            </td>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td class="tsecciones" style="font-size:50px;text-align:left;color:navy;font-weight:900;" colspan="3"><br>DATOS GENERALES</td>
        </tr>
        <tr style="height:0px">
            <td style="height:0px" colspan="3"></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre:</td>
            <td style="text-align:left; font-weight:bold;">País de Nacimiento:</td>
            <td style="text-align:left; font-weight:bold;">Nacionalidad:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['nombres'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['paisnac'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['nacionalidad'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;"> CURP:<br></td>
            <td style="text-align:left; font-weight:bold;"> RFC:<br></td>
            <td style="text-align:left; font-weight:bold;"> Estado Civil:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['curp'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['rfc'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['edoCivil'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;"> Régimen matrimonial:<br></td>
            <td style="text-align:left; font-weight:bold;">Correo Personal:<br></td>
            <td style="text-align:left; font-weight:bold;">Correo institucional:<br></td>
        </tr>

        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['regimen'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['ce_personal'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['ce_laboral'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="1">Número de celular personal:<br></td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Número telefónico de casa:<br></td>
        </tr>

        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="1">{{ $dpersonales['tel_celular'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dpersonales['tel_particular'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Aclaraciones y observaciones:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dpersonales['aclaraciones'] }}<br></td>
        </tr>
        <tr>
            <td class="tsecciones" style="font-size:50px;text-align:left;color:navy;font-weight:900;" colspan="3"><br>Domicilio del declarante</td>
        </tr>
        <tr class="titulostr">
            @if( $dpersonales['pais'] == 'México')
            <td style="text-align:left; font-weight:bold;" colspan="3">En México <br></td>
            @else
            <td style="text-align:left; font-weight:bold;" colspan="3">En el extranjero<br></td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">País:<br></td>
            <td style="text-align:left; font-weight:bold;">Municipio:<br></td>
            <td style="text-align:left; font-weight:bold;">Entidad:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['pais'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['municipio'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['entidad'] }}<br></td>

        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Colonia:<br></td>
            <td style="text-align:left; font-weight:bold;">Calle:<br></td>
            <td style="text-align:left; font-weight:bold;">No. Ext:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['colonia'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['calle'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['numext'] }}<br></td>
        </tr>

        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">No. Int:<br></td>
            <td style="text-align:left; font-weight:bold;">CP:<br></td>
            <td style="text-align:left; font-weight:bold;">Aclaraciones y observaciones del domicilio:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['numint'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['cp'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dpersonales['domicilio_observaciones'] }}<br></td>
        </tr>

        <tr>
            <td class="tsecciones" style="text-align:left;color:navy;font-weight:900;" colspan="3"><br>DATOS CURRICULARES DEL DECLARANTE</td>
        </tr>

        @foreach($dcurrc as $dato)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Grado de Escolaridad:</td>
            <td style="text-align:left; font-weight:bold;">Institución</td>
            <td style="text-align:left; font-weight:bold;">Lugar donde se ubica la institución educativa</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['grado'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['institucion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Carrera</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['carrera'] }}</td>
        </tr>
        <!-- <tr class="titulostr">
      <td style="text-align:left; font-weight:bold;">Periodos</td>
      <td style="text-align:left; font-weight:bold;">Periodos Cursados </td>
    </tr>
    <tr>
      <td style="text-align:left;text-transform:uppercase;">{{ $dato['periodos'] }}</td>
      <td style="text-align:left;text-transform:uppercase;">{{ $dato['periodosc'] }}</td>
    </tr> -->
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Estatus</td>
            <td style="text-align:left; font-weight:bold;">Documento Obtenido</td>
            <td style="text-align:left; font-weight:bold;">Fecha de obtención del documento </td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['estatus'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['doctoOb'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha'] }}</td>
        </tr>
        <!-- <tr class="titulostr">
      <td style="text-align:left; font-weight:bold;" colspan="3">Cédula</td>
    </tr>
    <tr>
      <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['cedula'] }}</td>
    </tr> -->
        @endforeach
        <tr>
            <td class="tsecciones" style="text-align:left; color:navy; font-weight:900;" colspan="3"><br>EXPERIENCIA LABORAL</td>
        </tr>

        @foreach($experiencia as $dato)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Ámbito / sector en el que laboraste:</td>
            <td style="text-align:left; font-weight:bold;">Nivel / Orden de Gobierno:</td>
            <td style="text-align:left; font-weight:bold;">Ambito público:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ambito'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nivel'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ambito_publico'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre del ente público, empresa, institución</td>
            <td style="text-align:left; font-weight:bold;">RFC</td>
            <td style="text-align:left; font-weight:bold;">Área de adscripción</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombreInst'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['area'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Sector al que pertenece</td>
            <!-- <td style="text-align:left; font-weight:bold;">Jerarquía</td> -->
            <td style="text-align:left; font-weight:bold;">Empleo, Cargo o comisión / Puesto</td>
            <td style="text-align:left; font-weight:bold;">Especifique funcion principal</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['sector'] }}</td>
            <!-- <td style="text-align:left;text-transform:uppercase;">{{ $dato['jerarquia'] }}</td> -->
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['cargo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['funcionesp'] }}</td>
        </tr>
        <tr class="titulostr">
            <!-- <td style="text-align:left; font-weight:bold;">Otras funciones</td> -->
            <td style="text-align:left; font-weight:bold;">Fecha Ingreso</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Fecha Salida</td>
        </tr>
        <tr>
            <!-- <td style="text-align:left;text-transform:uppercase;">{{ $dato['otras_funciones'] }}</td> -->
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fingreso'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['fsalida'] }}</td>
        </tr>
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3"><br>Domicilio</td>
        </tr>
        <tr class="titulostr">
            @if( $dato['pais'] == 'México')
            <td style="text-align:left; font-weight:bold;" colspan="3">En México <br></td>
            @else
            <td style="text-align:left; font-weight:bold;" colspan="3">En el extranjero<br></td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">País:<br></td>
            <td style="text-align:left;font-weight:bold;">Municipio:<br></td>
            <td style="text-align:left;font-weight:bold;">Entidad:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['municipio'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['entidad'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Colonia:<br></td>
            <td style="text-align:left;font-weight:bold;">Tipo Vialidad:<br></td>
            <td style="text-align:left; font-weight:bold;">Calle:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['colonia'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tvialidad'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['calle'] }}<br></td>
        </tr>

        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">No. Ext:<br></td>
            <td style="text-align:left; font-weight:bold;">No. Int:<br></td>
            <td style="text-align:left; font-weight:bold;">CP:<br></td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numext'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numint'] }}<br></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['cp'] }}<br></td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Aclaraciones y observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['aclaraciones'] }}</td>
        </tr>
        @endforeach

        <tr>
            <td class="tsecciones" style="text-align:left;color:navy;font-weight:900;" colspan="3">
                <br>DATOS DE LA PAREJA Y/O DEPENDIENTES ECONÓMICOS
                <!-- <br>CÓNYUGE, CONCUBINA, CONCUBINARIO Y/O DEPENDIENTES ECONÓMICOS -->
            </td>
        </tr>

        @foreach($dependientes as $dato)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre</td>
            <td style="text-align:left; font-weight:bold;">Parentesco o Relación con el Declarante</td>
            <td style="text-align:left; font-weight:bold;">CURP</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_r'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['curp'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">RFC</td>
            <td style="text-align:left; font-weight:bold;">Fecha de Nacimiento</td>
            <td style="text-align:left; font-weight:bold;">¿Es ciudadano extranjero?</td>
            <!-- <td style="text-align:left; font-weight:bold;">País de Nacimiento</td> -->
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha_nac'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ciudadano'] }}</td>
            <!-- <td style="text-align:left;text-transform:uppercase;">{{ $dato['paisnac'] }}</td> -->
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">País de Nacimiento</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Lugar donde reside:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['paisnac'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['lugar'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Domicilio</td>
        </tr>
        <!-- <tr class="titulostr">
      <td style="text-align:left; font-weight:bold;" colspan="3">{{ $dato['lugar'] }}<br></td>
    </tr> -->
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['domicilio'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Actividad laboral</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['ambito'] }}</td>
        </tr>
        @if( $dato['ambito_id'] == 1)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nivel/Orden de Gobierno</td>
            <td style="text-align:left; font-weight:bold;">Ambito público</td>
            <td style="text-align:left; font-weight:bold;">Nombre del ente público</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nivel_gobierno'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['poder_ente'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_dependencia'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Área de adscripción</td>
            <td style="text-align:left; font-weight:bold;">Empleo, cargo o comisión</td>
            <td style="text-align:left; font-weight:bold;">Especifique función principal</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['area_adscripcion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['empleo_cargo_comision'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['funcion_principal'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Salario mensual neto</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Fecha de ingreso al empleo</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['salario_mensual'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['fecha_inicio'] }}</td>
        </tr>
        @endif
        @if( $dato['ambito_id'] != 4)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre de la empresa, sociedad o asociación</td>
            <td style="text-align:left; font-weight:bold;">RFC</td>
            <td style="text-align:left; font-weight:bold;">Empleo, cargo o comisión</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_empresa'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfcempresa'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['empleo_cargo'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Salario mensual neto</td>
            <td style="text-align:left; font-weight:bold;">Fecha de ingreso al empleo</td>
            <td style="text-align:left; font-weight:bold;">¿Es proveedor o contratista de gobierno?</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['salario_mensual'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha_inicio'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['proveedor_contratista_gobierno'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Sectora al que pertenece:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Aclaraciones / observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['sector'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['observaciones'] }}</td>
        </tr>
        @endif

        @endforeach

        <tr>
            <br>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</td>
            @else
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>DATOS DEL EMPLEO, CARGO O COMISIÓN ACTUAL</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Nombre del Ente público:</td>
            <td style="text-align:left;font-weight:bold;">Nivel / Orden de Gobierno:</td>
            <td style="text-align:left;font-weight:bold;">Ámbito público:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['dependencia'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['nivel_gobierno'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['poder_ente'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Área de adscripción</td>
            <td style="text-align:left; font-weight:bold;">Cargo</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align:left; font-weight:bold;">Fecha de conclusión del empleo, cargo o comisión. </td>
            @else
            <td style="text-align:left; font-weight:bold;">Fecha de toma de Posesión del empleo, cargo o comisión</td>
            @endif
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['area'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['cargo'] }}</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['baja'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['ftomaposesion'] }}</td>
            @endif

        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">¿ Está contratado por honorarios?:</td>
            <td style="text-align:left;font-weight:bold;">Nivel del empleo, cargo o comisión:</td>
            <td style="text-align:left;font-weight:bold;">Funciones principales:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['honorarios'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['nivel_encargo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['funcionesp'] }}</td>
        </tr>
        <tr class="titulostr">
            <!-- <td style="text-align:left;font-weight:bold;">Otras funciones:</td> -->
            <td style="text-align:left;font-weight:bold;">Teléfono de oficina y extensión:</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Domicilio /Ubicación del Cargo</td>
        </tr>
        <tr>
            <!-- <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['otras_funciones'] }}</td> -->
            <td style="text-align:left;text-transform:uppercase;">{{ $dencargo['telefono'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dencargo['direccion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones y observaciones del domicilio:</td>

        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dencargo['domicilio_obs'] }}</td>
        </tr>


        <tr>
            <br>
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>¿CUENTA CON OTRO CARGO, EMPLEO O COMISIÓN EN EL SERVICIO PÚBLICO DISTINTO AL DECLARADO?</td>
        </tr>
        @if( count($oencargo) > 0)
        <tr>
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>DATOS DEL EMPLEO, CARGO O COMISIÓN QUE CONCLUYE</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Nombre del Ente público:</td>
            <td style="text-align:left;font-weight:bold;">Nivel / Orden de Gobierno:</td>
            <td style="text-align:left;font-weight:bold;">Ámbito público:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['dependencia'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['nivel_gobierno'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['poder_ente'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Área de adscripción</td>
            <td style="text-align:left; font-weight:bold;">Cargo</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align:left; font-weight:bold;">Fecha de conclusión del empleo, cargo o comisión. </td>
            @else
            <td style="text-align:left; font-weight:bold;">Fecha de toma de Posesión del empleo, cargo o comisión</td>
            @endif
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['area'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['cargo'] }}</td>
            @if( $datosDeclaracion['tipo_declaracion_id'] == 3)
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['baja'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['ftomaposesion'] }}</td>
            @endif

        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">¿ Está contratado por honorarios?:</td>
            <td style="text-align:left;font-weight:bold;">Nivel del empleo, cargo o comisión:</td>
            <td style="text-align:left;font-weight:bold;">Funciones principales:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['honorarios'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['nivel_encargo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['funcionesp'] }}</td>
        </tr>
        <tr class="titulostr">
            <!-- <td style="text-align:left;font-weight:bold;">Otras funciones:</td> -->
            <td style="text-align:left;font-weight:bold;">Teléfono Laboral:</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Domicilio /Ubicación del Cargo</td>
        </tr>
        <tr>
            <!-- <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['otras_funciones'] }}</td> -->
            <td style="text-align:left;text-transform:uppercase;">{{ $oencargo['telefono'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $oencargo['direccion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones y observaciones del domicilio:</td>

        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $oencargo['domicilio_obs'] }}</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">NO</td>
        </tr>
        @endif

        <tr>
            <br>
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>DECLARACIÓN DE INTERESES</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Participación en Empresas, Sociedades o Asociaciones</td>
        </tr>
        @if(count($empresas)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($empresas as $dato)
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre de la empresa, sociedad o asociación:</td>
            <td style="text-align:left; font-weight:bold;">RFC:</td>
            <td style="text-align:left; font-weight:bold;">Porcentaje de participación:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['grado'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['porcentaje_participacion'] }}% </td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo de participación:<br></td>
            <td style="text-align:left; font-weight:bold;">¿Recibe remuneración por su participación?:<br></td>
            <td style="text-align:left; font-weight:bold;">Monto Mensual Neto:<br></td>
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['participacion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">Sí</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['participacion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">No</td>
            <td style="text-align:left;text-transform:uppercase;"></td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Lugar donde se úbica:</td>
        </tr>
        @if($dato['domicilio_pais_id'] == 150)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">En México</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['domicilio_entidad_federativa_id'] }}</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;">En el extranjero</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['domicilio_pais_id'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['naturaleza'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach


        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Participa en la toma de Decisiones de alguna de estas instituciones</td>
        </tr>
        @if(count($tomaDecisiones)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($tomaDecisiones as $dato)
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="2">Tipo de institución:</td>
            <td style="text-align:left;font-weight:bold;">Especifique:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['tipo_institucion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre de la institución:</td>
            <td style="text-align:left; font-weight:bold;">RFC</td>
            <td style="text-align:left; font-weight:bold;">Puesto / Rol</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['puesto'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['remuneracion'] == 1)
            <td style="text-align:left; font-weight:bold;">Fecha de inicio de participación:<br></td>
            <td style="text-align:left; font-weight:bold;">¿Recibe Remuneración?:<br></td>
            <td style="text-align:left; font-weight:bold;">Monto Mensual Neto:<br></td>
            @else
            <td style="text-align:left; font-weight:bold;">Fecha de inicio de participación:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">¿Recibe Remuneración?:</td>
            @endif
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">Sí</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">No</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Lugar donde se úbica:</td>
        </tr>
        @if($dato['domicilio_pais_id'] == 150)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">En México</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['domicilio_entidad_federativa_id'] }}</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;">En el extranjero</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['domicilio_pais_id'] }}</td>
        </tr>
        @endif

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>

        @endforeach

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">APOYOS O BENEFICIOS PÚBLICOS</td>
        </tr>
        @if(count($beneficiosPubl)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($beneficiosPubl as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Beneficiario de algún programa público:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['beneficiario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Nombre del programa:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['programa'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Institución que otorga el apoyo:</td>
            <td style="text-align:left; font-weight:bold;">Nivel u orden de gobierno</td>
            <td style="text-align:left; font-weight:bold;">Tipo de Apoyo</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['institucion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nivel_gobierno'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_apoyo_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Especifique el apoyo</td>
            <td style="text-align:left; font-weight:bold;">Forma de recepción del apoyo</td>
            <td style="text-align:left; font-weight:bold;">Monto aproximado del apoyo mensual</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especificar_tipo_apoyo'] }}</td>
            @if($dato['forma'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Monetario</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Especie</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['monto'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['forma'] == 2)
            <td style="text-align:left;font-weight:bold;">Especifique el apoyol </td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Aclaraciones/Observaciones</td>
            @else
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
            @endif
        </tr>
        <tr>
            @if($dato['forma'] == 2)
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['observaciones'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
            @endif
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Representación</td>
        </tr>
        @if(count($reprActiva)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($reprActiva as $dato)
        @if($dato['representante'] == 1)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['representante'] == 2)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Dependiente Económico</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo de Representación:</td>
            <td style="text-align:left; font-weight:bold;">Especifique:</td>
            <td style="text-align:left; font-weight:bold;">Fecha de inicio de la representación:</td>
        </tr>
        <tr>
            @if($dato['tipo_representacion_id'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Representante</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Representado</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Representante / Representado:</td>
            <td style="text-align:left; font-weight:bold;">Nombre o razón social del parte representante/representado:</td>
            <td style="text-align:left; font-weight:bold;">RFC:</td>
        </tr>
        <tr>
            @if($dato['representado'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Persona física</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Persona moral</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['remuneracion'] == 1)
            <td style="text-align:left; font-weight:bold;">¿Recibe remuneración por su representación?:<br></td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Monto Mensual Neto:<br></td>
            @else
            <td style="text-align:left; font-weight:bold;" colspan="3">¿Recibe remuneración por su representación?</td>
            @endif
        </tr>
        <tr>
            @if($dato['remuneracion'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Sí</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['monto'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">No</td>
            @endif
        </tr>
        <tr class="titulostr">
            @if($dato['pais_id'] == 1)
            <td style="text-align:left; font-weight:bold;">País donde se localiza:<br></td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Entidad Federativa:<br></td>
            @else
            <td style="text-align:left; font-weight:bold;" colspan="3">País donde se localiza:</td>
            @endif
        </tr>
        <tr>
            @if($dato['pais_id'] == 150)
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['entidad_federativa_id'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['pais'] }}</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Clientes Principales</td>
        </tr>
        @if(count($clientesPrinc)== 0)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">¿Realiza alguna actividad lucrativa independiente al empleo, cargo o comisión?</td>
        </tr>
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($clientesPrinc as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">¿Realiza alguna actividad lucrativa independiente al empleo, cargo o comisión?</td>
        </tr>
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">SÍ</td>
        </tr>
        @if($dato['propietario'] == 1)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['propietario'] == 2)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Dependiente Económico</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="2">Nombre de la empresa o servicio que proporciona:</td>
            <td style="text-align:left; font-weight:bold;">RFC:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Cliente principal</td>
            <td style="text-align:left;font-weight:bold;">Nombre o razón social del cliente principal</td>
            <td style="text-align:left;font-weight:bold;">RFC</td>
        </tr>
        <tr>
            @if($dato['tipo_persona'] == 1)
            <td style="text-align:left; font-weight:bold;">Persona física</td>
            @else
            <td style="text-align:left; font-weight:bold;">Persona moral</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_cliente'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_cliente'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Sector productivo al que pertenece:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Monto aproximado del beneficio o ganancia mensual que obtiene del cliente principal:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['monto'] }}</td>
        </tr>
        <tr class="titulostr">
            @if($dato['pais_id'] == 1)
            <td style="text-align:left; font-weight:bold;">País donde se localiza:<br></td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Entidad Federativa:<br></td>
            @else
            <td style="text-align:left; font-weight:bold;" colspan="3">País donde se localiza:</td>
            @endif
        </tr>
        <tr>
            @if($dato['pais_id'] == 150)
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['entidad_federativa_id'] }}</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['pais'] }}</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Beneficios Privados</td>
        </tr>
        @if(count($beneficiosPriv)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($beneficiosPriv as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Beneficiario</td>
            <td style="text-align:left;font-weight:bold;">Tipo de Beneficio</td>
            <td style="text-align:left;font-weight:bold;">Especifique</td>
        </tr>
        <tr>
            @if($dato['beneficiario'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Declarante</td>
            @elseif($dato['beneficiario'] == 2)
            <td style="text-align:left;text-transform:uppercase;">Pareja</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Dependiente Económico</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_beneficio'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Otorgante</td>
            <td style="text-align:left;font-weight:bold;">Nombre o razón social del otorgante</td>
            <td style="text-align:left;font-weight:bold;">RFC</td>
        </tr>
        <tr>
            @if($dato['otorgante'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Persona física</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Persona moral</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Forma de recepción del beneficio</td>
            <td style="text-align:left; font-weight:bold;">Monto mensual aproximado del beneficio:</td>
            <td style="text-align:left; font-weight:bold;">Tipo de moneda:</td>
        </tr>
        <tr>
            @if($dato['forma'] == "1")
            <td style="text-align:left;text-transform:uppercase;">Monetario</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Especie</td>
            @endif
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['monto'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_moneda'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Especifique el beneficio </td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Sector productivo al que pertenece</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['beneficio'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Fideicomisos</td>
        </tr>
        @if(count($fideicomisos)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif
        @foreach($fideicomisos as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Participación en fideicomisos</td>
        </tr>
        @if($dato['participante'] == 1)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Declarante</td>
        </tr>
        @elseif($dato['participante'] == 2)
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Pareja</td>
        </tr>
        @else
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Dependiente</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo de fideicomiso:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Tipo de participación:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_fideicomiso_id'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['tipo_participacion_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">RFC del fideicomisos</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['rfc_fideicomiso'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Fideicomitente</td>
        </tr>
        <tr>
            @if($dato['fideicomitente'] == 1)
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Persona física</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre o razón social del fideicomitente:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['rfc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre o razón social del fiduciario:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_fiduciario'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['rfc_fiduciario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Fideicomisario</td>
        </tr>
        <tr>
            @if($dato['fideicomisario'] == 1)
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Persona física</td>
            @else
            <td style="text-align:left;text-transform:uppercase;" colspan="3">Persona moral</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Nombre o razón social del fideicomisario:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">RFC:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_fideicomisario'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['rfc_fideicomisario'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">País donde se localiza el fideicomiso</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Sector productivo al que pertenece</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['sector_industria_id'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones/Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach


        <tr>
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>INGRESOS NETOS DEL DECLARANTE, PAREJA Y/O DEPENDIENTE ECONÓMICO</td>
        </tr>
        @if( $datosDeclaracion['tipo_declaracion_id'] == 1)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Sueldos y salarios públicos (situación actual)</td>
        </tr>
        @foreach($spublicos as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Otros sueldos y salarios</td>
        </tr>

        @foreach($otross as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['cat'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach

        <tr>

            <td style="text-align:left; font-weight:bold; padding-left:15px"><br>TOTAL</br></td>
            <td style="text-align:left; font-weight:bold; padding-left:15px"></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $totalingresos['total'] }}</td>

        </tr>

        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Sueldos y salarios públicos (Año inmediato anterior)</td>
        </tr>

        @foreach($spublicosant as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Otros sueldos y salarios</td>
        </tr>

        @foreach($otrossant as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['cat'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr>

            <td style="text-align:left; font-weight:bold; padding-left:15px"><br>TOTAL</br></td>
            <td style="text-align:left; font-weight:bold; padding-left:15px"></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $totalingresosant['total'] }}</td>

        </tr>


        @else
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Sueldos y salarios públicos</td>
        </tr>

        @foreach($spublicosant as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3"><br>Otros sueldos y salarios</td>
        </tr>

        @foreach($otrossant as $dato)
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['cat'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ingreso'] }}</td>
        </tr>
        @endforeach
        <tr>

            <td style="text-align:left; font-weight:bold; padding-left:15px"><br>TOTAL</br></td>
            <td style="text-align:left; font-weight:bold; padding-left:15px"></td>
            <td style="text-align:left;text-transform:uppercase;">{{ $totalingresosant['total'] }}</td>

        </tr>
        @endif

        <tr>
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>ACTIVOS</td>
        </tr>
        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3">BIENES INMUEBLES</td>
        </tr>

        @foreach($binmuebles as $dato)

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tipo de inmueble</td>
            <td style="text-align:left;font-weight:bold;">Especifique</td>
            <td style="text-align:left;font-weight:bold;">Titular del inmueble</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['naturaleza'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique_inmueble'] }}</td>
            @if($dato['titular'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Declarante</td>
            @elseif($dato['titular'] == 2)
            <td style="text-align:left;text-transform:uppercase;">Pareja</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Dependiente</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Porcentaje de propiedad del declarante</td>
            <td style="text-align:left;font-weight:bold;">Superficie del terreno ( m<sup>2</sup> )</td>
            <td style="text-align:left;font-weight:bold;">Superficie de construcción ( m<sup>2</sup> )</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['porcentaje_propiedad'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['superficie_terreno'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['superficie_construccion'] }}</td>
        </tr>
        @if(!empty($dato['tercero']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tercero</td>
            <td style="text-align:left;font-weight:bold;">Nombre del tercero o terceros</td>
            <td style="text-align:left;font-weight:bold;">RFC del tercero</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_tercero'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Forma de adquisición</td>
            <td style="text-align:left;font-weight:bold;">Forma de pago</td>
            <td style="text-align:left;font-weight:bold;">Transmisor</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_adquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_pago'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['transmisor'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Nombre del transmisor</td>
            <td style="text-align:left;font-weight:bold;">RFC del transmisor</td>
            <td style="text-align:left;font-weight:bold;">Relación del transmisor</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_transmisor'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Especifique</td>
            <td style="text-align:left;font-weight:bold;">Valor de adquisición</td>
            <td style="text-align:left;font-weight:bold;">¿El valor de adquisición conforme a? </td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_especificar'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['padquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['valor_conforme'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Fecha de Adquisición</td>
            <td style="text-align:left;font-weight:bold;">Datos del registro público de la propiedad</td>
            <td style="text-align:left;font-weight:bold;">Domicilio</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha_adquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numero_registro_publico'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['domicilio'] }}</td>
        </tr>
        @if(!empty($dato['motivo_baja']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">En caso de baja del inmueble incluir motivo</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Especifique</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['motivo_baja'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['especifique_baja'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones / Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>

        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>VEHÍCULOS</td>
        </tr>

        @foreach($bmuebles as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tipo de vehículo</td>
            <td style="text-align:left;font-weight:bold;">Especifique</td>
            <td style="text-align:left;font-weight:bold;">Titular del vehículo</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipoBien'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
            @if($dato['titular'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Declarante</td>
            @elseif($dato['titular'] == 2)
            <td style="text-align:left;text-transform:uppercase;">Pareja</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Dependiente</td>
            @endif
        </tr>
        @if(!empty($dato['tercero']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tercero</td>
            <td style="text-align:left;font-weight:bold;">Nombre del tercero o terceros</td>
            <td style="text-align:left;font-weight:bold;">RFC del tercero</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_tercero'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Transmisor</td>
            <td style="text-align:left;font-weight:bold;">Nombre o razón social del transmisor</td>
            <td style="text-align:left;font-weight:bold;">RFC del transmisor</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_transmisor'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Relación del transmisor</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Especifique</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['relacion_especificar'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Marca</td>
            <td style="text-align:left;font-weight:bold;">Modelo</td>
            <td style="text-align:left;font-weight:bold;">Año </td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['marca'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['modelo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['anio'] }}</td>
        </tr>

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Número de serie o registro</td>
            <td style="text-align:left;font-weight:bold;">Forma de adquisición</td>
            <td style="text-align:left;font-weight:bold;">Forma de pago</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numero_serie'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_adquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_pago'] }}</td>
        </tr>

        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Valor de adquisición del vehículo / Tipo moneda</td>
            <td style="text-align:left;font-weight:bold;">Fecha de Adquisición</td>
            <td style="text-align:left;font-weight:bold;">¿Dónde se encuentra registrado?</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['padquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha_adquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ubicacion'] }}</td>
        </tr>
        <!-- @if(!empty($dato['motivo_baja'])) -->
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">En caso de la baja del vehículo incluir motivo</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Especifique</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['motivo_baja'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['especifique_baja'] }}</td>
        </tr>
        <!-- @endif -->
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones / Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach
        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>BIENES MUEBLES</td>
        </tr>

        @foreach($bmueblesnr as $dato)
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tipo de bien</td>
            <td style="text-align:left;font-weight:bold;">Especifique</td>
            <td style="text-align:left;font-weight:bold;">Titular del mueble</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipoBien'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
            @if($dato['titular'] == 1)
            <td style="text-align:left;text-transform:uppercase;">Declarante</td>
            @elseif($dato['titular'] == 2)
            <td style="text-align:left;text-transform:uppercase;">Pareja</td>
            @else
            <td style="text-align:left;text-transform:uppercase;">Dependiente</td>
            @endif
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Descripción general del bien</td>

        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['descripcion'] }}</td>

        </tr>
        @if(!empty($dato['tercero']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tercero</td>
            <td style="text-align:left;font-weight:bold;">Nombre del tercero o terceros</td>
            <td style="text-align:left;font-weight:bold;">RFC del tercero</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_tercero'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Transmisor</td>
            <td style="text-align:left;font-weight:bold;">Nombre o razón social del transmisor</td>
            <td style="text-align:left;font-weight:bold;">RFC del transmisor</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_transmisor'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Relación del transmisor</td>
            <td style="text-align:left;font-weight:bold;">Especifique</td>
            <td style="text-align:left;font-weight:bold;">Forma de adquisición</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_transmisor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_especificar'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_adquisicion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Forma de pago</td>
            <td style="text-align:left;font-weight:bold;">Valor de adquisición del bien mueble / Tipo moneda</td>
            <td style="text-align:left;font-weight:bold;">Fecha de Adquisición</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['forma_pago'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['padquisicion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha_adquisicion'] }}</td>
        </tr>
        <!-- @if(!empty($dato['motivo_baja'])) -->
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">En caso de baja del mueble incluir motivo</td>
            <td style="text-align:left;font-weight:bold;" colspan="2">Especifique</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['motivo_baja'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['especifique_baja'] }}</td>
        </tr>
        <!-- @endif -->
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;" colspan="3">Aclaraciones / Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>
        </tr>
        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3">INVERSIONES, CUENTAS BANCARIAS Y OTRO TIPO DE VALORES/ACTIVOS</td>
        </tr>

        @foreach($inversiones as $dato)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo de Inversión</td>
            <td style="text-align:left; font-weight:bold;">Bancaria</td>
            <td style="text-align:left; font-weight:bold;">Titular de la inversion, cuentas bancarias y otro tipo de valores</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipoInv'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['inversion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['titular'] }}</td>
        </tr>
        @if(!empty($dato['tercero']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tercero</td>
            <td style="text-align:left;font-weight:bold;">Nombre del tercero o terceros</td>
            <td style="text-align:left;font-weight:bold;">RFC del tercero</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_tercero'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="2">Institución o razón social</td>
            <td style="text-align:left; font-weight:bold;">RFC institución</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['nombre_institucion'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_institucion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Número de cuenta, contrato o póliza</td>
            <td style="text-align:left; font-weight:bold;">Monto Original / Tipo de moneda</td>
            <td style="text-align:left; font-weight:bold;">Pais</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numero_cuenta'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['monto_original'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Aclaraciones / Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['observaciones'] }}</td>

        </tr>
        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>ADEUDOS / PASIVOS</td>
        </tr>
        @if(count($pasivos)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;">NO</td>
        </tr>
        @endif

        @foreach($pasivos as $dato)
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Titular del adeudo:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['titular'] }}</td>
        </tr>
        @if(!empty($dato['tercero']))
        <tr class="titulostr">
            <td style="text-align:left;font-weight:bold;">Tercero</td>
            <td style="text-align:left;font-weight:bold;">Nombre del tercero o terceros</td>
            <td style="text-align:left;font-weight:bold;">RFC del tercero</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_tercero'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_tercero'] }}</td>
        </tr>
        @endif
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo adeudo:</td>
            <td style="text-align:left; font-weight:bold;">Número de cuenta o contrato:</td>
            <td style="text-align:left; font-weight:bold;">Especifique:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo_adeudo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['identificador'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Fecha adeudo:</td>
            <td style="text-align:left; font-weight:bold;">Monto original del adeudo / Tipo de moneda:</td>
            <td style="text-align:left; font-weight:bold;">Saldo insoluto:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['fecha'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['original'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pendiente'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Otorgante del crédito:</td>
            <td style="text-align:left; font-weight:bold;">Nombre, Institución o razón social:</td>
            <td style="text-align:left; font-weight:bold;">RFC:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['otorgante'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_acreedor'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc_acreedor'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">¿Dónde se localiza el adeudo?:</td>
            <td style="text-align:left; font-weight:bold;" colspan="2">Aclaraciones / Observaciones</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['pais'] }}</td>
            <td style="text-align:left;text-transform:uppercase;" colspan="2">{{ $dato['observaciones'] }}</td>
        </tr>

        @endforeach

        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>PRÉSTAMO O COMODATO POR TERCEROS</td>
        </tr>
        @if(count($prestamo)== 0)
        <tr>
            <td style="text-align:left; font-weight:bold;" colspan="3">NO</td>
        </tr>
        @endif

        @foreach($prestamo as $dato)

        @if($dato['tipo']== "Inmueble")
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo:</td>
            <td style="text-align:left; font-weight:bold;">Tipo de Bien:</td>
            <td style="text-align:left; font-weight:bold;">Detalle:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipoBien'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['especifique'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Ubicación del Inmueble:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['ubicacion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Aclaraciones / Observaciones:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['aclaraciones'] }}</td>
        </tr>
        @else
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Tipo:</td>
            <td style="text-align:left; font-weight:bold;">Tipo de Bien:</td>
            <td style="text-align:left; font-weight:bold;">Detalle:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['tipoBien'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['desc'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">Número de Serie :</td>
            <td style="text-align:left; font-weight:bold;">Dueño o titular:</td>
            <td style="text-align:left; font-weight:bold;">Nombre del Dueño o Titular:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['numero_serie'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['titular_tipo'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['nombre_titular'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;">RFC del Dueño o Titular:</td>
            <td style="text-align:left; font-weight:bold;">Relación con el dueño o titular:</td>
            <td style="text-align:left; font-weight:bold;">Donde se encuentra registrado:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['rfc'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['relacion_titular'] }}</td>
            <td style="text-align:left;text-transform:uppercase;">{{ $dato['ubicacion'] }}</td>
        </tr>
        <tr class="titulostr">
            <td style="text-align:left; font-weight:bold;" colspan="3">Aclaraciones / Observaciones:</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $dato['aclaraciones'] }}</td>
        </tr>
        @endif
        @endforeach
        <tr class="titulostr">
            <td class="tsecciones" style="text-align:left;color:navy; font-weight:900;" colspan="3"><br>DECLARACIÓN FISCAL</td>
        </tr>
        <tr>
            <td style="text-align:left;text-transform:uppercase;" colspan="3">{{ $resFis }}</td>
        </tr>
    </tbody>
</table>


<style type="text/css">
    tbody {
        font-size: 12px !important;
    }

    table {
        border: 1px solid black;
    }

    .tsecciones {
        font-size: 15px !important;
    }

    mark {
        background-color: gray;
        color: black;
        /*font-size:15px !important;*/

    }

    /* .tsecciones {
  background-color:#eeeeee !important;
} */
    .titulostr {
        background-color: #eeeeee !important;
    }
</style>
