<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<img src="encabezado.jpg" height="100" width="435">
 <table class="table table-striped" >
  <thead style="text-align: center;">
    <tr>
      <td >
        <h4>Resumen de la declaración patrimonial y de intereses</h4>
      </td>
    </tr>
  </thead>
    
  <tbody>
      <tr>
        <td style="text-align: left; vertical-align: top; text-transform: uppercase;">
            {{ $dpersonales['nombres'] }}
        </td>   
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $dencargo['dependencia'] }}<br></td> 

      </tr>
      <tr>        
        <td style="text-align: left;font-weight: bold; "><br>Datos Personales</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $dpersonales['nombres'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $dpersonales['curp'] }}</td> 
      </tr><tr>        
        <td style="text-align: left; ">{{ $dpersonales['rfc'] }}<br></td> 
      </tr>
      <tr>        
      <td style="text-align: left; font-weight: bold;"><br>Datos del Encargo Actual</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $dencargo['dependencia'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $dencargo['cargo'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; font-weight: bold;"><br>Intereses</td> 
      </tr>
      <tr>        
      <td style="text-align: left; font-weight: bold;"><br>Dependientes</td> 
      </tr>      
        @foreach($dependientes as $d)
          <tr>        
            <td style="text-align: left; padding-left: 15px;">{{ $d['TipoRelacion'] }}</td> 
          </tr>
          <tr>        
            <td style="text-align: left; padding-left: 15px; ">{{ $d['Nombre'] }}</td> 
          </tr>   
          @endforeach
       <tr>        
        <td style="text-align: left; ">Empresas, Sociedades y Asociaciones</td> 
      </tr>

      @foreach($empresas as $dato)
      <tr>        
        <td style="text-align: left; padding-left: 15px;">{{ $dato['nombreEmpresa'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['naturaleza'] }}</td> 
      </tr>   
      @endforeach
       <tr>        
        <td style="text-align: left; font-weight: bold;"><br>Ingresos</td> 
      </tr>
       <tr>        
        <td style="text-align: left; ">Sueldos y salarios públicos</td> 
      </tr>

      @foreach($spublicosant as $dato)
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['nombre'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['ingreso'] }}</td> 
      </tr>   
      @endforeach
       <tr>        
        <td style="text-align: left; "><br>Otros sueldos y salarios</td> 
      </tr>

      @foreach($otrossant as $dato)
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['cat'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['nombre'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['ingreso'] }}</td> 
      </tr>   
      @endforeach
      <tr>        
        <td style="text-align: left;font-weight: bold; "><br>Activos</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">Bienes inmuebles</td> 
      </tr>

      @foreach($binmuebles as $dato)
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['tipoOperacion'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['naturaleza'] }}</td> 
      </tr> 
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['domicilio'] }}</td> 
      </tr>  
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['padquisicion'] }}</td> 
      </tr>   
      @endforeach
       <tr>        
        <td style="text-align: left; "><br>Bienes muebles</td> 
      </tr>

      @foreach($bmuebles as $dato)
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['tipoOperacion'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left; padding-left: 15px; ">{{ $dato['tipoBien'] }}</td> 
      </tr> 
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['padquisicion'] }}</td> 
      </tr>   
      @endforeach
      <tr>        
        <td style="text-align: left; "><br>Bienes muebles no registrables</td> 
      </tr>

      @foreach($bmueblesnr as $dato)
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['tipoOperacion'] }}</td> 
      </tr>
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['tipoBien'] }}</td> 
      </tr> 
      <tr>        
        <td style="text-align: left;  padding-left: 15px;">{{ $dato['padquisicion'] }}</td> 
      </tr>   
      @endforeach
       <tr>        
        <td style="text-align: left; "><br>Declaración fiscal</td> 
      </tr>
      <tr>        
        <td style="text-align: left; ">{{ $resFis }}</td> 
      </tr>       
  </tbody>
</table>


<style type="text/css">
  tbody{
    font-size: 12px !important;
  }
</style>


