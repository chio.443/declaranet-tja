<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatRelacionPersonaAdquirio extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;
    protected $table = "cat_relacion_persona_adquirio";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}