<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BienesMueblesNoRegistrables extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "bienes_muebles_no_registrables";
    protected $guarded = ['id'];
    protected $casts = [
        'nombres_copropietarios' => 'array',
        'fecha_adquisicion' => 'date:d-m-Y',
        'fecha_venta' => 'date:d-m-Y'
    ];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $with = ['tipoOperacion', 'tipoBien'];

    public function tipoOperacion()
    {
        return $this->belongsTo('App\CatTipoOperacion', 'tipo_operacion_id', 'id')->withDefault();
    }
    public function tipoBien()
    {
        return $this->belongsTo('App\CatTipoBienMuebleNr', 'tipo_bien_id', 'id')->withDefault();
    }
}