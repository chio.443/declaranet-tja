<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aclaraciones extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "declaracion_aclaraciones";
    protected $guarded = ['id'];
    protected $hidden = array('updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}