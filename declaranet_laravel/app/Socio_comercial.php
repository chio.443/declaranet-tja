<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socio_comercial extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'socios_comerciales';
}