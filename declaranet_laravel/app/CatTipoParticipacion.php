<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatTipoParticipacion extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'cat_tipo_participacion';
}