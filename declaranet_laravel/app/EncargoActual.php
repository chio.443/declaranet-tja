<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncargoActual extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded       = ['id'];
    protected $table     = 'datos_encargo_actual';
    protected $casts = ['fecha_posesion' => 'date:d-m-Y'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');


    protected $with = ['funcionesp', 'vialidad', 'ente'];
    //protected $appends = ['dependencia2'];

    //public function getDependenciaAttribute()
    /* public function getDependencia2()
    {
        $ente = $this->ente->valor;
        return $ente;
    } */

    public function funcionesp()
    {
        return $this->hasMany('App\EncargoActualFuncP', 'datos_encargo_actual_id', 'id');
    }
    public function vialidad()
    {
        return $this->belongsTo('App\CatVialidad', 'direccion_encargo_vialidad_id', 'id')->withDefault();
    }
    public function ente()
    {
        return $this->belongsTo('App\Ente_publico', 'ente_publico_id', 'id')->withDefault();
    }
}
