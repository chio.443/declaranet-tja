<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representacion_pasiva extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'representacion_pasiva';

    public function nac()
    {
        return $this->hasMany('App\IGNacionalidades', 'ip_id', 'id'); //->select(array('id', 'nac_id'));;

    }
}