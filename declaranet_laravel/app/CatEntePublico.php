<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatEntePublico extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded	= ['id'];
    protected $table 	= 'cat_ente_publico';    
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
   public function declarantes()
	{
	    return $this->belongsTo('App\EncargoActual', 'id', 'ente_publico_id')->withDefault();
	}
}