<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\InformacionPersonal;

class RecuperaPass extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;
    protected $table = "recupera_pass";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $appends = ['nombre'];

    public function getNombreAttribute()
    {
        $search = InformacionPersonal::where('rfc', $this->rfc)->first();

        if ($search) {
            $nombre = '' . $search->nombres . ' ' . $search->primer_apellido . ' ' . $search->segundo_apellido . '';
        }

        return $nombre;
    }
}