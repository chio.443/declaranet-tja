<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ente_publico extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'cat_ente_publico';
}