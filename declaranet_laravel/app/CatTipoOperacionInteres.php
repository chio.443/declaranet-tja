<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatTipoOperacionInteres extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;
    protected $table = "cat_tipo_operacion_intereses";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}