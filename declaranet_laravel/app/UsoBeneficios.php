<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsoBeneficios extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "uso_especie_propiedad_tercero";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}