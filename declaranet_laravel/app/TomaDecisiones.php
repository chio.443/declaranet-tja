<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TomaDecisiones extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "toma_decisiones";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
    protected $casts = ['fecha' => 'date:d-m-Y'];
}