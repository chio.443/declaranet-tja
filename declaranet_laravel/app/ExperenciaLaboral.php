<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperenciaLaboral extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'experiencia_laboral';

    protected $casts = ['fecha_ingreso' => 'date:d-m-Y', 'fecha_salida' => 'date:d-m-Y'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $with = ['funciones', 'vialidad'];

    public function funciones()
    {
        return $this->hasMany('App\ExpLaboralFuncPrinc', 'exl_id', 'id');
    }
    public function vialidad()
    {
        return $this->belongsTo('App\CatVialidad', 'direccion_vialidad_id', 'id')->withDefault();
    }
    public function ambito()
    {
        return $this->belongsTo('App\Ambitos', 'ambito_id', 'id')->withDefault();
    }
    public function entidadFedD()
    {
        return $this->belongsTo('App\CatEntidadFederativa', 'direccion_entidad_federativa_id', 'id')->withDefault();
    }
    public function municipioD()
    {
        return $this->belongsTo('App\CatMunicipio', 'direccion_municipio_id', 'id')->withDefault();
    }
    public function localidadD()
    {
        return $this->belongsTo('App\CatLocalidad', 'direccion_localidad_id', 'id')->withDefault();
    }
    public function sector()
    {
        return $this->belongsTo('App\CatSectorIndustria', 'sector_industria_id', 'id')->withDefault();
    }
}