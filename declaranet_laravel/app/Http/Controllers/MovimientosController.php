<?php

namespace App\Http\Controllers;

use Auth;
use App\Token;
use App\Usuarios;
use Carbon\Carbon;
use App\Declaracion;
use App\Movimientos;
use App\Notificacion;
use App\EncargoActual;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\DeclaracionJustificada;

use App\UsuarioTipoDeclaracion;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\UsuarioTipoDecController;

class MovimientosController extends Controller
{

    public function lista(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;
        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];
        $permisos = $usuario->roles->Permisos;

        ///si tiene permiso para movimientos global, no se filtra por dependencia, si no, si
        // if (in_array(17, $permisos)) {
        //     $movimientos = Movimientos::with('usuarios', 'encargo')
        //         ->whereHas("servidor", function ($q) {
        //             $q->whereNotNull('rfc')
        //                 ->orderBy('nombres', 'asc')
        //                 ->orderBy('primer_apellido', 'asc')
        //                 ->orderBy('segundo_apellido', 'asc');
        //         })
        //         ->orderBy('id', 'DESC')
        //         ->get();
        // } else {
        $movimientos = Movimientos::with(['servidor' => function ($q) {
            $q->select([
                'informacion_personal.id',
                'informacion_personal.nombres',
                'informacion_personal.primer_apellido',
                'informacion_personal.segundo_apellido',
                'informacion_personal.curp',
                'informacion_personal.rfc'
            ])->without(['vialidad', 'nac', 'declaracion', 'ente', 'encargo', 'pendientes', 'utdeclaraciones']);
        }])
            ->with(['usuario' => function ($q) {
                $q->select([
                    'usuarios.id',
                    'usuarios.Nombre',
                    'usuarios.Paterno',
                    'usuarios.Materno',
                    'usuarios.User',
                    'usuarios.ID_Dependencia'
                ])->withTrashed();
            }])
            ->with(['aprobacion' => function ($q) {
                $q->select([
                    'usuarios.id',
                    'usuarios.Nombre',
                    'usuarios.Paterno',
                    'usuarios.Materno',
                    'usuarios.User',
                    'usuarios.ID_Dependencia'
                ])->withTrashed();
            }])
            ->whereHas("servidor", function ($q) {
                $q->without(['vialidad', 'nac', 'declaracion', 'ente', 'encargo', 'pendientes', 'utdeclaraciones'])
                    ->whereNotNull('rfc');
            })
            ->whereHas("encargo", function ($q) use ($ente) {
                $q->where('ente_publico_id', $ente);
            })
            ->withTrashed()
            ->orderBy('id', 'DESC')
            ->get();
        // }

        return response()->json(['movimientos' => $movimientos]);
    }
    public function index(Request $request)
    {
        /* $usuarios = Movimientos::with('usuarios', 'encargo')
            ->where("tipo_mov", 4)
            ->orderBy('id', 'DESC')->get();
        return response()->json(['usuarios' => $usuarios]); */

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        ///si tiene permiso para movimientos global, no se filtra por dependencia, si no, si
        if (in_array(17, $permisos)) {
            $usuarios = Movimientos::with('usuarios', 'encargo')
                ->whereHas("servidor", function ($q) {
                    $q->without(['vialidad', 'nac', 'declaracion', 'ente', 'encargo', 'pendientes', 'utdeclaraciones'])
                        ->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->where("tipo_mov", 4)
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            $usuarios = Movimientos::with('usuarios', 'encargo')
                ->whereHas("servidor", function ($q) {
                    $q->without(['vialidad', 'nac', 'declaracion', 'ente', 'encargo', 'pendientes', 'utdeclaraciones'])
                        ->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->whereHas("encargo", function ($q) use ($ente) {
                    $q->where('ente_publico_id', $ente);
                })
                ->where("tipo_mov", 4)
                ->orderBy('id', 'DESC')
                ->get();
        }

        return response()->json(['usuarios' => $usuarios]);
    }
    public function getCorrecciones(Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        ///si tiene permiso para movimientos global, no se filtra por dependencia, si no, si
        if (in_array(17, $permisos)) {
            $usuarios = Movimientos::with('usuarios', 'encargo')
                ->whereHas("servidor", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->where("tipo_mov", 5)
                ->where("status", 0)
                ->get();
        } else {
            $usuarios = Movimientos::with('usuarios', 'encargo')
                ->whereHas("servidor", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->whereHas("encargo", function ($q) use ($ente) {
                    $q->where('ente_publico_id', $ente);
                })
                ->where("tipo_mov", 5)
                ->where("status", 0)
                ->get();
        }

        return response()->json(['usuarios' => $usuarios]);
    }
    public function getHCorrecciones(Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        ///si tiene permiso para movimientos global, no se filtra por dependencia, si no, si
        if (in_array(17, $permisos)) {
            $usuarios = Movimientos::with('usuarios', 'usuario', 'encargo')
                ->whereHas("usuarios", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->where("tipo_mov", 5)
                ->whereIn('status', array(1, 2))
                ->get();
        } else {
            $usuarios = Movimientos::with('usuarios', 'usuario', 'encargo')
                ->whereHas("usuarios", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->whereHas("encargo", function ($q) use ($ente) {
                    $q->where('ente_publico_id', $ente);
                })
                ->where("tipo_mov", 5)
                ->whereIn('status', array(1, 2))
                ->get();
        }


        return response()->json(['usuarios' => $usuarios]);
    }
    public function store(Request $request)
    {
        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }
        //dd($token);
        $id = $token->uid;


        $format = 'Y-m-d';


        try {
            DB::beginTransaction();
            $movimiento = new Movimientos();

            $movimiento->ip_id = $request->id;
            $movimiento->tipo_mov = $request->tipo_mov;
            $movimiento->inicia = Carbon::parse($request->inicia)->format($format);
            $movimiento->termina = Carbon::parse($request->termina)->format($format);
            $movimiento->motivo = $request->motivo;
            $movimiento->tipo_baja = $request->tipo_baja;
            $movimiento->tipo_licencia = $request->tipo_licencia;
            $movimiento->origen = $request->origen;
            $movimiento->cambio = $request->cambio;
            $movimiento->status = 0;
            $movimiento->created_by = $id;

            $movimiento->save();

            $usuarios = InformacionPersonal::findOrFail($request->id);
            $usuarios->tipo_mov = $request->tipo_mov;

            if ($request->tipo_mov == '2') {
                //baja

                $encargo = EncargoActual::where("informacion_personal_id", $request->id)->first();
                ///Al momento de que un servidor público tiene estatus de baja, y siempre y cuando tenga una declaración patrimonial y de intereses inicial registrada, la siguiente declaración se tomará como de conclusión.
                //Verificar si tiene una declaración inicial o anual en esa dependencia, en caso de que no haya presentado inicial en el sistema

                $declaracionInicial = Declaracion::where('id_ip', $encargo->informacion_personal_id)
                    ->where('tipo_declaracion_id', 1)
                    ->where('ente_publico_id', $encargo->ente_publico_id)
                    ->first();

                $declaracionAnual = Declaracion::where('id_ip', $encargo->informacion_personal_id)
                    ->where('tipo_declaracion_id', 2)
                    ->where('ente_publico_id', $encargo->ente_publico_id)
                    ->first();

                if ($declaracionInicial || $declaracionAnual || $movimiento->tipo_baja != 'Normal') { //si tiene alguna de las dos, se cambia a final, si no, debe presentar primero la otra
                    $encargo->tipo_dec = 3;
                    $encargo->save();
                } else { //la va a tener pendiente, se guarda en informacion personal
                    $usuarios->tipo_dec = 3;
                    $usuarios->save();
                }

                $encargo->fecha_termino = Carbon::parse($request->encargo["fecha_termino"])->format($format);
                $encargo->save();

                $notificacion = new Notificacion();
                $notificacion->id_movimiento = $movimiento->id;
                $notificacion->texto = "Baja Registrada";
                $notificacion->texto_desc = "Se ha registrado su baja con fecha de " . $encargo->fecha_termino .
                    ", tiene 60 días para presentar su declaración final";
                $notificacion->id_ip = $request->id;
                $notificacion->id_tipo = 1;
                $notificacion->save();
                //crear notificación




                //guardo el ente en el que se dio la baja, para cuando se presente la declaración
                $movimiento->ente_publico_id = $encargo->ente_publico_id;
                $movimiento->save();

                if ($movimiento->tipo_baja == 'Normal') {
                    $usuario_tipo_dec = new UsuarioTipoDecController;

                    $tipo_dec  = new Request;
                    $tipo_dec->ip_id = $request->id;
                    $tipo_dec->tipo_movimiento_id = 2;
                    $tipo_dec->fecha_posesion =  $encargo->fecha_posesion;
                    $tipo_dec->fecha_termino =  $encargo->fecha_termino;
                    $tipo_dec->ente_publico_id = $encargo->ente_publico_id;

                    $tipo_dec_resp = $usuario_tipo_dec->movimiento($tipo_dec);
                }
            }
            if ($request->tipo_mov == '3') {
                $encargo = EncargoActual::where("informacion_personal_id", $request->id)->first();
                $encargo->empleo_cargo_comision = $request->encargo["empleo_cargo_comision"];
                $encargo->area_adscripcion = $request->encargo["area_adscripcion"];
                $encargo->nivel_encargo = $request->encargo["nivel_encargo"];
                //$encargo->fecha_termino = Carbon::parse($request->encargo["fecha_termino"])->format($format);
                $encargo->save();
            }
            if ($request->tipo_mov == '5') {
                //si es una correccion, solamente se guarda en movimientos, los cambios se aplican hasta que se aprueba
                $notificacion = new Notificacion();
                $notificacion->id_movimiento = $movimiento->id;
                $notificacion->texto = "Corrección";
                $notificacion->texto_desc = "Se ha registrado una solicitud de corrección para el servidor " . $usuarios->rfc;
                $notificacion->id_tipo = 3;
                //$notificacion->id_ip = $request->id;
                $notificacion->save();
                //crear notificación

            }

            if ($request->tipo_mov == '6') {
                $usuarios->password = md5($request->password);
            }
            if ($request->tipo_mov == '8') {

                $usuarios->publica = $request->publica;
            }
            if ($request->tipo_mov == '9') {
                ///si es justificación se inserta en la tabla declaracion_justificada para

                $encargo = EncargoActual::where("informacion_personal_id", $request->id)->first();

                $justificada = new DeclaracionJustificada();

                $justificada->id_ip = $request->id;
                $justificada->tipo_declaracion_id = $encargo->tipo_dec;
                $justificada->periodo =  date("Y");
                $justificada->id_movimiento = $movimiento->id;
                $justificada->ente_publico_id = $encargo->ente_publico_id;

                $justificada->save();

                if ($encargo->tipo_dec == 1) {
                    $encargo->tipo_dec = 2;
                }

                $encargo->save();
            }

            $usuarios->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['usuarios' => $usuarios]);
    }
    public function aplicaCorrecciones(Request $request)
    {
        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }
        //dd($token);
        $uid = $token->uid;

        $format = 'Y-m-d';

        ///obtener los datos a apliar en el cambio

        $movimiento = Movimientos::findOrFail($request->id);

        $cambio = $movimiento->cambio;
        $origen = $movimiento->origen;

        $movimiento->observaciones = $request->observaciones;
        $movimiento->status = 1;
        $movimiento->aprobado = $uid;

        $simplificada = '0';

        $titular = 0;

        if (isset($cambio['titular'])) {
            $titular = $cambio['titular'];
        }
        if (isset($cambio['simplificada'])) {
            $simplificada = $cambio['simplificada'];
        }

        $posesion = 0;

        if (isset($cambio['fecha_posesion'])) {
            $posesion = $cambio['fecha_posesion'];
        }


        $tipo_dec;

        $declarante = InformacionPersonal::findOrFail($movimiento->ip_id);

        $encargo = EncargoActual::where("informacion_personal_id", $movimiento->ip_id)->first();

        ////verificar si fué dado de baja en otra dependencia antes de cambiar

        $baja = Movimientos::where('ip_id', $movimiento->ip_id)
            ->where('ente_publico_id', $encargo->ente_publico_id)
            ->where('tipo_mov', 2)
            ->orderBy('id', 'desc')
            ->first();

        $anioact    =  date("Y");

        if ($posesion != 0) { //si en el cambio viene la fecha de posesion
            $posesion = Carbon::parse($cambio["fecha_posesion"])->format($format);

            $alta = Carbon::parse($declarante->created_at)->format('Y');
            $ingreso    =  Carbon::parse($posesion)->format('Y');
        }

        if ($baja) {
            ///verificar la fecha de baja y la nueva fecha de alta para mover los tipos de declaracion
            $posesion = Carbon::parse($cambio["fecha_posesion"])->format($format);

            $bajaF = Carbon::parse($baja->termina)->format($format);

            $dias = Carbon::parse($bajaF)->diffInDays($posesion, false);

            if ($dias < 60) {
                //si son menos de 60 días entre la baja y la alta, va a presentar anual
                $tipo_dec = 2;

                $declarante->tipo_dec = 0;
                $declarante->save();
            } else { // si son más de  60 días, t
                //
                if ($cambio["dependencia"] != $origen["dependencia"]) { //si se va a cambiar de deoendencia tiene que presentar las que sea que tenga pendiente antes de realizar el cambio
                    return response()->json(['error' => $cambio]);
                } else {
                    //si es la misma dependencia, la declaración inicial se va a quedar pendiente
                    $tipo_dec = $encargo->tipo_dec;
                    $declarante->tipo_dec = 1;
                    $declarante->save();
                }
            }
        } else {
            ////si no es una baja, solo se verifican las fechas de ingreso
            if (($anioact == $ingreso) || ($simplificada == '1' && $alta == $anioact)) {
                $tipo_dec = 1;
            } else {
                $tipo_dec = 2;
            }
        }




        $declarante->rfc = $cambio['rfc'];
        $declarante->nombres = $cambio['nombres'];
        $declarante->primer_apellido = $cambio['primer_apellido'];
        $declarante->segundo_apellido = $cambio['segundo_apellido'];
        $declarante->curp = $cambio['curp'];
        $declarante->correo_electronico_laboral = $cambio['correo_electronico_laboral'];
        $declarante->grupo_id = $cambio['grupo_id'];
        $declarante->ente_publico_id = $cambio['dependencia'];
        $declarante->simplificada = $simplificada;


        ////verificar si es una baja, cambiando de dependencia (reactivando)

        if ($encargo) {
            $encargo->empleo_cargo_comision = $cambio['cargo'];
            $encargo->area_adscripcion = $cambio['area'];
            $encargo->nivel_encargo = $cambio['nivel'];
            $encargo->titular = $titular;
            $encargo->correo_laboral = $cambio['correo_electronico_laboral'];
            $encargo->ente_publico_id = $cambio['dependencia'];
            $encargo->contratado_honorarios = $cambio['honorarios'];
            $encargo->tipo_dec = $tipo_dec;
            $encargo->fecha_posesion = $posesion;
        } else {
            $encargo = new EncargoActual([
                'informacion_personal_id' => $movimiento->ip_id,
                'area_adscripcion' => $cambio['area'],
                'empleo_cargo_comision' => $cambio['cargo'],
                'nivel_encargo' => $cambio['nivel'],
                'correo_laboral' => $cambio['correo_electronico_laboral'],
                'ente_publico_id' => $cambio['dependencia'],
                'fecha_posesion' => Carbon::parse($cambio['fecha_posesion'])->format($format),
                'contratado_honorarios' => $cambio['honorarios'],
                'titular' => $titular,
                'tipo_dec' => $tipo_dec
            ]);
        }
        $movimiento->save();

        $declarante->save();
        $encargo->save();

        //marca como leidas todas las que se hayan generado para ese movimiento
        $notificacion = Notificacion::where('id_movimiento', $movimiento->id);
        $notificacion->delete();

        //generar la notificacion para rh
        $notificacion = new Notificacion();
        $notificacion->id_movimiento = $movimiento->id;
        $notificacion->texto = "Corrección aprobada";
        $notificacion->texto_desc = "La corrección del servidor " . $declarante->nombres . ", RFC:" . $declarante->rfc . " ha sido aprobada, para más detalles, ingrese a Movimientos";
        $notificacion->id_usuario = $movimiento->created_by;
        $notificacion->id_tipo = 2;
        $notificacion->save();


        return response()->json(['movimiento' => $cambio]);
    }

    public function rechazaCorrecciones(Request $request)
    {
        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }
        //dd($token);
        $uid = $token->uid;


        ///obtener los datos a apliar en el cambio

        $movimiento = Movimientos::findOrFail($request->id);

        $movimiento->observaciones = $request->observaciones;
        $movimiento->status = 2;
        $movimiento->aprobado = $uid;
        $movimiento->save();

        //marca como leidas todas las que se hayan generado para ese movimiento
        $notificacion = Notificacion::where('id_movimiento', $movimiento->id);
        $notificacion->delete();

        //generar la notificacion para rh


        $declarante = InformacionPersonal::findOrFail($movimiento->ip_id);

        $notificacion = new Notificacion();
        $notificacion->id_movimiento = $movimiento->id;
        $notificacion->texto = "Corrección rechazada";
        $notificacion->texto_desc = "La corrección del servidor " . $declarante->nombres . ", RFC:" . $declarante->rfc . " ha sido rechazada.\nObservaciones: " . $request->observaciones . "\nPara más detalles, ingrese a movimientos";
        $notificacion->id_tipo = 2;
        $notificacion->id_usuario = $movimiento->created_by;
        $notificacion->save();

        return response()->json(['movimiento' => $movimiento]);
    }

    public function getMovimientos($tipo, $dependencia, $desde, $hasta)
    {
        if ($tipo != 1) { //Si no se seleccionan todos los movimientos
            $movimientos = Movimientos::where(DB::raw('date(created_at)'), '>=', DB::raw("date('" . $desde . "')"))
                ->where(DB::raw('date(created_at)'), '<=', DB::raw("date('" . $hasta . "')"))
                ->whereHas("encargo", function ($q) use ($dependencia) {
                    $q->where('ente_publico_id', $dependencia);
                })
                ->with('encargo', 'usuario', 'usuarios')
                ->where('tipo_mov', $tipo)
                ->get();
        } else { //Si no se seleccionan todos los movimientos
            $movimientos = Movimientos
                ::where(DB::raw('date(created_at)'), '>=', DB::raw("date('" . $desde . "')"))
                ->where(DB::raw('date(created_at)'), '<=', DB::raw("date('" . $hasta . "')"))
                ->whereHas("encargo", function ($q) use ($dependencia) {
                    $q->where('ente_publico_id', $dependencia);
                })
                ->with('encargo', 'usuario', 'usuarios')
                ->get();
        }

        return response()->json(['movimiento' => $movimientos]);
    }

    public function servidoresinactivos(Request $request)
    {
        /* $usuarios = Movimientos::with('usuarios', 'encargo')
            ->where("tipo_mov", 4)
            ->orderBy('id', 'DESC')->get();
        return response()->json(['usuarios' => $usuarios]); */

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        ///si tiene permiso para movimientos global, no se filtra por dependencia, si no, si
        if (in_array(17, $permisos)) {
            $usuarios = Movimientos::with('usuarios', 'encargo', 'usuario')
                ->whereHas("servidor", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->where("tipo_mov", 0)
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            $usuarios = Movimientos::with('usuarios', 'encargo', 'usuario')
                ->whereHas("servidor", function ($q) {
                    $q->whereNotNull('rfc')
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc');
                })
                ->whereHas("encargo", function ($q) use ($ente) {
                    $q->where('ente_publico_id', $ente);
                })
                ->where("tipo_mov", 0)
                ->orderBy('id', 'DESC')
                ->get();
        }

        return response()->json(['usuarios' => $usuarios]);
    }

    public function reactivarUsuario(Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $aid = $token->uid;

        $declarante = InformacionPersonal::find($request->ip_id);
        $declarante->tipo_mov = null;
        $declarante->save();

        //buscar si ya hay movimiento 0 en tabla movimientos
        $inactivo = Movimientos::where('ip_id', $request->ip_id)->where('tipo_mov', 0)->whereNull('deleted_at')->first();

        $inactivo->update(['deleted_by' => DB::raw($aid)]);

        $inactivo->delete();

        return response()->json(['inactivo' => $inactivo]);

        return response()->json(['response' => $declarante]);
    }

    public function cancelarbaja(Request $request)
    {
        try {
            DB::beginTransaction();
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
            $aid = $token->uid;
            // dd($id);
            $declarante = InformacionPersonal::find($request->id);

            $encargo = EncargoActual::where("informacion_personal_id", $request->id)->first();

            $origen = [
                'rfc' => $declarante->rfc,
                'curp' => $declarante->curp,
                'grupo_id' => $declarante->grupo_id,
                'correo_electronico_laboral' => $declarante->correo_electronico_laboral,
                'nombres' => $declarante->nombres,
                'primer_apellido' => $declarante->primer_apellido,
                'segundo_apellido' => $declarante->segundo_apellido,
                'dependencia' => $encargo->ente_publico_id,
                'cargo' => $encargo->empleo_cargo_comision,
                'honorarios' => $encargo->contratado_honorarios,
                'nivel' => $encargo->nivel_encargo,
                'area' => $encargo->area_adscripcion,
                'titular' => $encargo->titular,
                'fecha_posesion' => Carbon::parse($encargo->fecha_posesion)->format('Y-m-d'),
                'fecha_termino' => Carbon::parse($encargo->fecha_termino)->format('Y-m-d'),
                'simplificada' => $declarante->simplificada,
                'tipo_dec_id' => $encargo->tipo_dec,
            ];

            $declarante->tipo_mov = null;
            $declarante->save();

            $encargo_ = $encargo;
            $encargo->fecha_termino = null;
            $encargo->save();

            $cambio = [
                'rfc' => $declarante->rfc,
                'curp' => $declarante->curp,
                'grupo_id' => $declarante->grupo_id,
                'correo_electronico_laboral' => $declarante->correo_electronico_laboral,
                'nombres' => $declarante->nombres,
                'primer_apellido' => $declarante->primer_apellido,
                'segundo_apellido' => $declarante->segundo_apellido,
                'dependencia' => $encargo->ente_publico_id,
                'cargo' => $encargo->empleo_cargo_comision,
                'honorarios' => $encargo->contratado_honorarios,
                'nivel' => $encargo->nivel_encargo,
                'area' => $encargo->area_adscripcion,
                'titular' => $encargo->titular,
                'fecha_posesion' => Carbon::parse($encargo->fecha_posesion)->format('Y-m-d'),
                'fecha_termino' => null,
                'simplificada' => $declarante->simplificada,
                'tipo_dec_id' => $encargo->tipo_dec,
            ];

            //buscar si ya hay movimiento 0 en tabla movimientos
            $inactivo = Movimientos::where('ip_id', $request->id)
                ->where('tipo_mov', 2)
                ->whereNull('deleted_at')
                ->orderBy('id', 'DESC')
                ->first();

            $inactivo->delete();

            $movimiento = new Movimientos();

            $movimiento->ip_id = $request->id;
            $movimiento->tipo_mov = 10;
            $movimiento->motivo = $request->observaciones;
            $movimiento->origen = $origen;
            $movimiento->cambio = $cambio;
            $movimiento->ente_publico_id = $encargo->ente_publico_id;
            $movimiento->status = 0;
            $movimiento->created_by = $aid;
            $movimiento->inicia = now()->format('Y-m-d');
            $movimiento->termina = now()->format('Y-m-d');
            $movimiento->save();

            $usuario_tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $request->id)->where('tipo_dec_id', 3)
                ->orderBy('id', 'DESC')->first();

            $usuario_tipo_dec->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['response' => $declarante]);
    }
}
