<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EmpresasAsociaciones;
use App\CatTipoParticipacion;
use Auth;
use \App\Token;

class EmpresasAsociacionesController extends Controller
{
    public function index(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $response = EmpresasAsociaciones::where('ip_id', $id)->get();

        return response()->json(['response' => $response]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        /* $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid; */

        // $data = $request->only(['', '', '', '', 'tipo_participacion', 'monto', 'sector_industria_id', 'pais_id', 'entidad_federativa_id', 'observaciones', 'especifique', 'remuneracion', 'ip_id']);
        /* */
        $id = Auth::user()->id;
        $response = EmpresasAsociaciones::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $id,
                'participante' => $request->participante,
                'nombre_empresa_sociedad_asociacion' => $request->nombre_empresa_sociedad_asociacion,
                'rfc' => $request->rfc,
                'porcentaje_participacion' => $request->porcentaje_participacion,
                'naturaleza_vinculo' => $request->naturaleza_vinculo,
                'remuneracion' => $request->remuneracion,
                'monto' => $request->monto,
                'sector_industria_id' => $request->sector_industria_id,
                'domicilio_pais_id' => $request->domicilio_pais_id,
                'domicilio_entidad_federativa_id' => $request->domicilio_entidad_federativa_id,
                'observaciones' => $request->observaciones,
                'naturaleza_vinculo_especificar' => $request->naturaleza_vinculo_especificar,
            ]
        );
        return response()->json(['response' => $response]);
    }

    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $response = EmpresasAsociaciones::where('ip_id', $id)->get();

        return response()->json(['response' => $response]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        $prestamoc = EmpresasAsociaciones::findOrFail($id);
        $prestamoc->delete();
    }

    public function tipoParticipacion()
    {
        $response = CatTipoParticipacion::orderBy('id', 'DESC')->get();

        return response()->json(['tiposp' => $response]);
    }
}