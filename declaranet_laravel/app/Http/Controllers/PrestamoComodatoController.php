<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PrestamoComodato;
use \App\Token;

class PrestamoComodatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $prestamosc = PrestamoComodato::where('ip_id', $id)->get();

        return response()->json(['response' => $prestamosc]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $id = $token->uid;

        $registro = PrestamoComodato::updateOrCreate(
            [
                'ip_id' => $id,
                'id' => $request['id']
            ],
            $request->except('tipo_inm', 'tipo_veh')
        );
        /* $registro->save(); */
        return response()->json(['response' => $registro->refresh()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $prestamosc = PrestamoComodato::where('ip_id', $id)->get();

        return response()->json(['response' => $prestamosc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $registro = PrestamoComodato::updateOrCreate(
            [
                'ip_id' => $id,
                'id' => $request['id']
            ],
            $request->except('tipo_inm', 'tipo_veh')
        );

        $registro->save();

        return response()->json(['response' => $registro->refresh()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prestamoc = PrestamoComodato::findOrFail($id);
        $prestamoc->delete();
    }
}