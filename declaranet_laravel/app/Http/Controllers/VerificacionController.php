<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Pais;
use App\Token;
use Carbon\Carbon;
use App\CatTitular;

//Agregue
use App\RegimenMat;
use App\CatTipoBien;
use App\Declaracion;
use App\EstadoCivil;
use App\CatMunicipio;
use App\CatTipoAdeudo;
use App\CatEntePublico;
use App\CatTipoInversion;
use App\CatTipoOperacion;
use App\CatTitularAdeudo;
use App\CatTipoBienMueble;
use App\BitacoraDeclaracion;
use App\CatTipoBienMuebleNr;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\CatEntidadFederativa;




use App\BitacoraConsultaBalanza;

class VerificacionController extends Controller
{
    public function getServidores(Request $request)
    {

        $usuarios = InformacionPersonal::with('ente', 'encargo')
            ->whereHas(
                "encargo",
                function ($q) use ($request) {
                    $q->where("ente_publico_id", 29);
                }
            )

            ->orderBy('rfc', 'asc')
            ->get();

        return response()->json(['usuarios' => $usuarios]);
    }

    public function getGenerales(Request $request, $id)
    {
        /* $response = BitacoraDeclaracion::select('bitacora_declaraciones.declaracion_id','bitacora_declaraciones.ip_id', 'bitacora_declaraciones.informacion_personal', 'bitacora_declaraciones.created_at', 'bitacora_declaraciones.ingresos', 'bitacora_declaraciones.bienes_inmuebles', 'bitacora_declaraciones.bienes_muebles_no_registrables', 'bitacora_declaraciones.bienes_muebles_registrables', 'bitacora_declaraciones.pasivos_deudas', 'bitacora_declaraciones.datos_encargo_actual')
                ->where('declaracion_id', $id)
                ->join('declaraciones','declaraciones.id','=','bitacora_declaraciones.declaracion_id')
                ->whereNull('declaraciones.deleted_at')
                ->orderBy('created_at', 'desc')->firstOrFail();
        return response()->json(['response' => $response]); */

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $idus = $token->uid;

        $dpersonales = array();
        $declaracion = BitacoraDeclaracion::where('declaracion_id', $id)->first();
        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();



        // Datos Generales
        $func = json_decode($declaracion['informacion_personal']);

        $func = (object) $func[0];

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['ente_publico_id'] = CatEntePublico::where('id', $func->ente_publico_id)->first()['valor'];
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'];
        $dpersonales['entidad'] = CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'];
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['edoCivil'] = EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'];
        if ($func->regimen_matrimonial_id)
            $dpersonales['regimen'] = RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'];
        $dpersonales['curp'] = $func->curp;


        //Fin Datos Generales;

        //Datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo

        /// GUARDAR BITACORA DE CONSULTA DE VERIFICACIÓN ///
        $bitacora = new BitacoraConsultaBalanza([
            'ip_id' => $func->id,
            'declaracion_id' => $declaracion->declaracion_id,
            'created_by' => $idus
        ]);
        $bitacora->save();

        /// FIN DE BITACORA DE CONSULTA DE VERIFICACIÓN ///

        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array)$func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público ',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']), 2)
        );

        //dd($spublicos);

        /*
        $otross[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']), 2)
        );
        */


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        );


        ///TOTAL INGRESOS
        /* $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad'] ) +
        (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
        (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
        (float)str_replace(',', '', $sueldo['arrendamiento_cantidad'])); */

        ///

        $totalingresos = array();
        /* $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1'] ) +
        (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
        (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
        (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
        (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
        (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2); */

        $tingresos = $sueldo['sub_1'] + $sueldo['razon_social_cantidad'] + $sueldo['actividad_financiera_cantidad'] + $sueldo['servicios_profecionales_cantidad'] + $sueldo['neto_conyugue_cantidad'] + $sueldo['arrendamiento_cantidad'];
        $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $tingresos), 2);

        //dd($totalingresos);

        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2)
        );

        /* $otrossant[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        ); */

        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        );

        $otrossant[] = array(
            'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['ant_arrendamientos'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_arrendamientos_cantidad']), 2)
        );

        /* if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        } */

        ///TOTAL INGRESOS ANT
        /* $totalingresosant = array();
        $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad'] ) +
        (float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_otros_cantidad'])); */

        $totalingresosant = array();
        /* $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12'] ) +
        (float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']) +
        (float)str_replace(',', '', $sueldo['ant_arrendamientos_cantidad']),2); */

        $tingresosant = $sueldo['ant_sub_12'] + $sueldo['ant_actividad_comercial_cantidad'] + $sueldo['ant_actividad_financiera_cantidad'] + $sueldo['ant_servicios_profecionales_cantidad'] + $sueldo['ant_neto_conyugue_cantidad'] + $sueldo['ant_arrendamientos_cantidad'];
        $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $tingresosant), 2);
        //dd($totalingresosant);
        //dd($spublicosant,$otrossant,$totalingresosant);

        ///Fin Ingresos

        ///////////////////////////////////////activos
        //bienes inmuebles


        $func = json_decode($declaracion['bienes_inmuebles']);


        $binmuebles = array();
        foreach ($func as $inmueble) {
            if (empty($inmueble->motivo_baja)) {
                $binmuebles[] = array(
                    'tipoOperacion' => $inmueble->tipo_operacion_id ? CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'] : '',
                    'naturaleza' => $inmueble->tipo_bien_id ? CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'] : '',
                    'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                    'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2),
                    'padquisicion1' => $inmueble->precio_adquisicion_valor
                );
            }
        }
        $padquisicion = array_column($binmuebles, 'padquisicion1');
        $tbinmuebles = array_sum($padquisicion);
        $totalbinmuebles = '$' . number_format((float)str_replace(',', '', $tbinmuebles), 2);

        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {
            if (empty($mueble->motivo_baja)) {
                $bmuebles[] = array(
                    'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2),
                    'modelo' => $mueble->modelo,
                    'padquisicion1' => $mueble->precio_adquisicion_valor,

                );
            }
        }
        $padquisicionbm = array_column($bmuebles, 'padquisicion1');
        $tbmuebles = array_sum($padquisicionbm);
        $totalbmuebles = '$' . number_format((float)str_replace(',', '', $tbmuebles), 2);

        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            if (empty($mueble->motivo_baja)) {
                $bmueblesnr[] = array(
                    'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2),
                    'padquisicion1' => $mueble->precio_adquisicion_valor,

                );
            }
        }

        $padquisicionbmnr = array_column($bmueblesnr, 'padquisicion1');
        $tbmueblesnr = array_sum($padquisicionbmnr);
        $totalbmueblesnr = '$' . number_format((float)str_replace(',', '', $tbmueblesnr), 2);

        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => $dato->domicilio_institucion_pais_id ? Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'] : '',
                'entidad' => $dato->domicilio_institucion_entidad_federativa_id ? CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'] : '',
                'tipoOp' => $dato->tipo_operacion_id ? CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'] : '',
                'tipoInv' => $dato->tipo_inversion_id ? CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'] : '',
                'titular' => $dato->titular_id ? CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor'] : '',
                'monto_original1' => $dato->monto_original,
            );
        }

        $monto_original = array_column($inversiones, 'monto_original1');
        $tinversiones = array_sum($monto_original);
        $totalinversiones = '$' . number_format((float)str_replace(',', '', $tinversiones), 2);

        $tactivos = $tingresos + $tbinmuebles + $tbmuebles + $tbmueblesnr + $tinversiones;
        $totalactivos = '$' . number_format((float)str_replace(',', '', $tactivos), 2);

        $tactivosant = $tingresosant + $tbinmuebles + $tbmuebles + $tbmueblesnr + $tinversiones;
        $totalactivosant = '$' . number_format((float)str_replace(',', '', $tactivosant), 2);
        //dd($totalactivosant);


        //Fin Activos

        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'pendiente' => '$' . number_format((float)str_replace(',', '', $deuda->saldo_pendiente), 2),
                'pendiente1' => $deuda->saldo_pendiente,
            );
        }
        $pendiente = array_column($pasivos, 'pendiente1');
        $tpendiente = array_sum($pendiente);
        $totalpendiente = '$' . number_format((float)str_replace(',', '', $tpendiente), 2);


        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversionesad = array();
        foreach ($func as $dato) {
            $inversionesad[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => $dato->domicilio_institucion_pais_id ? Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'] : '',
                'entidad' => $dato->domicilio_institucion_entidad_federativa_id ? CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'] : '',
                'tipoOp' => $dato->tipo_operacion_id ? CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'] : '',
                'tipoInv' => $dato->tipo_inversion_id ? CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'] : '',
                'titular' => $dato->titular_id ? CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor'] : '',
                'monto_original1' => $dato->monto_original,
            );
        }
        $monto_original = array_column($inversionesad, 'monto_original1');
        $tmonto_original = array_sum($monto_original);
        $totalmontooriginal = '$' . number_format((float)str_replace(',', '', $tmonto_original), 2);

        $tpasivos = $tmonto_original + $tpendiente;
        $totalpasivos = '$' . number_format((float)str_replace(',', '', $tpasivos), 2);
        //dd($totalpasivos);

        ////fin pasivos

        //Balance general

        $balanza = $tactivos - $tpasivos;
        $totalbalanza = '$' . number_format((float)str_replace(',', '', $balanza), 2);

        $balanzaant = $tactivosant - $tpasivos;
        $totalbalanzaant = '$' . number_format((float)str_replace(',', '', $balanzaant), 2);

        //dd($totalbalanza, $totalbalanzaant);

        $dec_list = BitacoraDeclaracion::where('id', '<=', $declaracion->id)
            ->where('ip_id', $declaracion->ip_id)->orderBy('id', 'ASC')->get();

        $grafica = [];
        $pas_arr = [];
        $max_ = [];
        foreach ($dec_list as $dec) {
            $year = Carbon::parse($dec['created_at'])->year;

            $func = json_decode($dec['ingresos']);
            $sueldo = (array)$func[0];
            $tingresos = $sueldo['sub_1'] + $sueldo['razon_social_cantidad'] + $sueldo['actividad_financiera_cantidad'] + $sueldo['servicios_profecionales_cantidad'] + $sueldo['neto_conyugue_cantidad'] + $sueldo['arrendamiento_cantidad'];

            $tingresosant = $sueldo['ant_sub_12'] + $sueldo['ant_actividad_comercial_cantidad'] + $sueldo['ant_actividad_financiera_cantidad'] + $sueldo['ant_servicios_profecionales_cantidad'] + $sueldo['ant_neto_conyugue_cantidad'] + $sueldo['ant_arrendamientos_cantidad'];


            $inmuebles = json_decode($dec['bienes_inmuebles']);
            $padquisicion = array();
            foreach ($inmuebles as $inmueble) {
                if (empty($inmueble->motivo_baja)) {
                    $padquisicion[] = $inmueble->precio_adquisicion_valor;
                }
            }
            $tbinmuebles = array_sum($padquisicion);

            $vehiculos = json_decode($dec['bienes_muebles_registrables']);
            $padquisicionbm = array();
            foreach ($vehiculos as $vehiculo) {
                if (empty($vehiculo->motivo_baja)) {
                    $padquisicionbm[] = $vehiculo->precio_adquisicion_valor;
                }
            }
            $tbmuebles = array_sum($padquisicionbm);

            $muebles = json_decode($dec['bienes_muebles_no_registrables']);
            $padquisicionbmnr = array();
            foreach ($muebles as $mueble) {
                if (empty($mueble->motivo_baja)) {
                    $padquisicionbmnr[] = $mueble->precio_adquisicion_valor;
                }
            }
            $tbmueblesnr = array_sum($padquisicionbmnr);

            $inversiones = json_decode($dec['inversiones_cuentas_valores']);
            $inversiones_montos = array();
            foreach ($inversiones as $inversion) {
                $inversiones_montos[] = $inversion->monto_original;
            }

            $tinversiones = array_sum($inversiones_montos);

            $tactivos = $tingresos + $tbinmuebles + $tbmuebles + $tbmueblesnr + $tinversiones;

            $tactivosant = $tingresosant + $tbinmuebles + $tbmuebles + $tbmueblesnr + $tinversiones;


            $pasivos = json_decode($dec['pasivos_deudas']);
            $pas_arr[] = $pasivos;
            $deudas = array();
            $tpendiente = 0;
            foreach ($pasivos as $deuda) {
                $deudas[] =  $deuda->saldo_pendiente;
            }

            $tpendiente = array_sum($deudas);


            $adeudos = json_decode($dec['inversiones_cuentas_valores']);
            $inversionesad = array();
            foreach ($adeudos as $adeudo) {
                $inversionesad[] =  $adeudo->monto_original;
            }
            $tmonto_original = array_sum($deudas);

            $tpasivos = $tmonto_original + $tpendiente;

            $max_[] = max(array($tpasivos, $tactivos, $tactivosant));


            $grafica[] = array('anio' => $year, 'activos' => $tactivos, 'activos _ant' => $tactivosant, 'pasivos' => $tpasivos);
        }
        // = ;

        $max_vid = max($max_); //A number you want to round
        $nbr = strlen($max_vid);
        $nbr = pow(10, $nbr); // The exponent, same as in math  10^$nbr
        $max = ceil($max_vid / $nbr) * $nbr;
        // dd($max, $max_);
        // $max = ceil($max / 1000) * 1000

        return response()->json(['datosDeclaracion' => $datosDeclaracion, 'dpersonales' => $dpersonales, 'dencargo' => $dencargo, 'otross' => $otross, 'spublicos' => $spublicos, 'binmuebles' => $binmuebles, 'bmuebles' => $bmuebles, 'bmueblesnr' => $bmueblesnr, 'inversiones' => $inversiones, 'pasivos' => $pasivos, 'inversionesad' => $inversionesad, 'totalingresos' => $totalingresos, 'spublicosant' => $spublicosant, 'totalingresosant' => $totalingresosant, 'totalbinmuebles' => $totalbinmuebles, 'totalbmuebles' => $totalbmuebles, 'totalbmueblesnr' => $totalbmueblesnr, 'totalinversiones' => $totalinversiones, 'totalpendiente' => $totalpendiente, 'totalmontooriginal' => $totalmontooriginal, 'totalpasivos' => $totalpasivos, 'totalactivos' => $totalactivos, 'totalactivosant' => $totalactivosant, 'totalbalanzaant' => $totalbalanzaant, 'totalbalanza' => $totalbalanza, 'grafica' => $grafica, 'max' => $max]);
    }
}
