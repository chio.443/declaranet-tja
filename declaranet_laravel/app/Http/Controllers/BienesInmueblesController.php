<?php

namespace App\Http\Controllers;

use App\BienesInmuebles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;

class BienesInmueblesController extends Controller
{
    public function index()
    {
        $bienes = BienesInmuebles::orderBy('id', 'DESC')->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function item($id)
    {
        //$ip_id = Auth::user()->id;
        $response = BienesInmuebles::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        /*  $ip_id = Auth::user()->id; */
        $response = BienesInmuebles::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_bien_id' => $request->tipo_bien_id,
                'titular_id' => $request->titular_id,
                'porcentaje_propiedad' => $request->porcentaje_propiedad,
                'superficie_terreno' => $request->superficie_terreno,
                'superficie_construccion' => $request->superficie_construccion,
                'tercero' => $request->tercero,
                'nombre_tercero' => $request->nombre_tercero,
                'rfc_tercero' => $request->rfc_tercero,
                'forma_adquisicion_id' => $request->forma_adquisicion_id,
                'fecha_adquisicion' => Carbon::parse($request->fecha_adquisicion),
                'forma_pago' => $request->forma_pago,
                'transmisor' => $request->transmisor,
                'nombre_denominacion_quien_adquirio' => $request->nombre_denominacion_quien_adquirio,
                'rfc_quien_adquirio' => $request->rfc_quien_adquirio,
                'relacion_persona_adquirio_id' => $request->relacion_persona_adquirio_id,
                'precio_adquisicion_valor' => $request->precio_adquisicion_valor,
                'precio_adquisicion_moneda_id' => $request->precio_adquisicion_moneda_id,
                'valor_conforme' => $request->valor_conforme,
                'numero_registro_publico' => $request->numero_registro_publico,
                'motivo_baja' => $request->motivo_baja,
                'observaciones' => $request->observaciones,
                'domicilio_bien_pais_id' => $request->domicilio_bien_pais_id,
                'domicilio_bien_entidad_federativa_id' => $request->domicilio_bien_entidad_federativa_id,
                'domicilio_bien_municipio_id' => $request->domicilio_bien_municipio_id,
                'domicilio_bien_localidad_id' => $request->domicilio_bien_localidad_id,
                'domicilio_bien_cp' => $request->domicilio_bien_cp,
                'domicilio_bien_vialidad_nombre' => $request->domicilio_bien_vialidad_nombre,
                'domicilio_bien_vialidad_tipo' => $request->domicilio_bien_vialidad_tipo,
                'domicilio_bien_numext' => $request->domicilio_bien_numext,
                'domicilio_bien_numint' => $request->domicilio_bien_numint,
                'domicilio_bien_colonia' => $request->domicilio_bien_colonia,
                'especifique_inmueble' => $request->especifique_inmueble,
                'relacion_especificar' => $request->relacion_especificar,
                'especifique_baja' => $request->especifique_baja,

            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $bien = BienesInmuebles::findOrFail($id);
        $bien->delete();
    }
}