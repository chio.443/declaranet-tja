<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Deudas;
use App\Fiscal;
use App\Premios;
use App\Empresas;
use App\Ingresos;
use App\Intereses;
use App\Membresias;
use App\Declaracion;
use App\Enajenacion;
use App\Fideicomiso;
use App\OtroEncargo;
use App\Aclaraciones;
use App\EncargoActual;
use App\Observaciones;
use App\OtrosIngresos;
use App\UsoBeneficios;
use App\TomaDecisiones;
use App\BienesInmuebles;
use App\EfectivoMetales;
use App\PrestamoComodato;
use App\DatosCurriculares;
use App\ExperenciaLaboral;
use App\BeneficiosPrivados;
use App\EncargoActualFuncP;
use App\BitacoraDeclaracion;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\Clientes_principales;
use App\DependienteEconomico;
use App\ApoyoBeneficioPublico;
use App\Representacion_activa;
use App\SueldosSalariosPublicos;
use App\BienesMueblesRegistrables;
use App\InversionesCuentasValores;
use App\BienesMueblesNoRegistrables;
//use Carbon\Carbon;

class BitacoraDeclaracionController extends Controller
{
    public function item($id)
    {
        $ip_id = Auth::user()->id;
        $response = BitacoraDeclaracion::select('bitacora_declaraciones.id', 'bitacora_declaraciones.declaracion_fiscal', 'bitacora_declaraciones.datos_encargo_actual', 'bitacora_declaraciones.created_at', 'declaraciones.Tipo_Dec', 'usuario_tipo_dec.periodo')
            ->where('bitacora_declaraciones.ip_id', $ip_id)
            ->leftJoin('declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->leftJoin('usuario_tipo_dec', 'usuario_tipo_dec.declaracion_id', '=', 'declaraciones.id')
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('usuario_tipo_dec.deleted_at')
            ->whereNull('bitacora_declaraciones.deleted_at')
            ->orderBy('created_at', 'desc')->get();

        // $declarantesIds = array();
        /*  foreach ($response as $clave => $value) {
            $year = Carbon::createFromFormat('Y-m-d', $value->Fecha_Dec)->year . '';
            if ($value->declaracion_fiscal == '[]') {
                $fiscal  = Fiscal::where('ip_id', $value->ip_id)
                    ->whereYear('created_at', $year)
                    ->whereNull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)->get()->toJson();

                $response[$clave]->declaracion_fiscal = $fiscal;
            }
            $aclaraciones  = Aclaraciones::where('ip_id', $value->ip_id)
                ->whereYear('created_at', $year)
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->limit(1)->get()->toJson();

            $response[$clave]->aclaraciones = $aclaraciones;
        } */


        return response()->json(['response' => $response]);
    }

    public function guardaBitacora()
    {

        $declaraciones = Declaracion::get();


        foreach ($declaraciones as $declaracion) {


            $ip_id = $declaracion['id_ip'];

            $bienesInmuebles     = BienesInmuebles::where('ip_id', $ip_id)->get()->toJson();
            $bienesMueblesNR     = BienesMueblesNoRegistrables::where('ip_id', $ip_id)->get()->toJson();
            $bienesMueblesR      = BienesMueblesRegistrables::where('ip_id', $ip_id)->get()->toJson();
            $datosCurriculares     = DatosCurriculares::where('informacion_personal_id', $ip_id)->get()->toJson();
            $encargoActual         = EncargoActual::where('informacion_personal_id', $ip_id)->get()->toJson();
            $fiscal             = Fiscal::where('ip_id', $ip_id)->get()->toJson();
            $dependienteEcon     = DependienteEconomico::where('informacion_personal_id', $ip_id)->get()->toJson();
            $efectivoMetales     = EfectivoMetales::where('ip_id', $ip_id)->get()->toJson();
            $empresas             = Empresas::where('ip_id', $ip_id)->get()->toJson();
            $enajenacion         = Enajenacion::where('ip_id', $ip_id)->get()->toJson();
            $expLaboral         = ExperenciaLaboral::where('informacion_personal_id', $ip_id)->get()->toJson();
            $fideicomisos         = Fideicomiso::where('ip_id', $ip_id)->get()->toJson();
            $infPersonal         = InformacionPersonal::where('id', $ip_id)->get()->toJson();
            $ingresos             = Ingresos::where('ip_id', $ip_id)->get()->toJson();
            $intereses             = Intereses::where('ip_id', $ip_id)->get()->toJson();
            $invCtasValores     = InversionesCuentasValores::where('ip_id', $ip_id)->get()->toJson();
            $membresias         = Membresias::where('ip_id', $ip_id)->get()->toJson();
            $observaciones         = Observaciones::where('ip_id', $ip_id)->orderBy('id', 'desc')->first();
            $otrosIngresos         = OtrosIngresos::where('ip_id', $ip_id)->get()->toJson();
            $deudas             = Deudas::where('ip_id', $ip_id)->get()->toJson();
            $premios             = Premios::where('ip_id', $ip_id)->get()->toJson();
            $sspublicos         = SueldosSalariosPublicos::where('ip_id', $ip_id)->get()->toJson();
            $usoBeneficios         = UsoBeneficios::where('ip_id', $ip_id)->get()->toJson();



            $bDeclaracion =  new BitacoraDeclaracion([
                'ip_id'                             =>    $ip_id,
                'declaracion_id'                     =>    $declaracion->id,
                'bienes_inmuebles'                     =>  $bienesInmuebles,
                'bienes_muebles_registrables'         =>    $bienesMueblesR,
                'bienes_muebles_no_registrables'     =>    $bienesMueblesNR,
                'datos_curriculares'                =>    $datosCurriculares,
                'datos_encargo_actual'                =>    $encargoActual,
                'declaracion_fiscal'                =>    $fiscal,
                'dependientes_economicos'            =>    $dependienteEcon,
                'efectivo_metales'                    =>    $efectivoMetales,
                'empresas_sociedades_asociaciones'    =>    $empresas,
                'enajenacion_bienes'                =>    $enajenacion,
                'experiencia_laboral'                =>    $expLaboral,
                'fideicomisos'                        =>    $fideicomisos,
                'informacion_personal'                =>    $infPersonal,
                'ingresos'                            =>    $ingresos,
                'intereses'                            =>    $intereses,
                'inversiones_cuentas_valores'        =>    $invCtasValores,
                'membresias'                        =>    $membresias,
                'observaciones'                        =>    $observaciones,
                'otros_ingresos'                    =>    $otrosIngresos,
                'pasivos_deudas'                    =>    $deudas,
                'premios'                            =>    $premios,
                'sueldos_salarios_publicos'            =>    $sspublicos,
                'uso_especie_propiedad_tercero'        =>    $usoBeneficios
            ]);

            $bDeclaracion->save();
        }
    }
    public function actualizaEnteD()
    {

        $declaraciones = Declaracion::get();

        foreach ($declaraciones as $declaracion) {

            $ip_id = $declaracion['id_ip'];
            $id_dec = $declaracion['id'];

            $bitacora = BitacoraDeclaracion::where('declaracion_id', $id_dec)->first(); ///declaracion en tablas

            $encargoBita = json_decode($bitacora['datos_encargo_actual']);


            if (isset($encargoBita[0])) {
                $encargoBita = (object) $encargoBita[0];
                if (property_exists($encargoBita, "ente_publico_id")) {
                    $declaracion['ente_publico_id'] = $encargoBita->ente_publico_id;
                }
            } else {
                $encargoActual = EncargoActual::where('informacion_personal_id', $ip_id)->first();
                $declaracion['ente_publico_id'] = $encargoActual['ente_publico_id'];
            }


            $declaracion->save();
        }
    }
    public function store(Request $request)
    {
        $ip_id = Auth::user()->id;

        $bienesInmuebles     = BienesInmuebles::where('ip_id', $ip_id)->get()->toJson();
        $bienesMueblesNR     = BienesMueblesNoRegistrables::where('ip_id', $ip_id)->get()->toJson();
        $bienesMueblesR      = BienesMueblesRegistrables::where('ip_id', $ip_id)->get()->toJson();
        $datosCurriculares     = DatosCurriculares::where('informacion_personal_id', $ip_id)->get()->toJson();
        $encargoActual         = EncargoActual::where('informacion_personal_id', $ip_id)->get()->toJson();
        $fiscal             = Fiscal::where('ip_id', $ip_id)->get()->toJson();
        $dependienteEcon     = DependienteEconomico::where('informacion_personal_id', $ip_id)->get()->toJson();
        $efectivoMetales     = EfectivoMetales::where('ip_id', $ip_id)->get()->toJson();
        $empresas             = Empresas::where('ip_id', $ip_id)->get()->toJson();
        $enajenacion         = Enajenacion::where('ip_id', $ip_id)->get()->toJson();
        $expLaboral         = ExperenciaLaboral::where('informacion_personal_id', $ip_id)->get()->toJson();
        $fideicomisos         = Fideicomiso::where('ip_id', $ip_id)->get()->toJson();
        $infPersonal         = InformacionPersonal::where('id', $ip_id)->get()->toJson();
        $ingresos             = Ingresos::where('ip_id', $ip_id)->get()->toJson();
        $intereses             = Intereses::where('ip_id', $ip_id)->get()->toJson();
        $invCtasValores     = InversionesCuentasValores::where('ip_id', $ip_id)->get()->toJson();
        $membresias         = Membresias::where('ip_id', $ip_id)->get()->toJson();
        $observaciones         = Observaciones::where('ip_id', $ip_id)->orderBy('id', 'desc')->first();
        $otrosIngresos         = OtrosIngresos::where('ip_id', $ip_id)->get()->toJson();
        $deudas             = Deudas::where('ip_id', $ip_id)->get()->toJson();
        $premios             = Premios::where('ip_id', $ip_id)->get()->toJson();
        $sspublicos         = SueldosSalariosPublicos::where('ip_id', $ip_id)->get()->toJson();
        $usoBeneficios         = UsoBeneficios::where('ip_id', $ip_id)->get()->toJson();



        $response =  new BitacoraDeclaracion([
            'ip_id'                             =>    $ip_id,
            'declaracion_id'                     =>    $declaracion->id,
            'bienes_inmuebles'                     =>  $bienesInmuebles,
            'bienes_muebles_registrables'         =>    $bienesMueblesR,
            'bienes_muebles_no_registrables'     =>    $bienesMueblesNR,
            'datos_curriculares'                =>    $datosCurriculares,
            'datos_encargo_actual'                =>    $encargoActual,
            'declaracion_fiscal'                =>    $fiscal,
            'dependientes_economicos'            =>    $dependienteEcon,
            'efectivo_metales'                    =>    $efectivoMetales,
            'empresas_sociedades_asociaciones'    =>    $empresas,
            'enajenacion_bienes'                =>    $enajenacion,
            'experiencia_laboral'                =>    $expLaboral,
            'fideicomisos'                        =>    $fideicomisos,
            'informacion_personal'                =>    $infPersonal,
            'ingresos'                            =>    $ingresos,
            'intereses'                            =>    $intereses,
            'inversiones_cuentas_valores'        =>    $invCtasValores,
            'membresias'                        =>    $membresias,
            'observaciones'                        =>    $observaciones,
            'otros_ingresos'                    =>    $otrosIngresos,
            'pasivos_deudas'                    =>    $deudas,
            'premios'                            =>    $premios,
            'sueldos_salarios_publicos'            =>    $sspublicos,
            'uso_especie_propiedad_tercero'        =>    $usoBeneficios
        ]);

        $response->save();

        return response()->json(['response' => $response]);
    }

    public function actualizaFiscal()
    {

        $declaraciones = BitacoraDeclaracion::whereYear('created_at', 2020)->get();


        foreach ($declaraciones as $declaracion) {

            $declaracion->declaracion_fiscal = Fiscal::where('ip_id', $declaracion->ip_id)->whereYear('created_at', 2020)->get()->toJson();

            $declaracion->save();
        }
    }

    public function actualizaNuevosCampos()
    {

        //llenar los campos que se agregaron,

        $anio = 2020;
        $declaraciones = BitacoraDeclaracion::whereYear('created_at', $anio)->get();

        foreach ($declaraciones as $declaracion) {

            $declaracion->otro_encargo = OtroEncargo::where('informacion_personal_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->toma_decisiones = TomaDecisiones::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->apoyos_publicos = ApoyoBeneficioPublico::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->representacion_activa = Representacion_activa::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->clientes_principales = Clientes_principales::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->beneficios_privados      = BeneficiosPrivados::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();
            $declaracion->prestamo_comodato       = PrestamoComodato::where('ip_id', $declaracion->ip_id)->whereYear('created_at', $anio)->get()->toJson();

            $declaracion->save();
        }
    }
}
