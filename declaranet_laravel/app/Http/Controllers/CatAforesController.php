<?php

namespace App\Http\Controllers;

use App\CatAfore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatAforesController extends Controller
{
    public function index()
    {
        $afores = CatAfore::orderBy('valor', 'ASC')->get();
        return response()->json(['afores' => $afores]);
    }

    public function store(Request $request)
    {
        $afore = CatAfore::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['afore' => $afore]);
    }

    public function delete($id)
    {
        $afore = CatAfore::findOrFail($id);
        $afore->delete();
    }
}