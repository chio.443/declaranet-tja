<?php

namespace App\Http\Controllers;

use App\Token;
use App\Usuarios;
use Carbon\Carbon;
use App\EncargoActual;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\SolicitudTransferencia;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UsuarioTipoDecController;

class SolicitudTransferenciaController extends Controller
{
    public function nuevaTransferencia(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->header("Authorization")) {
                $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
                $token = Token::decodeToken($auth);
            }
            //dd($token);
            $id = $token->uid;

            $transferencia = SolicitudTransferencia::where('ip_id', $request->ip_id)->where('status', 0)->first();
            //buscar si ya existe una solicitud de trtansferencia para el declarante, y no duplicar

            if ($transferencia) {

                $transferencia  = array('id' => 'existe');
            } else {
                $transferencia = new SolicitudTransferencia();
                $transferencia->fill($request->all());
                $transferencia->datos_cambio = $request->datos_cambio;
                $transferencia->datos_actuales = $request->datos_actuales;
                $transferencia->created_by = $id;
                $transferencia->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['solicitud' => $transferencia]);
    }


    public function getSolicitudes(Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;
        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();   //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        if (in_array(20, $permisos)) { //si tiene el 20 es el que aprueba, entonces se muestra todo
            $transferencias = SolicitudTransferencia::where('status', 0)->get();
        } else {
            $transferencias = SolicitudTransferencia::where('ente_publico_destino', $ente)->get();
        }



        return response()->json(['solicitudes' => $transferencias]);
    }
    public function eliminaSolicitud(Request $request, $id)
    {

        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }

        $solicitud = SolicitudTransferencia::findOrFail($id);
        $solicitud->deleted_by = $token->uid;
        $solicitud->save();

        $solicitud->delete();

        return response()->json(['solicitudes' => $solicitud]);
    }
    public function rechazar(Request $request, $id)
    {

        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }

        $solicitud = SolicitudTransferencia::findOrFail($id);
        $solicitud->updated_by = $token->uid;
        $solicitud->status = 2;

        $solicitud->save();

        return response()->json(['solicitudes' => $solicitud]);
    }
    public function aprobar(Request $request, $id)
    {

        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }

        $solicitud = SolicitudTransferencia::findOrFail($id);
        $solicitud->updated_by = $token->uid;
        $solicitud->status = 1;


        $format = 'Y-m-d';

        $cambio = $solicitud->datos_cambio;

        $correo_laboral = '';

        if (array_key_exists('correo_laboral', $cambio)) {
            $correo_laboral = $cambio['correo_laboral'];
        }

        $declarante = InformacionPersonal::findOrFail($solicitud->ip_id);

        $declarante->correo_electronico_laboral = $correo_laboral;


        $declarante->grupo_id = $cambio['grupo_id'];
        $declarante->ente_publico_id = $cambio['ente_publico_id'];
        $declarante->updated_by = $token->uid;
        //$declarante->tipo_mov = 0;
        $declarante->tipo_mov = null;


        $encargo = EncargoActual::where("informacion_personal_id", $solicitud->ip_id)->first();


        //si se reactiva dentro de los 60 dias posteriores a la baja, presenta declaración anual

        $tipo_dec;
        $baja = $encargo->fecha_termino;

        $alta = $cambio['fecha_posesion'];

        // $dias = Carbon::parse($alta)->diffInDays($baja, false);

        $dias = Carbon::parse($baja)->diffInDays($alta, false);

        if ($dias > 60) { //si ya pasaron 60 dias tiene que presentar final e inicial
            //cuando se da de baja se marca como final

        } else {
            $encargo->tipo_dec = 2;
            // $encargo->fecha_termino = null;
        }


        if ($encargo) {
            $encargo->empleo_cargo_comision = $cambio['empleo_cargo_comision'];
            $encargo->area_adscripcion = $cambio['area_adscripcion'];
            $encargo->nivel_encargo = $cambio['nivel_encargo'];
            $encargo->correo_laboral = $correo_laboral;
            $encargo->ente_publico_id = $cambio['ente_publico_id'];
            $encargo->contratado_honorarios = $cambio['contratado_honorarios'];
            $encargo->fecha_posesion = Carbon::parse($cambio['fecha_posesion'])->format($format);
            // $encargo->fecha_termino = ($encargo->tipo_dec == 2) ? null : $encargo->fecha_termino;
            $encargo->fecha_termino = null;
        } else {
            $encargo = new EncargoActual([
                'informacion_personal_id' => $solicitud->ip_id,
                'area_adscripcion' => $cambio['area_adscripcion'],
                'empleo_cargo_comision' => $cambio['empleo_cargo_comision'],
                'nivel_encargo' => $cambio['nivel_encargo'],
                'correo_laboral' => $correo_laboral,
                'ente_publico_id' => $cambio['ente_publico_id'],
                'contratado_honorarios' => $cambio['contratado_honorarios'],
                'fecha_posesion' => Carbon::parse($cambio['fecha_posesion'])->format($format),
                'tipo_dec' => $cambio['tipo_dec'],
                'fecha_termino' => null,
            ]);
        }

        $encargo->updated_by = $token->uid;

        $declarante->save();
        $encargo->save();

        $solicitud->save();


        // Asignar a la tabla usuario tipo declaracion la transferencia realizada

        /* $usuario_tipo_dec = new UsuarioTipoDecController;

        $tipo_dec  = new Request;
        $tipo_dec->ip_id = $solicitud->ip_id;
        $tipo_dec->ente_publico_id = $cambio['ente_publico_id'];
        $tipo_dec->fecha_termino =  $baja;
        $tipo_dec->fecha_posesion = Carbon::parse($cambio['fecha_posesion'])->format($format);

        $tipo_dec_resp = $usuario_tipo_dec->transferencia($tipo_dec); */

        return response()->json(['solicitudes' => $solicitud]);
    }

    public function reabaja(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->header("Authorization")) {
                $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
                $token = Token::decodeToken($auth);
            }
            $format = 'Y-m-d';
            //dd($token);
            $id = $token->uid;
            //guarda para la bitacora solo como historial de la reactivacion
            $transferencia = new SolicitudTransferencia();
            $transferencia->fill($request->all());
            $transferencia->datos_cambio = $request->datos_cambio;
            $transferencia->datos_actuales = $request->datos_actuales;
            $transferencia->created_by = $id;
            $transferencia->status = 1;
            $transferencia->save();

            $cambio = $request->datos_cambio;

            $declarante = InformacionPersonal::findOrFail($request->ip_id);

            $correo_laboral = '';

            if (array_key_exists('correo_laboral', $cambio)) {
                $correo_laboral = $cambio['correo_laboral'];
            }

            $declarante->correo_electronico_laboral = $correo_laboral;
            // $declarante->grupo_id = $cambio['grupo_id'];
            $declarante->ente_publico_id = $cambio['ente_publico_id'];
            $declarante->tipo_mov = null;
            $declarante->simplificada = $cambio['simplificada'];
            $declarante->updated_by = $id;

            $encargo = EncargoActual::where("informacion_personal_id", $request->ip_id)->first();

            //si se reactiva dentro de los 60 dias posteriores a la baja, presenta declaración anual
            // Si la solicitud tiene periodo
            $periodo =  array_key_exists('periodo', $cambio) ? $cambio['periodo'] : null;

            // // Si se seleccione el tipo de declaracion a realizar
            $tipo_dec = $request->tipo_dec ? $request->tipo_dec : 2;

            // $tipo_dec = 2;
            $baja = $encargo->fecha_termino;

            $alta = $cambio['fecha_posesion'];

            $dias = Carbon::parse($alta)->diffInDays($baja, false);

            if ($dias > 60) {
                //si ya pasaron 60 dias tiene que presentar final e inicial
                //cuando se da de baja se marca como final
                $tipo_dec = 1;
            }

            $encargo->empleo_cargo_comision = $cambio['empleo_cargo_comision'];
            $encargo->area_adscripcion = $cambio['area_adscripcion'];
            $encargo->nivel_encargo = $cambio['nivel_encargo'];
            $encargo->correo_laboral = $correo_laboral;
            $encargo->ente_publico_id = $cambio['ente_publico_id'];
            $encargo->contratado_honorarios = $cambio['contratado_honorarios'];

            $encargo->tipo_dec = $tipo_dec;
            // dd(Carbon::parse($cambio['fecha_posesion'])->format($format));
            $encargo->fecha_posesion = Carbon::parse($cambio['fecha_posesion'])->format($format);
            $encargo->rfc_patron = $cambio['rfc_patron'];
            $encargo->telefono_laboral_numero = $cambio['telefono_laboral_numero'];
            $encargo->telefono_laboral_extension = $cambio['telefono_laboral_extension'];

            $encargo->direccion_encargo_colonia = $cambio['direccion_encargo_colonia'];
            $encargo->direccion_encargo_cp = $cambio['direccion_encargo_cp'];
            $encargo->direccion_encargo_entidad_federativa_id = $cambio['direccion_encargo_entidad_federativa_id'];
            $encargo->direccion_encargo_localidad_id = $cambio['direccion_encargo_localidad_id'];
            $encargo->direccion_encargo_municipio_id = $cambio['direccion_encargo_municipio_id'];
            $encargo->direccion_encargo_numext = $cambio['direccion_encargo_numext'];
            $encargo->direccion_encargo_numint = $cambio['direccion_encargo_numint'];
            $encargo->direccion_encargo_pais_id = $cambio['direccion_encargo_pais_id'];
            $encargo->direccion_vialidad_nombre = $cambio['direccion_vialidad_nombre'];
            $encargo->direccion_vialidad_tipo = $cambio['direccion_vialidad_tipo'];
            $encargo->domicilio_obs = $cambio['domicilio_observaciones'];

            $encargo->fecha_termino = null;

            $encargo->updated_by = $id;

            $declarante->save();

            $encargo->save();
            // Asignar a la tabla usuario tipo declaracion la transferencia realizada
            $usuario_tipo_dec = new UsuarioTipoDecController;

            $tipo_dec  = new Request;
            $tipo_dec->ip_id = $request->ip_id;
            $tipo_dec->ente_publico_id = $cambio['ente_publico_id'];
            $tipo_dec->fecha_termino = $baja;
            $tipo_dec->fecha_posesion = $alta;

            $tipo_dec->tipo_dec = $tipo_dec;

            if (!empty($periodo)) {
                $tipo_dec->periodo =  $periodo;
            }

            $tipo_dec_resp = $usuario_tipo_dec->transferencia($tipo_dec);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['mensaje' => 'ok']);
    }
}
