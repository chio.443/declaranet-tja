<?php

namespace App\Http\Controllers;

use App\CatTipoOperacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoOperacionController extends Controller
{
    public function index()
    {
        $tipos = CatTipoOperacion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoOperacion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoOperacion::findOrFail($id);
        $tipo->delete();
    }
}