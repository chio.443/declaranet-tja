<?php

namespace App\Http\Controllers;

use App\CuentasPorCobrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CuentasPorCobrarController extends Controller
{
    public function index()
    {
        $cuentas = CuentasPorCobrar::orderBy('id', 'DESC')->get();
        return response()->json(['cuentas' => $cuentas]);
    }

    public function item($id)
    {
        $cuentas = CuentasPorCobrar::where('ip_id', $id)->get();
        return response()->json(['cuentas' => $cuentas]);
    }

    public function store(Request $request)
    {
        $cuenta = CuentasPorCobrar::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'nombre' => $request->nombre,
                'numero_registro' => $request->numero_registro,
                'sector_industria_id' => $request->sector_industria_id,
                'fecha_prestamo' => $request->fecha_prestamo,
                'monto_original_prestamo' => $request->monto_original_prestamo,
                'tasa_interes' => $request->tasa_interes,
                'saldo_pendiente' => $request->saldo_pendiente,
                'fecha_vencimiento' => $request->fecha_vencimiento,
                'porcentaje_copropiedad' => $request->porcentaje_copropiedad,
                'nombre_copropietario' => $request->nombre_copropietario,
                'observaciones' => $request->observaciones,
                'domicilio_pais_id' => $request->domicilio_pais_id,
                'domicilio_entidad_federativa_id' => $request->domicilio_entidad_federativa_id,
                'domicilio_municipio_id' => $request->domicilio_municipio_id,
                'domicilio_cp' => $request->domicilio_cp,
                'domicilio_localidad_id' => $request->domicilio_localidad_id,
                'domicilio_colonia' => $request->domicilio_colonia,
                'domicilio_vialidad_tipo' => $request->domicilio_vialidad_tipo,
                'domicilio_vialidad_nombre' => $request->domicilio_vialidad_nombre,
                'domicilio_numExt' => $request->domicilio_numExt,
                'domicilio_numInt' => $request->domicilio_numInt,
            ]
        );

        return response()->json(['cuenta' => $cuenta]);
    }

    public function delete($id)
    {
        $cuenta = CuentasPorCobrar::findOrFail($id);
        $cuenta->delete();
    }
}