<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExperenciaLaboral;
use Storage;
use Auth;
use \App\Token;
use App\Aclaraciones;
use App\SolicitudModificacion;
use App\InformacionPersonal;
use App\Notificacion;
use DB;

class AclaracionesController extends Controller
{
    public function index()
    {
        $dec = Aclaraciones::orderBy('id', 'DESC')->get();
        return response()->json(['dec' => $dec]);
    }

    public function item(Request $request, $id)
    {
        // $anioact    =  date("Y");

        $files = Aclaraciones::where('ip_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(['files' => $files]);
    }

    public function ImportPdf(Request $request)
    {
        $id2 = Auth::user()->id;
        $year = date("Y");

        $uniqueFileName = uniqid() . "_" . $id2 . "_" . $year . '.' . $request->file('upload_file')->getClientOriginalExtension();

        $request->file('upload_file')->move(public_path('declaraciones_aclaraciones'), $uniqueFileName);

        $response = Aclaraciones::updateOrCreate(
            ['id' => $request->id],
            [
                'file' => $uniqueFileName,
                'ip_id' => $id2
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $uid = $token->uid;
        $anioact    =  date("Y");
        $municipio = $token->municipio;
        $ente = $token->ente_publico_id;
        $nombre = $token->nombre;

        $response = SolicitudModificacion::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $uid,
                'municipio_id' => $municipio,
                'ente_publico_id' => $ente,
                'descripcion' => $request->descripcion,
                'status' => 0,
            ]
        );

        if ($response) {
            $servidor = InformacionPersonal::findOrFail($uid);
            // dd($servidor->rfc);
            $notificacion = new Notificacion();
            //$notificacion->id_movimiento = $movimiento->id;
            $notificacion->texto = "Solicitud de modificación de declaración patrimonial";
            $notificacion->texto_desc = "Se ha registrado solicitud de modificación de la declaración patrimonial del servidor " . $servidor->rfc . ".";
            $notificacion->id_ip = $uid;
            $notificacion->id_solicitud = $response->id;
            $notificacion->id_tipo = 3;
            $notificacion->save();
            //crear notificación
        }

        return response()->json(['response' => $response]);
    }

    public function getsolicitudes(Request $request)
    {
        $response = SolicitudModificacion::with('servidor')
            ->orderBy('status', 'ASC')
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(['response' => $response]);
    }

    public function solicitudes(Request $request, $id)
    {
        $response = SolicitudModificacion::where('ip_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(['response' => $response]);
    }

    public function changeSolicitud(Request $request)
    {
        // dd($request);
        $solicitud = SolicitudModificacion::findOrFail($request->id);
        $solicitud->status = $request->status;
        $solicitud->respuesta = $request->respuesta;
        $solicitud->save();

        // Eliminar la notificacion
        $notificacion =  Notificacion::where('id_solicitud', $request->id)->first();
        if ($notificacion) {
            $notificacion->delete();
        }
        return response()->json(['response' => $solicitud]);
    }

    public function cron_actualizar()
    {
        /*
        $pass = RecuperaPass::where('enviado', '=', '0')->get();

        RecuperaPass::where('enviado', '0')
            ->update(['enviado' => '1']);

        return response()->json(['users' => $pass]);
        */
    }

    public function delete($id)
    {
        $tipo = Aclaraciones::findOrFail($id);
        $tipo->delete();
    }
}