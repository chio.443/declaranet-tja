<?php

namespace App\Http\Controllers;

use App\CatTipoMonedaMetales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoMonedaMetalesController extends Controller
{
    public function index()
    {
        $metales = CatTipoMonedaMetales::orderBy('valor', 'ASC')->get();
        return response()->json(['metales' => $metales]);
    }

    public function store(Request $request)
    {
        $metal = CatTipoMonedaMetales::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['metal' => $metal]);
    }

    public function delete($id)
    {
        $metal = CatTipoMonedaMetales::findOrFail($id);
        $metal->delete();
    }
}