<?php

namespace App\Http\Controllers;

use Auth;
use \App\Token;
use Carbon\Carbon;
use App\CatVialidad;
//use App\DependienteBeneficiario;
use App\DependenciaEntDep;
use Illuminate\Http\Request;
use App\DependienteEconomico;
use App\DependienteNacionalidad;
use Illuminate\Support\Facades\DB;

class DependientesEconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dependientes = DependienteEconomico::orderBy('id', 'asc')->get();
        return response()->json(['dependientes' => $dependientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        if (Auth::id() != $id) {
            return response()->json(['errorSession' => 'error']);
        }

        $dependientes = DependienteEconomico::where('informacion_personal_id', $id)->orderBy('id', 'asc')->get();

        return response()->json(['dependientes' => $dependientes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            ////////////////////vialidad///////////////////
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);

            $id = $token->uid;

            if (Auth::id() != $id) {
                return response()->json(['errorSession' => 'error']);
            }

            $nacs = $request->nac;

            $format = 'Y-m-d';

            $informacion = $request->except(['nac', 'vialidad', 'tipo_vial', 'nom_vial', 'vialidad_nombre', 'tipo_vialidad', 'programa', 'entidad', 'nombre_programa', 'institucion_otorga_apoyo', 'tipo_apoyo_id', 'valor_apoyo', 'observacionesprog', 'dependencia', 'funcionesp', 'numero_identificacion_nacional']);

            $informacion['fecha_nacimiento'] = Carbon::parse($request->fecha_nacimiento);
            $informacion['fecha_inicio'] = Carbon::parse($request->fecha_inicio);

            $registro = DependienteEconomico::updateOrCreate(
                ['informacion_personal_id' => $id, 'id' => $request['id']],
                $informacion
            );

            $registro->save();
            ////


            //si es beneficiario de algun programa publico, se guarda en la tabla de los programas

            if (isset($request['dependencia.id'])) {
                $iddep = $request['dependencia.id'];
            } else {
                $iddep = null;
            }


            if ($request['desempenia_admon_pub'] === "1") { //si es beneficiario de un programa publico, guarar los datos
                $fecha_inicio = Carbon::parse($request['fecha_inicio']);

                $dependencia = DependenciaEntDep::updateOrCreate(
                    ['id'  => $iddep],
                    [
                        'depec_id'                => $registro->id,
                        'nombre_dependencia'      => $request['nombre_dependencia'],
                        'fecha_inicio'       => $fecha_inicio,
                        'fecha_fin'         => $request['fecha_fin']
                    ]
                );
                $dependencia->save();
                //////////////////////////////////////////////

            }



            ///////////////////////////////////
            ///////////////////////nacionalidades/////////////
            /////primero borrar las que ya estan para no duplicar, no se si haya un método mejor
            $nacionalidades = DependienteNacionalidad::where('depec_id', $registro->id);
            $nacionalidades->forceDelete();


            $num = count($nacs);

            for ($i = 0; $i < $num; $i++) {
                $nacionalidad =  new DependienteNacionalidad([
                    'depec_id' =>      $registro->id,
                    'nac_id' =>  $request->input('nac.' . $i . '')
                ]);

                $nacionalidad->save();
            }

            ///////////////////////
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['registro' => $registro->refresh()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dependientes = DependienteEconomico::findOrFail($id);
        $dependientes->delete();

        $dependientes = DependenciaEntDep::where('depec_id', $id);
        $dependientes->delete();
    }
}
