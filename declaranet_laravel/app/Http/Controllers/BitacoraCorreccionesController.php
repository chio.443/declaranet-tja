<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Token;
use App\Declaracion;
use App\EncargoActual;
use App\CatEntePublico;
use App\BitacoraCorreccion;
use App\BitacoraDeclaracion;
use Illuminate\Http\Request;

use App\UsuarioTipoDeclaracion;
//use Carbon\Carbon;

class BitacoraCorreccionesController extends Controller
{
    public function item(Request $request)
    {

        $bitacora = BitacoraDeclaracion::select('bitacora_declaraciones.id', 'bitacora_declaraciones.informacion_personal', 'bitacora_declaraciones.datos_encargo_actual', 'declaraciones.tipo_declaracion_id', 'declaraciones.periodo')
            ->leftJoin('declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->leftJoin('usuario_tipo_dec', 'usuario_tipo_dec.declaracion_id', '=', 'declaraciones.id')
            ->where('bitacora_declaraciones.ip_id', $request->ip_id)
            ->where('bitacora_declaraciones.declaracion_id', $request->declaracion_id)
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('usuario_tipo_dec.deleted_at')
            ->whereNull('bitacora_declaraciones.deleted_at')
            ->orderBy('bitacora_declaraciones.created_at', 'desc')->first();

        return response()->json(['bitacora' => $bitacora]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
            $aid = $token->uid;

            $bitacora = BitacoraDeclaracion::select('id', 'datos_encargo_actual', 'informacion_personal')
                ->where('id', $request->id)
                ->where('ip_id', $request->ip_id)
                ->where('declaracion_id', $request->declaracion_id)
                ->first();

            if (empty($bitacora)) {
                return response()->json(['errors' => ['message' => ['No se encontro bitacora']]], 422);
            }

            $origen = clone ($bitacora);

            $encargo = json_decode($bitacora['datos_encargo_actual']);
            $infoBita = json_decode($bitacora['informacion_personal']);

            $ente = CatEntePublico::where('id', $request->dependencia_destino_id)->first()->getAttributes(); //->toJson();

            if ($ente) {
                $ente = (object)$ente;
            }

            if (isset($encargo[0])) {
                // $encargoOrigen = clone ((object) $encargo[0]);
                $encargoCambio = clone ((object) $encargo[0]);

                if (property_exists($encargoCambio, "ente_publico_id")) {
                    $encargoCambio->ente_publico_id = $request->dependencia_destino_id;
                }
                if (property_exists($encargoCambio, "ente")) {
                    $encargoCambio->ente = $ente;
                    $bitacora->datos_encargo_actual = json_encode([$encargoCambio]);
                }
            } else {
                return response()->json(['errors' => ['message' => ['Error en el encargo']]], 422);
            }

            if (isset($infoBita[0])) {
                $infoBitaCambio = clone ((object) $infoBita[0]);

                if (property_exists($infoBitaCambio, "ente_publico_id")) {
                    $infoBitaCambio->ente_publico_id = $request->dependencia_destino_id;
                }

                if (property_exists($infoBitaCambio, "encargo")) {

                    if (property_exists($encargoCambio, "ente")) {
                        $infoBitaCambio->encargo = $encargoCambio;
                        $bitacora->informacion_personal = json_encode([$infoBitaCambio]);
                    }
                }
            } else {
                return response()->json(['errors' => ['message' => ['Error en el encargo']]], 422);
            }

            $bitacora->save();

            $declaracion = Declaracion::where('id', $request->declaracion_id)->first();

            if (empty($declaracion)) {
                return response()->json(['errors' => ['message' => ['No se encontro declaracion']]], 422);
            }

            $declaracion_origen =  clone ($declaracion);

            if ($request->tipo_declaracion_id == 2) {
                $tipo = 'Anual';
            } else if ($request->tipo_declaracion_id == 3) {
                $tipo = 'Final';
            } else {
                $tipo = 'Inicial';
            }

            $declaracion->update(['periodo' => $request->periodo, 'Tipo_Dec' => $tipo, 'tipo_declaracion_id' => $request->tipo_declaracion_id]);

            $utd = UsuarioTipoDeclaracion::where('declaracion_id', $request->declaracion_id)
                ->where('ip_id', $request->ip_id)
                ->first();

            if (!empty($utd)) {
                $utd->tipo_dec_id = $request->tipo_declaracion_id;
                $utd->periodo = $request->periodo;
                $utd->save();
            }

            $response = BitacoraCorreccion::updateOrCreate(
                ['id' => null],
                [
                    'ip_id' => $request->ip_id,
                    'declaracion_id' => $request->declaracion_id,
                    'bitacora_declaracion_id' => $request->id,
                    'origen' => $origen,
                    'cambio' => $bitacora,
                    'orgen_declaracion' => $declaracion_origen,
                    'cambio_declaracion' => $declaracion,
                    'tipo_declaracion_id' => $request->tipo_declaracion_id,
                    'periodo' => $request->periodo,
                    'created_by' => $aid,
                    'updated_by' => $aid,
                ]
            );
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['correccion' => $response]);
    }
}
