<?php

namespace App\Http\Controllers;

use DB;

use Auth;
use \App\Token;
use Carbon\Carbon;
use App\CatVialidad;
use App\Declaracion;
use App\EncargoActual;
use App\IGNacionalidades;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\Http\Controllers\UsuarioTipoDecController;

class InfPersonalController extends Controller
{
    public function index()
    {
        $usuarios = InformacionPersonal::with('ente', 'encargo')
            ->whereHas(
                "encargo",
                function ($q) {
                    $q->where("fecha_termino", null);
                }
            )
            ->orderBy('nombres', 'ASC')->get();

        return response()->json(['usuarios' => $usuarios]);
    }

    public function item($ente)
    {
        $usuarios = InformacionPersonal::with('ente', 'encargo',  'utdeclaraciones')
            ->whereHas("encargo", function ($q) use ($ente) {
                $q->where("ente_publico_id", "=", $ente);
                //->where("fecha_termino",null);
            })
            ->without(['vialidad', 'nac', 'declaracion'])
            //                ->whereNotIn('id',function($query) {
            // 				   $query->select('id')
            // 				   ->from('informacion_personal')
            // 				   ->where('tipo_mov',2);
            // })
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();

        $usuarios = $usuarios->map(function ($item, $key) {
            // $nombre = $item->nombres . ' ' . $item->primer_apellido . ' ' . $item->segundo_apellido;

            $object = new \stdClass;
            $object->id = $item->id;
            $object->rfc = $item->rfc;
            $object->curp = $item->curp;
            $object->grupo_id = $item->grupo_id;
            $object->correo_electronico_laboral = $item->correo_electronico_laboral;

            $object->nombres = $item->nombres;
            $object->primer_apellido = $item->primer_apellido;
            $object->segundo_apellido = $item->segundo_apellido;
            $object->ente = $item->encargo->ente->valor;
            $object->ente_publico_id = $item->encargo->ente->id;
            $object->area = $item->encargo->area_adscripcion;
            $object->cargo = $item->encargo->empleo_cargo_comision;
            $object->nivel = $item->encargo->nivel_encargo;
            $object->simplificada = $item->simplificada;
            $object->tipo_mov = $item->tipo_mov;
            $object->titular = $item->encargo->titular;
            $object->contratado_honorarios = $item->encargo->contratado_honorarios;
            $object->fecha_posesion = Carbon::parse($item->encargo->fecha_posesion)->format('Y-m-d'); //  $item->encargo->fecha_posesion;
            $object->fecha_termino = $item->encargo->fecha_termino;
            $object->alta = $item->created_at; // Revisar
            $object->baja = $item->encargo->fecha_termino;

            // $object->encargo = $item->encargo;
            // $object->pendientes = $item->pendientes;
            $object->utdeclaraciones = $item->utdeclaraciones;

            return $object;
        });

        return response()->json(['usuarios' => $usuarios]);
    }
    public function item2($ente, $desde, $hasta)
    {
        $range = $desde . '-' . $hasta;
        $usuarios = InformacionPersonal::with('ente', 'encargo',  'utdeclaraciones')
            ->whereHas("encargo", function ($q) use ($ente) {
                $q->where("ente_publico_id", "=", $ente);
                //->where("fecha_termino",null);
            })
            ->without(['vialidad', 'nac', 'declaracion'])
            // ->whereNotIn('id',function($query) {
            //                    $query->select('id')
            //                    ->from('informacion_personal')
            //                    ->where('tipo_mov',2);
            // })
            // REGEXP
            ->where('rfc', '~', "^[{$range}].*$")
            // ->where('rfc', '>=', "'" . $desde . "'")
            // ->where('rfc', '<=', "'" . $hasta . "'")
            ->orderBy('rfc', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();

        $usuarios = $usuarios->map(function ($item, $key) {
            // $nombre = $item->nombres . ' ' . $item->primer_apellido . ' ' . $item->segundo_apellido;

            $object = new \stdClass;
            $object->id = $item->id;
            $object->rfc = $item->rfc;
            $object->curp = $item->curp;
            $object->grupo_id = $item->grupo_id;
            $object->correo_electronico_laboral = $item->correo_electronico_laboral;

            $object->nombres = $item->nombres;
            $object->primer_apellido = $item->primer_apellido;
            $object->segundo_apellido = $item->segundo_apellido;
            $object->ente = $item->encargo->ente->valor;
            $object->ente_publico_id = $item->encargo->ente->id;
            $object->area = $item->encargo->area_adscripcion;
            $object->cargo = $item->encargo->empleo_cargo_comision;
            $object->nivel = $item->encargo->nivel_encargo;
            $object->simplificada = $item->simplificada;
            $object->tipo_mov = $item->tipo_mov;
            $object->titular = $item->encargo->titular;
            $object->contratado_honorarios = $item->encargo->contratado_honorarios;
            $object->fecha_posesion = Carbon::parse($item->encargo->fecha_posesion)->format('Y-m-d'); //  $item->encargo->fecha_posesion;
            $object->fecha_termino = $item->encargo->fecha_termino;
            $object->alta = $item->created_at; // Revisar
            $object->baja = $item->encargo->fecha_termino;

            // $object->encargo = $item->encargo;
            // $object->pendientes = $item->pendientes;
            $object->utdeclaraciones = $item->utdeclaraciones;

            return $object;
        });

        return response()->json(['usuarios' => $usuarios]);
    }

    public function filtrorfc($rfc)
    {
        $usuarios = InformacionPersonal::with('ente', 'encargo', 'utdeclaraciones')
            ->without(['vialidad', 'nac', 'declaracion'])
            ->where('rfc', 'like', '%' . strtoupper($rfc) . '%')
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();

        $usuarios2 = $usuarios->map(function ($item, $key) {
            // $nombre = $item->nombres . ' ' . $item->primer_apellido . ' ' . $item->segundo_apellido;

            $object = new \stdClass;
            $object->id = $item->id;
            $object->rfc = $item->rfc;
            $object->curp = $item->curp;
            $object->grupo_id = $item->grupo_id;
            $object->correo_electronico_laboral = $item->correo_electronico_laboral;

            $object->nombres = $item->nombres;
            $object->primer_apellido = $item->primer_apellido;
            $object->segundo_apellido = $item->segundo_apellido;
            $object->ente = $item->encargo->ente->valor;
            $object->ente_publico_id = $item->encargo->ente->id;
            $object->area = $item->encargo->area_adscripcion;
            $object->cargo = $item->encargo->empleo_cargo_comision;
            $object->nivel = $item->encargo->nivel_encargo;
            $object->simplificada = $item->simplificada;
            $object->tipo_mov = $item->tipo_mov;
            $object->titular = $item->encargo->titular;
            $object->contratado_honorarios = $item->encargo->contratado_honorarios;

            $object->fecha_posesion = Carbon::parse($item->encargo->fecha_posesion)->format('Y-m-d'); //  $item->encargo->fecha_posesion;
            $object->fecha_termino = $item->encargo->fecha_termino;
            $object->alta = $item->created_at; // Revisar
            $object->baja = $item->encargo->fecha_termino;

            // $object->encargo = $item->encargo;
            // $object->pendientes = $item->pendientes;
            $object->utdeclaraciones = $item->utdeclaraciones;

            return $object;
        });

        return response()->json(['usuarios' => $usuarios2]);
    }

    public function filtrorfcaltas($rfc)
    {
        $usuario = InformacionPersonal::with('ente', 'encargo', 'pendientes', 'utdeclaraciones')
            ->without(['vialidad', 'nac', 'declaracion'])
            ->where('rfc', 'like', strtoupper($rfc) . '%')
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->first();

        if (empty($usuario)) {
            return response()->json(['usuarios' => []]);
        }

        $usuario2 = new \stdClass;

        $usuario2->id = $usuario->id;
        $usuario2->rfc = $usuario->rfc;
        $usuario2->curp = $usuario->curp;
        $usuario2->nombres = $usuario->nombres;
        $usuario2->primer_apellido = $usuario->primer_apellido;
        $usuario2->segundo_apellido = $usuario->segundo_apellido;
        $usuario2->tipo_mov = $usuario->tipo_mov;
        $usuario2->simplificada = $usuario->simplificada;
        $usuario2->fecha_nacimiento = $usuario->fecha_nacimiento;
        $usuario2->alta = $usuario->created_at; // Revisar

        $usuario2->correo_electronico_laboral = $usuario->correo_electronico_laboral;
        $usuario2->correo_laboral = $usuario->encargo->correo_laboral;

        $usuario2->ente = $usuario->encargo->ente->valor;
        $usuario2->ente_publico_id = $usuario->encargo->ente->id;
        $usuario2->area_adscripcion = $usuario->encargo->area_adscripcion;
        $usuario2->empleo_cargo_comision = $usuario->encargo->empleo_cargo_comision;
        $usuario2->nivel_encargo = $usuario->encargo->nivel_encargo;
        $usuario2->titular = $usuario->encargo->titular;
        $usuario2->contratado_honorarios = $usuario->encargo->contratado_honorarios;
        $usuario2->fecha_posesion = $usuario->encargo->fecha_posesion;
        // Carbon::parse($usuario->encargo->fecha_posesion)->format('Y-m-d');
        $usuario2->fecha_termino = $usuario->encargo->fecha_termino;
        $usuario2->telefono_laboral_numero = $usuario->encargo->telefono_laboral_numero;
        $usuario2->telefono_laboral_extension = $usuario->encargo->telefono_laboral_extension;
        $usuario2->rfc_patron = $usuario->encargo->rfc_patron;
        $usuario2->direccion_encargo_pais_id = $usuario->encargo->direccion_encargo_pais_id;
        $usuario2->direccion_encargo_entidad_federativa_id = $usuario->encargo->direccion_encargo_entidad_federativa_id;
        $usuario2->direccion_encargo_municipio_id = $usuario->encargo->direccion_encargo_municipio_id;
        $usuario2->direccion_encargo_localidad_id = $usuario->encargo->direccion_encargo_localidad_id;
        $usuario2->direccion_encargo_cp = $usuario->encargo->direccion_encargo_cp;
        $usuario2->direccion_vialidad_nombre = $usuario->encargo->direccion_vialidad_nombre;
        $usuario2->direccion_vialidad_tipo = $usuario->encargo->direccion_vialidad_tipo;
        $usuario2->direccion_encargo_numext = $usuario->encargo->direccion_encargo_numext;
        $usuario2->direccion_encargo_numint = $usuario->encargo->direccion_encargo_numint;
        $usuario2->direccion_encargo_colonia = $usuario->encargo->direccion_encargo_colonia;
        $usuario2->domicilio_observaciones = $usuario->encargo->domicilio_observaciones;


        $usuario2->utdeclaraciones = $usuario->utdeclaraciones;


        return response()->json(['usuarios' => $usuario2]);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {

        //alta de personal manual
        $format = 'Y-m-d';

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $uid = $token->uid;

        $existe = InformacionPersonal::where(function ($q) use ($request) {
            $q->where('rfc', $request->rfc)
                ->orWhere('curp', $request->curp);
        })
            ->first();

        if (!empty($existe['id'])) {

            if (!empty($existe->encargo->fecha_termino)) {
                return response()->json(['baja' => 'si', 'mensaje' => $existe]);
            } else {
                return response()->json(['mensaje' => $existe]);
            }
            // $baja = InformacionPersonal::where('tipo_mov', 2)
            //     ->where(function ($q) use ($request) {
            //         $q->where('rfc', $request->rfc)
            //             ->orWhere('curp', $request->curp);
            //     })
            //     ->first();
            // if ($baja) {
            //     return response()->json(['baja' => 'si', 'mensaje' => $baja]);
            // } else {
            //     return response()->json(['mensaje' => $existe]);
            // }
        } else {
            /* $simplificada='0';

            if($request->grupo_id ==3)
                $simplificada = '1'; */
            try {
                DB::beginTransaction();

                $declarante = new InformacionPersonal([
                    'curp' => $request->curp,
                    'nombres' => $request->nombres,
                    'primer_apellido' => $request->primer_apellido,
                    'rfc' => $request->rfc,
                    'segundo_apellido' => $request->segundo_apellido,
                    'fecha_nacimiento' => Carbon::parse($request->fecha_nacimiento)->format($format),
                    'ente_publico_id' => $request->ente_publico_id,
                    'grupo_id' => $request->grupo_id,
                    'correo_electronico_laboral' => $request->correo_laboral,
                    'rfc' => $request->rfc,
                    'created_by' => $uid,
                    'password' => md5($request->rfc),
                    'simplificada' => $request->simplificada,

                ]);

                $declarante->save();

                $tipo_dec;
                $anioact    =  date("Y");
                $ingreso  =  Carbon::parse($request->fecha_posesion)->format('Y');

                if ($ingreso == $anioact || $request->simplificada == '1') {
                    $tipo_dec = 1;
                } else {
                    $tipo_dec = 2;
                }
                // Si se seleccione el tipo de declaracion a realizar
                $tipo_dec = $request->tipo_dec ? $request->tipo_dec : $tipo_dec;

                $declaranteEnc = new EncargoActual([
                    'informacion_personal_id' => $declarante->id,
                    'area_adscripcion' => $request->area_adscripcion,
                    'contratado_honorarios' => $request->contratado_honorarios,
                    'correo_laboral' => $request->correo_laboral,
                    'direccion_encargo_colonia' => $request->direccion_encargo_colonia,
                    'direccion_encargo_cp' => $request->direccion_encargo_cp,
                    'direccion_encargo_entidad_federativa_id' => $request->direccion_encargo_entidad_federativa_id,
                    'direccion_encargo_localidad_id' => $request->direccion_encargo_localidad_id,
                    'direccion_encargo_municipio_id' => $request->direccion_encargo_municipio_id,
                    'direccion_encargo_numext' => $request->direccion_encargo_numext,
                    'direccion_encargo_numint' => $request->direccion_encargo_numint,
                    'direccion_encargo_pais_id' => $request->direccion_encargo_pais_id,
                    'direccion_vialidad_nombre' => $request->direccion_vialidad_nombre,
                    'direccion_vialidad_tipo' => $request->direccion_vialidad_tipo,
                    'empleo_cargo_comision' => $request->empleo_cargo_comision,
                    'ente_publico_id' => $request->ente_publico_id,
                    'fecha_posesion' => Carbon::parse($request->fecha_posesion)->format($format),
                    'nivel_encargo' => $request->nivel_encargo,
                    'telefono_laboral_extension' => $request->telefono_laboral_extension,
                    'tipo_dec' => $tipo_dec,
                    'rfc_patron' => $request->rfc_patron,
                    'telefono_laboral_numero' => $request->telefono_laboral_numero,
                    'domicilio_obs' => $request->domicilio_observaciones

                ]);

                $declaranteEnc->save();


                // Insertar registro de usuario tipo declaracion
                $usuario_tipo_dec = new UsuarioTipoDecController;

                // Si la solicitud tiene periodo
                $periodo = $request->periodo ? $request->periodo : null;

                $fecha_posesion =  Carbon::parse($request->fecha_posesion)->format($format);

                $u_tipo_dec  = new Request;
                $u_tipo_dec->ip_id = $declarante->id;
                $u_tipo_dec->tipo_dec_id = $tipo_dec;
                $u_tipo_dec->ente_publico_id = $request->ente_publico_id;
                $u_tipo_dec->fecha_posesion = $fecha_posesion;
                if (!empty($periodo)) {
                    $u_tipo_dec->periodo =  $periodo;
                }

                $tipo_dec_resp = $usuario_tipo_dec->nuevoUsuario($u_tipo_dec);

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }

            return response()->json(['nuevo' => $declarante, 'mensaje' => ['id' => 'Ok']]);
        }
    }

    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        if (Auth::id() != $id) {
            return response()->json(['errorSession' => 'error']);
        }

        $informacion = InformacionPersonal::find($id);

        return response()->json(['informacion' => $informacion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //toda la informaicon personal

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        if (Auth::id() != $id) {
            return response()->json(['errorSession' => 'error']);
        }

        $registro = InformacionPersonal::findOrFail($id);

        $nacs = $request->nac;

        $infPersonal = $request->except(['vialidad', 'nac', 'tipo_vial', 'nom_vial', 'rfc', 'curp', 'encargo', 'declaracion']);
        //guardar todos los datos
        $registro->fill($infPersonal);
        //dd($registro);
        $registro->save();

        ///////////////////////nacionalidades/////////////
        /////primero borrar las que ya estan para no duplicar, no se si haya un método mejor
        $nacionalidades = IGNacionalidades::where('ip_id', $id);
        $nacionalidades->forceDelete();


        $num = count($nacs);

        for ($i = 0; $i < $num; $i++) {
            $nacionalidad = new IGNacionalidades([
                'ip_id' => $id,
                'nac_id' => $request->input('nac.' . $i . '')
            ]);

            $nacionalidad->save();
        }

        ///////////////////////

        return response()->json(['registro' => $registro->refresh()]);
    }

    public function resetpass(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        //dd($request);

        $declarante = InformacionPersonal::find($request->id);

        $declarante->password = DB::raw("md5('" . $declarante->rfc . "')");

        $declarante->save();

        return response()->json(['response' => $declarante]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);

        $aid = $token->uid;

        //buscar si ya hay declaracion
        $declaracion = Declaracion::where('id_ip', $id)->first();

        //si no hay declaracion
        if (!$declaracion) {

            $status = 0;

            $declarante = InformacionPersonal::where('id', $id);

            $declarante->update(['deleted_by' => DB::raw($aid)]);

            $declarante->delete();


            $encargo = EncargoActual::where('informacion_personal_id', $id);

            $encargo->update(['deleted_by' => DB::raw($aid)]);

            $encargo->delete();

            return response()->json(['declarante' => $declarante]);
        } else {
            return response()->json(['error' => $declaracion]);
        }
    }
}
