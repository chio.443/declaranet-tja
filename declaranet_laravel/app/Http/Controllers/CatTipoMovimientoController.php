<?php

namespace App\Http\Controllers;

use App\CatTipoMovimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoMovimientoController extends Controller
{
    public function index()
    {
        $movimientos = CatTipoMovimiento::orderBy('tipo', 'ASC')->get();
        return response()->json(['movimientos' => $movimientos]);
    }

    public function store(Request $request)
    {
        $titular = CatTipoMovimiento::updateOrCreate(
            ['id' => $request->id],
            [
                'tipo' => $request->tipo,
            ]
        );

        return response()->json(['titular' => $titular]);
    }

    public function delete($id)
    {
        $titular = CatTipoMovimiento::findOrFail($id);
        $titular->delete();
    }
}
