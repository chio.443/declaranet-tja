<?php

namespace App\Http\Controllers;

use App\Deudas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Token;
use Auth;
use Carbon\Carbon;


class DeudasController extends Controller
{

    public function index()
    {
        $response = Deudas::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $response = Deudas::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $id = Auth::user()->id;

        $response   = Deudas::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $id,
                'titular' => $request->titular,
                'cat_tipo_adeudo_id' => $request->cat_tipo_adeudo_id,
                'especifique' => $request->especifique,
                'identificador_deuda' => $request->identificador_deuda,
                'nombre_tercero' => $request->nombre_tercero,
                'rfc_tercero' => $request->rfc_tercero,
                'observaciones' => $request->observaciones,
                'fecha_adeudo' => Carbon::parse($request->fecha_adeudo),
                'monto_original' => $request->monto_original,
                'cat_tipo_moneda_id' => $request->cat_tipo_moneda_id,
                'saldo_pendiente' => $request->saldo_pendiente,
                'saldo_cat_tipo_moneda_id' => $request->saldo_cat_tipo_moneda_id,
                'nombre_acreedor' => $request->nombre_acreedor,
                'rfc_acreedor' => $request->rfc_acreedor,
                'otorgante' => $request->otorgante,
                'domicilio_acreedor_cat_pais_id' => $request->domicilio_acreedor_cat_pais_id,
                'cat_tipo_operacion_id' => $request->cat_tipo_operacion_id,
                'cat_tipo_acreedor_id' => $request->cat_tipo_acreedor_id,
                'cat_pais_id' => $request->cat_pais_id,
                'cat_sector_indrustrial_id' => $request->cat_sector_indrustrial_id,
                'tasa_interes' => $request->tasa_interes,
                'montos_abonados' => $request->montos_abonados,
                'plazo_adeudo' => $request->plazo_adeudo,
                'unidad_medida_adeudo_cat_unidad_medida_plazo_id' => $request->unidad_medida_adeudo_cat_unidad_medida_plazo_id,
                'titularidad_deuda_cat_titularidad_id' => $request->titularidad_deuda_cat_titularidad_id,
                'porcentaje_adeudo_titular' => $request->porcentaje_adeudo_titular,
                'garantia' => $request->garantia,
                'nombre_garante' => $request->nombre_garante,
                'tercero' => $request->tercero,
                'domicilio_acreedor_cat_entidad_federativa_id' => $request->domicilio_acreedor_cat_entidad_federativa_id,
                'domicilio_acreedor_cat_municipio_id' => $request->domicilio_acreedor_cat_municipio_id,
                'domicilio_acreedor_cp' => $request->domicilio_acreedor_cp,
                'domicilio_acreedor_cat_localidad_id' => $request->domicilio_acreedor_cat_localidad_id,
                'domicilio_acreedor_cat_vialidad_id' => $request->domicilio_acreedor_cat_vialidad_id,
                'domicilio_acreedor_numExt' => $request->domicilio_acreedor_numExt,
                'domicilio_acreedor_numInt' => $request->domicilio_acreedor_numInt,
                'domicilio_acreedor_colonia' => $request->domicilio_acreedor_colonia,
                'domicilio_acreedor_vialidad_tipo' => $request->domicilio_acreedor_vialidad_tipo,
                'domicilio_acreedor_vialidad_nombre' => $request->domicilio_acreedor_vialidad_nombre,
            ]
            /* [

                'titular' => $request->titular,
                'tercero' => $request->tercero,
                'nombre' => $request->nombre,
                'rfc' => $request->rfc,
                'tipo_adeudo' => $request->tipo_adeudo,
                'cuenta' => $request->cuenta,
                'fecha' => Carbon::parse($request->fecha),
                'monto' => $request->monto,
                'tipo_moneda' => $request->tipo_moneda,
                'saldo_insoluto' => $request->saldo_insoluto,
                'otorgante' => $request->otorgante,
                'nombre_institucion' => $request->nombre_institucion,
                'rfc_institucion' => $request->rfc_institucion,
                'pais_id' => $request->pais_id,
                'especifique' => $request->especifique,
            ] */
        );
        /*
        $deuda   = Deudas::updateOrCreate(
            ['id' => $request->id],

        ); */

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $deuda = Deudas::findOrFail($id);
        $deuda->delete();
    }
}