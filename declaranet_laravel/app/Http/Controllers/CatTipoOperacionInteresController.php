<?php

namespace App\Http\Controllers;

use App\CatTipoOperacionInteres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoOperacionInteresController extends Controller
{
    public function index()
    {
        $tipos = CatTipoOperacionInteres::orderBy('id', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoOperacionInteres::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoOperacionInteres::findOrFail($id);
        $tipo->delete();
    }
}