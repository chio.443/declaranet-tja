<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        
        $tests = Test::orderBy('id', 'DESC')->get();
        return response()->json(['tests' => $tests]);
    }
    public function test($id)
    {
        dd($id);
        $tests = Test::orderBy('id', 'DESC')->get();
        return response()->json(['tests' => $tests]);
    }
    
    public function store(Request $request)
    {
        $test = Test::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['test' => $test]);
    }

    public function delete($id)
    {
        $test = Test::findOrFail($id);
        $test->delete();
    }
}