<?php

namespace App\Http\Controllers;

use App\CatFrecuenciaAnual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatFrecuenciaAnualController extends Controller
{
    public function index()
    {
        $tipos = CatFrecuenciaAnual::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatFrecuenciaAnual::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatFrecuenciaAnual::findOrFail($id);
        $tipo->delete();
    }
}