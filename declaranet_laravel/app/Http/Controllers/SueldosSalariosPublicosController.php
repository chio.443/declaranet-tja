<?php

namespace App\Http\Controllers;

use App\SueldosSalariosPublicos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;


class SueldosSalariosPublicosController extends Controller
{
    public function index()
    {
        $sueldos = SueldosSalariosPublicos::orderBy('id', 'DESC')->get();
        return response()->json(['sueldos' => $sueldos]);
    }

    public function item($id)
    {
        $id2 = Auth::user()->id;
        $sueldos = SueldosSalariosPublicos::where('ip_id', $id2)->first();
        return response()->json(['sueldos' => $sueldos]);
    }

    public function store(Request $request)
    {
        $id2 = Auth::user()->id;
        $sueldo = SueldosSalariosPublicos::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $id2,
                'actividad_financiera' => $request->actividad_financiera,
                'actividad_financiera_cantidad' => $request->actividad_financiera_cantidad,
                'arrendamiento' => $request->arrendamiento,
                'arrendamiento_cantidad' => $request->arrendamiento_cantidad,
                'neto_conyugue' => $request->neto_conyugue,
                'neto_conyugue_cantidad' => $request->neto_conyugue_cantidad,
                'razon_social' => $request->razon_social,
                'razon_social_cantidad' => $request->razon_social_cantidad,
                'servicios_profecionales' => $request->servicios_profecionales,
                'servicios_profecionales_cantidad' => $request->servicios_profecionales_cantidad,
                'sub_1' => $request->sub_1,
                'bandera' => $request->bandera,
            ]
        );

        return response()->json(['sueldo' => $sueldo]);
    }

    public function delete($id)
    {
        $sueldo = SueldosSalariosPublicos::findOrFail($id);
        $sueldo->delete();
    }
}