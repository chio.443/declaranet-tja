<?php

namespace App\Http\Controllers;

use Storage;
use \App\Token;
use App\Ingresos;
use Carbon\Carbon;
use App\Movimientos;
use App\CatLocalidad;
use App\EncargoActual;
use App\PlantillaIngresos;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\MovimientosController;
use App\Http\Controllers\UsuarioTipoDecController;



ini_set('memory_limit', 512000000);
class ExcelController extends Controller
{
    public function index()
    {
        //dd(1);
    }
    public function ImportPlantilla(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->header("Authorization")) {
                $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
                $token = Token::decodeToken($auth);
            }
            $id = $token->uid;
            $rfcCargados = array('1', '2', '1');
            $ar = array_count_values($rfcCargados);

            $format = 'd/m/Y';
            $anioact    =  date("Y");

            //Storage::put('file.txt', 'Your namsaase');
            //$content = Storage::get('file.txt');
            //dd($content);
            //die();
            //$auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            //$token = Token::decodeToken($auth);
            $grupo_id = $request['grupo'];
            $simplificada = $request['simplificada'];

            if ($request['ente_publico_id'] && $request['ente_publico_id'] != '')
                $ente_publico_id = $request['ente_publico_id'];

            $ente_publico_id = (int)$ente_publico_id;
            if ($ente_publico_id == 0)
                $ente_publico_id = $token->ente_publico_id;

            //dd($ente_publico_id);


            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            //$filterSubset = new $this->MyReadFilter();
            //$reader->setReadFilter($filterSubset);
            $xlsx = $request->file("xlsx");
            $spreadsheet = $reader->load($xlsx);

            $sheet = $spreadsheet->getActiveSheet();
            $sheet->getStyle("k")->getNumberFormat()->setFormatCode("DD/MM/YYYY");

            // $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $sheetData = $sheet->toArray(null, true, true, true);

            $renglon = 1; //inicial
            $renglonPrimer = 1;
            $columna = 'A';
            $registro = (object)[];
            $incorrectos = array();
            //        $infPersonal=(object)[];
            //primer ciclo donde revisa los datos
            $error = 0;
            $columnas = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'G', 'I', 'J', 'K', 'L', 'M', 'N', 'O');
            $arregloObjetos = array();
            $indice = 0;
            $rfcCargados = array();
            $curpCargados = array();
            foreach ($sheetData as $row) {

                $errorRFC = 0;
                if ($renglonPrimer > 4) {
                    if ($row['A'] != '') {
                        $vacia = 0;
                        $colsVacias = array();
                        $rfc = $row['E'];
                        $curp = $row['D'];
                        $arrNac = explode("/", $row['F']);
                        if (count($arrNac) == 3) {
                        } else {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'E';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'La fecha de nacimiento es incorrecta 2';
                            $indice++;
                        }
                        $arrPos = explode("/", $row['K']);
                        if (count($arrPos) == 3) {
                        } else {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'E';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'La fecha de posesión es incorrecta';
                            $indice++;
                        }

                        $resRFC = $this->valida_rfc(trim($row['E']));
                        if (!isset($rfcCargados[$row['E']]))
                            $rfcCargados[$row['E']] = '';
                        $rfcCargados[$row['E']]++;

                        $resCURP = $this->validate_curp(trim($row['D']));
                        if (!isset($curpCargados[$row['D']]))
                            $curpCargados[$row['D']] = '';
                        $curpCargados[$row['D']]++;

                        if ($resCURP === false) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'D';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'EL CURP es incorrecto';
                            $indice++;
                        }
                        if ($resRFC === false) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'E';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'EL RFC es incorrecto';
                            $indice++;
                        }
                        if (isset($rfcCargados[$row['E']])  && $rfcCargados[$row['E']] > 1) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'E';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'El RFC se encuentra duplicado en el archivo';
                            $indice++;
                        }
                        if (isset($curpCargados[$row['E']])  && $curpCargados[$row['D']] > 1) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'D';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'El CURP se encuentra duplicado en el archivo';
                            $indice++;
                        }
                        try {
                            //Carbon::parse($row['F'])->format($format);
                            Carbon::createFromFormat('d/m/Y', strval($row['F']));
                        } catch (\Exception $e) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'F';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'El formato de la fecha es incorrecto' . $e;
                            $indice++;
                        }
                        try {
                            //Carbon::parse($row['K'])->format($format);
                            //Carbon::createFromFormat('d/m/Y', strval($row['K']));


                            //$fechaPos= Carbon::createFromFormat('d/m/Y', strval($row['K']));
                            //$fechaPos =  $row['K'];

                            //$ingreso  =  Carbon::parse($fechaPos)->format('Y');
                            // $f_pos =Carbon::createFromFormat("m/d/Y", $dateWanted)->format('Y-m-d')
                            $f_pos = Carbon::createFromFormat('d/m/Y', strval($row['K']));
                            $fechaPos = $f_pos->format('d/m/Y');

                            $ingreso  =  $f_pos->format('Y');
                            // dd(strval($row['K']), $row['K']);
                            if (($ingreso > $anioact)) { //si la fecha de ingreso es mayor al año actual
                                $error = 1;
                                $inco = (object)[];
                                $incorrectos[$indice] = (object)[];
                                $incorrectos[$indice]->Renglon = $renglonPrimer;
                                $incorrectos[$indice]->Columna = 'K';
                                $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                                $incorrectos[$indice]->RFC = $row['E'];
                                $incorrectos[$indice]->Mensaje = 'El año de ingreso no puede ser mayor al actual ' . $ingreso;
                                $indice++;
                            }
                        } catch (\Exception $e) {
                            $error = 1;
                            $inco = (object)[];
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'K';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'El formato de la fecha es incorrecto' . $e;
                            $indice++;
                        }
                        if ($simplificada == '1') {
                            try {
                                number_format($row['Q']);
                            } catch (\Exception $e) {
                                $error = 1;
                                $inco = (object)[];
                                $incorrectos[$indice] = (object)[];
                                $incorrectos[$indice]->Renglon = $renglonPrimer;
                                $incorrectos[$indice]->Columna = 'Q';
                                $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                                $incorrectos[$indice]->RFC = $row['E'];
                                $incorrectos[$indice]->Mensaje = 'La remuneración mensual debe contener solo números';
                                $indice++;
                            }
                            try {
                                number_format($row['R']);
                            } catch (\Exception $e) {
                                $error = 1;
                                $inco = (object)[];
                                $incorrectos[$indice] = (object)[];
                                $incorrectos[$indice]->Renglon = $renglonPrimer;
                                $incorrectos[$indice]->Columna = 'R';
                                $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                                $incorrectos[$indice]->RFC = $row['E'];
                                $incorrectos[$indice]->Mensaje = 'La remuneración anual debe contener solo números';
                                $indice++;
                            }
                            try {
                                //Carbon::parse($row['K'])->format($format);
                                //Carbon::createFromFormat('d/m/Y', strval($row['K']));


                                //$fechaPos= Carbon::createFromFormat('d/m/Y', strval($row['K']));
                                //$fechaPos =  $row['K'];
                                //$ingreso  =  Carbon::parse($fechaPos)->format('Y');

                                $f_pos = Carbon::createFromFormat('d/m/Y', strval($row['K']));
                                $fechaPos = $f_pos->format('d/m/Y');
                                $ingreso  =  $f_pos->format('Y');

                                if (($ingreso > $anioact)) { //si la fecha de ingreso es mayor al año actual
                                    $error = 1;
                                    $inco = (object)[];
                                    $incorrectos[$indice] = (object)[];
                                    $incorrectos[$indice]->Renglon = $renglonPrimer;
                                    $incorrectos[$indice]->Columna = 'K';
                                    $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                                    $incorrectos[$indice]->RFC = $row['E'];
                                    $incorrectos[$indice]->Mensaje = 'El año de ingreso no puede ser mayor al actual ' . $ingreso;
                                    $indice++;
                                }
                            } catch (\Exception $e) {
                                $error = 1;
                                $inco = (object)[];
                                $incorrectos[$indice] = (object)[];
                                $incorrectos[$indice]->Renglon = $renglonPrimer;
                                $incorrectos[$indice]->Columna = 'K';
                                $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                                $incorrectos[$indice]->RFC = $row['E'];
                                $incorrectos[$indice]->Mensaje = 'El formato de la fecha es incorrecto' . $e;
                                $indice++;
                            }
                        }


                        //if()
                        foreach ($columnas as $col) {
                            if ($col == 'I') {
                                if ($col == '')
                                    $col = '0';
                            }

                            if ($row[$col] == '' && $col != 'C' && $col != 'I') {
                                $colsVacias[] = $col;
                                $vacia = 1;
                            }
                        }
                        if ($vacia == 1) {
                            $error = 1;
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = implode(',', $colsVacias);
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'La celda esta vacia';
                            $indice++;
                        }
                        $existe = InformacionPersonal::select('id')
                            ->whereRaw(" (RFC = '" . $rfc . "' or CURP='" . $curp . "') AND deleted_at IS NULL")
                            ->first();
                        $bandera = 0;
                        if ($existe) {
                            if ($existe->id != 0) {
                                $existe->id;
                                $bandera = 1;
                            }
                        }
                        if ($bandera == 1) {
                            $error = 1;
                            $incorrectos[$indice] = (object)[];
                            $incorrectos[$indice]->Renglon = $renglonPrimer;
                            $incorrectos[$indice]->Columna = 'E,D';
                            $incorrectos[$indice]->Nombre = $row['A'] . " " . $row['B'] . " " . $row['C'];
                            $incorrectos[$indice]->RFC = $row['E'];
                            $incorrectos[$indice]->Mensaje = 'El usuario ya existe';
                            $indice++;
                        }
                    }
                }
                $renglonPrimer++;
            }
            if ($error == 1) {
                DB::rollback();
                return response()->json(['incorrectos' => $incorrectos, 'correctos' => '']);
            } else {
                $correctos = 0;
                foreach ($sheetData as $row) {
                    //$infPersonal=(object)[];

                    if ($renglon > 4) {

                        if ($row['A']) {
                            $correctos++;
                            $nombres = $row['A'];
                            $primerApe = $row['B'];
                            $segudoApe = $row['C'];
                            $curp = trim($row['D']);
                            $rfc = trim($row['E']);


                            $fechaNa = Carbon::createFromFormat('d/m/Y', strval($row['F']));
                            //$fArr=explode('/',$row['F']);
                            //dd($fechaNa);
                            //$fechaNa=$fArr[2].$fArr[1].$fArr[0];
                            $correoLab = $row['O'];
                            $created_by = 1; //session

                            $InfPer = new InformacionPersonal();
                            $encargoActual = new EncargoActual();

                            $InfPer['nombres'] = $nombres;
                            $InfPer['primer_apellido'] = $primerApe;
                            $InfPer['segundo_apellido'] = $segudoApe;
                            $InfPer['rfc'] = $rfc;

                            //$InfPer['password'] = encrypt($rfc);
                            $InfPer['password'] = md5($rfc);
                            $InfPer['curp'] = $curp;
                            $InfPer['fecha_nacimiento'] = $fechaNa; //$fechaNa;//$fechaNa->format('Ymd');
                            $InfPer['grupo_id'] = $grupo_id;
                            $InfPer['simplificada'] = $simplificada;
                            $InfPer['ente_publico_id'] = $ente_publico_id;
                            $InfPer['correo_electronico_laboral'] = $row['O'];



                            //,segundo_apellido,curp,rfc,
                            $InfPer->save();

                            //datos del puesto
                            $puesto = $row['G'];
                            $rep = array('7', "'");

                            $puesto = str_replace($rep, '', trim($puesto));

                            $encargoActual->empleo_cargo_comision = $puesto;

                            $encargoActual->informacion_personal_id = $InfPer->id;
                            if (strtoupper($row['H']) == 'SI' || strtoupper($row['H']) == 'SÍ')
                                $hono = 1;
                            else
                                $hono = 2;
                            $encargoActual->contratado_honorarios = $hono;

                            $encargoActual->nivel_encargo = $row['I'];
                            $encargoActual->area_adscripcion = $row['J']; //=str_replace($rep,'',trim($row['J']));

                            // $fArr=explode('/',$row['K']);
                            // $fechaPos=$fArr[2].$fArr[1].$fArr[0];
                            // $fechaPos= Carbon::createFromFormat('d/m/Y', strval($row['K']));
                            //$fechaPos=$row['K'];
                            //tipo de declaracion puede ser inicial o anual
                            $tipo_dec;
                            //$ingreso  =  Carbon::parse($fechaPos)->format('Y');
                            $f_pos = Carbon::createFromFormat('d/m/Y', strval($row['K']));
                            $fechaPos = $f_pos->format('Y-m-d');
                            $ingreso  =  $f_pos->format('Y');

                            if ($simplificada == '1' || ($ingreso == $anioact)) {
                                $tipo_dec = 1;
                            } else {
                                $tipo_dec = 2;
                            }
                            // Si se seleccione el tipo de declaracion a realizar
                            $tipo_dec = $request['tipo_dec'] ? $request['tipo_dec'] : $tipo_dec;

                            $encargoActual->tipo_dec  =   $tipo_dec;


                            $encargoActual->fecha_posesion = $fechaPos;

                            $localidad = CatLocalidad::orderBy('id', 'asc')
                                ->where('nom_loc', $row['L'])
                                ->where('efe_id', '11')

                                ->first();
                            if ($localidad)
                                $loc = $localidad->id;
                            else
                                $loc = '97174';

                            $encargoActual->lugar_ubicacion_entidad_id = $loc;

                            // $encargoActual->lugar_ubicacion_entidad_id='11';
                            $encargoActual->direccion_encargo_pais_id = '150';
                            $encargoActual->direccion_cargo = $row['M'];
                            $encargoActual->telefono_laboral_numero = $row['N'];
                            $encargoActual->correo_laboral = $row['O'];
                            $encargoActual->rfc_patron = $row['P'];
                            $encargoActual->ente_publico_id = $ente_publico_id;


                            $encargoActual->save();

                            if ($simplificada == '1') {
                                $ingresos = new Ingresos();
                                $ingresos->ip_id = $InfPer->id;
                                $ingresos->created_by = $InfPer->id;
                                $ingresos->sub_1 = $row['Q']; //mensuales
                                $ingresos->ant_sub_12 = $row['R']; //anuales
                                $ingresos->save();
                            } else {
                                $simplificada = '0';
                            }

                            // Insertar registro de tipo declaracion
                            $usuario_tipo_dec = new UsuarioTipoDecController;
                            // Si la solicitud tiene periodo
                            $periodo = $request['periodo'] ? $request['periodo'] : null;

                            $u_tipo_dec  = new Request;
                            $u_tipo_dec->ip_id = $InfPer->id;
                            $u_tipo_dec->tipo_dec_id = $tipo_dec;
                            $u_tipo_dec->ente_publico_id = $ente_publico_id;
                            $u_tipo_dec->fecha_posesion =  $fechaPos;
                            if (!empty($periodo)) {
                                $u_tipo_dec->periodo =  $periodo;
                            }

                            $tipo_dec_resp = $usuario_tipo_dec->nuevoUsuario($u_tipo_dec);
                        }
                    }
                    $renglon++;
                }



                /*
            $correctos[0]=(object)[];
            $correctos[0]->Renglon='6';
            $correctos[0]->RFC='DUAJ820412TK1';
            $correctos[0]->Mensaje='El formato no es correcto';
                             */

                //return response()->json(['incorrectos' => '','correctos' => $correctos]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['incorrectos' => '', 'correctos' => $correctos]);

        /*
        $incorrectos[0]=(object)[];
        $incorrectos[0]->Renglon='5';
        $incorrectos[0]->RFC='DUAJJ20412TK1';
        $incorrectos[0]->Mensaje='Corerecto';
        */
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function ImportPlantillaIngresos(Request $request)
    {
        $start = now();

        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }
        $uid = $token->uid;
        // $rfcCargados = array('1', '2', '1');

        // if ($request['ente_publico_id'] && $request['ente_publico_id'] != '')
        //     $ente_publico_id = $request['ente_publico_id'];

        // $ente_publico_id = (int)$ente_publico_id;
        // if ($ente_publico_id == 0)
        //     $ente_publico_id = $token->ente_publico_id;

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $xlsx = $request->file("xlsx");
        $spreadsheet = $reader->load($xlsx);
        $sheet_dimension = $spreadsheet->getActiveSheet()->calculateWorksheetDimension();
        $spreadsheet->getActiveSheet()->getStyle($sheet_dimension)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $renglon = 1; //inicial
        $renglonPrimer = 1;
        $incorrectos = array();
        //primer ciclo donde revisa los datos
        $error = 0;
        $indice = 0;
        $rfcCargados = array();
        $curpCargados = array();
        foreach ($sheetData as $row) {
            $errorRFC = 0;
            if ($renglonPrimer > 2) {
                if ($row['A'] != '') {
                    $vacia = 0;
                    $colsVacias = array();
                    $rfc = $row['A'];
                    $curp = $row['B'];

                    $resRFC = $this->valida_rfc(trim($row['A']));
                    if (!isset($rfcCargados[$row['A']])) {
                        $rfcCargados[$row['A']] = '';
                    }
                    $rfcCargados[$row['A']]++;

                    $resCURP = $this->validate_curp(trim($row['B']));
                    if (!isset($curpCargados[$row['B']])) {
                        $curpCargados[$row['B']] = '';
                    }
                    $curpCargados[$row['B']]++;

                    if ($resCURP === false) {
                        $error = 1;

                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'B';
                        $incorrectos[$indice]->RFC = $row['B'];
                        $incorrectos[$indice]->Mensaje = 'EL CURP es incorrecto';
                        $indice++;
                    }
                    if ($resRFC === false) {
                        $error = 1;
                        $inco = (object)[];
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'A';
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'EL RFC es incorrecto';
                        $indice++;
                    }
                    if (isset($rfcCargados[$row['A']])  && $rfcCargados[$row['A']] > 1) {
                        $error = 1;
                        $inco = (object)[];
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'A';
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'El RFC se encuentra duplicado en el archivo';
                        $indice++;
                    }
                    if (isset($curpCargados[$row['A']])  && $curpCargados[$row['B']] > 1) {
                        $error = 1;
                        $inco = (object)[];
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'B';
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'El CURP se encuentra duplicado en el archivo';
                        $indice++;
                    }
                    if ($vacia == 1) {
                        $error = 1;
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = implode(',', $colsVacias);
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'La celda esta vacia';
                        $indice++;
                    }
                    $columnaC = trim($row['C']);
                    if (!preg_match('/^\d+\.?\d{0,2}$/', $columnaC)) {
                        // Error
                        $error = 1;
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'C ' . '(' . $columnaC . ')';
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'Solo debe contener numeros con maximo de 2 decimales';
                        $indice++;
                    }
                    $columnaD = trim($row['D']);
                    if (!preg_match('/^\d+\.?\d{0,2}$/', $columnaD)) {
                        // Error
                        $error = 1;
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'D' . '(' . $columnaD . ')';
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'Solo debe contener numeros con maximo de 2 decimales';
                        $indice++;
                    }
                }
            }
            $renglonPrimer++;
        }
        if ($error == 1) {
            return response()->json(['incorrectos' => $incorrectos, 'correctos' => '']);
        } else {
            $correctos = 0;
            try {
                DB::beginTransaction();
                $lista = [];
                foreach ($sheetData as $row) {

                    if ($renglon > 2) {

                        if ($row['A']) {

                            $curp = trim($row['B']);
                            $rfc = trim($row['A']);

                            $lista[] = [
                                'rfc' => $rfc,
                                'curp' => $curp,
                                'ultimo_sueldo_mensual' => $row['C'], //mensual
                                'ingreso_anual_neto' => $row['D'], //mensual
                                'created_by' => $uid,
                                'created_at' => now(),
                            ];
                        }
                    }
                    $renglon++;
                }

                $rfc_list = array_chunk($lista, 1000, true);

                foreach ($rfc_list as $key => $inser) {
                    $c = count($inser);
                    PlantillaIngresos::insert($inser);
                    $correctos += $c;
                }

                // foreach ($lista as $row) {

                //     $Plantilla = new PlantillaIngresos();

                //             $Plantilla['rfc'] = $rfc;
                //             $Plantilla['curp'] = $curp;
                //             $Plantilla['ultimo_sueldo_mensual'] = $row['C']; //mensual
                //             $Plantilla['ingreso_anual_neto'] = $row['D']; //mensual
                //             $Plantilla['created_by'] = $uid;
                //             $Plantilla->save();

                //             $correctos++;
                // }

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
            }
            $end = now();
            $dif = $start->diffInSeconds($end);

            return response()->json(['incorrectos' => '', 'correctos' => $correctos, 'tiempo' => gmdate('H:i:s', $dif)]);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////
    public function ImportPlantillaBajas(Request $request)
    {

        if ($request->header("Authorization")) {
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
        }
        $id = $token->uid;
        $rfcCargados = array('1', '2', '1');
        $ar = array_count_values($rfcCargados);

        $format = 'd/m/Y';
        $anioact    =  date("Y");

        if ($request['ente_publico_id'] && $request['ente_publico_id'] != '')
            $ente_publico_id = $request['ente_publico_id'];

        $ente_publico_id = (int)$ente_publico_id;
        if ($ente_publico_id == 0)
            $ente_publico_id = $token->ente_publico_id;

        //dd($ente_publico_id);


        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        //$filterSubset = new $this->MyReadFilter();
        //$reader->setReadFilter($filterSubset);
        $xlsx = $request->file("xlsx");
        $spreadsheet = $reader->load($xlsx);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $renglon = 1; //inicial
        $renglonPrimer = 1;
        $columna = 'A';
        $registro = (object)[];
        $incorrectos = array();
        //        $infPersonal=(object)[];
        //primer ciclo donde revisa los datos
        $error = 0;
        $columnas = array('A', 'B', 'C', 'D', 'E');
        $arregloObjetos = array();
        $indice = 0;
        $rfcCargados = array();
        $curpCargados = array();
        foreach ($sheetData as $row) {
            $errorRFC = 0;
            if ($renglonPrimer > 3) {
                if ($row['A'] != '') {
                    $vacia = 0;
                    $colsVacias = array();
                    $rfc = $row['A'];
                    $fechaBaja = $row['B'];

                    $resRFC = $this->valida_rfc(trim($row['A']));
                    if (!isset($rfcCargados[$row['A']]))
                        $rfcCargados[$row['A']] = '';
                    $rfcCargados[$row['A']]++;

                    if ($resRFC === false) {
                        $error = 1;
                        $inco = (object)[];
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'A';
                        //$incorrectos[$indice]->Nombre=$row['A']." ".$row['B']." ".$row['C'];
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'EL RFC es incorrecto';
                        $indice++;
                    }
                    /* if (isset($rfcCargados[$row['A']])  && $rfcCargados[$row['A']] > 1) {
                        $error = 1;
                        $inco = (object)[];
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = 'A';
                        //$incorrectos[$indice]->Nombre=$row['A']." ".$row['B']." ".$row['C'];
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'El RFC se encuentra duplicado en el archivo';
                        $indice++;
                    } */
                    if ($vacia == 1) {
                        $error = 1;
                        $incorrectos[$indice] = (object)[];
                        $incorrectos[$indice]->Renglon = $renglonPrimer;
                        $incorrectos[$indice]->Columna = implode(',', $colsVacias);
                        //$incorrectos[$indice]->Nombre=$row['A']." ".$row['B']." ".$row['C'];
                        $incorrectos[$indice]->RFC = $row['A'];
                        $incorrectos[$indice]->Mensaje = 'La celda esta vacia';
                        $indice++;
                    }
                }
            }
            $renglonPrimer++;
        }
        if ($error == 1) {

            return response()->json(['incorrectos' => $incorrectos, 'correctos' => '']);
        } else {
            $correctos = 0;
            foreach ($sheetData as $row) {
                //$infPersonal=(object)[];

                if ($renglon > 2) {
                    if ($row['A']) {
                        $correctos++;
                        //$f_b = trim($row['B']);
                        $rfc = trim($row['A']);
                        $f_i = trim($row['B']);
                        $f_ini = Carbon::createFromFormat('m/d/Y', strval($row['B']));
                        $inicia = $f_ini->format('Y-m-d');
                        $f_t = trim($row['C']);
                        $f_ter = Carbon::createFromFormat('m/d/Y', strval($row['C']));
                        $termina = $f_ter->format('Y-m-d');
                        $motivo = "Licencias por bloque";
                        $tipo_licencia = "Trabajo";
                        //$f_baj = Carbon::createFromFormat('m/d/Y', strval($row['B']));
                        //$fechaBaja = $f_baj->format('Y-m-d');
                        $servidor = InformacionPersonal::where('rfc', '=', strtoupper($rfc))->firstOrFail();
                        $encargo = EncargoActual::where('informacion_personal_id', $servidor->id)->first();
                        //-------------PARA BAJAS ------------------------//
                        /*$encargo->update(['fecha_termino' => $fechaBaja]);
                        //$motivo = "Bajas por bloque";
                        //$tipo_baja = "Normal";

                        $movimiento = new Movimientos();
                        $movimiento->ip_id = $servidor->id;
                        $movimiento->tipo_mov = 2;
                        $movimiento->motivo = $motivo;
                        $movimiento->tipo_baja = $tipo_baja;
                        $movimiento->status = 0;
                        $movimiento->created_by = $id;
                        $movimiento->save();

                        if ($movimiento->tipo_baja == 'Normal') {
                            $usuario_tipo_dec = new UsuarioTipoDecController;

                            $tipo_dec  = new Request;
                            $tipo_dec->ip_id = $servidor->id;
                            $tipo_dec->tipo_movimiento_id = 2;
                            $tipo_dec->fecha_termino =  $encargo->fecha_termino;
                            $tipo_dec->ente_publico_id = $encargo->ente_publico_id;

                            $tipo_dec_resp = $usuario_tipo_dec->movimiento($tipo_dec);
                        } */
                        try {
                            DB::beginTransaction();
                            $movimiento = new Movimientos();

                            $movimiento->ip_id = $servidor->id;
                            $movimiento->tipo_mov = 4;
                            $movimiento->inicia = $inicia;
                            $movimiento->termina = $termina;
                            $movimiento->motivo = $motivo;
                            $movimiento->tipo_baja = null;
                            $movimiento->tipo_licencia = $tipo_licencia;
                            $movimiento->origen = null;
                            $movimiento->cambio = null;
                            $movimiento->status = 0;
                            $movimiento->created_by = $id;

                            $movimiento->save();

                            $servidor->update(['tipo_mov' => 4]);

                            DB::commit();
                        } catch (Exception $e) {
                            DB::rollback();
                        }
                    }
                }
                $renglon++;
            }

            return response()->json(['incorrectos' => '', 'correctos' => $correctos]);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////

    public function ImportPlantilla2(Request $request)
    {

        //dd($spreadsheet);
        //readCell($column, $row, $worksheetName = '') ;

        /**  Create an Instance of our Read Filter  **/
        // $filterSubset = new MyReadFilter();

        /**  Create a new Reader of the type defined in $inputFileType  **/
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $xlsx = $request->file("xlsx");
        $spreadsheet = $reader->load($xlsx);
        /**  Tell the Reader that we want to use the Read Filter  **/
        //$reader->setReadFilter($filterSubset);
        /**  Load only the rows and columns that match our filter to Spreadsheet  **/
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $renglon = 0; //inicial
        $columna = 'A';
        $datos = '';
        $sqlIP = 'INSERT into informacion_personal (id,
            nombres,primer_apellido,segundo_apellido,curp,rfc,fecha_nacimiento
            pais_nacimiento_id,entidad_federativa_nacimiento_id,   fecha_nacimiento,numero_identificacion_oficial,correo_electronico_personal,

            correo_electronico_laboral,
            telefono_particular,telefono_celular, domicilio_pais_id,domicilio_entidad_federativa_id,domicilio_municipio_id,domicilio_cp,domicilio_localidad_id,domicilio_vialidad_id,
            domicilio_numext,domicilio_numint,estado_civil_id,regimen_matrimonial_id,fecha_declaracion,created_by,deleted_by,updated_by,created_at,deleted_at,updated_at
            )';


        $sqlDatosEncargo = " INSERT into datos_encargo_actual (id,
            informacion_personal_id,ente_publico_id,empleo_cargo_comision,nivel_gobierno_id,poder_juridico_id,
            contratado_honorarios,nivel_encargo,area_adscripcion,fecha_posesion,lugar_ubicacion_pais_id,lugar_ubicacion_entidad_id,direccion_encargo_pais_id,
            direccion_encargo_entidad_federativa_id,direccion_encargo_municipio_id,direccion_encargo_colonia,direccion_encargo_cp,direccion_encargo_localidad_id,
            direccion_encargo_vialidad_id,direccion_encargo_numext,direccion_encargo_numint,telefono_laboral_numero,telefono_laboral_extension,correo_laboral,
            sector_industria_id,created_by,deleted_by,updated_by,created_at,deleted_at,updated_at,direccion_cargo,rfc_patron,direccion)";


        //       Nombre del empleo, cargo o comisi�n	Contrato por honorarios	Nivel de encargo	�rea de adscripci�n	Fecha de toma de posesi�n	Lugar donde se ubica	Direcci�n en la que se presta el empleo, cargo o comisi�n	Tel�fono laboral	Direcci�n de correo electr�nico laboral	RFC Patr�n

        foreach ($sheetData as $row) {
            $renglon++;
            $inserta = 0;
            if ($renglon >= 5) {
                $nombres = $row['A'];
                $primerApe = $row['B'];
                $segudoApe = $row['C'];
                $curp = $row['D'];
                $rfc = $row['E'];
                $fechaN = $row['F'];
                $correoLab = $row['O'];

                $created_by = 1; //session
                if ($nombres) {
                    $inserta = 1;
                    echo $nombres;
                }
                $datos = "(NULL,'$nombres','$primerApe','$segudoApe', '$curp','$rfc','$fechaN',NUll,NUll,NUll,NUll,NUll,'$correoLab',"
                    . "NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,'$created_by',NUll,NUll,'" . date('Y-m-d H:i:s') . "',NUll,NUll)";
                //apartir de aqui son los detalle de la tabla cargo actual

                if ($inserta == 1) {
                    $res = DB::query($sqlIP . $datos);
                    $id = DB::table('informacion_personal')
                        ->select('id')
                        ->orderByRaw('id  DESC')
                        ->first();
                    $id_ip = $id->id;
                    //$ente_publico_id=1;//sesion
                    $puesto = $row['G'];
                    $puesto = $row['G'];
                    $datosPuesto = "(NULL,$id_ip,'$puesto',1, NULL,NULL,"
                        . "'$fechaN',NUll,NUll,NUll,NUll,NUll,'$puesto',1,1"
                        . "NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,NUll,'$created_by',NUll,NUll,'" . date('Y-m-d H:i:s') . "',NUll,NUll)";
                    $res = DB::query($sqlDatosEncargo . $datosPuesto);
                }
            }
        }


        // dd($sql);
        //var_dump($sheetData);

        //$spreadsheet = $reader->load($inputFileName);



    }
    public function download($filename)
    {


        return Storage::download('plantilla.xlsx');
    }
    public function store(Request $request)
    {
        dd(3);
    }
    public function valida_rfc3($rfc)
    {

        if (strlen($rfc) == 10 || strlen($rfc) == 13) {
            if (ctype_alpha(substr($rfc, 0, 4))) {
                if (!is_numeric(substr($rfc, 4, 6))) {
                    return "Error en RFC, últimos 6 caracteres deben ser dígitos";
                }
            } else {
                return "Error en RFC, primeros 4 caracteres deben ser letras";
            }
        } else {
            return "La longitud del RFC es incorrecta";
        }
    } //fin validaRFC
    public function valida_rfc2($rfc)
    {
        $regex = '/^[A-Z]{3,4}([0-9]{2})(1[0-2]|0[1-9])([0-3][0-9])([ -]?)([A-Z0-9]{4})$/';
        return preg_match($regex, $rfc);
    }
    public function valida_rfc($valor)
    {
        $valor = str_replace("-", "", $valor);
        $cuartoValor = substr($valor, 3, 1);
        //RFC sin homoclave
        if (strlen($valor) == 10) {
            $letras = substr($valor, 0, 4);
            $numeros = substr($valor, 4, 6);
            if (ctype_alpha($letras) && ctype_digit($numeros)) {
                return true;
            }
            return false;
        }
        // Sólo la homoclave
        else if (strlen($valor) == 3) {
            $homoclave = $valor;
            if (ctype_alnum($homoclave)) {
                return true;
            }
            return false;
        }
        //RFC Persona Moral.
        else if (ctype_digit($cuartoValor) && strlen($valor) == 12) {
            $letras = substr($valor, 0, 3);
            $numeros = substr($valor, 3, 6);
            $homoclave = substr($valor, 9, 3);
            if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) {
                return true;
            }
            return false;
            //RFC Persona Física.
        } else if (ctype_alpha($cuartoValor) && strlen($valor) == 13) {
            $letras = substr($valor, 0, 4);
            $numeros = substr($valor, 4, 6);
            $homoclave = substr($valor, 10, 3);
            if (ctype_alpha($letras) && ctype_digit($numeros) && ctype_alnum($homoclave)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    } //fin validaRFC
    function validate_curp($valor)
    {
        if (strlen($valor) == 18) {
            $letras     = substr($valor, 0, 4);
            $numeros    = substr($valor, 4, 6);
            $sexo       = substr($valor, 10, 1);
            $mxState    = substr($valor, 11, 2);
            $letras2    = substr($valor, 13, 3);
            $homoclave  = substr($valor, 16, 2);
            if (ctype_alpha($letras) && ctype_alpha($letras2) && ctype_digit($numeros) && $this->is_mx_state($mxState) && $this->is_sexo_curp($sexo)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }
    function is_mx_state($state)
    {
        $mxStates = [
            'AS', 'BS', 'CL', 'CS', 'DF', 'GT',
            'HG', 'MC', 'MS', 'NL', 'PL', 'QR',
            'SL', 'TC', 'TL', 'YN', 'NE', 'BC',
            'CC', 'CM', 'CH', 'DG', 'GR', 'JC',
            'MN', 'NT', 'OC', 'QT', 'SP', 'SR',
            'TS', 'VZ', 'ZS'
        ];
        if (in_array(strtoupper($state), $mxStates)) {
            return true;
        }
        return false;
    }
    function is_sexo_curp($sexo)
    {
        $sexoCurp = ['H', 'M'];
        if (in_array(strtoupper($sexo), $sexoCurp)) {
            return true;
        }
        return false;
    }
}
/**  Define a Read Filter class implementing \PhpOffice\PhpSpreadsheet\Reader\IReadFilter  */
class ChunkReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    private $startRow = 0;
    private $endRow   = 0;

    /**  Set the list of rows that we want to read  */
    public function setRows($startRow, $chunkSize)
    {
        $this->startRow = $startRow;
        $this->endRow   = $startRow + $chunkSize;
    }

    public function readCell($column, $row, $worksheetName = '')
    {
        //  Only read the heading row, and the configured rows
        if (($row == 1) || ($row >= $this->startRow && $row < $this->endRow)) {
            return true;
        }
        return false;
    }
}

class MyReadFilter implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '')
    {
        //  Read rows 1 to 7 and columns A to E only
        if ($row >= 1 && $row <= 7) {
            if (in_array($column, range('A', 'P'))) {
                return true;
            }
        }
        return false;
    }
}
