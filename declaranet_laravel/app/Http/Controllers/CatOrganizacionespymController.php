<?php

namespace App\Http\Controllers;

use App\CatOrganizacionpym;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatOrganizacionespymController extends Controller
{
    public function index()
    {
        $organizaciones = CatOrganizacionpym::orderBy('valor', 'ASC')->get();
        return response()->json(['organizaciones' => $organizaciones]);
    }

    public function store(Request $request)
    {
        $organizacion = CatOrganizacionpym::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['organizacion' => $organizacion]);
    }

    public function delete($id)
    {
        $organizacion = CatOrganizacionpym::findOrFail($id);
        $organizacion->delete();
    }
}