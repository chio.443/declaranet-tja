<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Movimientos;
use App\InformacionPersonal;
use Illuminate\Http\Request;
use App\DeclaracionJustificada;
use Illuminate\Support\Facades\DB;

class OmisosController extends Controller
{
    public function getOmisos(Request $request)
    {

        // $condicion = " and den.fecha_posesion > now() - INTERVAL '120 DAY' and den.tipo_dec =1";
        $condicion2 = "fecha_posesion > now() - INTERVAL '120 DAY'";
        // $condicion3 = 1;
        // $condicion4 = "den.fecha_posesion > now() - INTERVAL '120 DAY'";


        $inicial = InformacionPersonal::with('encargo')
            ->whereHas("encargo", function ($q) use ($request, $condicion2) {
                $q->when($request->dependencia_id > 0, function ($query) use ($request) {
                    $query->where("ente_publico_id", "=", $request->dependencia_id);
                })->whereRaw($condicion2);
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $anioact    =  intval(date('Y'));
                $query->select('id_ip')
                    ->from('declaraciones')
                    ->where('tipo_declaracion_id', 1)
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) {
                $query->select('id')
                    ->from('informacion_personal')
                    ->where('tipo_mov', '=', 9) //9 es el id de justificado
                    ->whereNull('deleted_at');
            })
            ->distinct('rfc')
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();


        // $condicion3 = 2;
        // $condicion = " and (date_part('year',den.fecha_posesion) < '" . intval(date('Y')) . "' or den.tipo_dec =" . $condicion3 . ") ";
        $condicion2 = "date_part('year',fecha_posesion) < '" . intval(date('Y')) . "'";
        // $condicion4 = "date_part('year',den.fecha_posesion) < '" . intval(date('Y')) . "'";

        $anual = InformacionPersonal::with('encargo')
            ->whereHas("encargo", function ($q) use ($request, $condicion2) {
                $q->when($request->dependencia_id > 0, function ($query) use ($request) {
                    $query->where("ente_publico_id", "=", $request->dependencia_id);
                })
                    //$q->where("ente_publico_id", "=", $request->dependencia_id)
                    ->where(function ($q) use ($condicion2) {
                        $q->whereRaw($condicion2);
                    });
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $anioact    =  intval(date('Y'));
                $query->select('id_ip')
                    ->from('declaraciones')
                    ->where('tipo_declaracion_id', 2)
                    ->whereNull('deleted_at')
                    //->where('Fecha_Dec','>',$anioact.'-01-01')
                    ->where(function ($q) use ($request) {
                        $q->where('Fecha_Dec', '>', intval(date('Y')) . '-01-01')
                            ->where('Fecha_Dec', '<', intval(date('Y')) . '-12-31');
                    });
            })
            ->whereNotIn('id', function ($query) {
                $query->select('id')
                    ->from('informacion_personal')
                    ->where('tipo_mov', '=', 9) //9 es el id de justificado
                    ->whereNull('deleted_at');
            })
            ->distinct('rfc')
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();

        // $condicion3 = 3;
        // $condicion = "and  den.fecha_termino is not null";


        $final = InformacionPersonal::with('encargo')
            ->whereHas("encargo", function ($q) use ($request) {
                $q->when($request->dependencia_id > 0, function ($query) use ($request) {
                    $query->where("ente_publico_id", "=", $request->dependencia_id);
                })
                    // $q->where("ente_publico_id", "=", $request->dependencia_id)
                    ->where(function ($q) {

                        $q->whereNotNull('fecha_termino');
                    });
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $query->select('id_ip')
                    ->from('declaraciones')
                    ->where('tipo_declaracion_id', 3)
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) {
                $query->select('id')
                    ->from('informacion_personal')
                    ->where('tipo_mov', '=', 9) //9 es el id de justificado
                    ->whereNull('deleted_at');
            })
            ->distinct('rfc')
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();


        $array = array();

        $anioact    =  date("Y");
        //limite de la anual
        $fechaLimite = Carbon::parse('30-05-' . $anioact);
        //dias para la final y la inicial
        $dias = 60;

        foreach ($inicial as $omiso) {

            $ingreso    =  Carbon::parse($omiso->encargo->fecha_posesion)->format('Y');
            $baja        =  Carbon::parse($omiso->encargo->fecha_termino)->format('Y');
            $alta       =  Carbon::parse($omiso->created_at)->format('Y');
            $tipo         = "";
            $limite;
            $fecha;

            $tipo = "Inicial";
            $fecha = Carbon::parse($omiso->encargo->fecha_posesion)->format('d/m/Y');
            $fechad = Carbon::parse($omiso->encargo->fecha_posesion);
            $limite = Carbon::now()->diffInDays($fechad, false);

            $limite += $dias;
            $fechaLimite = Carbon::parse($omiso->encargo->fecha_posesion)->addDays($dias);

            $fechaLimite = Carbon::parse($fechaLimite)->format('d/m/Y');


            $array[] = [
                "tipo" => $tipo,
                "rfc" => $omiso->rfc,
                "nombres" => $omiso->nombres . ' ' . $omiso->primer_apellido . ' ' . $omiso->segundo_apellido,
                "fecha" => $fecha,
                "limite" => $limite,
                "fechaLimite" => $fechaLimite
            ];
        }

        foreach ($anual as $omiso) {

            $fechaLimite = Carbon::parse('30-05-' . $anioact);

            $ingreso    =  Carbon::parse($omiso->encargo->fecha_posesion)->format('Y');
            $baja        =  Carbon::parse($omiso->encargo->fecha_termino)->format('Y');
            $alta       =  Carbon::parse($omiso->created_at)->format('Y');
            $tipo         = "";
            $limite;
            $fecha;

            $tipo = "Anual";
            $fecha = "";
            $limite = Carbon::now()->diffInDays($fechaLimite, false);

            //    $fechaLimite = Carbon::parse('30-05-'.$anioact);

            // $fechaLimite = Carbon::parse($fechaLimite)->format('d/m/Y');

            $array[] = [
                "tipo" => $tipo,
                "rfc" => $omiso->rfc,
                "nombres" => $omiso->nombres . ' ' . $omiso->primer_apellido . ' ' . $omiso->segundo_apellido,
                "fecha" => $fecha,
                "limite" => $limite,
                "fechaLimite" => '30/05/2020'
            ];
        }
        foreach ($final as $omiso) {

            $ingreso    =  Carbon::parse($omiso->encargo->fecha_posesion)->format('Y');
            $baja        =  Carbon::parse($omiso->encargo->fecha_termino)->format('Y');
            $alta       =  Carbon::parse($omiso->created_at)->format('Y');
            $tipo         = "";
            $limite;
            $fecha;


            $tipo = "Final";
            $fecha = Carbon::parse($omiso->encargo->fecha_termino)->format('d/m/Y');
            $fechad = Carbon::parse($omiso->encargo->fecha_termino);
            $limite = Carbon::now()->diffInDays($fechad, false);
            $limite += $dias;


            $fechaLimite = Carbon::parse($omiso->encargo->fecha_termino)->addDays($dias);

            $fechaLimite = Carbon::parse($fechaLimite)->format('d/m/Y');


            $array[] = [
                "tipo" => $tipo,
                "rfc" => $omiso->rfc,
                "nombres" => $omiso->nombres . ' ' . $omiso->primer_apellido . ' ' . $omiso->segundo_apellido,
                "fecha" => $fecha,
                "limite" => $limite,
                "fechaLimite" => $fechaLimite
            ];
        }

        return response()->json(['omisos' => $array]);
    }

    public function getOmisos2(Request $request)
    {
        $dependencia = $request->dependencia_id;
        // $periodo = $request->periodo;
        $periodo = date("Y");

        if (isset($request->fecha_fin)) {
            $fecha_fin = $request->fecha_fin;
        }

        $movimientos = new Movimientos;
        $justificion = new DeclaracionJustificada;
        $informacion = new InformacionPersonal;

        $justificadas = $justificion->where('periodo', $periodo)
            ->pluck('id_ip')->toArray();
        $licencias = $movimientos->where('tipo_mov', '=', 4) //4 es el id de licencia
            ->where('termina', '>', date('Y-m-d'))
            ->pluck('ip_id')->toArray();

        $bajas = $movimientos->where('tipo_mov', 2) //2 es el id de baja
            ->where('tipo_baja', '!=', 'Normal') //por defuncion
            ->pluck('ip_id')->toArray();

        $justificadas2 = $movimientos->where('tipo_mov', 9)
            ->whereYear('inicia', $periodo)
            ->pluck('ip_id')->toArray();

        $excluir = array_merge($bajas, $licencias, $justificadas2, $justificadas);

        $reporte = DB::table('cat_ente_publico as e')
            ->select(DB::raw("i.id,t.tipo_dec_id,i.rfc,i.nombres,i.primer_apellido,i.segundo_apellido,d.fecha_posesion,d.fecha_termino,t.fecha_declaracion"))
            ->join('datos_encargo_actual as d', function ($join) use ($dependencia) {
                $join->on('d.ente_publico_id', '=', 'e.id')
                    ->whereNull('d.deleted_at')
                    ->when($dependencia > 0, function ($query) use ($dependencia) {
                        $query->where('d.ente_publico_id', $dependencia);
                    });
            })
            ->join('informacion_personal as i', function ($join) use ($excluir) {
                $join->on('i.id', '=', 'd.informacion_personal_id')
                    ->whereNotIn('i.id',  $excluir)
                    ->whereNull('i.deleted_at');
            })
            ->rightJoin('usuario_tipo_dec as t', function ($join) use ($periodo) {
                $join->on('t.ip_id', '=', 'i.id')
                    ->where('t.periodo', $periodo)
                    ->whereNull('t.declaracion_id')
                    ->whereRaw('t.id IN (select MAX(id) from usuario_tipo_dec as t1 where t1.ip_id = t.ip_id and t1.deleted_at is null)')
                    ->whereNull('t.deleted_at');
            })
            ->when($dependencia > 0, function ($query) use ($dependencia) {
                $query->where('e.id', $dependencia);
            })

            ->whereNull('e.deleted_at')
            ->where('t.periodo', $periodo)
            // ->whereNull('t.declaracion_id')
            // ->groupBy('e.valor')
            ->orderBy('t.tipo_dec_id')
            ->orderBy('i.rfc')
            ->orderBy('i.nombres')
            ->orderBy('e.valor')
            ->get();

        $reporte2 = [];


        //limite de la anual
        $fechaLimite = Carbon::parse('30-05-' . $periodo);
        //dias para la final y la inicial
        $dias = 60;
        $id_error = [];
        foreach ($reporte as $i => $dep) {
            $tipo = '';
            $limite = 0;
            $fecha = '';
            $fechaLimite = '';

            switch ($dep->tipo_dec_id) {
                case 1:
                    // $condicion2 = "fecha_posesion > now() - INTERVAL '120 DAY'";
                    $tipo = "Inicial";
                    //// INICIAL
                    $fecha = Carbon::parse($dep->fecha_posesion)->format('d/m/Y');
                    $fechad = Carbon::parse($dep->fecha_posesion);
                    $limite = Carbon::now()->diffInDays($fechad, false);

                    $limite += $dias;
                    $fechaLimite = Carbon::parse($dep->fecha_posesion)->addDays($dias);
                    $fechaLimite = Carbon::parse($fechaLimite)->format('d/m/Y');
                    break;
                case 2:
                    // $condicion2 = "date_part('year',d.fecha_posesion) < '" . intval(date('Y')) . "'";
                    $tipo = "Anual";
                    //// ANUAL
                    $fecha = Carbon::parse($dep->fecha_posesion)->format('d/m/Y');
                    $fechaLimite = Carbon::parse('30-05-' . $periodo);
                    $limite = Carbon::now()->diffInDays($fechaLimite, false);
                    $fechaLimite = '30/05/' . $periodo;
                    break;
                case 3:
                    $tipo = "Final";
                    //// FINAL
                    $fecha = Carbon::parse($dep->fecha_termino)->format('d/m/Y');
                    $fechad = Carbon::parse($dep->fecha_termino);
                    $limite = Carbon::now()->diffInDays($fechad, false);
                    $limite += $dias;

                    $fechaLimite = Carbon::parse($dep->fecha_termino)->addDays($dias);

                    $fechaLimite = Carbon::parse($fechaLimite)->format('d/m/Y');
                    break;
                default:
                    $id_error[] = $dep;
                    break;
            }

            $datos = (object) [
                'tipo' => $tipo,
                'limite' => $limite,
                'nombres' => $dep->nombres . ' ' . $dep->primer_apellido . ' ' . $dep->segundo_apellido,
                'rfc' => $dep->rfc,
                'fecha' => $fecha,
                'fecha_posesion' => $dep->fecha_posesion ? Carbon::parse($dep->fecha_posesion)->format('d/m/Y') : '',
                'fecha_termino' => $dep->fecha_termino ? Carbon::parse($dep->fecha_termino)->format('d/m/Y') : '',
                'fechaLimite' => $fechaLimite,
            ];

            $reporte2[$i] = $datos;
        }

        // dd(($id_error));
        return response()->json(['omisos' => $reporte2, 'id_error' => json_encode($id_error)]);
    }
}
