<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;
use Auth;
use DB;

use App\Declaracion;
use App\InformacionPersonal;
use App\DatosCurriculares;
use App\ExperenciaLaboral;
use App\DependienteEconomico;
use App\InversionesCuentasValores;
use App\Deudas;
use App\BitacoraConsulta;
use App\Membresias;
use App\Fiscal;
use Carbon\Carbon;

class ConsultaController extends Controller
{
	 public function filtro($cadena)
    {
    	$cadena = strtoupper($cadena);

        $usuarios = DB::select("select informacion_personal.id,informacion_personal.nombres,informacion_personal.primer_apellido, informacion_personal.segundo_apellido, cat_ente_publico.valor
        						from informacion_personal
								inner join datos_encargo_actual
								on informacion_personal.id = datos_encargo_actual.informacion_personal_id
								inner join cat_ente_publico 
								on cat_ente_publico.id = datos_encargo_actual.ente_publico_id
								where (datos_encargo_actual.titular='1' or informacion_personal.publica='1')
								and informacion_personal.deleted_at is null
								and (CONCAT(UPPER(nombres),' ',UPPER(primer_apellido),' ',UPPER(segundo_apellido)) like '%".strtoupper($cadena)."%'
										or rfc like '%".strtoupper($cadena)."%')
								and informacion_personal.deleted_at is null");

        return response()->json(['usuarios' => $usuarios]);       

    }

    public function getDeclaraciones($id)
    {
    	$declaraciones = Declaracion::where('id_ip',$id)->with('declarante','tipoDec')->get();
    	
        return response()->json(['declaraciones' => $declaraciones]);       

    }
    public function getDeclaracion($id)
    {     	
    	$format = 'd/m/Y';
  
        

    	$dec = DB::table('declaraciones')
	    	->select('id_ip','created_at')
			->where('declaraciones.id',$id)->first();

    	$id = $dec->id_ip;

    	$fechaDeclaracion = Carbon::parse($dec->created_at)->format($format);

        $func = InformacionPersonal::where('id', $id)->first();
		
		$dpersonales = array();

         //información personal

         $dpersonales['nombres']=strtoupper($func->nombres.' '.$func->primer_apellido.' '.$func->segundo_apellido.'');
		 $dpersonales['curp']= $func->curp;
		 $dpersonales['rfc']= $func->rfc;
		 $dpersonales['paisnac']= $func->paisNac->valor;
		 $dpersonales['entnac']= $func->entNac->nom_ent;
		 $dpersonales['ce_personal']= $func->correo_electronico_personal;
		 $dpersonales['ce_laboral']= $func->correo_electronico_laboral;
		 $dpersonales['tel_particular']= $func->telefono_particular;
		 $dpersonales['cp']= $func->domicilio_cp;
		 $dpersonales['numext']= $func->domicilio_numext;
		 $dpersonales['numint']= $func->domicilio_numint;
		 $dpersonales['colonia']= $func->domicilio_colonia;
		 $dpersonales['municipio']= $func->municipioD->nom_mun;
		 $dpersonales['entidad']= $func->entidadFedD->nom_ent;
		 $dpersonales['localidad']= $func->localidadD->nom_loc;		 
		 $dpersonales['tvialidad']= $func->domicilio_vialidad_tipo;
		 $dpersonales['calle']= $func->domicilio_vialidad_nombre;
		 $dpersonales['edoCivil']= $func->edoCivil->valor;
 		 $dpersonales['regimen']= $func->regimenMat->valor;


		
		//datos curriculares

 		 $func = DatosCurriculares::where('informacion_personal_id', $id)
 		 							->where('grado_maximo_escolaridad_id', '>=',5)
 		 							->get();

	 	 $dcurrc = array();

		 foreach ($func as $escuela) {
				 $dcurrc[] = array(
				 					'grado' => $escuela->gradoEsc->grado_escolaridad, 
				   					'institucion' => $escuela->institucion_educativa,
				   					'entidad' => $escuela->ent->nom_ent, 
				   					'pais' => $escuela->pais->valor, 
				   					'carrera'=>$escuela->carrera, 
				   					'periodos' => $escuela->periodos->valor, 
				   					'periodosc'=>$escuela->periodos_cursados,
				   					'estatus' => $escuela->estatus->estatus,
				   					'doctoOb' => $escuela->docto->docto,
				   					'cedula' => $escuela->cedula,
								  );
		}
		//experiencia laboral

		$func = InformacionPersonal::where('id', $id)->first();

	 	 $experiencia = array();

		 foreach ($func->experiencia as $trabajo) {
				 $experiencia[] = array(
				 					'ambito' => $trabajo->ambito->valor, 
				   					'nombreInst' => $trabajo->nombre_institucion,
				   					'area' => $trabajo->unidad_administrativa, 
				   					'cp'=> $trabajo->direccion_cp,
									'numext'=> $trabajo->direccion_numext,
									'numint'=> $trabajo->direccion_numint,
									'municipio'=> $trabajo->municipioD->nom_mun,
									'entidad'=> $trabajo->entidadFedD->nom_ent,
									'localidad'=> $trabajo->localidadD->nom_loc,		 
									'tvialidad'=> $trabajo->direccion_vialidad_tipo,
									'calle'=> $trabajo->direccion_vialidad_nombre,
									'colonia'=> $trabajo->direccion_colonia,
				   					'sector'=>$trabajo->sector->valor, 
				   					'jerarquia' => $trabajo->jerarquia_rango, 
				   					'cargo'=>$trabajo->cargo_puesto,
				   					'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
				   					'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format)
								  );
		}
		//////////////////////////////////dependientes economicos//////////////////////////////////////////

 		 $func = DependienteEconomico::where('informacion_personal_id', $id)->get();

	 	 $dependientes = array();

		 foreach ($func as $dependiente) {
				 $dependientes[] = array(
				 					'tipo_r' => $dependiente->tipoRel->valor, 
				   					'nombre' => $dependiente->nombres.' '.$dependiente->primer_apellido.' '.$dependiente->segundo_apellido.'',
				   					'curp' => $dependiente->curp
								  );
		}

		//////////////////////////////////////////////////////////////////////////
		 //datos del encargo actual
		 $func = InformacionPersonal::where('id', $id)->first();


		 $dencargo = array();
		 $dencargo['dependencia']= strtoupper($func->encargo->ente->valor);//dependencia o ente
		 $dencargo['cargo']= ($func->encargo->empleo_cargo_comision);//cargo
		 $dencargo['area']= ($func->encargo->area_adscripcion);//cargo
		 $dencargo['ftomaposesion']=  Carbon::parse($func->encargo->fecha_posesion)->format($format);
		 $dencargo['baja']=  Carbon::parse($func->encargo->fecha_termino)->format($format);

		 if($func->encargo->contratado_honorarios == 1){
		 	$dencargo['honorarios']='SI';
		 }else{
		 	$dencargo['honorarios']='NO';
		 }
		 $dencargo['domicilio']= $func->encargo->direccion_vialidad_nombre.' '. $func->encargo->direccion_encargo_numext.', '.$func->encargo->direccion_encargo_colonia;//cargo

		 ///////////////////////////////////////////intereses
		 //empresas
		 $empresas = array();
		 foreach ($func->empresas as $empresa) {
				 $empresas[] = array('grado' => $empresa->nombre_empresa_sociedad_asociacion, 
				   					 'naturaleza' => $empresa->naturaleza->valor 
									);
				}




		//////////////////////////////////////////ingresos
		//////////sueldos publicos
		$spublicos = array();
		
		$sueldo = $func->ingresos;

	 	$spublicos['ingreso'] = '$ '.number_format($sueldo['ant_sub_12']);

		//////////otros sueldos
		$otross = array();
	 	// $otross[] = array('cat' => '1 Por actividad industrial y/o comercial
   //        Especifica nombre o razón social y tipo de negocio',
	 	// 				   'nombre' => $sueldo['razon_social'],
	  //  					  'ingreso' => $sueldo['razon_social_cantidad']
			// 			);
	 	// $otross[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 	// 				  'nombre' => $sueldo['actividad_financiera'], 
	  //  					  'ingreso' => $sueldo['actividad_financiera_cantidad']
			// 			);
	 	// $otross[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 	// 				  'nombre' => $sueldo['servicios_profecionales'], 
	  //  					  'ingreso' => $sueldo['servicios_profecionales_cantidad']
			// 			);
	 	// $otross[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 	// 				  'nombre' => $sueldo['arrendamiento'], 
	  //  					  'ingreso' => $sueldo['arrendamiento_cantidad']
			// 			);



		 	//////////////////////////////////////////ingresos periodo anterior
		//////////sueldos publicos
		$spublicosant = array();

	 	$spublicosant[] = array('nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)', 
	   					 	 'ingreso' => '$'.number_format($sueldo['ant_sub_12'],2)
						);


		//////////otros sueldos
		$otrossant = array();
	 	$otrossant[] = array('cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
	 					   'nombre' => $sueldo['ant_razon_social'],
	   					  'ingreso' => '$'.number_format($sueldo['ant_razon_social_cantidad'],2)
						);

	 	$otrossant[] = array('cat' => '',
	 					   'nombre' => $sueldo['ant_actividad_comercial'],
	   					  'ingreso' => '$'.number_format($sueldo['ant_actividad_comercial_cantidad'],2)
						);
	 	$otrossant[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 					  'nombre' => $sueldo['ant_actividad_financiera'], 
	   					  'ingreso' => '$'.number_format($sueldo['ant_actividad_financiera_cantidad'],2)
						);
	 	$otrossant[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 					  'nombre' => $sueldo['ant_servicios_profecionales'], 
	   					  'ingreso' => '$'.number_format($sueldo['ant_servicios_profecionales_cantidad'],2)
						);
	 	$otrossant[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 					  'nombre' => $sueldo['ant_arrendamiento'], 
	   					  'ingreso' => '$'.number_format($sueldo['ant_arrendamiento_cantidad'],2)
						);

	 	$otross['otros'] = '$ '.number_format($sueldo['ant_razon_social_cantidad']+
					 					   $sueldo['ant_actividad_comercial_cantidad']+
					   					   $sueldo['ant_actividad_financiera_cantidad']+
					   					   $sueldo['ant_servicios_profecionales_cantidad']+
					   					   $sueldo['ant_arrendamiento_cantidad']);
		///////////////////////////////////////activos
		//bienes inmuebles
		$func = DB::table('bienes_inmuebles')
	            ->join('cat_tipo_operacion', 'bienes_inmuebles.tipo_operacion_id', '=', 'cat_tipo_operacion.id')
	            ->join('cat_tipo_bien', 'bienes_inmuebles.tipo_bien_id', '=', 'cat_tipo_bien.id')
	            ->select(DB::raw("SUM(CAST(replace(precio_adquisicion_valor,',','') AS DOUBLE PRECISION)) as precio_adquisicion_valor"),"cat_tipo_operacion.valor as tipoOperacion","cat_tipo_bien.valor as tipoBien")
	            ->where('ip_id',$id)
	            ->groupBy('cat_tipo_operacion.valor','cat_tipo_bien.valor')
		 	 	->get();

		  $binmuebles = array();
		  foreach ($func as $inmueble) {
		 		 $binmuebles[] = array(	'tipoOperacion' => strtoupper($inmueble->tipoOperacion), 
		 		   					 	'naturaleza' => strtoupper($inmueble->tipoBien),
		 		   					 	'padquisicion' =>  '$'.number_format($inmueble->precio_adquisicion_valor,2)
		 							);

		 		}
		// //bienes inmuebles reg

		 $func = DB::table('bienes_muebles_registrables')
	            ->join('cat_tipo_operacion', 'bienes_muebles_registrables.tipo_operacion_id', '=', 'cat_tipo_operacion.id')
	            ->join('cat_tipo_bien', 'bienes_muebles_registrables.tipo_bien_id', '=', 'cat_tipo_bien.id')
	            ->select(DB::raw("SUM( CAST( replace(precio_adquisicion_valor,',','') AS DOUBLE PRECISION)) as precio_adquisicion_valor"),"cat_tipo_operacion.valor as tipoOperacion","cat_tipo_bien.valor as tipoBien")
	            ->where('ip_id',$id)
	            ->groupBy('cat_tipo_operacion.valor','cat_tipo_bien.valor')
		 	 	->get();


		 $bmuebles = array();
		  foreach ($func as $mueble) {
		 		 $bmuebles[] = array(	'tipoOperacion' => strtoupper($mueble->tipoOperacion), 
		 		   					 	'tipoBien' => strtoupper($mueble->tipoBien),
		 		   					 	'padquisicion' =>  '$'.number_format($mueble->precio_adquisicion_valor,2)
		 							);
		 		}
		 //bienes inmuebles no reg
		  $func = DB::table('bienes_muebles_no_registrables')
	            ->join('cat_tipo_operacion', 'bienes_muebles_no_registrables.tipo_operacion_id', '=', 'cat_tipo_operacion.id')
	            ->join('cat_tipo_bien', 'bienes_muebles_no_registrables.tipo_bien_id', '=', 'cat_tipo_bien.id')
	            ->select(DB::raw("SUM(CAST(replace(precio_adquisicion_valor,',','') AS DOUBLE PRECISION)) as precio_adquisicion_valor"),"cat_tipo_operacion.valor as tipoOperacion","cat_tipo_bien.valor as tipoBien")
	            ->where('ip_id',$id)
	            ->groupBy('cat_tipo_operacion.valor','cat_tipo_bien.valor')
		 	 	->get();


		  $bmueblesnr = array();
		  foreach ($func as $mueble) {
		 		 $bmueblesnr[] = array(	'tipoOperacion' => strtoupper($mueble->tipoOperacion), 
		 		   					 	'tipoBien' => strtoupper($mueble->tipoBien),
		 		   					 	'padquisicion' =>  '$'.number_format($mueble->precio_adquisicion_valor,2)
		 							);
	 		}

		///////////////////////////////////////	pasivos


	 	 $func = DB::table('pasivos_deudas')
	            ->join('cat_tipo_operacion', 'pasivos_deudas.cat_tipo_operacion_id', '=', 'cat_tipo_operacion.id')
	            ->join('cat_tipo_adeudo', 'pasivos_deudas.cat_tipo_adeudo_id', '=', 'cat_tipo_adeudo.id')
	            ->select(DB::raw("SUM(CAST(replace(saldo_pendiente,',','') AS DOUBLE PRECISION)) as saldo_pendiente"),"cat_tipo_operacion.valor as tipoOperacion","cat_tipo_adeudo.valor as tipoAdeudo")
	            ->where('ip_id',$id)
	            ->groupBy('cat_tipo_operacion.valor','cat_tipo_adeudo.valor')
		 	 	->get();


	 	 $pasivos = array();
		  foreach ($func as $deuda) {
		 		 $pasivos[] = array(	
		 		 					'tipoOperacion' => $deuda->tipoOperacion,
	 		 						'tipoAdeudo' =>$deuda->tipoAdeudo,
	 		 						'saldo_pendiente'=> '$'.number_format($deuda->saldo_pendiente,2)
		 							);
	 		}
	 	
	 	$func =InversionesCuentasValores::where('ip_id',$id)->get();

	 	$func = DB::table('inversiones_cuentas_valores')
	            ->join('cat_tipo_operacion', 'inversiones_cuentas_valores.tipo_operacion_id', '=', 'cat_tipo_operacion.id')
	            ->join('cat_tipo_inversiones', 'inversiones_cuentas_valores.tipo_inversion_id', '=', 'cat_tipo_inversiones.id')
	            ->select(DB::raw("SUM(CAST(replace(monto_original,',','') AS DOUBLE PRECISION)) as monto_original"),"cat_tipo_operacion.valor as tipoOperacion","cat_tipo_inversiones.valor as tipoInv")
	            ->where('ip_id',$id)
	            ->groupBy('cat_tipo_operacion.valor','cat_tipo_inversiones.valor')
		 	 	->get();



	 	 $inversiones = array();

		 foreach ($func as $dato) {
	 		 $inversiones[] = array(
	 		 						'monto_original'=> '$'.number_format($dato->monto_original,2),
	 		 						'tipoOp' => $dato->tipoOperacion, 
	 		 						'tipoInv' => $dato->tipoInv

	 							);
	 		}

	 	$func =	Membresias::where('ip_id',$id)
	 			->with('tipoSociedad','tipoOperacion','titular')
	 			->get();


	 	$intereses = array();
		foreach ($func as $dato) {
	 		 $intereses[] = array(
	 		 						'tipo' => $dato->tipoSociedad->valor,
	 		 						'tipoOperacion' =>$dato->tipoOperacion->valor,
	 		 						'titular'=> $dato->titular->valor
	 							);
	 	}

         $fiscal = Fiscal::where('ip_id', $id)->first();
         if($fiscal)
             $resFis='Si';
         else                     
             $resFis='No';



        $view =  \View::make('bdeclaracion', compact('dpersonales','dcurrc','experiencia','dependientes', 'dencargo',   'empresas','spublicos','otross','spublicosant','inversiones','otrossant','binmuebles','bmuebles','bmueblesnr','resFis','pasivos','intereses','fechaDeclaracion'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

       // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

      return $pdf->download('Declaracion.pdf');         

     //return $pdf->stream('resumen.pdf');
      }

      //reactivar la declaración 
   

}
