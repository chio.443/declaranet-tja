<?php

namespace App\Http\Controllers;

use App\CatTipoBeneficio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoBeneficioController extends Controller
{
    public function index()
    {
        $response = CatTipoBeneficio::orderBy('valor', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = CatTipoBeneficio::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $response = CatTipoBeneficio::findOrFail($id);
        $response->delete();
    }
}