<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BeneficiosPrivados;

class BeneficiosPrivadosController extends Controller
{
    public function index()
    {
        $response = BeneficiosPrivados::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $response = BeneficiosPrivados::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = BeneficiosPrivados::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_beneficio' => $request->tipo_beneficio,
                'beneficiario' => $request->beneficiario,
                'otorgante' => $request->otorgante,
                'nombre' => $request->nombre,
                'rfc' => $request->rfc,
                'forma' => $request->forma,
                'beneficio' => $request->beneficio,
                'monto' => $request->monto,
                'tipo_moneda' => $request->tipo_moneda,
                'sector_industria_id' => $request->sector_industria_id,
                'observaciones' => $request->observaciones,
                'especifique' => $request->especifique,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $response = BeneficiosPrivados::findOrFail($id);
        $response->delete();
    }
}