<?php

namespace App\Http\Controllers;

use App\CatRelacionPersonaAdquirio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatRelacionPersonaAdquirioController extends Controller
{
    public function index()
    {
        $relaciones = CatRelacionPersonaAdquirio::orderBy('valor', 'ASC')->get();
        return response()->json(['relaciones' => $relaciones]);
    }

    public function store(Request $request)
    {
        $relacion = CatRelacionPersonaAdquirio::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['relacion' => $relacion]);
    }

    public function delete($id)
    {
        $relacion = CatRelacionPersonaAdquirio::findOrFail($id);
        $relacion->delete();
    }
}