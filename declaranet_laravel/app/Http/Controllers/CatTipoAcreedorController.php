<?php

namespace App\Http\Controllers;

use App\CatTipoAcreedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoAcreedorController extends Controller
{
    public function index()
    {
        $tipos = CatTipoAcreedor::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoAcreedor::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoAcreedor::findOrFail($id);
        $tipo->delete();
    }
}