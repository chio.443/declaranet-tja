<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatTipoDeclaracion;

use App\Declaracion;
use App\InformacionPersonal;
use App\CatEntePublico;
use DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

use Carbon\Carbon;

class ReportesController2 extends Controller
{

    public function tipoDeclaracion(Request $req)
    {
        $tipoDeclaracion = CatTipoDeclaracion::orderBy('id','asc')->get();

        return response()->json(['tipoDeclaracion' =>$tipoDeclaracion]);
    }

    public function periodos()
    {
        $resultado = Declaracion::select(DB::raw("date_part('year',created_at) as year"))->distinct()->get();
		
		//$periodos = $resultado->pluck('year');

        return response()->json(['periodos' =>$resultado]);
    }

    public function declarantes(Request $request)
    {
       			$anioact    =  date("Y");

       			$condicion ="";
       			$condicion2 = "";
       			$condicion3=$request->tipo_declaracion;

       			if($request->tipo_declaracion == 1){//inicial
       				$condicion = " and (date_part('year',den.fecha_posesion) = '".$request->periodo."' and den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) = '".$anioact."'";
       			}
       			if($request->tipo_declaracion == 2){
       				$condicion = " and (date_part('year',den.fecha_posesion) != '".$request->periodo."' or den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) != '".$anioact."'";
       			}


				$declarantes = DB::select("select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
				from informacion_personal ip
				inner join declaraciones de 
				on ip.id = de.id_ip
				inner join datos_encargo_actual den
				on ip.id = den.informacion_personal_id
				where ip.deleted_at is null
				and de.deleted_at is null
				and den.deleted_at is null
				and ip.deleted_at is null
				".$condicion."
				and den.fecha_termino is null
				and den.ente_publico_id = ".$request->dependencia_id."
				and de.tipo_declaracion_id = ".$request->tipo_declaracion."
				GROUP BY (rfc)				
				order by nombres asc, primer_apellido asc, segundo_apellido asc");


			$faltantes = InformacionPersonal::with('encargo')
                    ->whereHas("encargo", function($q) use ($request, $condicion2,$condicion3){
                                                       $q->where("ente_publico_id","=",$request->dependencia_id)
                                                       ->where(function($q) use ($condicion3, $condicion2){

                                                       		if($condicion3 ==1){
                                                       			 $q->whereRaw($condicion2)
                                                       				->where('tipo_dec',$condicion3);
                                                       		}
                                                       		else{
                                                       			$q->whereRaw($condicion2)
                                                       			  ->orWhere('tipo_dec',$condicion3);

                                                       		}
														      
														  });
                                                       
                                                      })
                    ->whereNotIn('id',function($query) use ($request) {
									   $query->select('id_ip')
									   ->from('declaraciones')
									   ->where('tipo_declaracion_id',$request->tipo_declaracion)
									   ->whereNull('deleted_at');
					})->distinct('rfc')
					->orderBy('nombres','asc')
                    ->orderBy('primer_apellido','asc')
                    ->orderBy('segundo_apellido','asc')
                    ->get();
     



		$total = count($declarantes);


		$entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->get();

		$porcentaje = InformacionPersonal::select('rfc')
					->whereIn('id',function($query)use ($request,$condicion2, $condicion3) {
								   $query->select('informacion_personal_id')->from('datos_encargo_actual')->whereNull('deleted_at')
								   ->where('datos_encargo_actual.ente_publico_id',$request->dependencia_id)
									->where(function($q) use ($condicion3, $condicion2){
									       	if($condicion3 ==1){
                                               $q->whereRaw($condicion2)
									       		->where('datos_encargo_actual.tipo_dec',$condicion3);
                                       		}
                                       		else{
                                      			$q->whereRaw($condicion2)
										       ->orWhere('datos_encargo_actual.tipo_dec',$condicion3);
											}
									  })										
								   ->whereNull('datos_encargo_actual.fecha_termino');
					})
					->whereNull('informacion_personal.deleted_at')
					->groupBy('rfc')
					->havingRaw('count(rfc) = 1')
					->get()->count();

					


		if($porcentaje>0){
			$resp = ($total*100)/$porcentaje;
		}
		else{
			$resp = 0;
		}

		$respuesta  = ['declarantes' =>$declarantes, 'avance' =>$resp,'entidad'=>$entidad, 'porcentaje' =>$porcentaje, 'total'=>$total,'faltantes' => $faltantes];


	    return response()->json($respuesta);
    }

     public function exportarEx($tipo,$ente,$periodo)
    {
    ///consulta de los declarantes    
 	   $anioact    =  date("Y");

       			$condicion ="";
       			$condicion2 = "";
       			$condicion3 = $tipo;

       			if($tipo == 1){//inicial
       				$condicion = " and (date_part('year',den.fecha_posesion) = '".$periodo."'  and den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) = '".$anioact."'";
       			}
       			if($tipo == 2){
       				$condicion = " and (date_part('year', den.fecha_posesion) != '".$periodo."'  or den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) != '".$anioact."'";
       			}
   

				$declarantes = DB::select("select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec,
					max(ce.valor) as valor
				from informacion_personal ip
				inner join declaraciones de 
				on ip.id = de.id_ip
				inner join datos_encargo_actual den
				on ip.id = den.informacion_personal_id
				inner join cat_ente_publico ce
				on ce.id = den.ente_publico_id
				where ip.deleted_at is null
				and de.deleted_at is null
				and den.deleted_at is null
				and ip.deleted_at is null
				".$condicion."
				and den.fecha_termino is null
				and den.ente_publico_id = ".$ente."
				and de.tipo_declaracion_id = ".$tipo."
				GROUP BY (rfc)				
				order by nombres asc, primer_apellido asc, segundo_apellido asc");

	//se crea el objeto para el archivo

		$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
		$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/

		$spreadsheet->setActiveSheetIndex(0);
		$activeSheet = $spreadsheet->getActiveSheet();

		//encabezados
		$activeSheet->setCellValue('A1','RFC');
		$activeSheet->setCellValue('B1','Nombre');
		$activeSheet->setCellValue('C1','Dependencia');
		$activeSheet->setCellValue('D1','Cargo');
		$activeSheet->setCellValue('E1','Fecha');

		///Se escrben los registros
		foreach ($declarantes as $indice => $dato)
		   {
		    $activeSheet->setCellValue('A'.($indice+2), $dato->rfc);
		    $activeSheet->setCellValue('B'.($indice+2), $dato->nombres.' '.$dato->primer_apellido.' '.$dato->segundo_apellido);
		    $activeSheet->setCellValue('C'.($indice+2), $dato->valor);
		    $activeSheet->setCellValue('D'.($indice+2), $dato->empleo_cargo_comision);
		    $activeSheet->setCellValue('E'.($indice+2), Carbon::parse($dato->fecha_dec)->format('d/m/Y'));
		   }


		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Declaracion'.date('d.m.Y').'.xls"'); 
		header('Cache-Control: max-age=0');
		 
		$Excel_writer->save('php://output');
    }
    public function exportarExFalt($tipo,$ente,$periodo)
    {
    ///consulta de los declarantes    
 	   $anioact    =  date("Y");

       			$condicion ="";
       			$condicion2 = "";
       			$condicion3 = $tipo;

       			if($tipo == 1){//inicial
       				$condicion = " and (date_part('year',den.fecha_posesion) = '".$periodo."'  and den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) = '".$anioact."'";
       			}
       			if($tipo == 2){
       				$condicion = " and (date_part('year', den.fecha_posesion) != '".$periodo."'  or den.tipo_dec =".$condicion3.") ";
       				$condicion2 = "date_part('year',fecha_posesion) != '".$anioact."'";
       			}

	$faltantes = InformacionPersonal::with('encargo')
                    ->whereHas("encargo", function($q) use ($ente, $condicion2,$condicion3){
                                                       $q->where("ente_publico_id","=",$ente)
                                                       ->where(function($q) use ($condicion3, $condicion2){
														      if($condicion3 ==1){
                                                       			 $q->whereRaw($condicion2)
                                                       				->where('tipo_dec',$condicion3);
                                                       		}
                                                       		else{
                                                       			$q->whereRaw($condicion2)
                                                       			  ->orWhere('tipo_dec',$condicion3);

                                                       		}
                                                     		});
                                  })
                    ->whereNotIn('id',function($query) use ($tipo) {
									   $query->select('id_ip')
									   ->from('declaraciones')
									   ->where('tipo_declaracion_id',$tipo)
									   ->whereNull('deleted_at');
					})->distinct('rfc')
					->orderBy('nombres','asc')
                    ->orderBy('primer_apellido','asc')
                    ->orderBy('segundo_apellido','asc')
                    ->get();
     



	//se crea el objeto para el archivo

		$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
		$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/

		$spreadsheet->setActiveSheetIndex(0);
		$activeSheet = $spreadsheet->getActiveSheet();

		//encabezados
		$activeSheet->setCellValue('A1','RFC');
		$activeSheet->setCellValue('B1','Nombre');
		$activeSheet->setCellValue('C1','Dependencia');
		$activeSheet->setCellValue('D1','Cargo');
		$activeSheet->setCellValue('E1','Correo Personal');
		$activeSheet->setCellValue('F1','Correo Laboral');
		$activeSheet->setCellValue('G1','Tel Personal');
		$activeSheet->setCellValue('H1','Tel Laboral');

		///Se escrben los registros
		foreach ($faltantes as $indice => $dato)
		   {
		    $activeSheet->setCellValue('A'.($indice+2), $dato->rfc);
		    $activeSheet->setCellValue('B'.($indice+2), $dato->nombres.' '.$dato->primer_apellido.' '.$dato->segundo_apellido);
		    $activeSheet->setCellValue('C'.($indice+2), $dato->valor);
		    $activeSheet->setCellValue('D'.($indice+2), $dato->empleo_cargo_comision);
		    $activeSheet->setCellValue('E'.($indice+2), $dato->correo_electronico_personal);
		    $activeSheet->setCellValue('F'.($indice+2), $dato->correo_electronico_laboral);
		    $activeSheet->setCellValue('G'.($indice+2), $dato->telefono_particular);
		    $activeSheet->setCellValue('H'.($indice+2), $dato->telefono_laboral);
		   }


		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Faltantes'.date('d.m.Y').'.xls"'); 
		header('Cache-Control: max-age=0');
		 
		$Excel_writer->save('php://output');
    }

    ////////////////////Reporte de cumplimiento/////////////////////
    public function cumplimiento(Request $request)
    {
    	//si se seleccionan todas las dependencias
    	$total=0;
    	$anioact    =  date("Y");

    	$condicion3=$request->tipo_declaracion;

		if($request->tipo_declaracion == 1){//inicial
			$condicion = " and (date_part('year',datos_encargo_actual.fecha_posesion) = '".$request->periodo."' and den.tipo_dec =".$condicion3.") ";
			$condicion2 = "date_part('year',datos_encargo_actual.fecha_posesion) = '".$anioact."'";
		}

		if($request->tipo_declaracion == 2){
			$condicion = " and (date_part('year',datos_encargo_actual.fecha_posesion) != '".$request->periodo."' or den.tipo_dec =".$condicion3.") ";
			$condicion2 = "date_part('year',datos_encargo_actual.fecha_posesion) != '".$anioact."'";
		}

   		$total = InformacionPersonal::whereNull('deleted_at')
   				->whereHas("encargo", function($q) use ($condicion2,$condicion3){
                                   	if($condicion3 ==1){
                               			 $q->whereRaw($condicion2)
                               				->where('tipo_dec',$condicion3);
                               		}
                               		else{
                               			$q->whereRaw($condicion2)
                               			  ->orWhere('tipo_dec',$condicion3);
                               		}
                                  })
    			->distinct('rfc')
    			->get()
    			->count();


    	if($request->dependencia_id == 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;

    		foreach ($entidades as $indice => $dato)
		   {
		   	$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;


			$cumplidos =DB::table('declaraciones')
		            	->select("id_ip")
						->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
						->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
						->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
						->where('cat_ente_publico.id',$dato->id)
						->whereNotNull('datos_encargo_actual.ente_publico_id')
						->whereNull('declaraciones.deleted_at')
						->whereNull('datos_encargo_actual.fecha_termino')
						->where(function($q) use ($condicion2,$condicion3){
						       

						       if($condicion3 ==1){
                           			 $q->whereRaw($condicion2)	
						       			->where('datos_encargo_actual.tipo_dec',$condicion3);
                           		}
                           		else{
                           			$q->whereRaw($condicion2)	
						       			->orWhere('datos_encargo_actual.tipo_dec',$condicion3);

                           		}

						  })	
						->whereRaw("date_part('year',declaraciones.created_at) = '".$request->periodo."'")
						->where('tipo_declaracion_id',$request->tipo_declaracion)
						->whereNull('informacion_personal.deleted_at')
						->groupBy('id_ip')
						->get()
						->count();




			  $entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();

		
			 $obligados = InformacionPersonal::select('rfc')
					->whereIn('id',function($query)use ($dato,$condicion2,$condicion3) {
								   $query->select('informacion_personal_id')->from('datos_encargo_actual')->whereNull('deleted_at')
								   ->where('datos_encargo_actual.ente_publico_id',$dato->id)
									->where(function($q) use ($condicion2,$condicion3){
										          if($condicion3 ==1){
					                           			 $q->whereRaw($condicion2)	
											       				->where('datos_encargo_actual.tipo_dec',$condicion3);
					                           		}
					                           		else{
					                           			$q->whereRaw($condicion2)	
											       			->orWhere('datos_encargo_actual.tipo_dec',$condicion3);

					                           		}

										  })	
								   ->whereNull('datos_encargo_actual.fecha_termino');
					})
					->whereNull('informacion_personal.deleted_at')
					->groupBy('rfc')
					->havingRaw('count(rfc) = 1')
					->get()->count();

					
		

			
			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			$total2 = $cumplidos*100/$total;

			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;
		   }
				array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

    	}else{

			$cumplidos =DB::table('declaraciones')
		            	->select("id_ip")
						->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
						->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
						->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
						->where('cat_ente_publico.id',$request->dependencia_id)
						->whereNotNull('datos_encargo_actual.ente_publico_id')
						->whereNull('declaraciones.deleted_at')
						->whereNull('datos_encargo_actual.fecha_termino')
						->where(function($q) use ($condicion2,$condicion3){
						          if($condicion3 ==1){
                           			 $q->whereRaw($condicion2)	
						       				->where('datos_encargo_actual.tipo_dec',$condicion3);
                           		}
                           		else{
                           			$q->whereRaw($condicion2)	
						       			->orWhere('datos_encargo_actual.tipo_dec',$condicion3);

                           		}

						  })	
						->whereRaw("date_part('year',declaraciones.created_at) = '".$request->periodo."'")
						->where('tipo_declaracion_id',$request->tipo_declaracion)
						->whereNull('informacion_personal.deleted_at')
						->groupBy('id_ip')
						->get()
						->count();

			  $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

		
			 $obligados = InformacionPersonal::select('rfc')
					->whereIn('id',function($query)use ($request,$condicion2,$condicion3) {
								   $query->select('informacion_personal_id')->from('datos_encargo_actual')->whereNull('deleted_at')
								   ->where('datos_encargo_actual.ente_publico_id',$request->dependencia_id)
									->where(function($q) use ($condicion2,$condicion3){
										       if($condicion3 ==1){
					                           			 $q->whereRaw($condicion2)	
											       				->where('datos_encargo_actual.tipo_dec',$condicion3);
					                           		}
					                           		else{
					                           			$q->whereRaw($condicion2)	
											       			->orWhere('datos_encargo_actual.tipo_dec',$condicion3);

					                           		}

										  })	
								   ->whereNull('datos_encargo_actual.fecha_termino');
					})
					->whereNull('informacion_personal.deleted_at')
					->groupBy('rfc')
					->havingRaw('count(rfc) = 1')
					->get()->count();


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total = $cumplidos*100/$total;

			$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

    	}

	    return response()->json($respuesta);
    }
     ////////////////////Reporte de cumplimiento/////////////////////
    public function cumplimientoExc($tipo,$ente,$periodo)
    {
    	$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
		$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/

		$spreadsheet->setActiveSheetIndex(0);
		$activeSheet = $spreadsheet->getActiveSheet();

		//encabezados
		$activeSheet->setCellValue('A1','Dependencia');
		$activeSheet->setCellValue('B1','Obligados');
		$activeSheet->setCellValue('C1','Cumplidos');
		$activeSheet->setCellValue('D1','% Individual');
		$activeSheet->setCellValue('E1','Faltantes');
		$activeSheet->setCellValue('F1','% Total');
			//si se seleccionan todas las dependencias
    	$total=0;
    	$total = InformacionPersonal::select('*')->get()->count();


    	//si se seleccionan todas las dependencias
    	if($ente== 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;
			$row=0;

    		foreach ($entidades as $indice => $dato)
		   {

    		$cumplidos =DB::table('declaraciones')
            	->select("id_ip")
				->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
				->whereRaw("date_part('year',declaraciones.created_at) = '".$periodo."'")
				->where('datos_encargo_actual.ente_publico_id',$dato->id)
				->where('declaraciones.tipo_declaracion_id',$tipo)	
				->whereNull('declaraciones.deleted_at')
				->whereNull('datos_encargo_actual.fecha_termino')
				->whereNull('informacion_personal.deleted_at')
				->groupBy('id_ip')
				->get()
				->count();

			$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();

			 $obligados = InformacionPersonal::select('rfc')
					->whereIn('id',function($query)use ($dato) {
								   $query->select('informacion_personal_id')->from('datos_encargo_actual')->whereNull('deleted_at')
								   ->where('datos_encargo_actual.ente_publico_id',$dato->id)
								   ->whereNull('datos_encargo_actual.fecha_termino');
					})
					->whereNull('informacion_personal.deleted_at')
					->groupBy('rfc')
					->havingRaw('count(rfc) = 1')
					->get()->count();


			$total = InformacionPersonal::select('*')->get()->count();
			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			$total2 = $cumplidos*100/$total;

			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;

			$activeSheet->setCellValue('A'.($indice+2), $entidad->valor);
		    $activeSheet->setCellValue('B'.($indice+2), $obligados);
		    $activeSheet->setCellValue('C'.($indice+2), $cumplidos);
		    $activeSheet->setCellValue('D'.($indice+2), $individual);
		    $activeSheet->setCellValue('E'.($indice+2), $faltantes);
		    $activeSheet->setCellValue('F'.($indice+2), $total2);
		    $row=$indice;

		   }
			$activeSheet->setCellValue('A'.($indice+2), 'TOTAL');
		    $activeSheet->setCellValue('B'.($indice+2), $totalObligados);
		    $activeSheet->setCellValue('C'.($indice+2), $totalCumplidos);
		    $activeSheet->setCellValue('D'.($indice+2), $totalTotal);
		    $activeSheet->setCellValue('E'.($indice+2), $totalFaltantes);
		    $activeSheet->setCellValue('F'.($indice+2), $totalTotal);

    	}else{

    		$cumplidos =DB::table('declaraciones')
            	->select("*")
				->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
				->whereRaw("date_part('year',declaraciones.created_at) = '".$periodo."'")
				->where('datos_encargo_actual.ente_publico_id',$ente)
				->whereNull('declaraciones.deleted_at')
				->whereNull('informacion_personal.deleted_at')
				->whereNull('datos_encargo_actual.fecha_termino')
				->where('declaraciones.tipo_declaracion_id',$tipo)
				->orderBy('informacion_personal.rfc','asc')->get()->count();

			$entidad = CatEntePublico::select('valor')->where('id',$ente)->first();

			$obligados = InformacionPersonal::select('rfc')
					->whereIn('id',function($query)use ($request) {
								   $query->select('informacion_personal_id')->from('datos_encargo_actual')->whereNull('deleted_at')
								   ->where('datos_encargo_actual.ente_publico_id',$ente);
					})
					->whereNull('informacion_personal.deleted_at')
					->groupBy('rfc')
					->havingRaw('count(rfc) = 1')
					->get()->count();


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total2 = $cumplidos*100/$total;

			$activeSheet->setCellValue('A2', $entidad->valor);
		    $activeSheet->setCellValue('B2', $obligados);
		    $activeSheet->setCellValue('C2', $cumplidos);
		    $activeSheet->setCellValue('D2', $individual);
		    $activeSheet->setCellValue('E2', $faltantes);
		    $activeSheet->setCellValue('F2', $total2);
    	}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Cumplimiento'.date('d.m.Y').'.xls"'); 
		header('Cache-Control: max-age=0');
		 
		$Excel_writer->save('php://output');

	} ////////////////////Reporte de cumplimiento/////////////////////
    public function cumplimientoExc2(Request $request)
    {
    	dd($request);
    	$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
		$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/

		$spreadsheet->setActiveSheetIndex(0);
		$activeSheet = $spreadsheet->getActiveSheet();

		//encabezados
		$activeSheet->setCellValue('A1','Dependencia');
		$activeSheet->setCellValue('B1','Obligados');
		$activeSheet->setCellValue('C1','Cumplidos');
		$activeSheet->setCellValue('D1','% Individual');
		$activeSheet->setCellValue('E1','Faltantes');
		$activeSheet->setCellValue('F1','% Total');
			//si se seleccionan todas las dependencias
    	$total=0;
    	$total = InformacionPersonal::select('*')->get()->count();


    	//si se seleccionan todas las dependencias
    	if($ente== 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;
			$row=0;

    		foreach ($entidades as $indice => $dato)
		   {

    		$cumplidos =DB::table('declaraciones')
            	->select("*")
				->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
				->where('datos_encargo_actual.ente_publico_id',$dato->id)
				->whereNull('declaraciones.deleted_at')
				->whereNull('informacion_personal.deleted_at')
				->where('declaraciones.tipo_declaracion_id',$tipo)->get()->count();

			$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();

			$obligados = InformacionPersonal::select('*')
						->join('datos_encargo_actual', 'datos_encargo_actual.informacion_personal_id', '=', 'informacion_personal.id')
						->where('datos_encargo_actual.ente_publico_id',$dato->id)
						->whereNull('informacion_personal.deleted_at')
						->get()->count();

			$total = InformacionPersonal::select('*')->get()->count();
			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			$total2 = $cumplidos*100/$total;

			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;

			$activeSheet->setCellValue('A'.($indice+2), $entidad->valor);
		    $activeSheet->setCellValue('B'.($indice+2), $obligados);
		    $activeSheet->setCellValue('C'.($indice+2), $cumplidos);
		    $activeSheet->setCellValue('D'.($indice+2), $individual);
		    $activeSheet->setCellValue('E'.($indice+2), $faltantes);
		    $activeSheet->setCellValue('F'.($indice+2), $total2);
		    $row=$indice;

		   }
			$activeSheet->setCellValue('A'.($indice+2), 'TOTAL');
		    $activeSheet->setCellValue('B'.($indice+2), $totalObligados);
		    $activeSheet->setCellValue('C'.($indice+2), $totalCumplidos);
		    $activeSheet->setCellValue('D'.($indice+2), $totalTotal);
		    $activeSheet->setCellValue('E'.($indice+2), $totalFaltantes);
		    $activeSheet->setCellValue('F'.($indice+2), $totalTotal);

    	}else{

    		$cumplidos =DB::table('declaraciones')
            	->select("id_ip")
				->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
				->whereRaw("date_part('year',declaraciones.created_at) = '".$periodo."'")
				->where('datos_encargo_actual.ente_publico_id',$ente)
				->where('declaraciones.tipo_declaracion_id',$tipo)	
				->whereNull('declaraciones.deleted_at')
				->whereNull('informacion_personal.deleted_at')
				->groupBy('id_ip')
				->get()
				->count();


			$entidad = CatEntePublico::select('valor')->where('id',$ente)->first();

			$obligados = InformacionPersonal::select('*')
						->join('datos_encargo_actual', 'datos_encargo_actual.informacion_personal_id', '=', 'informacion_personal.id')
						->where('datos_encargo_actual.ente_publico_id',$ente)
						->whereNull('informacion_personal.deleted_at')
						->get()->count();


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total2 = $cumplidos*100/$total;

			$activeSheet->setCellValue('A2', $entidad->valor);
		    $activeSheet->setCellValue('B2', $obligados);
		    $activeSheet->setCellValue('C2', $cumplidos);
		    $activeSheet->setCellValue('D2', $individual);
		    $activeSheet->setCellValue('E2', $faltantes);
		    $activeSheet->setCellValue('F2', $total2);
    	}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Cumplimiento'.date('d.m.Y').'.xls"'); 
		header('Cache-Control: max-age=0');
		 
		$Excel_writer->save('php://output');

	}


 
}