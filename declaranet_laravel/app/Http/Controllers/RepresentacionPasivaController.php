<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Representacion_pasiva;

class RepresentacionPasivaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $representaciones = Representacion_pasiva::orderBy('id', 'DESC')->get();
        return response()->json(['representaciones' => $representaciones]);
    }
    public function item($id)
    {
        $representaciones = Representacion_pasiva::where('ip_id', $id)->get();
        return response()->json(['representaciones' => $representaciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $representacion = Representacion_pasiva::updateOrCreate(
            ['id' => $request->id],
            [
                'tipo_representacion_id' => $request->tipo_representacion_id,
                'nombre' => $request->nombre,
                'fecha_inicio_representacion' => $request->fecha_inicio_representacion,
                'nacionalidades_id' => $request->nacionalidades_id,
                'curp' => $request->curp,
                'rfc' => $request->rfc,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'tiene_intereses' => $request->tiene_intereses,
                'ocupacion_profesion' => $request->ocupacion_profesion,
                'sector_industria_id' => $request->sector_industria_id,
                'observaciones' => $request->observaciones,
                'tipo_representacion_especificar' => $request->tipo_representacion_especificar,
                'ip_id' => $request->ip_id,




            ]
        );

        return response()->json(['representacion' => $representacion]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $representacion = Representacion_pasiva::findOrFail($id);
        $representacion->delete();
    }
}
