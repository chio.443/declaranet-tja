<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CatTipoRepresentacion;

class CatTipoRepresentacionController extends Controller
{
    public function index()
    {
        $tipos = CatTipoRepresentacion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoRepresentacion::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoRepresentacion::findOrFail($id);
        $tipo->delete();
    }
}