<?php

namespace App\Http\Controllers;

use App\CatUnidadMedidaPlazo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatUnidadMedidaPlazoController extends Controller
{
    public function index()
    {
        $unidades = CatUnidadMedidaPlazo::orderBy('valor', 'ASC')->get();
        return response()->json(['unidades' => $unidades]);
    }

    public function store(Request $request)
    {
        $unidad = CatUnidadMedidaPlazo::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['unidad' => $unidad]);
    }

    public function delete($id)
    {
        $unidad = CatUnidadMedidaPlazo::findOrFail($id);
        $unidad->delete();
    }
}