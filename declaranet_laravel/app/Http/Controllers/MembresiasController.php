<?php

namespace App\Http\Controllers;

use App\Membresias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Auth;

class MembresiasController extends Controller
{
    public function index()
    {
        $membresias = Membresias::orderBy('id', 'DESC')->get();
        return response()->json(['membresias' => $membresias]);
    }

    public function getPersonas()
    {
        $personas = DB::table('cat_persona_juridica')
            ->select('id as id', 'codigo as codigo', 'valor as valor')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json(['personas' => $personas]);

    }

    public function getNaturalezas()
    {
        $naturalezas = DB::table('cat_naturaleza')
            ->select('id as id', 'codigo as codigo', 'valor as valor')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json(['naturalezas' => $naturalezas]);

    }

    public function getColaboraciones()
    {
        $colaboraciones = DB::table('cat_colaboraciones')
            ->select('id as id', 'codigo as codigo', 'valor as valor')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json(['colaboraciones' => $colaboraciones]);

    }


    public function item($id)
    {
        $id2 = Auth::user()->id;

        $membresias = Membresias::where('ip_id', $id2)->get();
        return response()->json(['membresias' => $membresias]);
    }

    public function store(Request $request)
    {
        $id2 = Auth::user()->id;

        $membresia = Membresias::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $id2,
                'tipo_operacion_id' => $request->tipo_operacion_id,
                'nombre_institucion' => $request->nombre_institucion,
                'registro_publico' => $request->registro_publico,
                'tipo_sociedad_id' => $request->tipo_sociedad_id,
                'tipo_sociedad_especificar' => $request->tipo_sociedad_especificar,
                'antiguedad' => $request->antiguedad,
                'sector_industria_id' => $request->sector_industria_id,
                'tipo_participacion' => $request->tipo_participacion,
                //'fecha_inicio' => $request->fecha_inicio,
                'fecha_inicio' => Carbon::parse($request->fecha_inicio),
                'observaciones' => $request->observaciones,
                'domicilio_pais_id' => $request->domicilio_pais_id,
                'domicilio_entidad_federativa_id' => $request->domicilio_entidad_federativa_id,
                'domicilio_municipio_id' => $request->domicilio_municipio_id,
                'domicilio_localidad_id' => $request->domicilio_localidad_id,
                'domicilio_colonia' => $request->domicilio_colonia,
                'domicilio_vialidad_tipo' => $request->domicilio_vialidad_tipo,
                'domicilio_vialidad_nombre' => $request->domicilio_vialidad_nombre,
                'domicilio_cp' => $request->domicilio_cp,
                'domicilio_numext' => $request->domicilio_numext,
                'domicilio_numint' => $request->domicilio_numint,
                'especificar_sector_industria' => $request->especificar_sector_industria,
                'especificar_sector_industria' => $request->especificar_sector_industria,
                'participacion' => $request->participacion,
                'tipo_responsable_id' => $request->tipo_responsable_id,
            ]
        );

        return response()->json(['membresia' => $membresia]);
    }

    public function delete($id)
    {
        $membresia = Membresias::findOrFail($id);
        $membresia->delete();
    }
}