<?php

namespace App\Http\Controllers;

use App\CatIdentificacionBien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatIdentificacionBienController extends Controller
{
    public function index()
    {
        $identificaciones = CatIdentificacionBien::orderBy('valor', 'ASC')->get();
        return response()->json(['identificaciones' => $identificaciones]);
    }

    public function store(Request $request)
    {
        $identificacion = CatIdentificacionBien::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['identificacion' => $identificacion]);
    }

    public function delete($id)
    {
        $identificacion = CatIdentificacionBien::findOrFail($id);
        $identificacion->delete();
    }
}