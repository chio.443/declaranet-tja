<?php

namespace App\Http\Controllers;

use App\CatTipoBienMuebleNr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoBienMuebleNrController extends Controller
{
    public function index()
    {
        $tipos = CatTipoBienMuebleNr::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoBienMuebleNr::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoBienMuebleNr::findOrFail($id);
        $tipo->delete();
    }
}