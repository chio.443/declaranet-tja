<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Declaracion;

use App\Movimientos;
use App\EncargoActual;
use App\CatEntePublico;
use App\CatTipoDeclaracion;
use App\BitacoraDeclaracion;
use App\InformacionPersonal;
use Illuminate\Http\Request;
//use DB;
use App\DeclaracionJustificada;

use App\UsuarioTipoDeclaracion;
use Illuminate\Support\Facades\DB;

class UsuarioTipoDecController extends Controller
{
    public function index()
    {
        $response = UsuarioTipoDeclaracion::get();
        return response()->json(['response' => $response]);
    }

    public function show($id)
    {
        $response = UsuarioTipoDeclaracion::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function movimiento(Request $request)
    {
        try {
            DB::beginTransaction();
            $ip_id =  $request->ip_id;
            $tipo_movimiento_id =  $request->tipo_movimiento_id;
            //  $tipo_dec_id =  $request->tipo_dec_id;
            $fecha_posesion =  $request->fecha_posesion;
            $fecha_termino =  $request->fecha_termino;
            $ente_publico_id =  $request->ente_publico_id;
            $date = Carbon::parse($fecha_termino);
            $periodo = $date->year;
            $mes = $date->month;
            // $periodo =  $request->periodo;
            // $declaracion_id =  $request->declaracion_id;
            // $fecha_posesion =  $request->fecha_posesion;

            if ($mes < 5) {
                // Baja (Antes de Mayo - Final)
                // Buscar la el tipo_dec anterior y borrarlo
                // Siempre y cuando tenga almenos una declaracion previa
                $decs = Declaracion::where('id_ip', $ip_id)->get();

                if (count($decs) == 0) {
                    $CHECK = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                        ->where('tipo_dec_id', 1)
                        ->whereNull('declaracion_id')->get();

                    if ((count($CHECK) == 0)) {
                        $u_tipo_dec_in = new UsuarioTipoDeclaracion;

                        $u_tipo_dec_in->ip_id = $ip_id;
                        $u_tipo_dec_in->tipo_dec_id = 1;
                        $u_tipo_dec_in->periodo = $periodo;
                        $u_tipo_dec_in->ente_publico_id = $ente_publico_id;
                        $u_tipo_dec_in->fecha_termino = $fecha_termino;
                        $u_tipo_dec_in->fecha_posesion = $fecha_posesion;

                        $u_tipo_dec_in->save();
                    }
                }

                // Eliminar
                $tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                    ->where('tipo_dec_id', 2)
                    ->where('periodo', $periodo)
                    ->whereNull('declaracion_id')
                    ->orderBy('id', 'desc')
                    ->first();
                //->firstOrFail();
                if ($tipo_dec) {
                    $tipo_dec->delete();
                }

                $CHECK = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                    ->where('tipo_dec_id', 3)
                    ->where('fecha_termino', $fecha_termino)
                    ->get();

                if ((count($CHECK) == 0)) {
                    $u_tipo_dec = new UsuarioTipoDeclaracion;

                    $u_tipo_dec->ip_id = $ip_id;
                    $u_tipo_dec->tipo_dec_id = 3;
                    $u_tipo_dec->periodo = $periodo;
                    $u_tipo_dec->ente_publico_id = $ente_publico_id;
                    $u_tipo_dec->fecha_termino = $fecha_termino;
                    $u_tipo_dec->tipo_movimiento_id = $tipo_movimiento_id;
                    $u_tipo_dec->fecha_posesion = $fecha_posesion;
                    //$u_tipo_dec_in->fecha_posesion = $fecha_posesion;
                    $u_tipo_dec->save();
                }
            } else {
                if ($periodo < '2021') {
                    //dd($periodo);
                    $decs = Declaracion::where('id_ip', $ip_id)->get();

                    if (count($decs) == 0) {
                        $CHECK = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                            ->where('tipo_dec_id', 1)
                            ->whereNull('declaracion_id')->get();

                        if ((count($CHECK) == 0)) {
                            $u_tipo_dec_in = new UsuarioTipoDeclaracion;

                            $u_tipo_dec_in->ip_id = $ip_id;
                            $u_tipo_dec_in->tipo_dec_id = 1;
                            $u_tipo_dec_in->periodo = $periodo;
                            $u_tipo_dec_in->ente_publico_id = $ente_publico_id;
                            $u_tipo_dec_in->fecha_termino = $fecha_termino;
                            $u_tipo_dec_in->fecha_posesion = $fecha_posesion;

                            $u_tipo_dec_in->save();
                        }
                    }

                    // Eliminar
                    $tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                        ->where('tipo_dec_id', 2)
                        //->where('periodo', $periodo)
                        ->whereNull('declaracion_id')
                        ->orderBy('id', 'desc')
                        ->first();
                    //->firstOrFail();
                    if ($tipo_dec) {
                        $tipo_dec->delete();
                    }

                    $CHECK = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                        ->where('tipo_dec_id', 3)
                        ->where('fecha_termino', $fecha_termino)
                        ->get();

                    if ((count($CHECK) == 0)) {
                        $u_tipo_dec = new UsuarioTipoDeclaracion;

                        $u_tipo_dec->ip_id = $ip_id;
                        $u_tipo_dec->tipo_dec_id = 3;
                        $u_tipo_dec->periodo = $periodo;
                        $u_tipo_dec->ente_publico_id = $ente_publico_id;
                        $u_tipo_dec->fecha_termino = $fecha_termino;
                        $u_tipo_dec->tipo_movimiento_id = $tipo_movimiento_id;
                        //$u_tipo_dec_in->fecha_posesion = $fecha_posesion;
                        $u_tipo_dec->fecha_posesion = $fecha_posesion;

                        $u_tipo_dec->save();
                    }
                } else {

                    $CHECK = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                        ->where('tipo_dec_id', 3)
                        ->where('fecha_termino', $fecha_termino)
                        ->get();

                    if ((count($CHECK) == 0)) {
                        // Baja - Anual ( Despues de Mayo )
                        // Dejar la declaracion Anual / Inicial y agregar la de baja
                        // Insertar registro para declaracion Final
                        // Baja - Inicial ( Sin declaraciones previas - Inicial y Final)

                        $u_tipo_dec = new UsuarioTipoDeclaracion;

                        $u_tipo_dec->ip_id = $ip_id;
                        $u_tipo_dec->tipo_dec_id = 3;
                        $u_tipo_dec->periodo = $periodo;
                        $u_tipo_dec->ente_publico_id = $ente_publico_id;
                        $u_tipo_dec->fecha_termino = $fecha_termino;
                        $u_tipo_dec->tipo_movimiento_id = $tipo_movimiento_id;
                        //$u_tipo_dec_in->fecha_posesion = $fecha_posesion;
                        $u_tipo_dec->fecha_posesion = $fecha_posesion;

                        $u_tipo_dec->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['mensaje' => 'ok']);
    }

    public function transferencia(Request $request)
    {
        try {
            DB::beginTransaction();
            // No mas de una declaracion Anual en el año
            // Si tiene anual y tiene final si puede tener inicial
            // Si se puede tener mas de una inicial en el año
            // Los Finales -- solo tienen  usuario_tipo_dec 3 porque ya cumpliron
            // Mayor a 60 dias y Finales que ya cumplieron les toca incial

            $ip_id = $request->ip_id; // 123115;

            $info = InformacionPersonal::with('encargo')->where('id', $ip_id)->first();

            $ente_publico_id =  $request->ente_publico_id;
            $fecha_posesion = Carbon::parse($request->fecha_posesion);
            $fecha_termino = Carbon::parse($request->fecha_termino);

            $tipo_dec = $request->tipo_dec ?: 1;

            $periodo = $request->periodo ?: now()->year;
            // $periodo = now()->year;
            /*
             $ente_publico_id =  29;
            */

            $tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                ->where('periodo', $periodo)
                ->orderBy('id', 'desc')
                ->first();

            // $mes = $date->month;

            if ($tipo_dec) {
                // Verificar si la ultima tipo declarcion es 3
                if ($tipo_dec->tipo_dec_id == 3) {
                    // Verificar que el tipo declaracion sea 3 y no tenga  declaracion_id
                    if ($tipo_dec->declaracion_id != null) {
                        // Si ya presento final le toca declaracion inicial
                        $tipo_dec = 1;
                    } else {
                        // Calculamos la realcion de fecha de termino y la fecha de posesion
                        $diff = $fecha_termino->diffInDays($fecha_posesion);
                        if ($diff < 60) {
                            // Fecha reintegro menor a 60 dias
                            // Eliminar tipo_dec con final
                            $tipo_dec->delete();

                            // Verificar si tiene o no declaraciones
                            $dec = Declaracion::where('id_ip', $ip_id)
                                ->orderBy('id', 'desc')
                                ->first();

                            if (empty($dec)) {
                                // Si no tiene declaraciones se creara una inicial
                                // Siempre y cuando no tenga una asignada previamente
                                $tipo_dec_2 = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                                    ->where('tipo_dec_id', 1)
                                    ->whereNull('declaracion_id')
                                    ->orderBy('id', 'desc')
                                    ->first();

                                if (empty($tipo_dec_2)) {
                                    $tipo_dec = 1;
                                }
                            } else {
                                // Verificar el periodo de la ultima declaracion
                                if ($dec->periodo != $periodo) {
                                    // Si el periodo es anterior se requeria declaracion anual
                                    // Se verifica que no tenga una declaracion anual asignada
                                    $tipo_dec_3 = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                                        ->where('tipo_dec_id', 2)
                                        ->where('periodo', $periodo)
                                        ->whereNull('declaracion_id')
                                        ->orderBy('id', 'desc')
                                        ->first();

                                    if (empty($tipo_dec_3)) {
                                        $tipo_dec = 2;
                                    }
                                } else {
                                    /*
                                    Si pertenece al periodo actual el usuario tiene continuidad
                                    y ha cumplido con su declaración del periodo (sea anual o inicial), por lo tanto no se le asigna declaraciones.
                                    */
                                    // No debe existir dos anuales en el mismo periodo
                                }
                            }
                        } else {
                            // Fecha reintegro mayor a 60 dias
                            // Inicial
                            $tipo_dec = 1;
                        }
                    }
                } else {
                    // Si su ultima tipo_dec no es 3 es una transferencia sin baja
                    // No deben existir transferencias sin baja
                    // Reporta error
                    return response()->json(['errors' => ['message' => "Verifica, no deben existir transferencias sin baja"]], 422);
                }
            } else {
                // Usuario sin tipo_dec
                // Como ya presento final le toca declaracion inicial
                $tipo_dec = 1;
            }

            $u_tipo_dec = new UsuarioTipoDeclaracion;

            $u_tipo_dec->ip_id = $ip_id;
            $u_tipo_dec->tipo_dec_id = $tipo_dec;
            $u_tipo_dec->periodo = $periodo;
            $u_tipo_dec->ente_publico_id = $ente_publico_id;
            $u_tipo_dec->fecha_posesion = $fecha_posesion;

            $u_tipo_dec->save();


            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json('ok');
    }

    public function delcaracione(Request $request)
    {
        try {
            DB::beginTransaction();
            $ip_id =  $request->ip_id;
            $tipo_dec_id =  $request->tipo_dec_id;
            $declaracion_id =  $request->declaracion_id;
            $fecha_declaracion =  $request->fecha_declaracion;
            $fecha_posesion =  $request->fecha_posesion;
            $periodo = $request->periodo; //now()->year;
            $ente_publico_id =  $request->ente_publico_id;
            $fecha_termino = null;

            // Actualizar tipo_dec agregar declaracion_id
            // Buscamos su ultimo tipo_dec con los datos anteriores
            $tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                ->where('tipo_dec_id', $tipo_dec_id)
                ->where('periodo', $periodo)
                ->whereNull('declaracion_id')
                ->orderBy('id', 'desc')
                ->first();

            // Si existe agregamos el id de la declaracion y el ente en el cual realizo la declaracion
            if ($tipo_dec) {
                $tipo_dec->declaracion_id = $declaracion_id;
                $tipo_dec->fecha_declaracion = $fecha_declaracion;
                $tipo_dec->ente_publico_id = $ente_publico_id;

                $tipo_dec->save();
            } else {
                // Si no tiene Tipo_dec anterior agregamos un nuevo registro con los datos
                $u_tipo_dec = new UsuarioTipoDeclaracion;

                $u_tipo_dec->ip_id = $ip_id;
                $u_tipo_dec->tipo_dec_id = $tipo_dec_id;
                $u_tipo_dec->declaracion_id = $declaracion_id;
                $u_tipo_dec->fecha_declaracion = $fecha_declaracion;
                $u_tipo_dec->periodo = $periodo;
                $u_tipo_dec->ente_publico_id = $ente_publico_id;
                $u_tipo_dec->fecha_termino = $fecha_termino;
                $u_tipo_dec->fecha_posesion = $fecha_posesion;

                $u_tipo_dec->save();
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json('ok');
    }

    public function reactivardec(Request $request)
    {
        try {
            DB::beginTransaction();
            $ip_id =  $request->ip_id;
            $declaracion_id =  $request->declaracion_id;
            $ente_publico_id =  $request->ente_publico_id;
            $fecha_posesion =  $request->fecha_posesion;
            $tipo_dec_id =  $request->tipo_dec_id;
            // $periodo = now()->year;
            $periodo =  $request->periodo ? $request->periodo : now()->year;
            // Buscamos su ultimo tipo_dec con los datos anteriores
            $tipo_dec = UsuarioTipoDeclaracion::where('ip_id', $ip_id)
                ->where('declaracion_id', $declaracion_id)
                ->first();

            if ($tipo_dec) {
                // $tipo_dec = UsuarioTipoDeclaracion::findOrFail($tipo_dec->id);
                // $tipo_dec->delete();
                // Al tener una tipo dec anterior lo eliminamos
                $tipo_dec->delete();
            }
            // Agregar Tipo Dec
            $new_tipo_dec = new UsuarioTipoDeclaracion;

            $new_tipo_dec->ip_id = $ip_id;
            $new_tipo_dec->tipo_dec_id = $tipo_dec_id;
            $new_tipo_dec->periodo = $periodo;
            $new_tipo_dec->fecha_posesion = $fecha_posesion;
            $new_tipo_dec->ente_publico_id = $ente_publico_id;

            $new_tipo_dec->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json('ok');
    }

    public function nuevoUsuario(Request $request)
    {
        try {
            DB::beginTransaction();

            $ip_id =  $request->ip_id;
            $tipo_dec_id =  $request->tipo_dec_id;
            $periodo =  $request->periodo ? $request->periodo : now()->year;
            $ente_publico_id =  $request->ente_publico_id;
            $fecha_posesion = $request->fecha_posesion;
            // $periodo = now()->year;
            // Agregar Usuario
            $u_tipo_dec = new UsuarioTipoDeclaracion;

            $u_tipo_dec->ip_id = $ip_id;
            $u_tipo_dec->tipo_dec_id = $tipo_dec_id;
            $u_tipo_dec->periodo = $periodo;
            $u_tipo_dec->ente_publico_id = $ente_publico_id;
            // $u_tipo_dec->fecha_posesion = $fecha_posesion;
            $u_tipo_dec->fecha_posesion = Carbon::parse($fecha_posesion)->format('Y-m-d');

            $u_tipo_dec->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json('ok');
    }

    public function agregar(Request $request)
    {
        //dd($request);
        try {
            DB::beginTransaction();

            $ip_id =  $request->ip_id;
            $tipo_dec_id =  $request->tipo_dec;
            $periodo =  $request->periodo ? $request->periodo : now()->year;

            $encargo = EncargoActual::where('informacion_personal_id', $ip_id)->first();

            $ente_publico_id =  $encargo->ente_publico_id;
            $fecha_posesion = $encargo->fecha_posesion;
            $fecha_termino = $encargo->fecha_termino;
            // Agregar Usuario
            $u_tipo_dec = new UsuarioTipoDeclaracion;

            $u_tipo_dec->ip_id = $ip_id;
            $u_tipo_dec->tipo_dec_id = $tipo_dec_id;
            $u_tipo_dec->periodo = $periodo;
            $u_tipo_dec->ente_publico_id = $ente_publico_id;
            $u_tipo_dec->fecha_posesion = $fecha_posesion;
            $u_tipo_dec->fecha_termino = $fecha_termino;

            $u_tipo_dec->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['tipo_dec' => $u_tipo_dec]);
    }

    public function delete($id)
    {
        $u_tipo_dec = UsuarioTipoDeclaracion::findOrFail($id);
        $u_tipo_dec->delete();
    }
}
