<?php

namespace App\Http\Controllers;

use App\CatBancaria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatBancariaController extends Controller
{
    public function index()
    {
        $bancarias = CatBancaria::orderBy('valor', 'ASC')->get();
        return response()->json(['bancarias' => $bancarias]);
    }

    public function store(Request $request)
    {
        $bancaria = CatBancaria::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['bancaria' => $bancaria]);
    }

    public function delete($id)
    {
        $bancaria = CatBancaria::findOrFail($id);
        $bancaria->delete();
    }
}