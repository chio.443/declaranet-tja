<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ExperenciaLaboral;
use App\ExpLaboralFuncPrinc;

use App\CatVialidad;
use \App\Token;
use Carbon\Carbon;
use Auth;

class ExperienciaLaboralController extends Controller
{

    public function index()
    {
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request, $id)
    {
        /* $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid; */

        $experiencia = ExperenciaLaboral::where('informacion_personal_id', $id)->orderBy('id', 'asc')->get();

        return response()->json(['experiencia' => $experiencia]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //toda la informaicon personal
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        if (Auth::id() != $id) {
            return response()->json(['errorSession' => 'error']);
        }


        $funciones = $request->funcionesp;

        $informacion = $request->except([
            'tipo_vialidad', 'vialidad_nombre', 'vialidad', 'funciones', 'funcionesp',
            'rel_experiencia_laboral_funciones_princ'
        ]);
        //guardar todos los datos
        //dd($request->funcionesp);
        $informacion['fecha_ingreso'] = Carbon::parse($request->fecha_ingreso);

        $informacion['fecha_salida'] = Carbon::parse($request->fecha_salida);

        $registro = ExperenciaLaboral::updateOrCreate(
            ['informacion_personal_id' => $id, 'id' => $request['id']],
            $informacion
        );

        $registro->save();

        ///////////////////////explabfuncion/////////////
        /////primero borrar las que ya estan para no duplicar, no se si haya un método mejor
        $funcionesp = ExpLaboralFuncPrinc::where('exl_id', $registro->id);
        $funcionesp->forceDelete();

        // $num = count($funciones);
        $num = count((array) $funciones);

        for ($i = 0; $i < $num; $i++) {
            $funcion =  new ExpLaboralFuncPrinc([
                'exl_id' =>      $registro->id,
                'funcion_id' =>  $request->input('funcionesp.' . $i . '')
            ]);

            $funcion->save();
        }
        ///////////////////////
        // Carga de nuevo esl registro
        //$response = ExperenciaLaboral::where('informacion_personal_id', $registro->id)->orderBy('id', 'asc')->get();
        $response = $registro->refresh();

        return response()->json(['registro' => $response]);
        //////////////////////////////////////////////////////////////////
    }

    public function destroy($id)
    {
        $experiencia = ExperenciaLaboral::findOrFail($id);
        $experiencia->delete();
    }
}