<?php

namespace App\Http\Controllers;

use App\Representacion_activa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class RepresentacionActivaController extends Controller
{
    public function index()
    {
        $response = Representacion_activa::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $response = Representacion_activa::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = Representacion_activa::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_representacion_id' => $request->tipo_representacion_id,
                'nombre' => $request->nombre,
                'monto' => $request->monto,
                'rfc' => $request->rfc,
                'sector_industria_id' => $request->sector_industria_id,
                'fecha' => Carbon::parse($request->fecha),
                'remuneracion' => $request->remuneracion,
                'observaciones' => $request->observaciones,
                'representante' => $request->representante,
                'representado' => $request->representado,
                'pais_id' => $request->pais_id,
                'entidad_federativa_id' => $request->entidad_federativa_id,
                'especifique' => $request->especifique,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $response = Representacion_activa::findOrFail($id);
        $response->delete();
    }
}