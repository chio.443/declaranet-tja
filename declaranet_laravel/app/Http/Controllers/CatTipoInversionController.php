<?php

namespace App\Http\Controllers;

use App\CatTipoInversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoInversionController extends Controller
{
    public function index()
    {
        $tipos = CatTipoInversion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoInversion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoInversion::findOrFail($id);
        $tipo->delete();
    }
}