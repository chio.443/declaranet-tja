<?php

namespace App\Http\Controllers;

use App\BienIntangible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BienesIntangiblesController extends Controller
{
    public function index()
    {
        $bienes = BienIntangible::orderBy('id', 'DESC')->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function item($id)
    {
        $bienes = BienIntangible::where('ip_id', $id)->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function store(Request $request)
    {
        $bien = BienIntangible::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_operacion_id' => $request->tipo_operacion_id,
                'propietario_registrado' => $request->propietario_registrado,
                'descripcion' => $request->descripcion,
                'ente_publico_encargado_id' => $request->ente_publico_encargado_id,
                'numero_registro' => $request->numero_registro,
                'fecha_registro' => $request->fecha_registro,
                'sector_industria_id' => $request->sector_industria_id,
                'precio_adquisicion_valor' => $request->precio_adquisicion_valor,
                'precio_adquisicion_moneda_id' => $request->precio_adquisicion_moneda_id,
                'forma_adquisicion_id' => $request->forma_adquisicion_id,
                'forma_especificar' => $request->forma_especificar,
                'fecha_vencimiento' => $request->fecha_vencimiento,
                'porcentaje_copropiedad' => $request->porcentaje_copropiedad,
                'precio_total_copropiedad_valor' => $request->precio_total_copropiedad_valor,
                'precio_total_copropiedad_moneda_id' => $request->precio_total_copropiedad_moneda_id,
                'nombre_copropietario' => $request->nombre_copropietario,
                'porcentaje_propiedad_copropietario' => $request->porcentaje_propiedad_copropietario,
                'observaciones' => $request->observaciones,
                'tipo_operacion_especificar' => $request->tipo_operacion_especificar,
            ]
        );

        return response()->json(['bien' => $bien]);
    }

    public function delete($id)
    {
        $bien = BienIntangible::findOrFail($id);
        $bien->delete();
    }
}