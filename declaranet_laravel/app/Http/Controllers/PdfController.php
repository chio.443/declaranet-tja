<?php

namespace App\Http\Controllers;


use \Auth;
use \App\Token;


use App\Declaracion;
use  Barryvdh\DomPDF\PDF;

use App\InformacionPersonal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PdfController extends Controller
{
    public function index(Request $request, $id)
    {
        /*
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML('<h1>Test</h1>');
        return $pdf->stream();
        */
        if (Auth::user())
            $id = Auth::user()->id;


        if ($id) {
            $informacion =  InformacionPersonal::find($id);

            $data = $this->getData();
            $date = explode('-', date('Y-m-d'));
            $meses = array(
                "01" => "Enero",
                "02" => "Febrero",
                "03" => "Marzo",
                "04" => "Abril",
                "05" => "Mayo",
                "06" => "Junio",
                "07" => "Julio",
                "08" => "Agosto",
                "09" => "Septiembre",
                "10" => "Octubre",
                "11" => "Noviembre",
                "12" => "Diciembre"
            );
            $fechaActual = $date[2] . " de " . $meses[$date[1]] . " del " . $date[0];
            // Establecer la zona horaria predeterminada a usar. Disponible desde PHP 5.1
            //date_default_timezone_set('UTC');
            date_default_timezone_set('america/mexico_city');
            $hora = date('H:i');
            $declarante = $informacion->nombres . " " . $informacion->primer_apellido . " " . $informacion->segundo_apellido;

            $declaracion = Declaracion::where('id_ip', $id)
                ->select(DB::raw('declaraciones."Tipo_Dec"', 'substring(to_char("declaraciones"."created_at", ' . "'" . 'MM-DD-YYYY HH24:MI:SS' . "'" . ')  from 12 for 5) as hora'), DB::raw('DATE("declaraciones"."created_at") as fecha'))
                ->orderBy('id', 'desc')
                ->first();
            $date2 = explode('-', $declaracion['fecha']);
            $anio = $date2[0];

            $anio = (int)$anio;
            $anio--;

            $anio2 = $anio;

            $fechaDec = $date2[2] . " de " . $meses[$date2[1]] . " del " . $date2[0];
            $horaDec = $declaracion['hora'];

            $Tipo_DecText = '';
            $Tipo_Dec = $declaracion['Tipo_Dec'];
            $director = "Lic. Sergio Ojeda Cano";
            if ($Tipo_Dec == 'Final') {
                $anio++;
                $anio2 = $date2[0];
                $Tipo_DecText = 'final';
            } elseif ($Tipo_Dec == 'MODIFICACIÓN') {
                $Tipo_DecText = 'de modificación';
            } elseif ($Tipo_Dec == 'Anual') {
                $Tipo_DecText = 'anual';
            } elseif ($Tipo_Dec == 'Inicial') {
                $Tipo_DecText = 'inicial';
                $anio2 = $date2[0];
                $anio++;
            }

            if ($declaracion['Tipo_Dec'] == 'Final') {
                $view =  \View::make('pdf.invoicefinal', compact('data', 'fechaActual', 'director', 'declarante', 'hora', 'fechaDec', 'horaDec', 'anio', 'Tipo_DecText', 'anio2'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                return $pdf->download('Acuse_Final_' . $informacion->rfc . '.pdf');
            } else {
                $view =  \View::make('pdf.invoice', compact('data', 'fechaActual', 'director', 'declarante', 'hora', 'fechaDec', 'horaDec', 'anio', 'Tipo_DecText', 'anio2'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->loadHTML($view);
                return $pdf->download('Acuse' . $informacion->rfc . '.pdf');
            }
        }
    }

    public function getData()
    {
        $data =  [
            'quantity'      => '1',
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        return $data;
    }
}
