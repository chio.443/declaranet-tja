<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificacion;
use \App\Token;
use \App\Usuarios;

class NotificacionController extends Controller
{

    public function index(Request $request)
    {

        //para los usuafrios autentificados

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;
        $ente = $token->ente_publico_id;
        $permisos = $token->permisos;

        if (in_array(23, $permisos)) { //si tiene el permiso STRC
            $notificaciones = Notificacion::with('movimiento')
                ->where("id_tipo", 3)
                ->orWhere(function ($q) use ($uid) {
                    $q->where('id_tipo', 2)
                        ->where("id_usuario", $uid);
                })
                ->orderBy('id', 'DESC')->get();
        } else if (in_array(22, $permisos)) { //si tiene el permiso RH
            $notificaciones = Notificacion::with('movimiento')
                ->where("id_tipo", 2)
                ->where("id_usuario", $uid)
                ->orderBy('id', 'DESC')->get();
        } else if (in_array(24, $permisos)) { //si tiene el permiso oic
            $notificaciones = Notificacion::with('movimiento')
                ->whereIn("id_tipo", [2, 3])
                ->whereHas("movimiento", function ($q) use ($ente) {
                    $q->whereHas("servidor", function ($q2) use ($ente) {
                        $q2->whereHas("encargo", function ($q3) use ($ente) {
                            $q3->where('ente_publico_id', $ente);
                        });
                    });
                })
                ->where('visto2', 0)
                ->withTrashed()
                ->orderBy('id', 'DESC')->get();
        } else { //si no tiene ninguno, se busca como declarante
            $notificaciones = Notificacion::with('movimiento')
                ->where("id_tipo", 1)
                ->where("id_ip", $uid)
                ->orderBy('id', 'DESC')->get();
        }


        return response()->json(['notificaciones' => $notificaciones]);
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $notificacion = Notificacion::findOrFail($id);
        $notificacion->visto2 = 1;
        $notificacion->save();
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $notificacion = Notificacion::findOrFail($id);
        $notificacion->delete();
    }
}
