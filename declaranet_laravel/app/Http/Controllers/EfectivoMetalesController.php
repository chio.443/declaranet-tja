<?php

namespace App\Http\Controllers;

use App\EfectivoMetales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class EfectivoMetalesController extends Controller
{
    public function index()
    {
        $efectivos = EfectivoMetales::orderBy('id', 'DESC')->get();
        return response()->json(['efectivos' => $efectivos]);
    }

    public function item($id)
    {
        $efectivos = EfectivoMetales::where('ip_id', $id)->get();
        return response()->json(['efectivos' => $efectivos]);
    }

    public function store(Request $request)
    {
        $efectivo = EfectivoMetales::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_operacion_id' => $request->tipo_operacion_id,
                'moneda_id' => $request->moneda_id,
                'monto_moneda' => $request->monto_moneda,
                'tipo_metal_id' => $request->tipo_metal_id,
                'unidades' => $request->unidades,
                'monto_metal' => $request->monto_metal,
                'forma_adquisicion_id' => $request->forma_adquisicion_id,
                'observaciones' => $request->observaciones,
                'tipo_operacion_especificar' => $request->tipo_operacion_especificar,
                'forma_especificar'=> $request->forma_especificar,
                'tipo_metal_especificar'=> $request->tipo_metal_especificar,
            ]
        );

        return response()->json(['efectivo' => $efectivo]);
    }

    public function delete($id)
    {
        $efectivo = EfectivoMetales::findOrFail($id);
        $efectivo->delete();
    }
}