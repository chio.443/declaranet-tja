<?php

namespace App\Http\Controllers;

use App\CatFormaPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatFormaPagoController extends Controller
{
    public function index()
    {
        $pagos = CatFormaPago::orderBy('valor', 'ASC')->get();
        return response()->json(['pagos' => $pagos]);
    }

    public function store(Request $request)
    {
        $pago = CatFormaPago::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['pago' => $pago]);
    }

    public function delete($id)
    {
        $pago = CatFormaPago::findOrFail($id);
        $pago->delete();
    }
}