<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatTipoDeclaracion;

use App\Declaracion;
use App\InformacionPersonal;
use App\CatEntePublico;
use DB;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

use Carbon\Carbon;

class ReportesController extends Controller
{

    public function tipoDeclaracion(Request $req)
    {
        $tipoDeclaracion = CatTipoDeclaracion::orderBy('id','asc')->get();

        return response()->json(['tipoDeclaracion' =>$tipoDeclaracion]);
    }

    public function periodos()
    {
        $resultado = Declaracion::select(DB::raw("date_part('year',created_at) as year"))->distinct()->get();

		//$periodos = $resultado->pluck('year');

        return response()->json(['periodos' =>$resultado]);
    }

    public function intereses(Request $request)
    {
		$anioact    =  $request->periodo;

		$condicion4 = "date_part('year',den.fecha_posesion) < '".$anioact."'";

		//si hay intereses
		$int ="select count(id) as count
				from bitacora_declaraciones
				where intereses::text!='[]'";

		$int = DB::select($int);//si no hay nadie con conclicto de intereses, no realiza la consulta proque toma mucho tiempo

		if($int[0]->count>0){



		//con conflicto
		$declarantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
			from informacion_personal ip
			inner join declaraciones de
			on ip.id = de.id_ip
			inner join datos_encargo_actual den
			on ip.id = den.informacion_personal_id
			inner join bitacora_declaraciones bi on bi.declaracion_id = de.id
			where ip.deleted_at is null	
			and bi.intereses::text!='[]'				
			and de.deleted_at is null
			and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
			and den.deleted_at is null ";
             		
       		$declarantes .=" and den.ente_publico_id = ".$request->dependencia_id."
							GROUP BY (rfc)
							order by nombres asc, primer_apellido asc, segundo_apellido asc";

			$declarantes = DB::select($declarantes);

			//sin conflicto
			$faltantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
						from informacion_personal ip
						inner join declaraciones de
						on ip.id = de.id_ip
						inner join datos_encargo_actual den
						on ip.id = den.informacion_personal_id
						inner join bitacora_declaraciones bi on bi.declaracion_id = de.id
						where ip.deleted_at is null	
						and bi.intereses::text='[]'				
						and de.deleted_at is null
						and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
						and den.deleted_at is null ";
			       		   		

			       		$faltantes .=" and den.ente_publico_id = ".$request->dependencia_id."
										GROUP BY (rfc)
										order by nombres asc, primer_apellido asc, segundo_apellido asc";

						$faltantes = DB::select($faltantes);

		$total = count($declarantes);

		}
		else{
			//para que regrese vacios
		$declarantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
						from informacion_personal ip
						inner join declaraciones de
						on ip.id = de.id_ip
						inner join datos_encargo_actual den
						on ip.id = den.informacion_personal_id
						where de.id is null
						GROUP BY (rfc)
						order by nombres asc, primer_apellido asc, segundo_apellido asc";

			$declarantes = DB::select($declarantes);

			//sin conflicto
			$faltantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
						from informacion_personal ip
						inner join declaraciones de
						on ip.id = de.id_ip
						inner join datos_encargo_actual den
						on ip.id = den.informacion_personal_id
						where de.id is null
						GROUP BY (rfc)
						order by nombres asc, primer_apellido asc, segundo_apellido asc";

						$faltantes = DB::select($faltantes);

		$total = 0;

		}


		$entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->get();

		$porcentaje = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
			from informacion_personal ip
			inner join declaraciones de
			on ip.id = de.id_ip
			inner join datos_encargo_actual den
			on ip.id = den.informacion_personal_id
			where ip.deleted_at is null	
			and ip.created_at <= '".$anioact.'-12-31'."'
			and de.deleted_at is null
			and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
			and den.deleted_at is null ";       
       		

       		$porcentaje .=" and den.ente_publico_id = ".$request->dependencia_id."
							GROUP BY (rfc)
							order by nombres asc, primer_apellido asc, segundo_apellido asc";

			$porcentaje = DB::select($porcentaje);


			$porcentaje = count($porcentaje);





		if($porcentaje>0){
			$resp = ($total*100)/$porcentaje;
		}
		else{
			$resp = 0;
		}

		$respuesta  = ['declarantes' =>$declarantes, 'avance' =>$resp,'entidad'=>$entidad, 'porcentaje' =>$porcentaje, 'total'=>$total,'faltantes' => $faltantes];


	    return response()->json($respuesta);
    }
    public function fiscales(Request $request)
    {
		$anioact    =  $request->periodo;

		$condicion4 = "date_part('year',den.fecha_posesion) < '".$anioact."'";

		//con conflicto
		$declarantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
			from informacion_personal ip
			inner join declaraciones de
			on ip.id = de.id_ip
			inner join datos_encargo_actual den
			on ip.id = den.informacion_personal_id
			inner join declaracion_fiscal df on df.ip_id = ip.id
			where ip.deleted_at is null					
			and de.deleted_at is null
			and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
			and den.deleted_at is null ";
             		
       		$declarantes .=" and den.ente_publico_id = ".$request->dependencia_id."
							GROUP BY (rfc)
							order by nombres asc, primer_apellido asc, segundo_apellido asc";

			$declarantes = DB::select($declarantes);

			//sin conflicto
			$faltantes = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
						from informacion_personal ip
						inner join declaraciones de
						on ip.id = de.id_ip
						inner join datos_encargo_actual den
						on ip.id = den.informacion_personal_id
						inner join declaracion_fiscal df on df.ip_id = de.id
						where ip.deleted_at is null					
						and de.deleted_at is null
						and df.id is null
						and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
						and den.deleted_at is null ";
			       		   		

			       		$faltantes .=" and den.ente_publico_id = ".$request->dependencia_id."
										GROUP BY (rfc)
										order by nombres asc, primer_apellido asc, segundo_apellido asc";

						$faltantes = DB::select($faltantes);

		$total = count($declarantes);

	

		$entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->get();

		$porcentaje = "select max(segundo_apellido) as segundo_apellido, max(primer_apellido) as primer_apellido, max(nombres) as nombres, rfc, max(den.empleo_cargo_comision)as empleo_cargo_comision, max(de.created_at) as Fecha_Dec
			from informacion_personal ip
			inner join declaraciones de
			on ip.id = de.id_ip
			inner join datos_encargo_actual den
			on ip.id = den.informacion_personal_id
			where ip.deleted_at is null	
			and ip.created_at <= '".$anioact.'-12-31'."'
			and de.deleted_at is null
			and date_part('year',de.".'"'."Fecha_Dec".'"'.") = '".$anioact."'
			and den.deleted_at is null ";       
       		

       		$porcentaje .=" and den.ente_publico_id = ".$request->dependencia_id."
							GROUP BY (rfc)
							order by nombres asc, primer_apellido asc, segundo_apellido asc";

			$porcentaje = DB::select($porcentaje);


			$porcentaje = count($porcentaje);





		if($porcentaje>0){
			$resp = ($total*100)/$porcentaje;
		}
		else{
			$resp = 0;
		}

		$respuesta  = ['declarantes' =>$declarantes, 'avance' =>$resp,'entidad'=>$entidad, 'porcentaje' =>$porcentaje, 'total'=>$total,'faltantes' => $faltantes];


	    return response()->json($respuesta);
    }
    public function declarantes(Request $request)
    {
		$anioact    =  $request->periodo;


		$declarantes = DB::table('declaraciones')
			            ->select(DB::raw('informacion_personal.id, segundo_apellido, primer_apellido, nombres, rfc, empleo_cargo_comision, declaraciones.created_at as Fecha_Dec'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get()	;	            
						//->count()

			$declarantesIds = array();
			foreach($declarantes as $dec) {
				$declarantesIds[]=$dec->id;       	
    		}



		$total = count($declarantes);


		$entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->get();

		$porcentaje =  InformacionPersonal::select('id')
						->whereHas("encargo", function($q) use ($request){							 
							if($request->tipo_declaracion ==3){
								$q->where('tipo_dec',$request->tipo_declaracion)
							 	->where('ente_publico_id',$request->dependencia_id)		
							 	->whereYear('fecha_termino',$request->periodo)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$request->tipo_declaracion)
							 	->where('ente_publico_id',$request->dependencia_id)			
								->whereNull('deleted_at');
							}
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})					
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->orWhereIn('id',$declarantesIds)
						->whereNull('deleted_at')					
						->get();


			$obligadosIds = array();

			foreach($porcentaje as $obligado) {
				$obligadosIds[]=$obligado->id;       	
    		}

			$porcentaje=$porcentaje->count();


			$faltantes = InformacionPersonal::with('encargo')  
					->whereIn('id',$obligadosIds)               
					->whereNotIn('id',$declarantesIds)
					->orderBy('nombres','asc')
                    ->orderBy('primer_apellido','asc')
                    ->orderBy('segundo_apellido','asc')
                    ->get();


		if($porcentaje>0){
			$resp = ($total*100)/$porcentaje;
		}
		else{
			$resp = 0;
		}

		$respuesta  = ['declarantes' =>$declarantes, 'avance' =>$resp,'entidad'=>$entidad, 'porcentaje' =>$porcentaje, 'total'=>$total,'faltantes' => $faltantes, 'obligadosid'=>$obligadosIds,'declarantesIds' => $declarantesIds];


	    return response()->json($respuesta);
    }


    ////////////////////Reporte de cumplimiento/////////////////////
    public function cumplimiento(Request $request)
    {
    	//si se seleccionan todas las dependencias

    	$total=0;
    	$anioact    =  $request->periodo;


    	$condicion3=$request->tipo_declaracion;

    	$condicion2="";

		if($request->tipo_declaracion == 1){//inicial
			$condicion = " and (date_part('year',datos_encargo_actual.fecha_posesion) = '".$request->periodo."' and den.tipo_dec =".$condicion3.") ";
			$condicion2 = "date_part('year',datos_encargo_actual.fecha_posesion) < '".$anioact."'";
		}
		if($request->tipo_declaracion == 2){
			$condicion = " and (date_part('year',datos_encargo_actual.fecha_posesion) < '".$request->periodo."' or den.tipo_dec =".$condicion3.") ";
			$condicion2 = "date_part('year',datos_encargo_actual.fecha_posesion) < '".$anioact."'";
		}
		if($request->tipo_declaracion == 3){
       		$condicion = "and  den.fecha_termino is not null";
       	}

      $total = InformacionPersonal::select('rfc')
				->whereHas("encargo", function($q) use ($anioact, $condicion3){
							if($condicion3 ==3){
								$q->where('tipo_dec',$condicion3)
							 	->whereYear('fecha_termino',$anioact)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$condicion3)	
								->whereNull('deleted_at');
							}

                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->whereNull('deleted_at')						
						->where('informacion_personal.created_at','<=',$anioact.'-12-31')
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();


    	if($request->dependencia_id == 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;

    		foreach ($entidades as $indice => $dato)
		   {
		   	$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;


			$cumplidos = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get();

			$declarantesIds = array();
			foreach($cumplidos as $dec) {
				$declarantesIds[]=$dec->id_ip;       	
    		}

    		$cumplidos=$cumplidos->count();

			$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


			$obligados = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
							if($condicion3 ==3){
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$dato->id)		
							 	->whereYear('fecha_termino',$anioact)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$dato->id)			
								->whereNull('deleted_at'); 
							}                    

                         })
                        ->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})						
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->orWhereIn('id',$declarantesIds)
						->whereNull('deleted_at')						
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();




			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			if($total>0)
				$total2 = $cumplidos*100/$total;
			else
				$total2 = 0;

			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;
		   }
				array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

    	}else{



			$cumplidos = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
					//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get();

			 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

			$declarantesIds = array();
			foreach($cumplidos as $dec) {
				$declarantesIds[]=$dec->id_ip;       	
    		}

    	$cumplidos=$cumplidos->count();


		$obligados = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
							         
							if($request->tipo_declaracion ==3){
								$q->where('tipo_dec',$request->tipo_declaracion)
							 	->where('ente_publico_id',$request->dependencia_id)		
							 	->whereYear('fecha_termino',$request->periodo)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$request->tipo_declaracion)
							 	->where('ente_publico_id',$request->dependencia_id)			
								->whereNull('deleted_at');  
							}                    
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->whereNull('deleted_at')		
						->orWhereIn('id',$declarantesIds)			
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total = $cumplidos*100/$total;

			$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

    	}

	    return response()->json($respuesta);
    }
 



}