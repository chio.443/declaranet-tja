<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatTipoDeclaracion;

use App\Declaracion;
use App\InformacionPersonal;
use App\CatEntePublico;
use DB;

use Carbon\Carbon;
class CumplimientoController2 extends Controller
{
    
    ////////////////////Reporte de cumplimiento/////////////////////
    //todos
    public function cumplimiento(Request $request)
    {
    //si se seleccionan todas las dependencias

      $total=0;
      $anioact    =  $request->periodo;

      if($request->tipo_declaracion == 4){

      	$total=0;

         $acumInicial=0;
		 $acumAnual=0;
		 $acumFinal=0;

    	if($request->dependencia_id == 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

		    foreach ($entidades as $indice => $dato)
			 {
   	     		
	   	     	$declarantes =  DB::table('declaraciones')
		            ->select(DB::raw('id_ip'))
		            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
		            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
		            ->whereNull('informacion_personal.deleted_at')
		            ->whereNull('declaraciones.deleted_at')
		            ->where('declaraciones.ente_publico_id',$dato->id)
		            ->whereYear('declaraciones.Fecha_Dec', $anioact)
		            ->where('declaraciones.tipo_declaracion_id',1)
		            ->get();

				$declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			  	$declarantesInicial = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $dato){
	                                   $q->where("tipo_dec",1)
								 		->where('ente_publico_id',$dato->id);
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->get()->count();


					$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',2)
			            ->get(); 

						$declarantesIds = array();
						foreach($declarantes as $dec) {
							$declarantesIds[]=$dec->id_ip;       	
			    		}
		  

				 $declarantesAnual = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact,$dato){
	                                   $q->where("tipo_dec",2)
								 		->where('ente_publico_id',$dato->id);
	                        
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})	
							->orWhereIn('id',$declarantesIds)
							->get()->count();

				$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',3)
			            ->get();

			    $declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			$declarantesFinal = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact,$dato){
                                   $q->where("tipo_dec",3)
							 		->where('ente_publico_id',$dato->id)
							 		->whereYear('fecha_termino',$anioact);
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->orWhereIn('id',$declarantesIds)
						->get()->count();

			$acumInicial+=$declarantesInicial;
			$acumAnual+=$declarantesAnual;
			$acumFinal+=$declarantesFinal;

		   }

		   $total = $acumAnual+$acumInicial+$acumFinal;	




			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;

    		foreach ($entidades as $indice => $dato)
		   {
		   	$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

		   	for($x = 0; $x < 3; $x++) {

      			$condicion3 = $x+1;


		   		$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
			            ->get();
			    

				$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    			$cumplidos+=$cumplidosarr->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligadosObj= InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();

				$obligados+=$obligadosObj;

		   	}	


			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			if($total>0)
				$total2 = $cumplidos*100/$total;
			else
				$total2 = 0;


			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;
		   }
				array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

    	}else{

    		$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

   		for($x = 0; $x < 3; $x++) {

      		$condicion3 = $x+1;

			$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
			            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
					//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get();





			 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

		

    		$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    		$cumplidos+=$cumplidosarr->count();


			$obligadosObj = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
							         
							if($condicion3 ==3){
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)		
							 	->whereYear('fecha_termino',$request->periodo)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)			
								->whereNull('deleted_at');  
							}                    
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->whereNull('deleted_at')		
						->orWhereIn('id',$declarantesIds)			
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();

				$obligados+=$obligadosObj;
				$total+=$obligados;


			}


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total = $cumplidos*100/$total;

			$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

    	}

    	

      }else{
      	  
      	  $condicion3=$request->tipo_declaracion;

	      $total = InformacionPersonal::select('rfc')
					->whereHas("encargo", function($q) use ($anioact, $condicion3){
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)	
									->whereNull('deleted_at');
								}

	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')						
							->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


	    	if($request->dependencia_id == 0){//

	    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

				$totalObligados=0;
				$totalCumplidos=0;
				$totalFaltantes=0;
				$totalInd =0;
				$totalTotal=0;

	    		foreach ($entidades as $indice => $dato)
			   {
			   	$cumplidos = 0;
			   	$faltantes = 0;
			   	$obligados = 0;
			   	$total2 = 0;
			   	$individual = 0;


				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
				            ->where('declaraciones.ente_publico_id',$dato->id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
							//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
				            ->get();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    		$cumplidos=$cumplidos->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();




				$faltantes = $obligados - $cumplidos;

				if($obligados>0)
					$individual = $cumplidos*100/$obligados;
				else
					$individual=0;

				if($total>0)
					$total2 = $cumplidos*100/$total;
				else
					$total2 = 0;

				$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
				////////////////////////////////
				$totalObligados+=$obligados;
				$totalCumplidos+=$cumplidos;
				$totalFaltantes+=$faltantes;
				$totalInd+=$individual;
				$totalTotal+=$total2;
			   }
					array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

	    	}else{



				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
				            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
						//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
				            ->get();

				 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    	$cumplidos=$cumplidos->count();


			$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
								         
								if($request->tipo_declaracion ==3){
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)		
								 	->whereYear('fecha_termino',$request->periodo)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)			
									->whereNull('deleted_at');  
								}                    
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')		
							->orWhereIn('id',$declarantesIds)			
							//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


				$faltantes = $obligados - $cumplidos;

				$individual = $cumplidos*100/$obligados;

				$total = $cumplidos*100/$total;

				$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

	    	}
      }

   

	    return response()->json($respuesta);
    }
    //activos
    public function cumplimientoAct(Request $request)
    {
    //si se seleccionan todas las dependencias

      $total=0;
      $anioact    =  $request->periodo;

      if($request->tipo_declaracion == 4){

      	$total=0;

         $acumInicial=0;
		 $acumAnual=0;
		 $acumFinal=0;

    	if($request->dependencia_id == 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

		    foreach ($entidades as $indice => $dato)
			 {
   	     		
	   	     	$declarantes =  DB::table('declaraciones')
		            ->select(DB::raw('id_ip'))
		            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
		            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
		            ->whereNull('informacion_personal.deleted_at')
		            ->whereNull('declaraciones.deleted_at')
					->where('datos_encargo_actual.tipo_dec','!=',3)
		            ->where('declaraciones.ente_publico_id',$dato->id)
		            ->whereYear('declaraciones.Fecha_Dec', $anioact)
		            ->where('declaraciones.tipo_declaracion_id',1)
		            ->get();

				$declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			  	$declarantesInicial = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $dato){
	                                   $q->where("tipo_dec",1)
								 		->where('ente_publico_id',$dato->id);
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->get()->count();


					$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')				            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec','!=',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',2)
			            ->get(); 

						$declarantesIds = array();
						foreach($declarantes as $dec) {
							$declarantesIds[]=$dec->id_ip;       	
			    		}
		  

				 $declarantesAnual = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact,$dato){
	                                   $q->where("tipo_dec",2)
								 		->where('ente_publico_id',$dato->id);
	                        
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})	
							->orWhereIn('id',$declarantesIds)
							->get()->count();

				$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec','!=',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',3)
			            ->get();

			    $declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			$declarantesFinal = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact,$dato){
                                   $q->where("tipo_dec",3)
							 		->where('ente_publico_id',$dato->id)
							 		->whereYear('fecha_termino',$anioact);
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->orWhereIn('id',$declarantesIds)
						->get()->count();

			$acumInicial+=$declarantesInicial;
			$acumAnual+=$declarantesAnual;
			$acumFinal+=$declarantesFinal;

		   }

		   $total = $acumAnual+$acumInicial+$acumFinal;	




			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;

    		foreach ($entidades as $indice => $dato)
		   {
		   	$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

		   	for($x = 0; $x < 3; $x++) {

      			$condicion3 = $x+1;


		   		$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')			            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec','!=',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
			            ->get();
			    

				$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    			$cumplidos+=$cumplidosarr->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligadosObj= InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();

				$obligados+=$obligadosObj;

		   	}	


			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			if($total>0)
				$total2 = $cumplidos*100/$total;
			else
				$total2 = 0;


			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;
		   }
				array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

    	}else{

    		$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

   		for($x = 0; $x < 3; $x++) {

      		$condicion3 = $x+1;

			$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')			            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec','!=',3)
			            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
					//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get();





			 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

		

    		$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    		$cumplidos+=$cumplidosarr->count();


			$obligadosObj = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
							         
							if($condicion3 ==3){
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)		
							 	->whereYear('fecha_termino',$request->periodo)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)			
								->whereNull('deleted_at');  
							}                    
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','!=','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->whereNull('deleted_at')		
						->orWhereIn('id',$declarantesIds)			
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();

				$obligados+=$obligadosObj;
				$total+=$obligados;


			}


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total = $cumplidos*100/$total;

			$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

    	}

    	

      }else{
      	  
      	  $condicion3=$request->tipo_declaracion;

	      $total = InformacionPersonal::select('rfc')
					->whereHas("encargo", function($q) use ($anioact, $condicion3){
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)	
									->whereNull('deleted_at');
								}

	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')						
							->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


	    	if($request->dependencia_id == 0){//

	    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

				$totalObligados=0;
				$totalCumplidos=0;
				$totalFaltantes=0;
				$totalInd =0;
				$totalTotal=0;

	    		foreach ($entidades as $indice => $dato)
			   {
			   	$cumplidos = 0;
			   	$faltantes = 0;
			   	$obligados = 0;
			   	$total2 = 0;
			   	$individual = 0;


				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
							->where('datos_encargo_actual.tipo_dec','!=',3)
				            ->where('declaraciones.ente_publico_id',$dato->id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
							//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
				            ->get();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    		$cumplidos=$cumplidos->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();




				$faltantes = $obligados - $cumplidos;

				if($obligados>0)
					$individual = $cumplidos*100/$obligados;
				else
					$individual=0;

				if($total>0)
					$total2 = $cumplidos*100/$total;
				else
					$total2 = 0;

				$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
				////////////////////////////////
				$totalObligados+=$obligados;
				$totalCumplidos+=$cumplidos;
				$totalFaltantes+=$faltantes;
				$totalInd+=$individual;
				$totalTotal+=$total2;
			   }
					array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

	    	}else{



				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')				            
					        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
							->where('datos_encargo_actual.tipo_dec','!=',3)
				            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
						//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
				            ->get();

				 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    	$cumplidos=$cumplidos->count();


			$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
								         
								if($request->tipo_declaracion ==3){
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)		
								 	->whereYear('fecha_termino',$request->periodo)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)			
									->whereNull('deleted_at');  
								}                    
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','!=','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')		
							->orWhereIn('id',$declarantesIds)			
							//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


				$faltantes = $obligados - $cumplidos;

				$individual = $cumplidos*100/$obligados;

				$total = $cumplidos*100/$total;

				$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

	    	}
      }

   

	    return response()->json($respuesta);
    }

    //bajas
     //activos
    public function cumplimientoBj(Request $request)
    {
    //si se seleccionan todas las dependencias

      $total=0;
      $anioact    =  $request->periodo;

      if($request->tipo_declaracion == 4){

      	$total=0;

         $acumInicial=0;
		 $acumAnual=0;
		 $acumFinal=0;

    	if($request->dependencia_id == 0){//

    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

		    foreach ($entidades as $indice => $dato)
			 {
   	     		
	   	     	$declarantes =  DB::table('declaraciones')
		            ->select(DB::raw('id_ip'))
		            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
		            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
		            ->whereNull('informacion_personal.deleted_at')
		            ->whereNull('declaraciones.deleted_at')
					->where('datos_encargo_actual.tipo_dec',3)
		            ->where('declaraciones.ente_publico_id',$dato->id)
		            ->whereYear('declaraciones.Fecha_Dec', $anioact)
		            ->where('declaraciones.tipo_declaracion_id',1)
		            ->get();

				$declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			  	$declarantesInicial = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $dato){
	                               $q->where("tipo_dec",1)
							 		->where('ente_publico_id',$dato->id)
							 		->whereYear('fecha_termino',$anioact);
	                        
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->get()->count();


					$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')				            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',2)
			            ->get(); 

						$declarantesIds = array();
						foreach($declarantes as $dec) {
							$declarantesIds[]=$dec->id_ip;       	
			    		}
		  

				 $declarantesAnual = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact,$dato){
	                                $q->where("tipo_dec",2)
							 		->where('ente_publico_id',$dato->id)
							 		->whereYear('fecha_termino',$anioact);
	                        
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})	
							->whereIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->get()->count();

				$declarantes =  DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
			            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $anioact)
			            ->where('declaraciones.tipo_declaracion_id',3)
			            ->get();

			    $declarantesIds = array();
				foreach($declarantes as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

			$declarantesFinal = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact,$dato){
                                   $q->where("tipo_dec",3)
							 		->where('ente_publico_id',$dato->id)
							 		->whereYear('fecha_termino',$anioact);
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->orWhereIn('id',$declarantesIds)
						->get()->count();

			$acumInicial+=$declarantesInicial;
			$acumAnual+=$declarantesAnual;
			$acumFinal+=$declarantesFinal;

		   }

		   $total = $acumAnual+$acumInicial+$acumFinal;	




			$totalObligados=0;
			$totalCumplidos=0;
			$totalFaltantes=0;
			$totalInd =0;
			$totalTotal=0;

    		foreach ($entidades as $indice => $dato)
		   {
		   	$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

		   	for($x = 0; $x < 3; $x++) {

      			$condicion3 = $x+1;


		   		$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')			            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec',3)
			            ->where('declaraciones.ente_publico_id',$dato->id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
			            ->get();
			    

				$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    			$cumplidos+=$cumplidosarr->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligadosObj= InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();

				$obligados+=$obligadosObj;

		   	}	


			$faltantes = $obligados - $cumplidos;

			if($obligados>0)
				$individual = $cumplidos*100/$obligados;
			else
				$individual=0;

			if($total>0)
				$total2 = $cumplidos*100/$total;
			else
				$total2 = 0;


			$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
			////////////////////////////////
			$totalObligados+=$obligados;
			$totalCumplidos+=$cumplidos;
			$totalFaltantes+=$faltantes;
			$totalInd+=$individual;
			$totalTotal+=$total2;
		   }
				array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

    	}else{

    		$cumplidos = 0;
		   	$faltantes = 0;
		   	$obligados = 0;
		   	$total2 = 0;
		   	$individual = 0;

   		for($x = 0; $x < 3; $x++) {

      		$condicion3 = $x+1;

			$cumplidosarr = DB::table('declaraciones')
			            ->select(DB::raw('id_ip'))
			            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
			            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')			            
				        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
			            ->whereNull('informacion_personal.deleted_at')
			            ->whereNull('declaraciones.deleted_at')
						->where('datos_encargo_actual.tipo_dec','!=',3)
			            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
			            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
			            ->where('declaraciones.tipo_declaracion_id',$condicion3)
					//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
			            ->get();





			 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

		

    		$declarantesIds = array();
				foreach($cumplidosarr as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

    		$cumplidos+=$cumplidosarr->count();


			$obligadosObj = InformacionPersonal::select('rfc')
						->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
							         
							if($condicion3 ==3){
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)		
							 	->whereYear('fecha_termino',$request->periodo)			
								->whereNull('deleted_at');
							}else{
								$q->where('tipo_dec',$condicion3)
							 	->where('ente_publico_id',$request->dependencia_id)			
								->whereNull('deleted_at');  
							}                    
                         })
						->whereNotIn('id',function($query){
									   $query->select('ip_id')
									   ->from('movimientos')
									   ->where('tipo_mov','=',4)//4 es el id de licencia
									   ->where('termina','>',date('Y-m-d'))
									   ->whereNull('deleted_at');
						})
						->whereNotIn('id',function($query){
							$query->select('id')
							->from('informacion_personal')
							->where('tipo_mov','=',9)//9 es el id de justificado
							->whereNull('deleted_at');
						})
						->whereIn('id',function($query){
							$query->select('ip_id')
							->from('movimientos')
							->where('tipo_mov',2)//2 es el id de baja
							->where('tipo_baja','Normal')//por defuncion
							->whereNull('deleted_at');
						})
						->whereNull('deleted_at')		
						->orWhereIn('id',$declarantesIds)			
						//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
						->groupBy('rfc')
						->havingRaw('count(rfc) = 1')
						->get()->count();

				$obligados+=$obligadosObj;
				$total+=$obligados;


			}


			$faltantes = $obligados - $cumplidos;

			$individual = $cumplidos*100/$obligados;

			$total = $cumplidos*100/$total;

			$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

    	}

    	

      }else{
      	  
      	  $condicion3=$request->tipo_declaracion;

	      $total = InformacionPersonal::select('rfc')
					->whereHas("encargo", function($q) use ($anioact, $condicion3){
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)	
									->whereNull('deleted_at');
								}

	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')						
							->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


	    	if($request->dependencia_id == 0){//

	    		$entidades = CatEntePublico::select('id')->orderBy('valor')->get();

				$totalObligados=0;
				$totalCumplidos=0;
				$totalFaltantes=0;
				$totalInd =0;
				$totalTotal=0;

	    		foreach ($entidades as $indice => $dato)
			   {
			   	$cumplidos = 0;
			   	$faltantes = 0;
			   	$obligados = 0;
			   	$total2 = 0;
			   	$individual = 0;


				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
				            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
							->where('datos_encargo_actual.tipo_dec',3)
				            ->where('declaraciones.ente_publico_id',$dato->id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
				            ->get();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    		$cumplidos=$cumplidos->count();

				$entidad = CatEntePublico::select('valor')->where('id',$dato->id)->first();


				$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($dato,$anioact, $condicion3){ 
								if($condicion3 ==3){
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)		
								 	->whereYear('fecha_termino',$anioact)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$condicion3)
								 	->where('ente_publico_id',$dato->id)			
									->whereNull('deleted_at'); 
								}                    

	                         })
	                        ->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})						
							->whereIn('id',function($query){
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->orWhereIn('id',$declarantesIds)
							->whereNull('deleted_at')						
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();




				$faltantes = $obligados - $cumplidos;

				if($obligados>0)
					$individual = $cumplidos*100/$obligados;
				else
					$individual=0;

				if($total>0)
					$total2 = $cumplidos*100/$total;
				else
					$total2 = 0;

				$respuesta[$indice] = ['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total2];
				////////////////////////////////
				$totalObligados+=$obligados;
				$totalCumplidos+=$cumplidos;
				$totalFaltantes+=$faltantes;
				$totalInd+=$individual;
				$totalTotal+=$total2;
			   }
					array_push($respuesta, ['obligados' =>$totalObligados, 'entidad' =>array('valor' => 'TOTAL'),'cumplidos'=>$totalCumplidos, 'faltantes' =>$totalFaltantes, 'individual'=>$totalTotal, 'total'=>$totalTotal]);

	    	}else{



				$cumplidos = DB::table('declaraciones')
				            ->select(DB::raw('id_ip'))
				            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
				            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')				            
					        ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
				            ->whereNull('informacion_personal.deleted_at')
				            ->whereNull('declaraciones.deleted_at')
							->where('datos_encargo_actual.tipo_dec',3)
				            ->where('declaraciones.ente_publico_id',$request->dependencia_id)
				            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
				            ->where('declaraciones.tipo_declaracion_id',$request->tipo_declaracion)
						//	->where('informacion_personal.created_at','<=',$anioact.'-12-31')
				            ->get();

				 $entidad = CatEntePublico::select('valor')->where('id',$request->dependencia_id)->first();

				$declarantesIds = array();
				foreach($cumplidos as $dec) {
					$declarantesIds[]=$dec->id_ip;       	
	    		}

	    	$cumplidos=$cumplidos->count();


			$obligados = InformacionPersonal::select('rfc')
							->whereHas("encargo", function($q) use ($anioact, $condicion3,$request){
								         
								if($request->tipo_declaracion ==3){
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)		
								 	->whereYear('fecha_termino',$request->periodo)			
									->whereNull('deleted_at');
								}else{
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)			
									->whereNull('deleted_at');  
								}                    
	                         })
							->whereNotIn('id',function($query){
										   $query->select('ip_id')
										   ->from('movimientos')
										   ->where('tipo_mov','=',4)//4 es el id de licencia
										   ->where('termina','>',date('Y-m-d'))
										   ->whereNull('deleted_at');
							})
							->whereNotIn('id',function($query){
								$query->select('id')
								->from('informacion_personal')
								->where('tipo_mov','=',9)//9 es el id de justificado
								->whereNull('deleted_at');
							})
							->whereIn('id',function($query){//que haya sido baja
								$query->select('ip_id')
								->from('movimientos')
								->where('tipo_mov',2)//2 es el id de baja
								->where('tipo_baja','Normal')//por defuncion
								->whereNull('deleted_at');
							})
							->whereNull('deleted_at')		
							->orWhereIn('id',$declarantesIds)			
							//->where('informacion_personal.created_at','<=',$anioact.'-12-31')
							->groupBy('rfc')
							->havingRaw('count(rfc) = 1')
							->get()->count();


				$faltantes = $obligados - $cumplidos;

				$individual = $cumplidos*100/$obligados;

				$total = $cumplidos*100/$total;

				$respuesta  = [['obligados' =>$obligados, 'entidad' =>$entidad,'cumplidos'=>$cumplidos, 'faltantes' =>$faltantes, 'individual'=>$individual, 'total'=>$total]];

	    	}
      }

   

	    return response()->json($respuesta);
    }

 

}
