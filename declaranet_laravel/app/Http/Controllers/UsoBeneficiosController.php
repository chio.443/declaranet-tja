<?php

namespace App\Http\Controllers;

use App\UsoBeneficios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsoBeneficiosController extends Controller
{
    public function index()
    {
        $bienes = UsoBeneficios::orderBy('id', 'DESC')->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function item($id)
    {
        $bienes = UsoBeneficios::where('ip_id', $id)->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function store(Request $request)
    {
        $bien = UsoBeneficios::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_bien_id' => $request->tipo_bien_id,
                'valor_mercado_valor' => $request->valor_mercado_valor,
                'valor_mercado_cat_moneda_id' => $request->valor_mercado_cat_moneda_id,
                'nombre_tercero_propietario' => $request->nombre_tercero_propietario,
                'rfc_tercero_propietario' => $request->rfc_tercero_propietario,
                'curp_tercero_propietario' => $request->curp_tercero_propietario,
                'relacion_persona_id' => $request->relacion_persona_id,
                'sector_industria_id' => $request->sector_industria_id,
                'fecha_inicio' => $request->fecha_inicio,
                'observaciones' => $request->observaciones,
                'tipo_bien_especificar' => $request->tipo_bien_especificar,
                'relacion_especificar' => $request->relacion_especificar,
                'domicilio_pais_id' => $request->domicilio_pais_id,
                'domicilio_entidad_federativa_id' => $request->domicilio_entidad_federativa_id,
                'domicilio_municipio_id' => $request->domicilio_municipio_id,
                'domicilio_cp' => $request->domicilio_cp,
                'domicilio_localidad_id' => $request->domicilio_localidad_id,
                'domicilio_colonia' => $request->domicilio_colonia,
                'domicilio_vialidad_tipo' => $request->domicilio_vialidad_tipo,
                'domicilio_vialidad_nombre' => $request->domicilio_vialidad_nombre,
                'domicilio_numExt' => $request->domicilio_numExt,
                'domicilio_numInt' => $request->domicilio_numInt,
            ]
        );

        return response()->json(['bien' => $bien]);
    }

    public function delete($id)
    {
        $bien = UsoBeneficios::findOrFail($id);
        $bien->delete();
    }
}