<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Token;
use App\Deudas;

use App\Fiscal;
use Carbon\Carbon;
use App\Declaracion;
use App\BitacoraConsulta;
use App\DatosCurriculares;
use App\ExperenciaLaboral;
use App\InformacionPersonal;
use Illuminate\Http\Request;
use App\DependienteEconomico;
use App\InversionesCuentasValores;

class DeclaracionesReporteBckp extends Controller
{

    public function dec($tipoDecl, $ent, $periodo)
    {

        $declarantes = DB::table('declaraciones')
            ->select('informacion_personal.primer_apellido', 'informacion_personal.segundo_apellido', 'informacion_personal.nombres', 'informacion_personal.id as ipid', 'declaraciones.id as decid', 'informacion_personal.rfc', 'cat_ente_publico.valor as ente', 'informacion_personal.correo_electronico_laboral as correo')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
            ->where('datos_encargo_actual.ente_publico_id', $ent)
            ->where('declaraciones.tipo_declaracion_id', $tipoDecl)
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('informacion_personal.deleted_at')
            ->orderBy('informacion_personal.nombres', 'asc')
            ->orderBy('informacion_personal.primer_apellido', 'asc')
            ->orderBy('informacion_personal.segundo_apellido', 'asc')
            ->get();

        return response()->json(['declarantes' => $declarantes]);
    }

    public function descargaDec($id, $idus)
    {
        $format = 'd/m/Y';



        $dec = DB::table('declaraciones')
            ->select('id_ip')
            ->where('declaraciones.id', $id)->first();

        $id = $dec->id_ip;

        $func = InformacionPersonal::where('id', $id)->first();

        $dpersonales = array();

        //información personal

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = $func->paisNac->valor;
        $dpersonales['entnac'] = $func->entNac->nom_ent;
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = $func->municipioD->nom_mun;
        $dpersonales['entidad'] = $func->entidadFedD->nom_ent;
        $dpersonales['localidad'] = $func->localidadD->nom_loc;
        $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;
        $dpersonales['edoCivil'] = $func->edoCivil->valor;
        $dpersonales['regimen'] = $func->regimenMat->valor;



        //datos curriculares

        $func = DatosCurriculares::where('informacion_personal_id', $id)->get();

        $dcurrc = array();

        foreach ($func as $escuela) {
            $dcurrc[] = array(
                'grado' => $escuela->gradoEsc->grado_escolaridad,
                'institucion' => $escuela->institucion_educativa,
                'entidad' => $escuela->ent->nom_ent,
                'pais' => $escuela->pais->valor,
                'carrera' => $escuela->carrera,
                'periodos' => $escuela->periodos->valor,
                'periodosc' => $escuela->periodos_cursados,
                'estatus' => $escuela->estatus->estatus,
                'doctoOb' => $escuela->docto->docto,
                'cedula' => $escuela->cedula,
            );
        }
        //experiencia laboral

        $func = InformacionPersonal::where('id', $id)->first();

        $experiencia = array();

        foreach ($func->experiencia as $trabajo) {
            $experiencia[] = array(
                'ambito' => $trabajo->ambito->valor,
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'municipio' => $trabajo->municipioD->nom_mun,
                'entidad' => $trabajo->entidadFedD->nom_ent,
                'localidad' => $trabajo->localidadD->nom_loc,
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => $trabajo->sector->valor,
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_baja)->format($format)
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = DependienteEconomico::where('informacion_personal_id', $id)->get();

        $dependientes = array();

        foreach ($func as $dependiente) {
            $dependientes[] = array(
                'tipo_r' => $dependiente->tipoRel->valor,
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = InformacionPersonal::where('id', $id)->first();


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);


        $bitacora = new BitacoraConsulta([
            'ip_id' => $func->id,
            'created_by' => $idus
        ]);

        $bitacora->save();

        ///////////////////////////////////////////intereses
        //empresas
        $empresas = array();
        foreach ($func->empresas as $empresa) {
            $empresas[] = array(
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'naturaleza' => $empresa->naturaleza->valor
            );
        }




        //////////////////////////////////////////ingresos
        //////////sueldos publicos
        $spublicos = array();

        $sueldo = $func->ingresos;

        $spublicos[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público ',
            'ingreso' => $sueldo['sub_1']
        );


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' => $sueldo['razon_social_cantidad']
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' => $sueldo['actividad_financiera_cantidad']
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' => $sueldo['servicios_profecionales_cantidad']
        );
        $otross[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' => $sueldo['arrendamiento_cantidad']
        );
        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format($sueldo['ant_sub_12'], 2)
        );


        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_razon_social'],
            'ingreso' => '$' . number_format($sueldo['ant_razon_social_cantidad'], 2)
        );

        $otrossant[] = array(
            'cat' => '',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format($sueldo['ant_actividad_comercial_cantidad'], 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format($sueldo['ant_actividad_financiera_cantidad'], 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format($sueldo['ant_servicios_profecionales_cantidad'], 2)
        );
        $otrossant[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['ant_arrendamiento'],
            'ingreso' => '$' . number_format($sueldo['ant_arrendamiento_cantidad'], 2)
        );

        ///////////////////////////////////////activos
        //bienes inmuebles
        $binmuebles = array();
        foreach ($func->binmuebles as $inmueble) {
            $binmuebles[] = array(
                'tipoOperacion' => $inmueble->tipoOperacion->valor,
                'naturaleza' => $inmueble->tipoBien->valor,
                'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                'padquisicion' =>  '$' . number_format($inmueble->precio_adquisicion_valor, 2)
            );
        }
        // //bienes inmuebles reg
        $bmuebles = array();
        foreach ($func->bmuebles as $mueble) {
            $bmuebles[] = array(
                'tipoOperacion' => $mueble->tipoOperacion->valor,
                'tipoBien' => $mueble->tipoBien->valor,
                'padquisicion' =>  '$' . number_format($mueble->precio_adquisicion_valor, 2)
            );
        }
        //bienes inmuebles no reg
        $bmueblesnr = array();
        foreach ($func->bmueblesnr as $mueble) {
            $bmueblesnr[] = array(
                'tipoOperacion' => $mueble->tipoOperacion->valor,
                'tipoBien' => $mueble->tipoBien->valor,
                'padquisicion' =>  '$' . number_format($mueble->precio_adquisicion_valor, 2)
            );
        }

        ///////////////////////////////////////	pasivos


        $func = Deudas::where('ip_id', $id)->get();


        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'tipoOperacion' => $deuda->tipoOperacion->valor,
                'tipoAdeudo' => $deuda->tipoAdeudo->valor,
                'acreedor' => $deuda->nombre_acreedor,
                'identificador' => $deuda->identificador_deuda,
                'original' => '$' . number_format($deuda->saldo_original, 2),
                'pendiente' => '$' . number_format($deuda->saldo_pendiente, 2),
                'fadeudo' => Carbon::parse($deuda->fecha_adeudo)->format($format)

            );
        }

        $func = InversionesCuentasValores::where('ip_id', $id)->get();


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format($dato->monto_original, 2),
                'pais' => $dato->pais->valor,
                'entidad' => $dato->entidad->nom_ent,
                'tipoOp' => $dato->tipoOperacion->valor,
                'tipoInv' => $dato->tipoInv->valor,
                'titular' => $dato->titular->valor

            );
        }

        $fiscal = Fiscal::where('ip_id', $id)->first();
        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';



        $view =  \View::make('declaracion', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }

    //reactivar la declaración
    public function reactivar($idDecl, Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $declaracion = Declaracion::where('id', $idDecl);

        $declaracion->update(array('deleted_by' => $id, 'deleted_at' => date('Ymd')));

        $declaracion->delete();

        return response()->json(['declarante' => $declaracion]);
    }
}
