<?php

namespace App\Http\Controllers;

use App\CatTipoBienMueble;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoBienMuebleController extends Controller
{
    public function index()
    {
        $tipos = CatTipoBienMueble::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoBienMueble::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoBienMueble::findOrFail($id);
        $tipo->delete();
    }
}