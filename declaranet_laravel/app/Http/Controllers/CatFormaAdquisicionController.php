<?php

namespace App\Http\Controllers;

use App\CatFormaAdquisicion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatFormaAdquisicionController extends Controller
{
    public function index()
    {
        $response = CatFormaAdquisicion::orderBy('valor', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $adquisicion = CatFormaAdquisicion::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['adquisicion' => $adquisicion]);
    }

    public function delete($id)
    {
        $adquisicion = CatFormaAdquisicion::findOrFail($id);
        $adquisicion->delete();
    }
}