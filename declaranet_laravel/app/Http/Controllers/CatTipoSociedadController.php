<?php

namespace App\Http\Controllers;

use App\CatTipoSociedad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoSociedadController extends Controller
{
    public function index()
    {
        $tipos = CatTipoSociedad::orderBy('id', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoSociedad::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoSociedad::findOrFail($id);
        $tipo->delete();
    }
}