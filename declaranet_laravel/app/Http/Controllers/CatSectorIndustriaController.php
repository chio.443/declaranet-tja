<?php

namespace App\Http\Controllers;

use App\CatSectorIndustria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatSectorIndustriaController extends Controller
{
    public function index()
    {
        $sectores = CatSectorIndustria::orderBy('valor', 'ASC')->get();
        return response()->json(['sectores' => $sectores]);
    }

    public function store(Request $request)
    {
        $sector = CatSectorIndustria::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['sector' => $sector]);
    }

    public function delete($id)
    {
        $sector = CatSectorIndustria::findOrFail($id);
        $sector->delete();
    }
}