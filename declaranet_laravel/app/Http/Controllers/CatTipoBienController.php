<?php

namespace App\Http\Controllers;

use App\CatTipoBien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoBienController extends Controller
{
    public function index()
    {
        $tipos = CatTipoBien::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoBien::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoBien::findOrFail($id);
        $tipo->delete();
    }
}