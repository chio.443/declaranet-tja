<?php

namespace App\Http\Controllers;

use App\OtrosIngresos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class OtrosIngresosController extends Controller
{
    public function index()
    {
        $actividades = OtrosIngresos::orderBy('id', 'DESC')->get();
        return response()->json(['actividades' => $actividades]);
    }

    public function item($id)
    {
        $actividades = OtrosIngresos::where('ip_id', $id)->get();
        return response()->json(['actividades' => $actividades]);
    }

    public function store(Request $request)
    {
        $actividad = OtrosIngresos::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'nombre_denominacion_razon_social' => $request->nombre_denominacion_razon_social,
                'rfc' => $request->rfc,
                'curp' => $request->curp,
                'sector_industria_id' => $request->sector_industria_id,
                'tipo_actividad_servicio_id' => $request->tipo_actividad_servicio_id,
                'descripcion_actividad' => $request->descripcion_actividad,
                'pais_id' => $request->pais_id,
                'entidad_federativa_id' => $request->entidad_federativa_id,
                'municipio_id' => $request->municipio_id,
                'cp' => $request->cp,
                'localidad_id' => $request->localidad_id,
                'colonia' => $request->colonia,
                'vialidad_tipo_vial' => $request->vialidad_tipo_vial,
                'vialidad_nom_vial' => $request->vialidad_nom_vial,
                'numExt' => $request->numExt,
                'numInt' => $request->numInt,
                'ingreso_bruto_anual' => $request->ingreso_bruto_anual,
                'moneda_id' => $request->moneda_id,
                'unidad_temporal_id' => $request->unidad_temporal_id,
                'duracion_frecuencia' => $request->duracion_frecuencia,
                'fecha_transaccion' => $request->fecha_transaccion,
                'observaciones' => $request->observaciones,
            ]
        );

        return response()->json(['actividad' => $actividad]);
    }

    public function delete($id)
    {
        $actividad = OtrosIngresos::findOrFail($id);
        $actividad->delete();
    }
}