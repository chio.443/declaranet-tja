<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CatTipoDeclaracion;

use App\Declaracion;
use App\InformacionPersonal;
use App\CatEntePublico;
use App\BitacoraDeclaracion;
use App\DeclaracionJustificada;
use App\Movimientos;
use Illuminate\Support\Facades\DB;
use App\Usuarios;
use App\UsuarioTipoDeclaracion;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

use Carbon\Carbon;

class ReportesController3_1 extends Controller
{
    public function repUsuarioTipoDec(Request $request)
    {

        $tipo_dec = $request->tipo_declaracion;
        $dependencia = $request->dependencia_id;
        $periodo = $request->periodo;
        $estatus = $request->estatus;

        $informacion = new InformacionPersonal;
        $movimientos = new Movimientos;
        $justificion = new DeclaracionJustificada;

        $usuarios = $informacion->with(['encargo', 'tipodeclaracion.bitacora', 'tipodeclaracion.declaracion'])
            ->whereHas("encargo", function ($q) use ($dependencia, $estatus) {
                $q->when($dependencia > 0, function ($query) use ($dependencia) {
                    $query->where('datos_encargo_actual.ente_publico_id', $dependencia);
                })
                    ->when($estatus == 1, function ($query) {
                        return $query->whereNull('datos_encargo_actual.fecha_termino');
                    })
                    ->when($estatus == 2, function ($query) {
                        return $query->whereNotNull('datos_encargo_actual.fecha_termino');
                    });
            })
            ->whereHas("tipodeclaracion", function ($q) use ($tipo_dec, $periodo) {
                $q->where('usuario_tipo_dec.periodo', $periodo)
                    ->where('usuario_tipo_dec.tipo_dec_id', $tipo_dec)
                    ->orderBy('usuario_tipo_dec.id', 'desc');
                // ->first();
            })
            ->orderBy('id', 'ASC')
            ->get();

        $obligados_ids = $usuarios->pluck('id')->toArray();


        $justificadas = $justificion->whereIn('id_ip', $obligados_ids)
            ->where('periodo', $periodo)
            ->where('tipo_declaracion_id', $tipo_dec)
            ->pluck('id_ip')->toArray();

        $movimientos = $movimientos->whereIn('ip_id', $obligados_ids);

        $licencias = $movimientos->where('tipo_mov', '=', 4) //4 es el id de licencia
            ->where('termina', '>', date('Y-m-d'))
            ->pluck('ip_id')->toArray();

        $bajas = $movimientos->where('tipo_mov', 2) //2 es el id de baja
            ->where('tipo_baja', '!=', 'Normal') //por defuncion
            ->pluck('ip_id')->toArray();

        // Agregar Excluir
        $excluir = array_merge($bajas, $licencias, $justificadas);

        $usuarios = $usuarios->whereNotIn('id', $excluir);

        $usuarios_tipo_dec = $usuarios->pluck('tipodeclaracion', 'id');

        $cumplidos = collect();
        $pendientes = collect();
        $cumplidos_ids = collect();
        $pendientes_ids = collect();
        $fiscales = collect();
        $intereses = collect();
        $bitacoras = collect();
        $fechas = collect();

        /*
        $usuarios_tipo_dec2 = $usuarios->pluck('tipodeclaracionl');

        $cumplidos_ids = $usuarios_tipo_dec2->where('declaracion_id', '!=', null)
            ->pluck('ip_id');

        $pendientes_ids = $usuarios_tipo_dec2->where('declaracion_id',  null)
            ->pluck('ip_id'); */

        foreach ($usuarios_tipo_dec as $i => $usuario) {
            $tipo = $usuario->where('declaracion_id', '!=', null)
                ->where('tipo_dec_id', $tipo_dec)
                ->where('periodo', $periodo)
                ->sortByDesc('id')
                ->first();

            if ($tipo) {
                $cumplidos_ids->push($i);

                // $declaraciones_ids->push([$i => $tipo->declaracion_id]);
                $fiscales[$i] = isset($tipo->bitacora->declaracion_fiscal) ? $tipo->bitacora->declaracion_fiscal : '';
                // $fiscales[$i] = $tipo->bitacora->declaracion_fiscal;
                $intereses[$i] = isset($tipo->bitacora->intereses) ? $tipo->bitacora->intereses : '';
                // $intereses[$i] = $tipo->bitacora->intereses;
                $bitacoras[$i] = $tipo->bitacora;

                $fechas[$i] = isset($tipo->declaracion->Fecha_Dec) ? $tipo->declaracion->Fecha_Dec : '';
            } else {
                $tipo2 = $usuario->where('declaracion_id', null)
                    ->where('tipo_dec_id', $tipo_dec)
                    ->sortByDesc('id');
                if (count($tipo2)) {
                    $pendientes_ids->push($i);
                }
            }
        }

        $cumplidos = $usuarios->whereIn('id', $cumplidos_ids);
        $pendientes = $usuarios->whereIn('id', $pendientes_ids);

        $porcentaje = count($cumplidos_ids) + count($pendientes_ids); // esto es el total
        $total = count($cumplidos_ids); // Esto es el avance
        $avance = 0;
        if ($porcentaje > 0) {
            $avance = $total * 100 / $porcentaje; // Esto es el porcentaje
        }


        foreach ($cumplidos as $i => $dec) {
            $bitacora = $bitacoras[$dec->id];
            if (isset($bitacora->datos_encargo_actual)) {
                $encargo = json_decode($bitacora->datos_encargo_actual)[0];
                $cumplidos[$i]['dependencia'] = (isset($encargo->ente->valor)) ? $encargo->ente->valor : '';
                $cumplidos[$i]['area_adscripcion'] = (isset($encargo->area_adscripcion)) ? $encargo->area_adscripcion : '';
                $cumplidos[$i]['empleo_cargo_comision'] = (isset($encargo->empleo_cargo_comision)) ? $encargo->empleo_cargo_comision : '';
            } else {
                $cumplidos[$i]['dependencia'] =  '';
                $cumplidos[$i]['area_adscripcion'] = '';
                $cumplidos[$i]['empleo_cargo_comision'] = '';
            }

            $cumplidos[$i]['fecha_dec'] = (isset($fechas[$dec->id])) ? $fechas[$dec->id] : '';
            $cumplidos[$i]['declaracion_fiscal'] = (isset($fiscales[$dec->id])) ? $fiscales[$dec->id] : '';
            $cumplidos[$i]['intereses'] = (isset($intereses[$dec->id])) ? $intereses[$dec->id] : '';
        }

        $respuesta  = [
            'declarantes' => $cumplidos,
            'faltantes' => $pendientes,
            'avance' => $avance,
            'porcentaje' => $porcentaje,
            'total' => $total,
            'obligadosid' => $obligados_ids,
            'declarantesIds' => $cumplidos_ids,
            // 'entidad' => $entidad,
        ];

        return response()->json($respuesta);
    }



    ////////////////////Reporte de cumplimiento/////////////////////
    //todos
    public function cumplimiento(Request $request)
    {
        $dependencia = $request->dependencia_id;
        $estatus = $request->estatus;
        $periodo = $request->periodo;
        $tipo_dec = $request->tipo_declaracion;

        $movimientos = new Movimientos;
        $justificion = new DeclaracionJustificada;
        $informacion = new InformacionPersonal;

        $justificadas = $justificion->where('periodo', $periodo)
            ->where('tipo_declaracion_id', $tipo_dec)
            ->pluck('id_ip')->toArray();

        $licencias = $movimientos->where('tipo_mov', '=', 4) //4 es el id de licencia
            ->where('termina', '>', date('Y-m-d'))
            ->pluck('ip_id')->toArray();

        $bajas = $movimientos->where('tipo_mov', 2) //2 es el id de baja
            ->where('tipo_baja', '!=', 'Normal') //por defuncion
            ->pluck('ip_id')->toArray();

        $excluir = array_merge($bajas, $licencias, $justificadas);

        $reporte = DB::table('informacion_personal as i')
            ->select(DB::raw('e.valor as dependencia,
            COUNT(case when t.declaracion_id is not null then 1 end) as Cumplidos,
            COUNT(case when t.declaracion_id is null then 1 end) as Faltantes,
            COUNT(t.id) as Obligados'))
            ->join('usuario_tipo_dec as t', 't.ip_id', '=', 'i.id')
            ->join('datos_encargo_actual as d', 'd.informacion_personal_id', '=', 'i.id')
            ->join('cat_ente_publico as e', 'e.id', '=', 'd.ente_publico_id')
            ->whereNotIn('i.id',  $excluir)
            ->where('t.periodo', $periodo)
            ->when($tipo_dec < 4, function ($query) use ($tipo_dec) {
                $query->where('t.tipo_dec_id', $tipo_dec);
            })
            ->when($dependencia > 0, function ($query) use ($dependencia) {
                $query->where('d.ente_publico_id', $dependencia);
            })
            ->when($estatus == 1, function ($query) {
                return $query->whereNull('d.fecha_termino');
            })
            ->when($estatus == 2, function ($query) {
                return $query->whereNotNull('d.fecha_termino');
            })
            ->groupBy('e.valor')
            ->orderBy('e.valor')
            ->get();


        $total_obligados = array_sum($reporte->pluck('obligados')->toArray());
        $total_cumplidos = array_sum($reporte->pluck('cumplidos')->toArray());



        foreach ($reporte as $i => $dep) {
            if ($dep->cumplidos > 0) {
                $individual = $dep->cumplidos * 100 / $dep->obligados;
            } else {
                $individual = 0;
            }

            if ($dep->obligados > 0) {
                $total = $dep->cumplidos * 100 / $total_obligados;
            } else {
                $total = 0;
            }

            $reporte[$i]->individual = $individual;
            $reporte[$i]->total = $total;
        }
        /*
        -- Cumplidos
        select e.valor,
            COUNT(case when t.declaracion_id is not null then 1 end) as Cumplidos,
            COUNT(case when t.declaracion_id is null then 1 end) as Faltantes,
            COUNT(case when t.tipo_dec_id = 1 then 1 end) as Iniciales,
            COUNT(case when t.tipo_dec_id = 2 then 1 end) as Anuales,
            COUNT(case when t.tipo_dec_id = 1 and t.declaracion_id is not null then 1 end) as Iniciales_realizadas,
            COUNT(case when t.tipo_dec_id = 2 and t.declaracion_id is not null then 1 end) as Anuales_realizadas
        from informacion_personal as i
        join usuario_tipo_dec as t on t.ip_id = i.id
        join datos_encargo_actual as d on d.informacion_personal_id = i.id
        join cat_ente_publico as e on e.id = d.ente_publico_id
        where  t.periodo = '2021' and d.fecha_termino is null
        GROUP BY  e.valor
        ORDER BY e.valor
        */

        return response()->json($reporte);
    }
}