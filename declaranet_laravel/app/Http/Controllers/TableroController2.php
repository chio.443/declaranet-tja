<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformacionPersonal;
use App\Declaracion;
use DB;
use App\Usuarios;
use \App\Token;

use App\CatEntePublico;

class TableroController2 extends Controller
{


    public function globales(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;
        $anioact    =  date("Y");

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia
        $usuario = Usuarios::where('id', $uid)->with('roles')->first();      //

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        $desglosado;

        $acumInicial = 0;
        $acumAnual = 0;
        $acumFinal = 0;

        if (in_array(18, $permisos)) {

            $entidades = CatEntePublico::select('id')->orderBy('valor')->get();

            $declaracionesInic = 0;
            $declaracionesAnu = 0;
            $declaracionesFin = 0;


            foreach ($entidades as $indice => $dato) {
                $declarantes =  DB::table('declaraciones')
                    ->select(DB::raw('id_ip'))
                    ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                    ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                    ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                    ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                    ->whereNull('datos_encargo_actual.deleted_at')
                    ->whereNull('bitacora_declaraciones.deleted_at')
                    ->whereNull('informacion_personal.deleted_at')
                    ->whereNull('declaraciones.deleted_at')
                    ->whereNull('cat_ente_publico.deleted_at')
                    ->where('declaraciones.ente_publico_id', $dato->id)
                    ->whereYear('declaraciones.Fecha_Dec', $anioact)
                    ->where('declaraciones.tipo_declaracion_id', 1)
                    ->get();



                $declarantesIds = array();
                foreach ($declarantes as $dec) {
                    $declarantesIds[] = $dec->id_ip;
                }

                $declaracionesInic += count($declarantesIds);


                $declarantesInicial = InformacionPersonal::select('rfc')
                    ->whereHas("encargo", function ($q) use ($anioact, $dato) {
                        $q->where("tipo_dec", 1)
                            ->where('ente_publico_id', $dato->id);
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', '=', 4) //4 es el id de licencia
                            ->where('termina', '>', date('Y-m-d'))
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) use ($anioact) {
                        $query->select('id_ip')
                            ->from('declaracion_justificada')
                            ->where('tipo_declaracion_id', 1)
                            ->where('periodo', $anioact)
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', 2) //2 es el id de baja
                            ->where('tipo_baja', '!=', 'Normal') //por defuncion
                            ->whereNull('deleted_at');
                    })
                    ->orWhereIn('id', $declarantesIds)
                    ->whereNull('deleted_at')
                    ->groupBy('rfc')
                    ->havingRaw('count(rfc) = 1')
                    ->get()->count();


                $declarantes =  DB::table('declaraciones')
                    ->select(DB::raw('id_ip'))
                    ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                    ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                    ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                    ->whereNull('bitacora_declaraciones.deleted_at')
                    ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                    ->whereNull('datos_encargo_actual.deleted_at')
                    ->whereNull('informacion_personal.deleted_at')
                    ->whereNull('declaraciones.deleted_at')
                    ->whereNull('cat_ente_publico.deleted_at')
                    ->where('declaraciones.ente_publico_id', $dato->id)
                    ->whereYear('declaraciones.Fecha_Dec', $anioact)
                    ->where('declaraciones.tipo_declaracion_id', 2)
                    ->get();

                $declarantesIds = array();
                foreach ($declarantes as $dec) {
                    $declarantesIds[] = $dec->id_ip;
                }


                $declaracionesAnu += count($declarantesIds);


                $declarantesAnual = InformacionPersonal::select('rfc')
                    ->whereHas("encargo", function ($q) use ($anioact, $dato) {
                        $q->where("tipo_dec", 2)
                            ->where('ente_publico_id', $dato->id);
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', '=', 4) //4 es el id de licencia
                            ->where('termina', '>', date('Y-m-d'))
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) use ($anioact) {
                        $query->select('id_ip')
                            ->from('declaracion_justificada')
                            ->where('tipo_declaracion_id', 2)
                            ->where('periodo', $anioact)
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', 2) //2 es el id de baja
                            ->where('tipo_baja', '!=', 'Normal') //por defuncion
                            ->whereNull('deleted_at');
                    })
                    ->orWhereIn('id', $declarantesIds)
                    ->whereNull('deleted_at')
                    ->groupBy('rfc')
                    ->havingRaw('count(rfc) = 1')
                    ->get()->count();

                $declarantes =  DB::table('declaraciones')
                    ->select(DB::raw('id_ip'))
                    ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                    ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                    ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                    ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                    ->whereNull('datos_encargo_actual.deleted_at')
                    ->whereNull('bitacora_declaraciones.deleted_at')
                    ->whereNull('informacion_personal.deleted_at')
                    ->whereNull('declaraciones.deleted_at')
                    ->whereNull('cat_ente_publico.deleted_at')
                    ->where('declaraciones.ente_publico_id', $dato->id)
                    ->whereYear('declaraciones.Fecha_Dec', $anioact)
                    ->where('declaraciones.tipo_declaracion_id', 3)
                    ->get();

                $declarantesIds = array();
                foreach ($declarantes as $dec) {
                    $declarantesIds[] = $dec->id_ip;
                }


                $declaracionesFin += count($declarantesIds);

                $declarantesFinal = InformacionPersonal::select('rfc')
                    ->whereHas("encargo", function ($q) use ($anioact, $dato) {
                        $q->where("tipo_dec", 3)
                            ->where('ente_publico_id', $dato->id)
                            ->whereYear('fecha_termino', $anioact);
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', '=', 4) //4 es el id de licencia
                            ->where('termina', '>', date('Y-m-d'))
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) use ($anioact) {
                        $query->select('id_ip')
                            ->from('declaracion_justificada')
                            ->where('tipo_dec', 3)
                            ->where('periodo', $anioact)
                            ->whereNull('deleted_at');
                    })
                    ->whereNotIn('id', function ($query) {
                        $query->select('ip_id')
                            ->from('movimientos')
                            ->where('tipo_mov', 2) //2 es el id de baja
                            ->where('tipo_baja', '!=', 'Normal') //por defuncion
                            ->whereNull('deleted_at');
                    })
                    ->orWhereIn('id', $declarantesIds)
                    ->whereNull('deleted_at')
                    ->groupBy('rfc')
                    ->havingRaw('count(rfc) = 1')
                    ->get()->count();

                $acumInicial += $declarantesInicial;
                $acumAnual += $declarantesAnual;
                $acumFinal += $declarantesFinal;
            }

            $declarantesInicial = $acumInicial;
            $declarantesAnual = $acumAnual;
            $declarantesFinal = $acumFinal;


            $declaraciones = DB::table('declaraciones')
                ->select("id_ip")
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('datos_encargo_actual.deleted_at')
                ->whereNull('bitacora_declaraciones.deleted_at')
                ->whereNull('declaraciones.deleted_at')
                ->whereYear('declaraciones.Fecha_Dec', $anioact)
                ->whereNull('informacion_personal.deleted_at')
                ->get()
                ->count();


            // $desglosado = DB::table('declaraciones')
            //            ->select(DB::raw(' count(declaraciones.id), cat_tipo_declaracion.tipo_declaracion'))
            //            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            //            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            //            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            //            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            // 		->whereNull('datos_encargo_actual.deleted_at')
            //            ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            //      			->whereNull('bitacora_declaraciones.deleted_at')
            //            ->whereNull('informacion_personal.deleted_at')
            //            ->whereNull('declaraciones.deleted_at')
            //            ->whereYear('declaraciones.Fecha_Dec', $anioact)
            //            ->groupBy('cat_tipo_declaracion.tipo_declaracion')
            //            ->orderBy('cat_tipo_declaracion.tipo_declaracion')
            //            ->get();



        } else {
            $declarantes = DB::table('declaraciones')
                ->select(DB::raw('informacion_personal.id'))
                ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('datos_encargo_actual.deleted_at')
                ->whereNull('bitacora_declaraciones.deleted_at')
                ->whereNull('informacion_personal.deleted_at')
                ->whereNull('declaraciones.deleted_at')
                ->whereNull('cat_ente_publico.deleted_at')
                ->whereYear('declaraciones.Fecha_Dec', $anioact)
                ->where('declaraciones.tipo_declaracion_id', 1)
                ->where('declaraciones.ente_publico_id', $ente)
                ->get();

            $declarantesIds = array();
            foreach ($declarantes as $dec) {
                $declarantesIds[] = $dec->id;
            }


            $declaracionesInic += count($declarantesIds);

            $declarantesInicial = InformacionPersonal::select('rfc')
                ->whereHas("encargo", function ($q) use ($anioact, $ente) {
                    $q->where("tipo_dec", 1)
                        ->where('ente_publico_id', $ente);
                })
                ->whereNotIn('id', function ($query) {
                    $query->select('ip_id')
                        ->from('movimientos')
                        ->where('tipo_mov', '=', 4) //4 es el id de licencia
                        ->where('termina', '>', date('Y-m-d'))
                        ->whereNull('deleted_at');
                })
                ->whereNotIn('id', function ($query) use ($anioact) {
                    $query->select('id_ip')
                        ->from('declaracion_justificada')
                        ->where('tipo_declaracion_id', 1)
                        ->where('periodo', $anioact)
                        ->whereNull('deleted_at');
                })
                ->orWhereIn('id', $declarantesIds)
                ->get()->count();

            $declarantes = DB::table('declaraciones')
                ->select(DB::raw('informacion_personal.id'))
                ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('datos_encargo_actual.deleted_at')
                ->whereNull('bitacora_declaraciones.deleted_at')
                ->whereNull('informacion_personal.deleted_at')
                ->whereNull('declaraciones.deleted_at')
                ->whereNull('cat_ente_publico.deleted_at')
                ->whereYear('declaraciones.Fecha_Dec', $anioact)
                ->where('declaraciones.tipo_declaracion_id', 2)
                ->where('declaraciones.ente_publico_id', $ente)
                ->get();

            $declarantesIds = array();
            foreach ($declarantes as $dec) {
                $declarantesIds[] = $dec->id;
            }


            $declaracionesAnu += count($declarantesIds);


            $declarantesAnual = InformacionPersonal::select('rfc')
                ->whereHas("encargo", function ($q) use ($anioact, $ente) {
                    $q->where("tipo_dec", 2)
                        ->where('ente_publico_id', $ente);
                })
                ->whereNotIn('id', function ($query) {
                    $query->select('ip_id')
                        ->from('movimientos')
                        ->where('tipo_mov', '=', 4) //4 es el id de licencia
                        ->where('termina', '>', date('Y-m-d'))
                        ->whereNull('deleted_at');
                })
                ->whereNotIn('id', function ($query) use ($anioact) {
                    $query->select('id_ip')
                        ->from('declaracion_justificada')
                        ->where('tipo_declaracion_id', 2)
                        ->where('periodo', $anioact)
                        ->whereNull('deleted_at');
                })
                ->orWhereIn('id', $declarantesIds)
                ->get()->count();

            $declarantes = DB::table('declaraciones')
                ->select(DB::raw('informacion_personal.id'))
                ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('datos_encargo_actual.deleted_at')
                ->whereNull('informacion_personal.deleted_at')
                ->whereNull('declaraciones.deleted_at')
                ->whereNull('cat_ente_publico.deleted_at')
                ->whereYear('declaraciones.Fecha_Dec', $anioact)
                ->where('declaraciones.tipo_declaracion_id', 3)
                ->where('declaraciones.ente_publico_id', $ente)
                ->get();

            $declarantesIds = array();
            foreach ($declarantes as $dec) {
                $declarantesIds[] = $dec->id;
            }


            $declaracionesFin += count($declarantesIds);


            $declarantesFinal = InformacionPersonal::select('rfc')
                ->whereHas("encargo", function ($q) use ($anioact, $ente) {
                    $q->where("tipo_dec", 3)
                        ->where('ente_publico_id', $ente)
                        ->whereYear('fecha_termino', $anioact);
                })
                ->whereNotIn('id', function ($query) {
                    $query->select('ip_id')
                        ->from('movimientos')
                        ->where('tipo_mov', '=', 4) //4 es el id de licencia
                        ->where('termina', '>', date('Y-m-d'))
                        ->whereNull('deleted_at');
                })
                ->whereNotIn('id', function ($query) use ($anioact) {
                    $query->select('id_ip')
                        ->from('declaracion_justificada')
                        ->where('tipo_dec', 3)
                        ->where('periodo', $anioact)
                        ->whereNull('deleted_at');
                })
                ->orWhereIn('id', $declarantesIds)
                ->get()->count();
            $declaraciones = DB::table('declaraciones')
                ->select("id_ip")
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
                ->whereNull('cat_ente_publico.deleted_at')
                ->whereNull('datos_encargo_actual.deleted_at')
                // ->whereNotNull('datos_encargo_actual.ente_publico_id')
                ->whereNull('declaraciones.deleted_at')
                ->where('declaraciones.ente_publico_id', $ente)
                // //->whereNull('datos_encargo_actual.fecha_termino')
                // ->where(function($q) use ($anioact){
                //        $q->whereRaw("date_part('year',datos_encargo_actual.fecha_posesion) != '".$anioact."'")
                //        ->orWhere('datos_encargo_actual.tipo_dec',2);
                //   })
                //->whereRaw("date_part('year',declaraciones.created_at) = '".$anioact."'")
                ->whereYear('declaraciones.Fecha_Dec', $anioact)
                ->whereNull('informacion_personal.deleted_at')
                // ->groupBy('id_ip')
                ->get()
                ->count();

            // $desglosado = DB::table('declaraciones')
            //            ->select(DB::raw(' count(declaraciones.id), cat_tipo_declaracion.tipo_declaracion'))
            //            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            //            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            //            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            //            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            // 		->whereNull('datos_encargo_actual.deleted_at')
            //            ->whereNull('informacion_personal.deleted_at')
            //            ->whereNull('declaraciones.deleted_at')
            //          		->whereNull('cat_ente_publico.deleted_at')
            //            ->where('declaraciones.ente_publico_id',$ente)
            //            ->whereYear('declaraciones.Fecha_Dec', $anioact)
            //            ->groupBy('cat_tipo_declaracion.tipo_declaracion')
            //            ->orderBy('cat_tipo_declaracion.tipo_declaracion')
            //            ->get();



        }


        $declarantes = $declarantesInicial + $declarantesAnual + $declarantesFinal;

        $faltantes = $declarantes - $declaraciones;


        $porcentaje = 0;
        if ($declarantes > 0)
            $porcentaje = $declaraciones * 100 / $declarantes;

        $respuesta  = ['declarantes' => $declarantes, 'faltantes' => $faltantes, 'declaraciones' => $declaraciones, 'porcentaje' => (($porcentaje)), 'dinic' => $declaracionesInic, 'danu' => $declaracionesAnu, 'dfin' => $declaracionesFin, 'decIni' => $declarantesInicial, 'decAn' => $declarantesAnual, 'decFin' => $declarantesFinal];


        return response()->json($respuesta);
    }

    public function getUltimos7(Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);
        //usuario logueado
        $uid = $token->uid;

        //obtener si tiene el permiso para las cifras globales, si no, solo de su dependencia


        $usuario = Usuarios::where('id', $uid)->with('roles')->first();

        $ente = $usuario['ID_Dependencia'];

        $permisos = $usuario->roles->Permisos;

        $fecha =  \Carbon\Carbon::today()->subDays(7);

        if (in_array(18, $permisos)) {
            $declaraciones = Declaracion::where('created_at', '>=', $fecha)
                ->groupBy('created_at')
                ->orderBy('created_at', 'DESC')
                ->where('tipo_declaracion_id', 2)
                ->get(array(
                    DB::raw("CASE WHEN to_char(created_at,'TMDay')='Monday' 	THEN 'LUN'
						                            WHEN to_char(created_at,'TMDay')='Tuesday' 		THEN 'MAR'
						                            WHEN to_char(created_at,'TMDay')='Wednesday' 	THEN 'MIE'
						                            WHEN to_char(created_at,'TMDay')='Thursday' 	THEN 'JUE'
						                            WHEN to_char(created_at,'TMDay')='Friday'  		THEN 'VIE'
						                            WHEN to_char(created_at,'TMDay')='Saturday'  	THEN 'SAB'
						                            WHEN to_char(created_at,'TMDay')='Sunday' 		THEN 'DOM'
											       END as date"), DB::raw('COUNT(*) as "total"')
                ));
        } else {
            $declaraciones = DB::table('declaraciones')
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('declaraciones.deleted_at')
                ->where('datos_encargo_actual.ente_publico_id', $ente)
                ->whereNull('informacion_personal.deleted_at')
                ->groupBy('declaraciones.created_at')
                ->where('tipo_declaracion_id', 2)
                ->where('declaraciones.created_at', '>=', $fecha)
                ->orderBy('declaraciones.created_at', 'DESC')
                ->get(array(
                    DB::raw("CASE WHEN to_char(declaraciones.created_at,'TMDay')='Monday' 	THEN 'LUN'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Tuesday' 		THEN 'MAR'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Wednesday' 	THEN 'MIE'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Thursday' 	THEN 'JUE'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Friday'  		THEN 'VIE'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Saturday'  	THEN 'SAB'
			                            WHEN to_char(declaraciones.created_at,'TMDay')='Sunday' 		THEN 'DOM'
								       END as date"), DB::raw('COUNT(*) as "total"')
                ));
        }



        return response()->json($declaraciones);
    }
}