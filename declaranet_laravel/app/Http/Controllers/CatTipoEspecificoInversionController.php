<?php

namespace App\Http\Controllers;

use App\CatTipoEspecificoInversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoEspecificoInversionController extends Controller
{
    public function index()
    {
        $tipos = CatTipoEspecificoInversion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoEspecificoInversion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoEspecificoInversion::findOrFail($id);
        $tipo->delete();
    }
}