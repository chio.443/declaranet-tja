<?php

namespace App\Http\Controllers;

use App\CatTitularAdeudo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTitularAdeudoController extends Controller
{
    public function index()
    {
        $response = CatTitularAdeudo::orderBy('id', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = CatTitularAdeudo::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $titular = CatTitularAdeudo::findOrFail($id);
        $titular->delete();
    }
}