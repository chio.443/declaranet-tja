<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DatosCurriculares;

use \App\Token;

use Carbon\Carbon;

class DatosCurricController extends Controller
{
   
    public function index()
    {
       
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $datosCurr = DatosCurriculares::where('informacion_personal_id',$id)->get();

        return response()->json(['datosCurr' =>$datosCurr]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    { 
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;


        $informacion = $request->all();
        $informacion['fecha_obtencion'] = Carbon::parse($request->fecha_obtencion);

        $registro = DatosCurriculares::updateOrCreate(['informacion_personal_id'=> $id,
                                                        'id'=> $request['id']],
                                                $informacion
                                                );    

        
        $registro->save();
        
        return response()->json(['registro' =>$registro->refresh()]);

    }

       public function destroy($id)
    {        
        $dcurruc = DatosCurriculares::findOrFail($id);
        $dcurruc->delete();        
    }
}
