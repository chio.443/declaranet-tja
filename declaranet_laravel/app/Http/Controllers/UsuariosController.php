<?php

namespace App\Http\Controllers;

use App\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


use App\Token;

class UsuariosController extends Controller
{
    public function index()
    {
        $usuarios = Usuarios::orderBy('Nombre', 'ASC')
            ->orderBy('Paterno', 'ASC')
            ->orderBy('Materno', 'ASC')
            ->get();

        return response()->json(['usuarios' => $usuarios]);
    }
    /* public function item($id)
    {
        $usuarios = Usuarios::where('id', $id)->get();
        return response()->json(['usuarios' => $usuarios]);
    } */

    public function getTipoUsuarios()
    {
        $tipos = DB::table('cat_roles')
            ->select('id as id', 'Rol as valor')
            ->whereNull('deleted_at')
            ->orderBy('id', 'ASC')
            ->get();

        return response()->json(['tipos' => $tipos]);
    }



    public function store(Request $request)
    {

        $usuario = Usuarios::where('User', strtolower($request->User))->first();

        if ((!$usuario) || ($request->id === $usuario->id)) {
            $usuario = Usuarios::updateOrCreate(
                ['id' => $request->id],
                [
                    'Nombre' => $request->Nombre,
                    'Paterno' => $request->Paterno,
                    'Materno' => $request->Materno,
                    'User' => strtolower($request->User),
                    'Password' => ($request->modificar == "0") ? DB::raw("md5('" . $request->Password . "')") : $request->Password,
                    'tipo_id' => $request->tipo_id,
                    'ID_Dependencia' => $request->ID_Dependencia
                ]
            );

            return response()->json(['usuario' => $usuario]);
        } else {
            return response()->json(['error' => $usuario]);
        }
    }

    public function delete($uid, Request $request)
    {

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $usuario = Usuarios::findOrFail($uid);
        $usuario->update(array('deleted_by' => $id, 'deleted_at' => date('Ymd')));
        $usuario->delete();
    }
}
