<?php

namespace App\Http\Controllers;

use DB;
use App\Usuarios;

use Carbon\Carbon;
use App\Declaracion;
use App\CatEntePublico;
use App\CatTipoDeclaracion;
use App\InformacionPersonal;

use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReportesController extends Controller
{

    public function tipoDeclaracion(Request $req)
    {
        $tipoDeclaracion = CatTipoDeclaracion::orderBy('id', 'asc')->get();

        return response()->json(['tipoDeclaracion' => $tipoDeclaracion]);
    }

    public function periodos()
    {
        $resultado = Declaracion::select(DB::raw("date_part('year',created_at) as year"))->distinct()
            ->orderBy('year', 'DESC')
            ->get();

        //$periodos = $resultado->pluck('year');

        return response()->json(['periodos' => $resultado]);
    }
    //para cuando se seleccionan todos
    public function declarantes(Request $request)
    {
        $anioact    =  $request->periodo;

        $anio2 = date('Y');

        $declarantes = DB::table('declaraciones')
            ->select(DB::raw('informacion_personal.id, segundo_apellido, primer_apellido, nombres, rfc, empleo_cargo_comision, datos_encargo_actual.area_adscripcion,declaraciones.created_at as Fecha_Dec, bitacora_declaraciones.declaracion_fiscal, bitacora_declaraciones.intereses'))
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->whereNull('informacion_personal.deleted_at')
            ->whereNull('declaraciones.deleted_at')
            //->where('declaraciones.ente_publico_id',$request->dependencia_id)
            ->where('informacion_personal.ente_publico_id', $request->dependencia_id)
            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
            ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
            //->where('informacion_personal.created_at','<=',$anioact.'-12-31')
            ->get();
        //->count()

        $declarantesIds = array();
        foreach ($declarantes as $dec) {
            $declarantesIds[] = $dec->id;
        }



        $declarantesIds = array_unique($declarantesIds, SORT_NUMERIC);

        $total = count($declarantesIds);



        $entidad = CatEntePublico::select('valor')->where('id', $request->dependencia_id)->get();

        $porcentaje =  InformacionPersonal::select('id')
            ->whereHas("encargo", function ($q) use ($request, $anio2) {

                if ($request->periodo == $anio2) { //para cuando el periodo seleccionado en el reporte es diferente al año actual
                    if ($request->tipo_declaracion == 3) {
                        $q->where('tipo_dec', $request->tipo_declaracion)
                            ->where('ente_publico_id', $request->dependencia_id)
                            ->whereYear('fecha_termino', $request->periodo)
                            ->whereNull('deleted_at');
                    } else {
                        $q->where('tipo_dec', $request->tipo_declaracion)
                            ->where('ente_publico_id', $request->dependencia_id)
                            ->whereNull('deleted_at');
                    }
                } else {
                    // no entra
                    if ($request->tipo_declaracion == 3) {
                        $q->where('tipo_dec', $request->tipo_declaracion)
                            ->where('ente_publico_id', $request->dependencia_id)
                            ->whereNull('deleted_at');
                    } else {
                        $q->where('ente_publico_id', $request->dependencia_id)
                            ->whereYear('fecha_posesion', '<', $request->periodo)
                            ->whereYear('created_at', '<', $request->periodo)
                            ->whereNull('fecha_termino')
                            ->whereNull('deleted_at');
                    }
                }
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', '=', 4) //4 es el id de licencia
                    ->where('termina', '>', date('Y-m-d'))
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $query->select('id_ip')
                    ->from('declaracion_justificada')
                    ->where('tipo_declaracion_id', $request->tipo_declaracion)
                    ->where('periodo', $request->periodo)
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', 2) //2 es el id de baja
                    ->where('tipo_baja', '!=', 'Normal') //por defuncion
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                if ($request->tipo_declaracion == 1) {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        //->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                } else {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        ->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                }
            })
            ->orWhereIn('id', $declarantesIds)
            ->whereNull('deleted_at')
            ->get();


        $obligadosIds = array();

        foreach ($porcentaje as $obligado) {
            $obligadosIds[] = $obligado->id;
        }

        $porcentaje = $porcentaje->count();


        //si son más de 20 mil obligados
        if (count($obligadosIds) > 20000) {

            $collection = collect();

            $obligadosChnks = array_chunk($obligadosIds, 10000, true);
            //buscar a pedacitos
            foreach ($obligadosChnks as $obligadosIdsC) {

                $faltantes = InformacionPersonal::with('encargo')
                    ->whereIn('id', $obligadosIdsC)
                    ->whereNotIn('id', $declarantesIds)
                    ->orderBy('nombres', 'asc')
                    ->orderBy('primer_apellido', 'asc')
                    ->orderBy('segundo_apellido', 'asc')
                    ->get();


                foreach ($faltantes as $faltante)
                    $collection->push($faltante);
            }

            $faltantes = $collection;
        } else {
            $faltantes = InformacionPersonal::with('encargo')
                ->whereIn('id', $obligadosIds)
                ->whereNotIn('id', $declarantesIds)
                ->orderBy('nombres', 'asc')
                ->orderBy('primer_apellido', 'asc')
                ->orderBy('segundo_apellido', 'asc')
                ->get();
        }


        if ($porcentaje > 0) {
            $resp = ($total * 100) / $porcentaje;
        } else {
            $resp = 0;
        }

        $respuesta  = ['declarantes' => $declarantes, 'avance' => $resp, 'entidad' => $entidad, 'porcentaje' => $porcentaje, 'total' => $total, 'faltantes' => $faltantes, 'obligadosid' => $obligadosIds, 'declarantesIds' => $declarantesIds];


        return response()->json($respuesta);
    }
    //para cuando se seleccionan todos y todas
    public function declarantesTodos(Request $request)
    {
        $entidades = CatEntePublico::select('id')->orderBy('valor')->get();

        $faltantesTodos = collect();
        $declarantesTodos = collect();
        $respTodos = 0;
        $entidad = "Todas";
        $porcentajeTodos = 0;
        $totalTodos = 0;

        $anioact    =  $request->periodo;
        $anio2 = date('Y');

        foreach ($entidades as $indice => $dato) {

            $declarantes =  DB::table('declaraciones')
                ->select(DB::raw('informacion_personal.id, segundo_apellido, primer_apellido, nombres, rfc, empleo_cargo_comision, datos_encargo_actual.area_adscripcion,declaraciones.created_at as Fecha_Dec, bitacora_declaraciones.declaracion_fiscal, bitacora_declaraciones.intereses, cat_ente_publico.valor'))
                ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
                ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
                ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
                ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
                ->whereNull('datos_encargo_actual.deleted_at')
                ->whereNull('bitacora_declaraciones.deleted_at')
                ->whereNull('informacion_personal.deleted_at')
                ->whereNull('declaraciones.deleted_at')
                ->whereNull('cat_ente_publico.deleted_at')
                ->where('declaraciones.ente_publico_id', $dato->id)
                ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
                ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
                ->get();


            $declarantesTodos = $declarantesTodos->merge($declarantes); // Contains foo and bar.

            $declarantesIds = array();
            foreach ($declarantes as $dec) {
                $declarantesIds[] = $dec->id;
            }

            $total = count($declarantesIds);

            //$declarantesIds = array_unique($declarantesIds,SORT_NUMERIC);


            $entidad = CatEntePublico::select('valor')->where('id', $dato->id)->get();

            $porcentaje =  InformacionPersonal::select('id')
                ->whereHas("encargo", function ($q) use ($request, $anio2, $dato) {

                    if ($request->periodo == $anio2) { //para cuando el periodo seleccionado en el reporte es diferente al año actual
                        if ($request->tipo_declaracion == 3) {
                            $q->where('tipo_dec', $request->tipo_declaracion)
                                ->where('ente_publico_id', $dato->id)
                                ->whereYear('fecha_termino', $request->periodo)
                                ->whereNull('deleted_at');
                        } else {
                            $q->where('tipo_dec', $request->tipo_declaracion)
                                ->where('ente_publico_id', $dato->id)
                                ->whereNull('deleted_at');
                        }
                    } else {
                        if ($request->tipo_declaracion == 3) {
                            $q->where('tipo_dec', $request->tipo_declaracion)
                                ->where('ente_publico_id', $dato->id)
                                ->whereNull('deleted_at');
                        } else {
                            $q->where('ente_publico_id', $dato->id)
                                ->whereYear('fecha_posesion', '<', $request->periodo)
                                ->whereYear('created_at', '<', $request->periodo)
                                ->whereNull('fecha_termino')
                                ->whereNull('deleted_at');
                        }
                    }
                })
                ->whereNotIn('id', function ($query) {
                    $query->select('ip_id')
                        ->from('movimientos')
                        ->where('tipo_mov', '=', 4) //4 es el id de licencia
                        ->where('termina', '>', date('Y-m-d'))
                        ->whereNull('deleted_at');
                })
                ->whereNotIn('id', function ($query) use ($request) {
                    $query->select('id_ip')
                        ->from('declaracion_justificada')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        ->where('periodo', $request->periodo)
                        ->whereNull('deleted_at');
                })
                ->whereNotIn('id', function ($query) {
                    $query->select('ip_id')
                        ->from('movimientos')
                        ->where('tipo_mov', 2) //2 es el id de baja
                        ->where('tipo_baja', '!=', 'Normal') //por defuncion
                        ->whereNull('deleted_at');
                })
                ->get();


            $obligadosIds = array();

            foreach ($porcentaje as $obligado) {
                $obligadosIds[] = $obligado->id;
            }

            $porcentaje = $porcentaje->count();


            //si son más de 20 mil obligados
            if (count($obligadosIds) > 20000) {

                $collection = collect();

                $obligadosChnks = array_chunk($obligadosIds, 10000, true);
                //buscar a pedacitos
                foreach ($obligadosChnks as $obligadosIdsC) {

                    $faltantes = InformacionPersonal::with('encargo')
                        ->whereIn('id', $obligadosIdsC)
                        ->whereNotIn('id', $declarantesIds)
                        ->orderBy('nombres', 'asc')
                        ->orderBy('primer_apellido', 'asc')
                        ->orderBy('segundo_apellido', 'asc')
                        ->get();

                    foreach ($faltantes as $faltante)
                        $collection->push($faltante);
                }

                $faltantes = $collection;
            } else {
                $faltantes = InformacionPersonal::with('encargo')
                    ->whereIn('id', $obligadosIds)
                    ->whereNotIn('id', $declarantesIds)
                    ->orderBy('nombres', 'asc')
                    ->orderBy('primer_apellido', 'asc')
                    ->orderBy('segundo_apellido', 'asc')
                    ->get();
            }

            $faltantesTodos = $faltantesTodos->merge($faltantes); // Contains foo and bar.

            $porcentajeTodos += $porcentaje;
            $totalTodos += $total;
        }

        if ($porcentajeTodos > 0) {
            $respTodos = ($totalTodos * 100) / $porcentajeTodos;
        } else {
            $respTodos = 0;
        }


        $respuesta  = ['declarantes' => $declarantesTodos, 'avance' => $respTodos, 'entidad' => $entidad, 'porcentaje' => $porcentajeTodos, 'total' => $totalTodos, 'faltantes' => $faltantesTodos];


        return response()->json($respuesta);
    }
    public function declarantesActivos(Request $request)
    {
        $anioact    =  $request->periodo;

        $anio2 = date('Y');


        $declarantes =         $declarantes = DB::table('declaraciones')
            ->select(DB::raw('informacion_personal.id, segundo_apellido, primer_apellido, nombres, rfc, empleo_cargo_comision, datos_encargo_actual.area_adscripcion,declaraciones.created_at as Fecha_Dec, cat_ente_publico.valor as dependencia, bitacora_declaraciones.declaracion_fiscal, bitacora_declaraciones.intereses'))
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->whereNull('informacion_personal.deleted_at')
            ->where('datos_encargo_actual.tipo_dec', '!=', 3)
            ->whereNull('declaraciones.deleted_at')
            //PRUEBA ->where('declaraciones.ente_publico_id',$request->dependencia_id)
            ->where('informacion_personal.ente_publico_id', $request->dependencia_id)
            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
            ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
            //->where('informacion_personal.created_at','<=',$anioact.'-12-31')
            ->get();
        //->count()

        $declarantesIds = array();
        foreach ($declarantes as $dec) {
            $declarantesIds[] = $dec->id;
        }


        $declarantesIds = array_unique($declarantesIds, SORT_NUMERIC);

        $total = count($declarantesIds);


        $entidad = CatEntePublico::select('valor')->where('id', $request->dependencia_id)->get();

        $porcentaje =  InformacionPersonal::select('id')
            ->whereHas("encargo", function ($q) use ($request, $anio2) {
                if ($request->periodo == $anio2) {
                    if ($request->tipo_declaracion == 3) {
                        $q->where('tipo_dec', $request->tipo_declaracion)
                            ->where('ente_publico_id', $request->dependencia_id)
                            ->whereYear('fecha_termino', $request->periodo)
                            ->whereNull('deleted_at');
                    } else {
                        $q->where('tipo_dec', $request->tipo_declaracion)
                            ->where('ente_publico_id', $request->dependencia_id)
                            ->whereNull('deleted_at');
                    }
                } else {
                    $q->where('ente_publico_id', $request->dependencia_id)
                        ->whereYear('fecha_posesion', '<', $request->periodo)
                        ->whereYear('created_at', '<', $request->periodo)
                        ->whereNull('fecha_termino')
                        ->whereNull('deleted_at');
                }
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', '=', 4) //4 es el id de licencia
                    ->where('termina', '>', date('Y-m-d'))
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $query->select('id_ip')
                    ->from('declaracion_justificada')
                    ->where('tipo_declaracion_id', $request->tipo_declaracion)
                    ->where('periodo', $request->periodo)
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', 2) //2 es el id de baja
                    ->where('tipo_baja', '!=', 'Normal') //por defuncion
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                if ($request->tipo_declaracion == 1) {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        //->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                } else {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        ->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                }
            })
            ->orWhereIn('id', $declarantesIds)
            ->whereNull('deleted_at')
            ->get();


        $obligadosIds = array();

        foreach ($porcentaje as $obligado) {
            $obligadosIds[] = $obligado->id;
        }

        $porcentaje = $porcentaje->count();

        $diferencia = array_diff($obligadosIds, $declarantesIds);


        //si son más de 20 mil obligados
        if (count($obligadosIds) > 20000) {


            $collection = collect();

            $obligadosChnks = array_chunk($obligadosIds, 10000, true);
            //buscar a pedacitos
            foreach ($obligadosChnks as $obligadosIdsC) {

                $faltantes = InformacionPersonal::with('encargo')
                    ->whereIn('id', $obligadosIdsC)
                    ->whereNotIn('id', $declarantesIds)
                    ->orderBy('nombres', 'asc')
                    ->orderBy('primer_apellido', 'asc')
                    ->orderBy('segundo_apellido', 'asc')
                    ->get();
                foreach ($faltantes as $faltante)
                    $collection->push($faltante);
            }

            $faltantes = $collection;
        } else {
            $faltantes = InformacionPersonal::with('encargo')
                ->whereIn('id', $obligadosIds)
                ->whereNotIn('id', $declarantesIds)
                ->orderBy('nombres', 'asc')
                ->orderBy('primer_apellido', 'asc')
                ->orderBy('segundo_apellido', 'asc')
                ->get();
        }


        if ($porcentaje > 0) {
            $resp = ($total * 100) / $porcentaje;
        } else {
            $resp = 0;
        }

        $respuesta  = ['diferen' => $diferencia, 'declarantes' => $declarantes, 'avance' => $resp, 'entidad' => $entidad, 'porcentaje' => $porcentaje, 'total' => $total, 'faltantes' => $faltantes, 'obligadosid' => $obligadosIds, 'declarantesIds' => $declarantesIds];


        return response()->json($respuesta);
    }
    public function declarantesBajas(Request $request)
    {
        $anioact    =  $request->periodo;
        $anio2 = date('Y');


        $declarantes =         $declarantes = DB::table('declaraciones')
            ->select(DB::raw('informacion_personal.id, segundo_apellido, primer_apellido, nombres, rfc, empleo_cargo_comision, datos_encargo_actual.area_adscripcion,declaraciones.created_at as Fecha_Dec, bitacora_declaraciones.declaracion_fiscal, bitacora_declaraciones.intereses'))
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->leftjoin('bitacora_declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->whereNull('informacion_personal.deleted_at')
            ->where('datos_encargo_actual.tipo_dec', 3)
            ->whereNull('declaraciones.deleted_at')
            //->where('declaraciones.ente_publico_id',$request->dependencia_id)
            ->where('informacion_personal.ente_publico_id', $request->dependencia_id)
            ->whereYear('declaraciones.Fecha_Dec', $request->periodo)
            ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
            //->where('informacion_personal.created_at','<=',$anioact.'-12-31')
            ->get();
        //->count()

        $declarantesIds = array();
        foreach ($declarantes as $dec) {
            $declarantesIds[] = $dec->id;
        }



        $declarantesIds = array_unique($declarantesIds, SORT_NUMERIC);

        $total = count($declarantesIds);



        $entidad = CatEntePublico::select('valor')->where('id', $request->dependencia_id)->get();

        $porcentaje =  InformacionPersonal::select('id')
            /* ->whereHas("encargo", function($q) use ($request, $anio2){

							if($request->periodo != $anio2){

								if($request->tipo_declaracion ==3){
									$q->where('tipo_dec')
								 	->where('ente_publico_id',$request->dependencia_id)
								 	->whereYear('fecha_termino',$request->periodo)
									->whereNull('deleted_at');

								}else{
									$q->where('tipo_dec',$request->tipo_declaracion)
								 	->where('ente_publico_id',$request->dependencia_id)
								 	->whereNotNull('fecha_termino')
									->whereNull('deleted_at');
								}


							}else{
								if($request->tipo_declaracion ==3){
									$q->where('tipo_dec')
								 	->where('ente_publico_id',$request->dependencia_id)
								 	->whereYear('fecha_termino',$request->periodo)
									->whereNull('deleted_at');

								}else{
									$q->where('ente_publico_id',$request->dependencia_id)
									->whereYear('fecha_posesion','<',$request->periodo)
									->whereYear('created_at','<',$request->periodo)
									->whereNull('fecha_termino')
									->whereNull('deleted_at');
								}

							}

						 }*/
            ->whereHas("encargo", function ($q) use ($request, $anio2) {
                if ($request->periodo != $anio2) {
                    $q->where('tipo_dec', $request->tipo_declaracion)
                        ->when($request->dependencia_id != 0, function ($query) use ($request) {
                            return $query->where('ente_publico_id', $request->dependencia_id);
                        })
                        ->whereNotNull('fecha_termino')
                        ->whereNull('deleted_at');
                } else {
                    $q->when($request->dependencia_id != 0, function ($query) use ($request) {
                        return $query->where('ente_publico_id', $request->dependencia_id);
                    })
                        ->whereYear('fecha_posesion', '<', $request->periodo)
                        ->whereYear('created_at', '<', $request->periodo)
                        ->whereNotNull('fecha_termino')
                        ->whereNull('deleted_at');
                }
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', '=', 4) //4 es el id de licencia
                    ->where('termina', '>', date('Y-m-d'))
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                $query->select('id_ip')
                    ->from('declaracion_justificada')
                    ->where('tipo_dec', $request->tipo_declaracion)
                    ->where('periodo', $request->periodo)
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_baja', '!=', 'Normal') //por defuncion
                    ->whereNull('deleted_at');
            })
            ->whereNotIn('id', function ($query) use ($request) {
                if ($request->tipo_declaracion == 1) {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        //->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                } else {
                    $query->select('id_ip')
                        ->from('declaraciones')
                        ->where('tipo_declaracion_id', $request->tipo_declaracion)
                        ->whereYear('Fecha_Dec', $request->periodo)
                        ->whereNull('deleted_at');
                }
            })
            ->orWhereIn('id', $declarantesIds)
            ->whereNull('deleted_at')
            ->get();


        $obligadosIds = array();

        foreach ($porcentaje as $obligado) {
            $obligadosIds[] = $obligado->id;
        }

        $perc = array_unique($obligadosIds, SORT_NUMERIC);

        $porcentaje = count($perc);

        // $porcentaje=$porcentaje->count();


        $faltantes = InformacionPersonal::with('encargo')
            ->whereIn('id', $obligadosIds)
            ->whereNotIn('id', $declarantesIds)
            ->orderBy('nombres', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();


        if ($porcentaje > 0) {
            $resp = ($total * 100) / $porcentaje;
        } else {
            $resp = 0;
        }

        $respuesta  = ['declarantes' => $declarantes, 'avance' => $resp, 'entidad' => $entidad, 'porcentaje' => $porcentaje, 'total' => $total, 'faltantes' => $faltantes, 'obligadosid' => $obligadosIds, 'declarantesIds' => $declarantesIds];


        return response()->json($respuesta);
    }

    public function getServidoresConflictos()
    {

        $usuarios = InformacionPersonal::with('declaracion')
            ->orderBy('nombres', 'ASC')
            ->whereIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('membresias')
                    ->whereNull('deleted_at');
            })
            ->OrWhereIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('empresas_sociedades_asociaciones')
                    ->whereNull('deleted_at');
            })
            ->whereNull('deleted_at')
            ->where('created_at', '>=', '2020-01-01 00:00:00')
            ->orderBy('primer_apellido', 'ASC')
            ->orderBy('segundo_apellido', 'ASC')
            ->get();

        return response()->json(['usuarios' => $usuarios]);
    }

    public function getBajasDefuncion()
    {

        $usuarios = InformacionPersonal::with('encargo')
            ->orderBy('nombres', 'ASC')
            ->whereIn('id', function ($query) {
                $query->select('ip_id')
                    ->from('movimientos')
                    ->where('tipo_mov', 2) //2 es el id de baja
                    ->where('tipo_baja', '!=', 'Normal') //por defuncion
                    ->whereNull('deleted_at');
            })
            ->whereNull('deleted_at')
            ->orderBy('primer_apellido', 'ASC')
            ->orderBy('segundo_apellido', 'ASC')
            ->get();

        return response()->json(['usuarios' => $usuarios]);
    }
}
