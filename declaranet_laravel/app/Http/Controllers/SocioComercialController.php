<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Socio_comercial;

class SocioComercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socios = Socio_comercial::orderBy('id', 'DESC')->get();
        return response()->json(['socios' => $socios]);
    }
    public function item($id)
    {
        $socios = Socio_comercial::where('ip_id', $id)->get();
        return response()->json(['socios' => $socios]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $socio = Socio_comercial::updateOrCreate(
            ['id' => $request->id],
            [
                'nombre_actividad' => $request->nombre_actividad,
                'tipo_vinculo' => $request->tipo_vinculo,
                'antiguedad_vinculo' => $request->antiguedad_vinculo,
                'rfc_entidad' => $request->rfc_entidad,
                'nombre' => $request->nombre,
                'rfc' => $request->rfc,
                'curp' => $request->curp,
                //'lugar_nacimiento' => $request->lugar_nacimiento,
                'pais_id' => $request->pais_id,
                'entidad_federativa_id' => $request->entidad_federativa_id,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'porcentaje_participacion' => $request->porcentaje_participacion,
                'sector_industria_id' => $request->sector_industria_id,
                'observaciones' => $request->observaciones,
                'ip_id' => $request->ip_id,




            ]
        );

        return response()->json(['socio' => $socio]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $socio = Socio_comercial::findOrFail($id);
        $socio->delete();
    }
}
