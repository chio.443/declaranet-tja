<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Declaracion;

use App\Movimientos;
use App\CatEntePublico;
use App\CatTipoDeclaracion;
use App\BitacoraDeclaracion;
use App\InformacionPersonal;
use Illuminate\Http\Request;
//use DB;
use App\DeclaracionJustificada;

use App\UsuarioTipoDeclaracion;
use Illuminate\Support\Facades\DB;

class CronController extends Controller
{
    public function tipo_dec($dependencia)
    {
        // Ejemplo de relaciones en realaciones
        // Solo id, name y email se regresaran junto con la relacion de contacts y publishers
        // id siempre debe ser incluido
        // $books = App\Book::with(['author: id, name, email', 'author.contacts', 'author.publishers'])->get();
        /*
       $books = App\Book::with(['author: id, name, email', 'author.contacts' => function ($query) {
            $query->where('address', 'like', '%city%');
        }, 'author.publishers'])->get();
        */

        // 29 = STRC
        // $dependencia = 29;

        $periodo = date("Y");
        $periodo_anterior = $periodo - 1;
        $declaracion = new Declaracion;
        $informacion = new InformacionPersonal;
        $hoy = now()->format('Y-m-d');
        $dia_uno = "$periodo-01-01";
        $dia_uno_anterior = "$periodo_anterior-01-01";

        // $movimientos = new Movimientos;

        $usuarios = $informacion->with(['encargo'])
            ->with(['movimientos' => function ($q)  use ($periodo_anterior, $hoy) {
                $q->where('tipo_mov', 2)
                    ->where('tipo_baja', 'Normal')
                    ->whereBetween('created_at', ["$periodo_anterior-01-01", $hoy])
                    ->setEagerLoads([]);
            }])
            ->with(['transferencias' => function ($q) use ($dependencia, $periodo_anterior, $hoy) {
                $q->where('ente_publico_destino', $dependencia)
                    // ->whereBetween('created_at', ["$periodo_anterior-01-01", "$periodo_anterior-12-31"])
                    ->orderBy('id', 'desc')
                    ->setEagerLoads([]);
            }])
            ->with(['declaraciones' => function ($q) use ($periodo_anterior, $hoy) {
                // $q->whereBetween('Fecha_Dec', ["$periodo_anterior-01-01", $hoy])
                $q->orderBy('id', 'desc')->setEagerLoads([]);
            }])
            ->whereHas("encargo", function ($q) use ($dependencia) {
                $q->where('datos_encargo_actual.ente_publico_id', $dependencia);
                //->whereNull('fecha_termino');
            })
            ->orderBy('id', 'ASC')
            ->get();

        $obligados_ids = $usuarios->pluck('id')->toArray();

        //$movimientos = $movimientos->whereIn('ip_id', $obligados_ids);
        $encargos = $usuarios->pluck('encargo', 'id');
        $declaraciones = $usuarios->pluck('declaraciones', 'id');
        $declaraciones_arr = $usuarios->pluck('declaraciones', 'id')->toArray();
        $movimientos = $usuarios->pluck('movimientos', 'id');
        $movimientos_arr = $usuarios->pluck('movimientos', 'id')->toArray();
        $transferencias = $usuarios->pluck('transferencias', 'id');
        $transferencias_arr = $usuarios->pluck('transferencias', 'id')->toArray();

        $declaraciones_iniciales_periodo = collect();
        $declaraciones_anuales_periodo = collect();
        $declaraciones_anuales = collect();
        $declaraciones_anuales_posibles = collect();
        $iniciales_nuevo_ingreso = collect();
        $movimientos_list = collect();
        $usuarios_sin_movimiento = collect();
        $transferencias_list = collect();
        $usuarios_sin_transferencia = collect();
        $transferencia_posible_anual = collect();
        $transferencia_posible_inicial = collect();

        $usuarios_activos = collect();
        $usuarios_inactivos = collect();

        $usuarios_anuales = collect();

        $usuarios_nuevo_ingreso = collect();
        $usuarios_requiere_inicial = collect();
        $usuarios_requiere_anual = collect();
        $usuarios_realizaron_anual = collect();
        $usuarios_realizaron_inicial = collect();
        $usuarios_inicial_final = collect();
        $usuarios_finales_pendientes = collect();
        $usuarios_finales_realizadas = collect();

        $declaraciones_finales_realizadas = collect();
        $declaraciones_realizadas_anuales = collect();
        $declaraciones_realizadas_iniciales = collect();


        /*
       $declaraciones_list = array_filter($declaraciones_arr, function ($value) {
            return count($value) > 0;
        });
        */
        foreach ($encargos as $i => $e) {
            if ($e->fecha_termino) {
                $usuarios_inactivos->push($i);
            } else {
                $usuarios_activos->push($i);
            }
        }

        foreach ($transferencias as $i => $transferencia) {
            if (count($transferencia) > 0) {
                //foreach ($transferencia as $t) {$transferencia[0]
                if (isset($transferencia[0]->datos_actuales['fecha_termino'])) {
                    if ($transferencia[0]->datos_actuales['fecha_termino'] != null) {
                        $date = Carbon::parse($transferencia[0]->datos_actuales['fecha_termino']);
                        $now = Carbon::parse($transferencia[0]->datos_cambio['fecha_posesion']);
                        $diff = $date->diffInDays($now);
                        if ($diff > 60) {
                            // inicial
                            $transferencia_posible_inicial->push($i);
                        } else {
                            // anuales
                            $transferencia_posible_anual->push($i);
                        }
                    } else {
                        // anuales
                        $transferencia_posible_anual->push($i);
                    }
                } else {
                    // anuales
                    $transferencia_posible_anual->push($i);
                }
                //}
                $transferencias_list->push($transferencia);
            } else {

                $usuarios_sin_transferencia->push($i);
            }
        }

        // No se usan los datos de moviminetos
        foreach ($movimientos as $i => $mov) {
            if (count($mov) > 0) {
                $movimientos_list->push($i);
            } else {
                $usuarios_sin_movimiento->push($i);
            }
        }
        // REGLAS TIPO DECLARACIONES
        foreach ($declaraciones as $i => $dec) {
            if (in_array($i, $usuarios_inactivos->toArray())) {
                // Usuarios inactivos que no tengan declaraciones
                $finales = $dec->whereIn('id_ip', $usuarios_inactivos->toArray())
                    ->sortByDesc('id')->first(); // sortByDesc es orderBy para collects

                if (!$finales) {
                    // Requieren Inicial y Final
                    $usuarios_inicial_final->push($i);
                } else {
                    if ($finales->tipo_declaracion_id == 3) {
                        // Declaracion Final realizada
                        $usuarios_finales_realizadas->push($i);
                        $declaraciones_finales_realizadas->push($finales);
                    } else {
                        // Declaracion Final pendiente
                        $usuarios_finales_pendientes->push($i);
                    }
                }
            } else {
                // Usuarios activos que no tengan declaraciones
                $declaraciones_ = $dec->whereIn('id_ip', $usuarios_activos->toArray())
                    ->sortByDesc('id')->first();

                if (!$declaraciones_ && in_array($i, $usuarios_activos->toArray())) {
                    // Usuario sin declaracion
                    $usuarios_nuevo_ingreso->push($i);
                } else {
                    $date = Carbon::parse($declaraciones_->Fecha_Dec);
                    $anio = $date->year;

                    if ($declaraciones_->tipo_declaracion_id == 2) {
                        // Declaracion Anual -- Ultima es inicial o anual
                        $usuarios_anuales->push($i);
                        if ($anio == $periodo) {
                            // Verificar si es del periodo actual   -- Cumnplio declaracion
                            $usuarios_realizaron_anual->push($i);
                            $declaraciones_realizadas_anuales->push($declaraciones_);
                        } else {

                            $usuarios_requiere_anual->push($i);
                            // Verificar si es del periodo anterior -- Requiere declaracion
                        }
                    } else if ($declaraciones_->tipo_declaracion_id == 1) {
                        // Declaracion Anual -- Ultima es inicial o anual
                        if ($anio == $periodo) {

                            // Verificar si es del periodo actual   -- Cumnplio declaracion
                            $usuarios_realizaron_inicial->push($i);
                            $usuarios_nuevo_ingreso->push($i);
                            $declaraciones_realizadas_iniciales->push($declaraciones_);
                        } else {
                            $usuarios_anuales->push($i);
                            $usuarios_requiere_anual->push($i);
                            // Verificar si es del periodo anterior -- Requiere declaracion
                        }
                    } else {
                        // Ultima declaracion es final
                        // Requieren declaracion inicial
                        $usuarios_nuevo_ingreso->push($i);
                        $usuarios_requiere_inicial->push($i);
                    }
                }
            }
        }
        //  $iniciales_nuevos = array_diff($iniciales_nuevo_ingreso->toArray(), $usuarios_inactivos->toArray());
        $iniciales_posibles = array_diff($transferencia_posible_inicial->toArray(), $usuarios_inactivos->toArray());
        $anuales_posibles = array_diff($transferencia_posible_anual->toArray(), $usuarios_activos->toArray());
        // $iniciales_posibles = array_diff($iniciales_posibles, $iniciales_nuevos);


        $respuesta = [
            // 'iniciales_nuevos' => $iniciales_nuevos,
            'usuarios_activos' => $usuarios_activos->toArray(),
            'usuarios_inactivos' => $usuarios_inactivos->toArray(),

            'iniciales_posibles' => $iniciales_posibles,
            'anuales_posibles' => $anuales_posibles,

            'usuarios_anuales' => $usuarios_anuales,

            'encargos' => $encargos,

            'usuarios_nuevo_ingreso' => $usuarios_nuevo_ingreso->toArray(),
            'usuarios_realizaron_anual' => $usuarios_realizaron_anual->toArray(),
            'usuarios_realizaron_inicial' => $usuarios_realizaron_inicial->toArray(),
            'usuarios_requiere_anual' => $usuarios_requiere_anual->toArray(),
            'usuarios_requiere_inicial' => $usuarios_requiere_anual->toArray(),

            'usuarios_finales_inicial_final' => $usuarios_inicial_final->toArray(),
            'usuarios_finales_pendientes' => $usuarios_finales_pendientes->toArray(),
            'usuarios_finales_realizadas' => $usuarios_finales_realizadas->toArray(),

            'declaraciones_realizadas_iniciales' => $declaraciones_realizadas_iniciales->toArray(),
            'declaraciones_anuales_realizadas' => $declaraciones_realizadas_anuales->toArray(),
            'declaraciones_finales_realizadas' => $declaraciones_finales_realizadas->toArray(),
        ];
        //dd($iniciales_nuevos, $iniciales_posibles, $declaraciones_anuales, $declaraciones_anuales_posibles, $transferencia_posible_anual->toArray(), count($usuarios_inactivos->toArray()), count($obligados_ids));
        return response()->json($respuesta);
    }

    public function cronTipoDec($dep)
    {
        try {

            $format = 'Y-m-d';


            $periodo = date("Y");
            $respuesta =  $this->tipo_dec($dep)->getData();
            DB::beginTransaction();
            // $iniciales_nuevos = $respuesta->iniciales_nuevos;
            $iniciales_posibles = $respuesta->iniciales_posibles;
            $encargos = $respuesta->encargos;

            // $anuales = $respuesta->anuales;
            $anuales_posibles = $respuesta->anuales_posibles;
            $usuarios_anuales = $respuesta->usuarios_anuales;

            $usuarios_inactivos = $respuesta->usuarios_inactivos;
            $usuarios_activos = $respuesta->usuarios_activos;

            $usuarios_nuevo_ingreso = $respuesta->usuarios_nuevo_ingreso;
            $usuarios_realizaron_anual = $respuesta->usuarios_realizaron_anual;
            $usuarios_realizaron_inicial = $respuesta->usuarios_realizaron_inicial;
            $usuarios_requiere_anual = $respuesta->usuarios_requiere_anual;
            $usuarios_requiere_inicial = $respuesta->usuarios_requiere_inicial;

            $usuarios_finales_inicial_final = $respuesta->usuarios_finales_inicial_final;
            $usuarios_finales_pendientes = $respuesta->usuarios_finales_pendientes;
            $usuarios_finales_realizadas = $respuesta->usuarios_finales_realizadas;

            // Declaraciones ya realizadas
            $declaraciones_realizadas_iniciales = $respuesta->declaraciones_realizadas_iniciales;
            $declaraciones_anuales_realizadas = $respuesta->declaraciones_anuales_realizadas;
            $declaraciones_finales_realizadas = $respuesta->declaraciones_finales_realizadas;

            $contador = 0;

            // "INICIALES" -- OK
            // $iniciales = array_merge((array)$iniciales_nuevos, (array)$iniciales_posibles);
            foreach ($usuarios_nuevo_ingreso as $id) {
                $revision = in_array($id, (array)$iniciales_posibles);

                $key = array_search($id, array_column($declaraciones_realizadas_iniciales, 'id_ip'));
                $dec_id = null;
                $fecha_declaracion = null;

                if (is_numeric($key)) {
                    // echo 'id: ' . $id . '  dec: ' . $declaraciones_realizadas_iniciales[$key]->id . " ***";
                    $dec_id =  $declaraciones_realizadas_iniciales[$key]->id;
                    $fecha_declaracion =  $declaraciones_realizadas_iniciales[$key]->Fecha_Dec;
                }

                $encargo_ = $encargos->$id;
                $fecha_posesion = null;
                if (isset($encargo_)) {
                    $fecha_posesion = Carbon::parse($encargo_->fecha_posesion)->format($format);
                    // $fecha_posesion = $encargo_->fecha_posesion;
                }

                $check = UsuarioTipoDeclaracion::where('ip_id', $id)
                    ->where('tipo_dec_id', 1)
                    ->where('periodo', $periodo)
                    ->get();

                if ((count($check) == 0)) {
                    $dec_ini = new UsuarioTipoDeclaracion;

                    $dec_ini->ip_id = $id;
                    $dec_ini->tipo_dec_id = 1;
                    $dec_ini->periodo = $periodo;
                    $dec_ini->revision = $revision;
                    $dec_ini->declaracion_id = $dec_id;
                    $dec_ini->fecha_declaracion = $fecha_declaracion;
                    $dec_ini->fecha_posesion = $fecha_posesion;
                    $dec_ini->ente_publico_id = $dep;

                    $dec_ini->save();
                }
            }

            // "ANUALES " -- OK
            // $anuales_ = array_merge((array)$usuarios_realizaron_anual, (array)$usuarios_requiere_anual);
            foreach ($usuarios_anuales as $id) {
                $revision = in_array($id, (array)$anuales_posibles);
                $key = array_search($id, array_column($declaraciones_anuales_realizadas, 'id_ip'));

                $dec_id = null;
                $fecha_declaracion = null;
                if (is_numeric($key)) {
                    // echo 'id: ' . $id . '  dec: ' . $declaraciones_anuales_realizadas[$key]->id . " +++";
                    $dec_id =  $declaraciones_anuales_realizadas[$key]->id;
                    $fecha_declaracion =  $declaraciones_anuales_realizadas[$key]->Fecha_Dec;
                }

                $encargo_ = $encargos->$id;
                $fecha_posesion = null;
                if (isset($encargo_)) {
                    $fecha_posesion = Carbon::parse($encargo_->fecha_posesion)->format($format);
                    // $fecha_posesion = $encargo_->fecha_posesion;
                }

                $check = UsuarioTipoDeclaracion::where('ip_id', $id)
                    ->where('tipo_dec_id', 2)
                    ->where('periodo', $periodo)
                    ->get();

                if ((count($check) == 0)) {
                    $dec_a = new UsuarioTipoDeclaracion;

                    $dec_a->ip_id = $id;
                    $dec_a->tipo_dec_id = 2;
                    $dec_a->periodo = $periodo;
                    $dec_a->revision = $revision;
                    $dec_a->declaracion_id = $dec_id;
                    $dec_a->fecha_declaracion = $fecha_declaracion;
                    $dec_a->fecha_posesion = $fecha_posesion;
                    $dec_a->ente_publico_id = $dep;

                    $dec_a->save();
                }
            }

            // "FINALES" -- OK
            foreach ($usuarios_inactivos as $id) {
                $key = array_search($id, array_column($declaraciones_finales_realizadas, 'id_ip'));
                $dec_id = null;
                $fecha_declaracion  = null;
                if (is_numeric($key)) {
                    // echo 'id: ' . $id . '  dec: ' . $declaraciones_finales_realizadas[$key]->id . " --- \n";
                    $dec_id =  $declaraciones_finales_realizadas[$key]->id;
                    $fecha_declaracion =  $declaraciones_finales_realizadas[$key]->Fecha_Dec;
                }

                // FALTA VERIFICAR QUE EL USUARIO NO TENGA OTRA INICIAL Y FINAL PENDIENTE
                // LO IGNORAMOS SI "count($CHECK) > 0"

                $CHECK = UsuarioTipoDeclaracion::where('ip_id', $id)
                    ->where('tipo_dec_id', 1)
                    ->whereNull('declaracion_id')->get();

                $inicial_anual = in_array($id, (array)$usuarios_finales_inicial_final);

                // Agregar fecha termino
                // fecha_termino
                $encargo_ = $encargos->$id;
                $fecha_posesion = null;
                if (isset($encargo_)) {
                    $fecha_posesion = Carbon::parse($encargo_->fecha_posesion)->format($format);
                    // $fecha_posesion = $encargo_->fecha_posesion;
                }

                if ($inicial_anual && (count($CHECK) == 0)) {
                    // Requiere inicial para realizar anual

                    $dec_f_ini = new UsuarioTipoDeclaracion;

                    $dec_f_ini->ip_id = $id;
                    $dec_f_ini->tipo_dec_id = 1;
                    $dec_f_ini->periodo = $periodo;
                    $dec_f_ini->revision = true;
                    $dec_f_ini->declaracion_id = $dec_id;
                    $dec_f_ini->fecha_declaracion = $fecha_declaracion;
                    $dec_f_ini->fecha_posesion = $fecha_posesion;
                    $dec_f_ini->ente_publico_id = $dep;

                    $dec_f_ini->save();
                }

                // FALTA VERIFICAR QUE EL USUARIO NO TENGA OTRA INICIAL Y FINAL PENDIENTE
                // $CHECK = UsuarioTipoDeclaracion::where('tipo_dec_id',3)->whereNull('declaracion_id')->get();
                // LO IGNORAMOS SI "count($CHECK) > 0"
                $CHECK_2 = UsuarioTipoDeclaracion::where('ip_id', $id)
                    ->where('tipo_dec_id', 3)
                    ->whereNull('declaracion_id')->get();

                if (count($CHECK_2) == 0) {

                    $encargo_ = $encargos->$id;
                    $fecha_termino = null;
                    if (isset($encargo_)) {
                        $fecha_termino = $encargo_->fecha_termino;
                        $fecha_posesion = Carbon::parse($encargo_->fecha_posesion)->format($format);
                        // $fecha_posesion = $encargo_->fecha_posesion;
                        $periodo = Carbon::parse($fecha_termino)->year;
                    }

                    $dec_f_pen = new UsuarioTipoDeclaracion;

                    $dec_f_pen->ip_id = $id;
                    $dec_f_pen->tipo_dec_id = 3;
                    $dec_f_pen->periodo = $periodo;
                    $dec_f_pen->revision = true;
                    $dec_f_pen->declaracion_id = $dec_id;
                    $dec_f_pen->fecha_declaracion = $fecha_declaracion;
                    $dec_f_pen->ente_publico_id = $dep;
                    $dec_f_pen->fecha_posesion = $fecha_posesion;
                    $dec_f_pen->fecha_termino = $fecha_termino;

                    $dec_f_pen->save();
                }
            }

            $usuarios_ = array_merge((array)$usuarios_anuales, (array)$usuarios_nuevo_ingreso);
            $usuarios_dif = array_diff((array)$usuarios_activos, (array)$usuarios_);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function setTipoDec()
    {
        echo now();
        try {
            DB::beginTransaction();

            $entes = CatEntePublico::orderBy('id', 'ASC')->get();
            foreach ($entes as $ente) {
                $this->cronTipoDec($ente->id);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        echo ' --- ' . now();
    }
}
