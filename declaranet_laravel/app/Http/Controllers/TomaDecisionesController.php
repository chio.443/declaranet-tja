<?php

namespace App\Http\Controllers;

use App\TomaDecisiones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class TomaDecisionesController extends Controller
{
    public function index()
    {
        $response = TomaDecisiones::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function show($id)
    {
        $response = TomaDecisiones::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = TomaDecisiones::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'nombre' => $request->nombre,
                'rfc' => $request->rfc,
                'participante' => $request->participante,
                'tipon_id' => $request->tipon_id,
                'puesto' => $request->puesto,
                'fecha' => Carbon::parse($request->fecha),
                'remuneracion' => $request->remuneracion,
                'monto' => $request->monto,
                'pais_id' => $request->pais_id,
                'entidad_federativa_id' => $request->entidad_federativa_id,
                'especifique' => $request->especifique,
                'observaciones' => $request->observaciones,
            ]
        );
        return response()->json(['response' => $response]);
    }

    public function destroy($id)
    {
        $response = TomaDecisiones::findOrFail($id);
        $response->delete();
    }
}