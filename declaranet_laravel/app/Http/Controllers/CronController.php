<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Declaracion;

use App\Movimientos;
use App\CatEntePublico;
use App\CatTipoDeclaracion;
use App\BitacoraDeclaracion;
use App\InformacionPersonal;
use Illuminate\Http\Request;
//use DB;
use App\DeclaracionJustificada;

use App\UsuarioTipoDeclaracion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CronController extends Controller
{
    public function setTipoDec()
    {
        echo now();
        $start = now();
        $idList = [];
        $checkList = [];
        $sinMov = [];
        $total = 0;
        try {
            DB::beginTransaction();
            // where('id', 29)->
            $entes = CatEntePublico::orderBy('id', 'ASC')->get();

            foreach ($entes as $ente) {
                $format = 'Y-m-d';
                $periodo = date("Y");

                $informacion = new InformacionPersonal;

                $usuarios = $informacion->select(['id', 'rfc'])
                    ->with(['encargo' => function ($q) {
                        $q->select(['id', 'informacion_personal_id', 'fecha_posesion', 'fecha_termino'])
                            ->setEagerLoads([]);
                    }])
                    ->without(['vialidad', 'nac', 'declaracion'])
                    ->whereHas("encargo", function ($q) use ($ente) {
                        $q->where('datos_encargo_actual.ente_publico_id', $ente->id);
                    })
                    ->orderBy('id', 'ASC')
                    // ->setEagerLoads([])
                    ->get();

                $encargos = $usuarios->pluck('encargo', 'id');
                $rfcs = $usuarios->pluck('rfc', 'id');

                $usuarios_activos = collect();
                $usuarios_inactivos = collect();

                foreach ($encargos as $i => $e) {
                    if ($e->fecha_termino) {
                        $usuarios_inactivos->push($i);
                    } else {
                        $usuarios_activos->push($i);
                    }
                }
                $idList = [];
                $checkList = [];
                $sinMov = [];
                // "ANUALES "
                foreach ($usuarios_activos as $id) {
                    $rfc = '';
                    if (!empty($rfcs[$id])) {
                        $rfc = $rfcs[$id];
                    }

                    if (empty($encargos[$id])) {

                        $checkList[] = ['id' => $id, 'rfc' =>  $rfc];
                        continue;
                    }

                    $encargo_ = $encargos[$id];
                    $fecha_posesion = null;

                    if (isset($encargo_)) {
                        $fecha_posesion = Carbon::parse($encargo_->fecha_posesion)->format($format);
                    }
                    // Se verifica si en el perido actual cuenta con un tipo de declaracion, si no se le asigna una anual
                    $check = UsuarioTipoDeclaracion::where('ip_id', $id)
                        // ->where('tipo_dec_id', 2)
                        ->where('periodo', $periodo)
                        ->get();

                    if ((count($check) == 0)) {
                        $total++;
                        $dec_a = new UsuarioTipoDeclaracion;

                        $dec_a->ip_id = $id;
                        $dec_a->tipo_dec_id = 2;
                        $dec_a->periodo = $periodo;
                        $dec_a->fecha_posesion = $fecha_posesion;
                        $dec_a->ente_publico_id = $ente->id;

                        $dec_a->save();

                        $idList[] = ['id' => $id, 'utd_id' => $dec_a->id];
                    } else {
                        $sinMov[] = ['id' => $id, 'rfc' => $rfc];
                    }
                }
                $d = preg_replace('/[\W]/', '', $ente->codigo);
                $file_name = 'anuales_' . now()->format('Y') . '/Cron' . '_' . $d  . '_' . now()->format('Y_m_d') . '.json';
                $data = [
                    'message' => 'Lista de errores del ' . now(),
                    'errores' => $checkList,
                    'sinMovimientos' => $sinMov,
                    'registros' => $idList,
                ];
                Storage::put($file_name, json_encode($data));
            } // End ForEach

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        $end = now();
        $dif = $start->diffInSeconds($end);
        echo ' --- ' . gmdate('H:i:s', $dif);
        echo ' --- Total: ' . $total;

        dd($checkList);
    }
}
