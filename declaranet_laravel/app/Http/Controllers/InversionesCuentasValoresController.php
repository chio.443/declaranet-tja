<?php

namespace App\Http\Controllers;

use App\InversionesCuentasValores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;

class InversionesCuentasValoresController extends Controller
{
    public function index()
    {
        $inversiones = InversionesCuentasValores::orderBy('id', 'DESC')->get();
        return response()->json(['inversiones' => $inversiones]);
    }

    public function item($id)
    {
        $ip_id = Auth::user()->id;
        $response = InversionesCuentasValores::where('ip_id', $ip_id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $ip_id = Auth::user()->id;
        $response = InversionesCuentasValores::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $ip_id,
                'tipo_inversion_id' => $request->tipo_inversion_id,
                'titular_id' => $request->titular_id,
                'tercero' => $request->tercero,
                'nombre_tercero' => $request->nombre_tercero,
                'rfc_tercero' => $request->rfc_tercero,
                'bancaria_id' => $request->bancaria_id,
                'fondo_inversion_id' => $request->fondo_inversion_id,
                'organizacionespym_id' => $request->organizacionespym_id,
                'moneda_metales_id' => $request->moneda_metales_id,
                'seguro_id' => $request->seguro_id,
                'valores_id' => $request->valores_id,
                'afore_id' => $request->afore_id,
                'numero_cuenta' => $request->numero_cuenta,
                'nombre_institucion' => $request->nombre_institucion,
                'rfc_institucion' => $request->rfc_institucion,
                'domicilio_institucion_pais_id' => $request->domicilio_institucion_pais_id,
                'monto_original' => $request->monto_original,
                'tipo_moneda' => $request->tipo_moneda,
                'observaciones' => $request->observaciones,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $inversion = InversionesCuentasValores::findOrFail($id);
        $inversion->delete();
    }
}