<?php

namespace App\Http\Controllers;

use App\CatTipoValores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoValoresController extends Controller
{
    public function index()
    {
        $valores = CatTipoValores::orderBy('valor', 'ASC')->get();
        return response()->json(['valores' => $valores]);
    }

    public function store(Request $request)
    {
        $valor = CatTipoValores::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['valor' => $valor]);
    }

    public function delete($id)
    {
        $valor = CatTipoValores::findOrFail($id);
        $valor->delete();
    }
}