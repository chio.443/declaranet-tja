<?php

namespace App\Http\Controllers;

use DB;

use Auth;
use App\Token;
use App\Deudas;


use App\Fiscal;
use App\Premios;
use App\Empresas;
use App\Ingresos;
use App\Intereses;
use Carbon\Carbon;
use App\Membresias;
use App\Declaracion;
use App\Enajenacion;
use App\Fideicomiso;
use App\OtroEncargo;
use App\EncargoActual;
use App\Observaciones;
use App\OtrosIngresos;
use App\UsoBeneficios;
use App\TomaDecisiones;
use App\BienesInmuebles;
use App\EfectivoMetales;
use App\PrestamoComodato;
use App\DatosCurriculares;
use App\ExperenciaLaboral;
use App\BeneficiosPrivados;
use App\EncargoActualFuncP;
use App\BitacoraDeclaracion;
use App\InformacionPersonal;
use Illuminate\Http\Request;

use App\Clientes_principales;
use App\DependienteEconomico;

use App\ApoyoBeneficioPublico;
use App\Representacion_activa;
use App\SueldosSalariosPublicos;
use App\BienesMueblesRegistrables;
use App\InversionesCuentasValores;
use App\BienesMueblesNoRegistrables;

use App\Http\Controllers\UsuarioTipoDecController;


class DeclaracionController extends Controller
{
    public function index(Request $request)
    {
        try {
            DB::beginTransaction();
            /////////////////////////////////////////////////////////////////////////
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

            $token = Token::decodeToken($auth);
            $tipo_declaracion = $token->tipo_dec; // Texto
            // $tipo_declaracion2 = $token->tipo_dec;
            $tipo_dec_id = $token->tipo_dec_usuario;
            $periodo = $token->periodo;
            // $tipo_dec_list = array_diff($token->tipo_dec_list, [$token->tipo_dec_usuario]);
            $tipo_dec_list_ = [];
            foreach ($token->tipo_dec_list as $key => $value) {
                if ($value->tipo_dec_id != $token->tipo_dec_usuario || $value->periodo != $token->periodo) {
                    $tipo_dec_list_[] = $value;
                }
            }
            $anioact    =  date("Y");

            $tokenData = (object) [
                'id' => $token->uid,
                'status_dec' => (count($tipo_dec_list_) > 0) ? '1' : '0',
                'ente_publico_id' => $token->ente_publico_id,
                'fecha_termino' => $token->fecha_termino,
                'correo_electronico_laboral' => $token->email,
                'rol_id' => $token->role,
                'grupo' => $token->grupo,
                'simplificada' => $token->simplificada,
                'nombre' => $token->nombre,
                'ente' => $token->ente,
                'periodo'  => (count($tipo_dec_list_) > 0) ? $tipo_dec_list_[0]->periodo : $anioact,
                'tipo_dec' => $token->tipo_dec, // Tipo declaracion texto
                'tipo_dec1' => $token->tipo_dec1,
                "tipo_dec_usuario"  => $token->tipo_dec_usuario,
                'tipo_dec_list' => $tipo_dec_list_,
                'permisos' => $token->permisos,
                'movimiento' => $token->movimiento
            ];

            $token =  Token::getToken($tokenData);
            //////////////////////////////////////////////////////////////////////////
            $id = Auth::user()->id;
            ///Obtener el tipo de declaración , el "calculo" se hace en el login y se guarda en el token

            /* if ($tipo_declaracion == "Inicial")
            $tipo_declaracion = 1;
        if ($tipo_declaracion == "Anual")
            $tipo_declaracion = 2;
        if ($tipo_declaracion == "Final")
            $tipo_declaracion = 3; */

            $encargo = EncargoActual::where('informacion_personal_id', $id)->first();
            $ente_publico_id = $encargo['ente_publico_id'];
            $fecha_posesion = $encargo['fecha_posesion'];

            $registro  = new  Declaracion(
                [
                    'id_ip' => $id,
                    'Fecha_Dec' => date('Ymd'),
                    /* 'Balanza'=>'0',
                'Balanza_Conyuge'=>'0',    */
                    'Tipo_Dec' => $tipo_declaracion,
                    'tipo_declaracion_id' => $tipo_dec_id,
                    'created_by' => $id,
                    'ente_publico_id' => $ente_publico_id,
                    'periodo' => $periodo,
                ]
            );

            $registro->save();

            //$registro->refresh();
            /////////////////////////////////////////////////Registro en la bitacora //////////////////////

            $registro = Declaracion::where('id_ip', '=', $id)->orderBy('id', 'desc')->first();

            ////////////////////////////////////////////////////////////////////////////////////////////
            // Insertar registro de usuario tipo declaracion
            $usuario_tipo_dec = new UsuarioTipoDecController;

            $tipo_dec  = new Request;
            $tipo_dec->ip_id = $id;
            $tipo_dec->tipo_dec_id = $tipo_dec_id;
            $tipo_dec->periodo = $periodo ?: $anioact;
            $tipo_dec->ente_publico_id = $ente_publico_id;
            $tipo_dec->declaracion_id =  $registro['id'];
            $tipo_dec->fecha_declaracion =  $registro['Fecha_Dec'];
            $tipo_dec->fecha_posesion =  $fecha_posesion;

            $tipo_dec_resp = $usuario_tipo_dec->delcaracione($tipo_dec);
            ////////////////////////////////////////////////////////////////////////////////////////////
            $ip_id          = $id;
            $idDeclaracion  = $registro['id'];

            $bienesInmuebles    = BienesInmuebles::where('ip_id', $ip_id)->get()->toJson();
            $bienesMueblesNR    = BienesMueblesNoRegistrables::where('ip_id', $ip_id)->get()->toJson();
            $bienesMueblesR     = BienesMueblesRegistrables::where('ip_id', $ip_id)->get()->toJson();
            $datosCurriculares  = DatosCurriculares::where('informacion_personal_id', $ip_id)->get()->toJson();
            $encargoActual      = EncargoActual::where('informacion_personal_id', $ip_id)->get()->toJson();
            $otroEncargo        = OtroEncargo::where('informacion_personal_id', $ip_id)->get()->toJson();
            $fiscal             = Fiscal::where('ip_id', $ip_id)
                ->whereYear('created_at', $anioact)
                ->where('tipo_dec', $tipo_dec_id)
                ->get()
                ->toJson();
            $dependienteEcon    = DependienteEconomico::where('informacion_personal_id', $ip_id)->get()->toJson();
            $efectivoMetales    = EfectivoMetales::where('ip_id', $ip_id)->get()->toJson();
            $empresas           = Empresas::where('ip_id', $ip_id)->get()->toJson();
            $enajenacion        = Enajenacion::where('ip_id', $ip_id)->get()->toJson();
            $expLaboral         = ExperenciaLaboral::where('informacion_personal_id', $ip_id)->get()->toJson();
            $fideicomisos       = Fideicomiso::where('ip_id', $ip_id)->get()->toJson();
            $infPersonal        = InformacionPersonal::where('id', $ip_id)->get()->toJson();
            $ingresos           = Ingresos::where('ip_id', $ip_id)->get()->toJson();
            $intereses          = Intereses::where('ip_id', $ip_id)->get()->toJson();
            $invCtasValores     = InversionesCuentasValores::where('ip_id', $ip_id)->get()->toJson();
            $membresias         = Membresias::where('ip_id', $ip_id)->get()->toJson();
            $observaciones      = Observaciones::where('ip_id', $ip_id)->orderBy('id', 'desc')->first();
            $otrosIngresos      = OtrosIngresos::where('ip_id', $ip_id)->get()->toJson();
            $deudas             = Deudas::where('ip_id', $ip_id)->get()->toJson();
            $premios            = Premios::where('ip_id', $ip_id)->get()->toJson();
            $sspublicos         = SueldosSalariosPublicos::where('ip_id', $ip_id)->get()->toJson();
            $usoBeneficios      = UsoBeneficios::where('ip_id', $ip_id)->get()->toJson();
            $tomaDecisiones     = TomaDecisiones::where('ip_id', $ip_id)->get()->toJson();
            $apoyoPublico       = ApoyoBeneficioPublico::where('ip_id', $ip_id)->get()->toJson();
            $represAct          = Representacion_activa::where('ip_id', $ip_id)->get()->toJson();
            $clientesPrinc      = Clientes_principales::where('ip_id', $ip_id)->get()->toJson();
            $benefPrivados      = BeneficiosPrivados::where('ip_id', $ip_id)->get()->toJson();
            $presComodato       = PrestamoComodato::where('ip_id', $ip_id)->get()->toJson();






            $bDeclaracion =  new BitacoraDeclaracion([
                'ip_id'                             =>  $ip_id,
                'declaracion_id'                    =>  $idDeclaracion,
                'bienes_inmuebles'                  =>  $bienesInmuebles,
                'bienes_muebles_registrables'       =>  $bienesMueblesR,
                'bienes_muebles_no_registrables'    =>  $bienesMueblesNR,
                'datos_curriculares'                =>  $datosCurriculares,
                'datos_encargo_actual'              =>  $encargoActual,
                'declaracion_fiscal'                =>  $fiscal,
                'dependientes_economicos'           =>  $dependienteEcon,
                'efectivo_metales'                  =>  $efectivoMetales,
                'empresas_sociedades_asociaciones'  =>  $empresas,
                'enajenacion_bienes'                =>  $enajenacion,
                'experiencia_laboral'               =>  $expLaboral,
                'fideicomisos'                      =>  $fideicomisos,
                'informacion_personal'              =>  $infPersonal,
                'ingresos'                          =>  $ingresos,
                'intereses'                         =>  $intereses,
                'inversiones_cuentas_valores'       =>  $invCtasValores,
                'membresias'                        =>  $membresias,
                'observaciones'                     =>  $observaciones,
                'otros_ingresos'                    =>  $otrosIngresos,
                'pasivos_deudas'                    =>  $deudas,
                'premios'                           =>  $premios,
                'sueldos_salarios_publicos'         =>  $sspublicos,
                'uso_especie_propiedad_tercero'     =>  $usoBeneficios,
                'otro_encargo'                      =>  $otroEncargo,
                'toma_decisiones'                   =>  $tomaDecisiones,
                'apoyos_publicos'                   =>  $apoyoPublico,
                'representacion_activa'             =>  $represAct,
                'clientes_principales'              =>  $clientesPrinc,
                'beneficios_privados'               =>  $benefPrivados,
                'prestamo_comodato'                 =>  $presComodato
            ]);

            $bDeclaracion->save();


            // if($tipo_declaracion==1){//si es inicial, vertificar el año para cambiar el tipo de dec en encargo

            //     $registro = EncargoActual::where('informacion_personal_id', $ip_id)->first();
            //     $registrop = InformacionPersonal::where('id', $ip_id)->first();
            //     $alta     = Carbon::parse($registrop->created_at)->format('Y');
            //     $ingreso    =  Carbon::parse($registro->fecha_posesion)->format('Y');

            //     if($anioact>$ingreso && &&$registrop->simplificada!='1'){
            //         $registro->tipo_dec=2;
            //         $registro->save();
            //     }

            // }

            if ($tipo_dec_id == 1 || $tipo_dec_id == 2) {
                //si es inicial o anual, verificar si tiene pendiente una final para esa dependencia
                $registro = EncargoActual::where('informacion_personal_id', $ip_id)->first();
                $registrop = InformacionPersonal::where('id', $ip_id)->first();

                //si tiene fecha de termino y tiene la final pendiente
                if ($registro->fecha_termino && $registrop->tipo_dec == 3) {
                    $registro->tipo_dec = 3;
                    $registro->save();
                    $registrop->tipo_dec = 0;
                    $registrop->save();
                }
            }


            if ($tipo_dec_id == 3) { //si es final, vertificar si está dado de alta de nuevo tiene que presentar incial

                $registro = EncargoActual::where('informacion_personal_id', $ip_id)->first();
                $ingreso    =  Carbon::parse($registro->fecha_posesion);
                $termino    =  Carbon::parse($registro->fecha_termino);

                if ($ingreso->gt($termino)) {
                    $registro->tipo_dec = 1;
                    $registro->save();
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['data' => 'Correcto', 'token' => $token]);
    }

    public function insertDec($id)
    {        /////////////////////////////////////////////////////////////////////////


        $declaracion = Declaracion::where('id_ip', $id)->first();

        if (!($declaracion['id_ip'] == $id)) {
            $registro  = Declaracion::updateOrCreate(
                ['id_ip' => $id],
                [
                    'id_ip' => $id,
                    'Fecha_Dec' => date('Ymd'),
                    'Balanza' => '0',
                    'Balanza_Conyuge' => '0',
                    'Balanza_Depend' => '0',
                    'tipo_declaracion_id' => '2',
                    'created_by' => $id
                ]
            );

            if ($registro->save()) {
                return response()->json(['data' => 'Correcto']);
            } else {
                return response()->json(['data' => 'Error']);
            }
        }
    }

    public function getModificadas()
    {
        $declaraciones = Declaracion::with('usuario', 'declarante', 'tipoDec')
            ->whereNotNull('deleted_at')
            ->whereNotNull('deleted_by')
            ->withTrashed()->get();

        return response()->json(['declaraciones' => $declaraciones]);
    }
}
