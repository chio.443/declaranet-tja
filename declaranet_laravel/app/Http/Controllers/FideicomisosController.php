<?php

namespace App\Http\Controllers;

use App\Fideicomiso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FideicomisosController extends Controller
{
    public function index()
    {
        $responses = Fideicomiso::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $responses]);
    }

    public function item($id)
    {
        $responses = Fideicomiso::where('ip_id', $id)->get();
        return response()->json(['response' => $responses]);
    }

    public function store(Request $request)
    {

        $response = Fideicomiso::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'participante' => $request->participante,
                'tipo_fideicomiso_id' => $request->tipo_fideicomiso_id,
                'tipo_participacion_id' => $request->tipo_participacion_id,
                'rfc_fideicomiso' => $request->rfc_fideicomiso,
                'fideicomitente' => $request->fideicomitente,
                'nombre_fideicomitente' => $request->nombre_fideicomitente,
                'rfc_fideicomitente' => $request->rfc_fideicomitente,
                'nombre_fiduciario' => $request->nombre_fiduciario,
                'rfc_fiduciario' => $request->rfc_fiduciario,
                'fideicomisario' => $request->fideicomisario,
                'nombre_fideicomisario' => $request->nombre_fideicomisario,
                'rfc_fideicomisario' => $request->rfc_fideicomisario,
                'sector_industria_id' => $request->sector_industria_id,
                'pais_id' => $request->pais_id,
                'observaciones' => $request->observaciones,

            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $response = Fideicomiso::findOrFail($id);
        $response->delete();
    }
}