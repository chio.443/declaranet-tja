<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\OtroEncargo;
use \App\Token;
use Carbon\Carbon;
use Auth;


class OtroEncargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show(Request $request, $id)
    {

        $otroenc = OtroEncargo::where('informacion_personal_id',$id)->orderBy('id','asc')->get();

        return response()->json(['otroenc' =>$otroenc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        // $token = Token::decodeToken($auth);

        // $id = $token->uid;

        if(Auth::id() != $id){
            return response()->json(['errorSession' => 'error']);
        }
     

        $informacion = $request->all();

        $informacion['fecha_posesion'] =Carbon::parse($request->fecha_posesion);

        $registro = OtroEncargo::updateOrCreate(['informacion_personal_id'=> $id],
                                                $informacion
                                                );
        
        $registro->save();

        return response()->json(['registro' =>$registro->refresh()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        // $token = Token::decodeToken($auth);

        // $id = $token->uid;


        $otroenc = OtroEncargo::where('informacion_personal_id',$id);
        $otroenc->delete();
    }
}
