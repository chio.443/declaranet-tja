<?php

namespace App\Http\Controllers;

use App\CatTipoOperacionInversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoOperacionInversionController extends Controller
{
    public function index()
    {
        $tipos = CatTipoOperacionInversion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoOperacionInversion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoOperacionInversion::findOrFail($id);
        $tipo->delete();
    }
}