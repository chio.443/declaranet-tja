<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformacionPersonal;
use App\EncargoActual;
use App\ExperenciaLaboral;
use Carbon\Carbon;
use Storage;
use Auth;
use \App\Token;
use App\Fiscal;
use DB;


class FiscalController extends Controller
{


    public function index()
    {


        $dec = Fiscal::orderBy('id', 'DESC')->get();
        return response()->json(['dec' => $dec]);
    }

    public function item(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);

        $tipo_dec;

        if($token->tipo_dec=="Inicial")
            $tipo_dec = 1;
        if($token->tipo_dec=="Anual")
            $tipo_dec = 2;
        if($token->tipo_dec=="Final")
            $tipo_dec = 3;

        $anioact    =  date("Y");

        $files = Fiscal::where('ip_id', $id)
                ->whereYear('created_at',$anioact)
                ->where('tipo_dec',$tipo_dec)
                ->orderBy('created_at', 'desc')
                ->limit(1)
                ->get();

        return response()->json(['files' => $files]);
    }

    public function hasfiscal(Request $request, $id)
    {
        $anioact    =  date("Y");

        $files = Fiscal::where('ip_id', $id)
            ->whereYear('created_at', $anioact)
            ->select('id')
            ->first();

        return response()->json(['files' => $files]);
    }

    public function ImportPdf(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        $token = Token::decodeToken($auth);

        $tipo_dec;

        if($token->tipo_dec=="Inicial")
            $tipo_dec = 1;
        if($token->tipo_dec=="Anual")
            $tipo_dec = 2;
        if($token->tipo_dec=="Final")
            $tipo_dec = 3;

        $id2 = Auth::user()->id;
        $year = date("Y");

        $uniqueFileName = uniqid() . "_" . $id2 ."_" . $year . '.' . $request->file('upload_file')->getClientOriginalExtension();

        $request->file('upload_file')->move(public_path('declaraciones_fiscales'), $uniqueFileName);

        $response = Fiscal::updateOrCreate(
            ['id' => $request->id],
            [
                'file' => $uniqueFileName,
                'ip_id' => $id2,
                'tipo_dec' => $tipo_dec
            ]
        );

        return response()->json(['response' => $response]);
    }
    public function delete($id)
    {
        $tipo = Fiscal::findOrFail($id);
        $tipo->delete();
    }


    public function obligado($id){

        $anio   =  date("Y");

        $anio-=1;


        $cargo = DB::table('ingresos')
                    ->where('ip_id', $id)
                    ->sum(\DB::raw('ant_sub_12::numeric'));

        $otros = DB::table('ingresos')
                    ->where('ip_id', $id)
                    ->sum(\DB::raw('ant_actividad_comercial_cantidad::numeric +
                                    ant_actividad_financiera_cantidad::numeric + ant_servicios_profecionales_cantidad::numeric + ant_arrendamientos_cantidad::numeric + ant_otros_ingresos_cantidad::numeric'));

        $exp = ExperenciaLaboral::where('informacion_personal_id',$id)
               ->where(function($q) use($anio) {
                    $q->whereYear('fecha_ingreso', '=', $anio)
                      ->orWhere(function($nest) use($anio) {
                            $nest->whereYear('fecha_salida', '=', $anio);
                        });
                })
              ->get()->count();

        if(($cargo+$otros)>400000 ||$exp>=2 ||$otros>0){
             return response()->json(['obligado' => 'SI','in' => $cargo,'ot' => $otros,'exp' => $exp]);
        }else{
             return response()->json(['obligado' => 'NO']);
         }

    }
}