<?php

namespace App\Http\Controllers;

use App\CatTitular;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTitularController extends Controller
{
    public function index()
    {
        $titulares = CatTitular::orderBy('id', 'ASC')->get();
        return response()->json(['titulares' => $titulares]);
    }

    public function store(Request $request)
    {
        $titular = CatTitular::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['titular' => $titular]);
    }

    public function delete($id)
    {
        $titular = CatTitular::findOrFail($id);
        $titular->delete();
    }
}