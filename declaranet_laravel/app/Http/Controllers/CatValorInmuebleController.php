<?php

namespace App\Http\Controllers;

use App\CatValorInmueble;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatValorInmuebleController extends Controller
{
    public function index()
    {
        $response = CatValorInmueble::orderBy('valor', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = CatValorInmueble::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $tipo = CatValorInmueble::findOrFail($id);
        $tipo->delete();
    }
}