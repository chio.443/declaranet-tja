<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\PlantillaIngresos;
use Illuminate\Http\Request;
use App\SueldosSalariosOtrosEmpleos;

use Illuminate\Support\Facades\Validator;

class SueldosSalariosOtrosEmpleosController extends Controller
{
    public function index()
    {
        $sueldos = SueldosSalariosOtrosEmpleos::orderBy('id', 'DESC')->get();
        return response()->json(['sueldos' => $sueldos]);
    }

    public function item($id)
    {
        $id2 = Auth::user()->id;
        $rfc = Auth::user()->rfc;
        $sueldos = SueldosSalariosOtrosEmpleos::where('ip_id', $id2)->first();
        $plantilla = PlantillaIngresos::select('ingreso_anual_neto')->where('rfc', $rfc)
            ->orderBy('id', 'DESC')
            ->first();
        $respuesta = ['sueldos' => $sueldos, 'plantilla' => $plantilla];
        return response()->json(['respuesta' => $respuesta]);
    }

    public function rfcingresos(request $request)
    {
        $itemsPerPage = ($request->itemsPerPage) ? $request->itemsPerPage : 10;
        $sortDesc =  'ASC';

        $info = DB::table('informacion_personal');

        // if (!empty($request->search)) {
        //     $info = $info->where('informacion_personal.rfc', 'ilike', '%' . $request->search . '%')
        //         ->orWhere('cat_ente_publico.valor', 'ilike', '%' . $request->search . '%');
        // }

        if (!empty($request->sortDesc)) {
            $sortDesc = ($request->sortDesc == 'true') ? 'DESC' : 'ASC';
        }

        $lista = $info
            ->select('informacion_personal.rfc as rfc', 'cat_ente_publico.valor as dependencia')
            ->leftJoin('plantilla_ingresos', 'plantilla_ingresos.rfc', '=', 'informacion_personal.rfc')
            ->whereNull('plantilla_ingresos.rfc')
            //->join('plantilla_ingresos', 'informacion_personal.rfc', '=', 'plantilla_ingresos.rfc')
            // ->whereNotIn('informacion_personal.rfc', function ($q) {
            //     $q->select('plantilla_ingresos.rfc')->from('plantilla_ingresos')->groupBy('plantilla_ingresos.rfc');
            // })
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('cat_ente_publico', 'cat_ente_publico.id', '=', 'datos_encargo_actual.ente_publico_id')
            ->when(!empty($request->search), function ($q) use ($request) {
                $q->where('informacion_personal.rfc', 'ilike', '%' . $request->search . '%')
                    ->orWhere('cat_ente_publico.valor', 'ilike', '%' . $request->search . '%');
            })
            ->whereNull('datos_encargo_actual.fecha_termino')
            ->orderBy('cat_ente_publico.valor', $sortDesc)
            ->orderBy('informacion_personal.rfc', $sortDesc)
            ->groupBy('informacion_personal.rfc')
            ->groupBy('cat_ente_publico.valor')
            ->paginate($itemsPerPage);
        // ->get();

        return response()->json(['lista' => $lista], 200);
    }

    public function store(Request $request)
    {
        $id2 = Auth::user()->id;
        $sueldo = SueldosSalariosOtrosEmpleos::updateOrCreate(
            ['ip_id' => $id2],
            [
                'ip_id' => $id2,
                'ant_sub_12' => $request->ant_sub_12,
                'ant_actividad_comercial' => $request->ant_actividad_comercial,
                'ant_actividad_comercial_cantidad' => $request->ant_actividad_comercial_cantidad,
                'ant_actividad_financiera' => $request->ant_actividad_financiera,
                'ant_actividad_financiera_cantidad' => $request->ant_actividad_financiera_cantidad,
                'ant_arrendamientos' => $request->ant_arrendamientos,
                'tipo_bien_enajenacion' => $request->tipo_bien_enajenacion,
                'tiponegocio' => $request->tiponegocio,
                'ant_arrendamientos_cantidad' => $request->ant_arrendamientos_cantidad,
                'ant_neto_declarante' => $request->ant_neto_declarante,
                'ant_neto_conyugue' => $request->ant_neto_conyugue,
                'ant_neto_conyugue_cantidad' => $request->ant_neto_conyugue_cantidad,
                'ant_servicios_profecionales' => $request->ant_servicios_profecionales,
                'ant_servicios_profecionales_cantidad' => $request->ant_servicios_profecionales_cantidad,
                'fecha_fin_periodo' => $request->fecha_fin_periodo,
                'fecha_inicio_periodo' => $request->fecha_inicio_periodo,
                'otros_ingresos' => $request->otros_ingresos,
                'ant_otros_cantidad' => $request->ant_otros_cantidad,
                'bandera' => $request->bandera,
            ]
        );

        return response()->json(['sueldo' => $sueldo]);
    }

    public function delete($id)
    {
        $sueldo = SueldosSalariosOtrosEmpleos::findOrFail($id);
        $sueldo->delete();
    }
}
