<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Token;
use App\Fiscal;
use Carbon\Carbon;
use App\Aclaraciones;
use App\EncargoActual;
use App\BitacoraDeclaracion;

use App\InformacionPersonal;

use Illuminate\Http\Request;
use App\DeclaracionJustificada;

class HistoricoDeclaracionesController extends Controller
{
    public function dec(Request $request)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $iddep = $token->ente_publico_id;
        $rol = $token->role;
        //dd($rol);
        if ($rol == 5 || $rol == 14)
            $dependencia = $iddep;
        else
            $dependencia = $request->dependencia_id ? $request->dependencia_id : 0;


        $declarantes = InformacionPersonal::with('encargo')
            ->whereHas(
                "encargo",
                function ($q) use ($dependencia) {
                    $q->where("ente_publico_id", $dependencia);
                }

            )
            ->without(['vialidad', 'nac', 'declaracion'])
            ->orderBy('rfc', 'asc')
            ->get();

        $declarantes2 = $declarantes->map(function ($item, $key) {
            $nombre = $item->nombres . ' ' . $item->primer_apellido . ' ' . $item->segundo_apellido;

            $object = new \stdClass;
            $object->id = $item->id;
            $object->rfc = $item->rfc;
            $object->nombre = $nombre;
            $object->entidad = $item->encargo->ente->valor;
            $object->area = $item->encargo->area_adscripcion;
            $object->cargo = $item->encargo->empleo_cargo_comision;
            $object->activo = $item->tipo_mov; // Revisar
            $object->alta = $item->created_at; // Revisar
            $object->baja = $item->encargo->fecha_termino;

            return $object;
        });

        return response()->json(['declarantes' => $declarantes2]);
    }

    public function dec2($ente, $desde, $hasta)
    {

        $declarantes = InformacionPersonal::with('ente', 'encargo')
            ->whereHas("encargo", function ($q) use ($ente) {
                $q->where("ente_publico_id", $ente);
                //->where("fecha_termino",null);
            })
            ->without(['vialidad', 'nac', 'declaracion'])
            // ->whereNotIn('id',function($query) {
            //                    $query->select('id')
            //                    ->from('informacion_personal')
            //                    ->where('tipo_mov',2);
            // })
            ->where('rfc', '>=', "'" . $desde . "'")
            ->where('rfc', '<=', "'" . $hasta . "'")
            ->orderBy('rfc', 'asc')
            ->orderBy('primer_apellido', 'asc')
            ->orderBy('segundo_apellido', 'asc')
            ->get();

        $declarantes2 = $declarantes->map(function ($item, $key) {
            $nombre = $item->nombres . ' ' . $item->primer_apellido . ' ' . $item->segundo_apellido;

            $object = new \stdClass;
            $object->id = $item->id;
            $object->rfc = $item->rfc;
            $object->nombre = $nombre;
            $object->entidad = $item->encargo->ente->valor;
            $object->area = $item->encargo->area_adscripcion;
            $object->cargo = $item->encargo->empleo_cargo_comision;
            $object->activo = $item->tipo_mov; // Revisar
            $object->alta = $item->created_at; // Revisar
            $object->baja = $item->encargo->fecha_termino;

            return $object;
        });


        return response()->json(['declarantes' => $declarantes2]);
    }

    public function historico($id)
    {
        $response = BitacoraDeclaracion::select('bitacora_declaraciones.declaracion_id', 'bitacora_declaraciones.ip_id', 'bitacora_declaraciones.declaracion_fiscal', 'bitacora_declaraciones.datos_encargo_actual', 'bitacora_declaraciones.created_at', 'declaraciones.periodo', 'declaraciones.Tipo_Dec', 'declaraciones.Fecha_Dec')
            ->where('bitacora_declaraciones.ip_id', $id)
            ->join('declaraciones', 'declaraciones.id', '=', 'bitacora_declaraciones.declaracion_id')
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('bitacora_declaraciones.deleted_at')
            ->orderBy('created_at', 'desc')->get();

        // $declarantesIds = array();
        foreach ($response as $clave => $value) {

            $year = Carbon::createFromFormat('Y-m-d', $value->Fecha_Dec)->year . '';
            if ($value->declaracion_fiscal == '[]') {
                $fiscal  = Fiscal::where('ip_id', $value->ip_id)
                    ->whereYear('created_at', $year)
                    ->whereNull('deleted_at')
                    ->orderBy('created_at', 'desc')
                    ->limit(1)->get()->toJson();

                $response[$clave]->declaracion_fiscal = $fiscal;
            }
            $aclaraciones  = Aclaraciones::where('ip_id', $value->ip_id)
                ->whereYear('created_at', $year)
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->limit(1)->get()->toJson();

            $response[$clave]->aclaraciones = $aclaraciones;
        }

        $justificados = DeclaracionJustificada::select('cat_tipo_declaracion.tipo_declaracion', 'declaracion_justificada.periodo', 'cat_ente_publico.valor')
            ->where('declaracion_justificada.id_ip', $id)
            ->join('cat_tipo_declaracion', 'cat_tipo_declaracion.id', '=', 'declaracion_justificada.tipo_declaracion_id')
            ->join('cat_ente_publico', 'cat_ente_publico.id', '=', 'declaracion_justificada.ente_publico_id')
            ->whereNull('declaracion_justificada.deleted_at')
            ->orderBy('declaracion_justificada.created_at', 'desc')->get();

        return response()->json(['response' => $response, 'justificados' => $justificados]);
    }
}
