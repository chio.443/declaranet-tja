<?php

namespace App\Http\Controllers;

use App\CatRelacionTransmisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatRelacionTransmisorController extends Controller
{
    public function index()
    {
        $response = CatRelacionTransmisor::orderBy('id', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = CatRelacionTransmisor::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $relacion = CatRelacionTransmisor::findOrFail($id);
        $relacion->delete();
    }
}