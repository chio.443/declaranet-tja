<?php

namespace App\Http\Controllers;

use App\BienesMueblesNoRegistrables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;

class BienesMueblesNoRegistrablesController extends Controller
{

    public function index()
    {
        $response = BienesMueblesNoRegistrables::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $ip_id = Auth::user()->id;
        $response = BienesMueblesNoRegistrables::where('ip_id', $ip_id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $ip_id = Auth::user()->id;
        $response = BienesMueblesNoRegistrables::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $ip_id,
                'tipo_bien_id' => $request->tipo_bien_id,
                'especifique_bien' => $request->especifique_bien,
                'descripcion' => $request->descripcion, //tipo_bien_especificar
                'titular_id' => $request->titular_id,
                'tercero' => $request->tercero,
                'nombre_tercero' => $request->nombre_tercero, //nombres_copropietarios
                'rfc_tercero' => $request->rfc_tercero,
                'transmisor' => $request->transmisor,
                'nombre_transmisor' => $request->nombre_transmisor, // nombre_denominacion_adquirio
                'rfc_transmisor' => $request->rfc_transmisor,
                'relacion_persona_adquirio_id' => $request->relacion_persona_adquirio_id,
                'relacion_especificar' => $request->relacion_especificar,
                'forma_adquisicion_id' => $request->forma_adquisicion_id,
                'forma_pago' => $request->forma_pago,
                'precio_adquisicion_valor' => $request->precio_adquisicion_valor,
                'precio_adquisicion_moneda_id' => $request->precio_adquisicion_moneda_id,
                'fecha_adquisicion' => Carbon::parse($request->fecha_adquisicion),
                'motivo_baja' => $request->motivo_baja,
                'especifique_baja' => $request->especifique_baja,
                'observaciones' => $request->observaciones,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $bien = BienesMueblesNoRegistrables::findOrFail($id);
        $bien->delete();
    }
}