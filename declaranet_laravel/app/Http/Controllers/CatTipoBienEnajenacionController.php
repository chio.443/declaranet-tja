<?php

namespace App\Http\Controllers;

use App\CatTipoBienEnajenacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class CatTipoBienEnajenacionController extends Controller
{
    public function index()
    {
        $bienes = CatTipoBienEnajenacion::orderBy('id', 'DESC')->get();
        return response()->json(['bienes' => $bienes]);
    }
}
