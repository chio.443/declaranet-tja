<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Otra_parte;

class OtrasPartesController extends Controller
{
    public function index()
    {
        $otras = Otra_parte::orderBy('id', 'DESC')->get();
        return response()->json(['otras' => $otras]);
    }

    public function item($id)
    {
        $otras = Otra_parte::where('ip_id', $id)->get();
        return response()->json(['otras' => $otras]);
    }

    public function store(Request $request)
    {
        $otra = Otra_parte::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'tipo_relacion' => $request->tipo_relacion,
                'tipo_relacion_especificar' => $request->tipo_relacion_especificar,
                'nombre_denominacion_parte' => $request->nombre_denominacion_parte,
                'fecha_inicio_relacion' => $request->fecha_inicio_relacion,
                'nacionalidad_id' => $request->nacionalidad_id,
                'curp' => $request->curp,
                'rfc' => $request->rfc,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'ocupacion' => $request->ocupacion,
                'tiene_interes' => $request->tiene_interes,
                'sector_industria_id' => $request->sector_industria_id,
                'especificar_sector_industria' => $request->especificar_sector_industria,
                'observaciones' => $request->observaciones
            ]
        );

        return response()->json(['otra' => $otra]);
    }

    public function delete($id)
    {
        $otra = Otra_parte::findOrFail($id);
        $otra->delete();
    }
}
