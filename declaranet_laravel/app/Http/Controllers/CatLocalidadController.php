<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CatLocalidad;
use App\CatMunicipio;

class CatLocalidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $localidad = CatLocalidad::orderBy('id','asc')->get();

        return response()->json(['localidad' =>$localidad]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //obtener con el id del municipio, los datos de la entidad, para el filtrado más rápido

        $localidad = CatLocalidad::where('municipio_id',$id)->orderBy('id','asc')->get();

        return response()->json(['localidades' =>$localidad]);

    }
    
    public function localidad($id)
    {
        //obtener con el id del municipio, los datos de la entidad, para el filtrado más rápido

        $localidad = CatLocalidad::where('id',$id)->first();

        return response()->json(['localidad' =>$localidad]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}