<?php

namespace App\Http\Controllers;

use App\Empresas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class EmpresasController extends Controller
{
    public function index()
    {
        $empresas = Empresas::orderBy('id', 'DESC')->get();
        return response()->json(['empresas' => $empresas]);
    }

    public function item($id)
    {
        $id2 = Auth::user()->id;

        $empresas = Empresas::where('ip_id', $id2)->get();
        return response()->json(['empresas' => $empresas]);
    }

    public function store(Request $request)
    {
        $id2 = Auth::user()->id;
        $empresa = Empresas::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $id2,
                'nombre_empresa_sociedad_asociacion' => $request->nombre_empresa_sociedad_asociacion,
                'tipo_operacion_id' => $request->tipo_operacion_id,
                'frecuencia_anual_id' => $request->frecuencia_anual_id,
                'frecuencia_anual_especificar' => $request->frecuencia_anual_especificar,
                'persona_juridica' => $request->persona_juridica,
                'otra_persona_juridica' => $request->otra_persona_juridica,
                'responsable_conflicto' => $request->responsable_conflicto,
                'naturaleza_vinculo' => $request->naturaleza_vinculo,
                'naturaleza_vinculo_especificar' => $request->naturaleza_vinculo_especificar,
                'antiguedad_vinculo' => $request->antiguedad_vinculo,
                'participacion' => $request->participacion,
                'tipo_colaboracion' => $request->tipo_colaboracion,
                'tipo_colaboracion_especificar' => $request->tipo_colaboracion_especificar,
                'domicilio_pais_id' => $request->domicilio_pais_id,
                'domicilio_entidad_federativa_id' => $request->domicilio_entidad_federativa_id,
                'domicilio_municipio_id' => $request->domicilio_municipio_id,
                'domicilio_localidad_id' => $request->domicilio_localidad_id,
                'domicilio_colonia' => $request->domicilio_colonia,
                'domicilio_vialidad_tipo' => $request->domicilio_vialidad_tipo,
                'domicilio_vialidad_nombre' => $request->domicilio_vialidad_nombre,
                'domicilio_cp' => $request->domicilio_cp,
                'domicilio_numext' => $request->domicilio_numext,
                'domicilio_numint' => $request->domicilio_numint,
                'es_publica' => $request->es_publica
            ]
        );

        return response()->json(['empresa' => $empresa]);
    }

    public function delete($id)
    {
        $empresa = Empresas::findOrFail($id);
        $empresa->delete();
    }
}