<?php

namespace App\Http\Controllers;

use App\CatTipoInstrumento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
class CatTipoInstrumentoController extends Controller
{
    public function index()
    {
        $instrumentos = CatTipoInstrumento::orderBy('id', 'DESC')->get();
        return response()->json(['instrumentos' => $instrumentos]);
    }
}
