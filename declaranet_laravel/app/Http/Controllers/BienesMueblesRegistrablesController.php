<?php

namespace App\Http\Controllers;

use App\BienesMueblesRegistrables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use Carbon\Carbon;


class BienesMueblesRegistrablesController extends Controller
{
    public function index()
    {
        $response = BienesMueblesRegistrables::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $ip_id = Auth::user()->id;
        $response = BienesMueblesRegistrables::where('ip_id', $ip_id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $ip_id = Auth::user()->id;
        $response = BienesMueblesRegistrables::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $ip_id,
                'tipo_bien_id' => $request->tipo_bien_id,
                'tipo_bien_especificar' => $request->tipo_bien_especificar,
                'titular_id' => $request->titular_id,
                'tercero' => $request->tercero,
                'nombre_tercero' => $request->nombre_tercero,
                'rfc_tercero' => $request->rfc_tercero,
                'transmisor' => $request->transmisor,
                'nombre_denominacion_adquirio' => $request->nombre_denominacion_adquirio,
                'rfc_quien_adquirio' => $request->rfc_quien_adquirio,
                'relacion_persona_adquirio_id' => $request->relacion_persona_adquirio_id,
                'relacion_especificar' => $request->relacion_especificar,
                'marca' => $request->marca,
                'modelo' => $request->modelo,
                'anio' => $request->anio,
                'numero_serie' => $request->numero_serie,
                'forma_adquisicion' => $request->forma_adquisicion,
                'forma_pago' => $request->forma_pago,
                'fecha_adquisicion' => Carbon::parse($request->fecha_adquisicion),
                'precio_adquisicion_valor' => $request->precio_adquisicion_valor,
                'precio_adquisicion_moneda_id' => $request->precio_adquisicion_moneda_id,
                'observaciones' => $request->observaciones,
                'motivo_baja' => $request->motivo_baja,
                'especifique_baja' => $request->especifique_baja,
                'lugar_registro_pais_id' => $request->lugar_registro_pais_id,
                'lugar_registro_entidad_id' => $request->lugar_registro_entidad_id,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $bien = BienesMueblesRegistrables::findOrFail($id);
        $bien->delete();
    }
}