<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BitacoraConsulta;
use DB;

use App\BitacoraConsultaBalanza;

class BitacoraConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBitacora($finicio, $ffin)
    {

        $bitacora = BitacoraConsulta::with('usuario', 'declarante')
            ->where(DB::raw('ip_id'), '!=', DB::raw('created_by'))
            ->where(DB::raw('date(created_at)'), '>=', DB::raw("date('" . $finicio . "')"))
            ->get();

        return response()->json(['bitacora' => $bitacora]);
    }

    public function getBitacorab($finicio, $ffin)
    {

        $bitacora = BitacoraConsultaBalanza::with('usuario', 'declarante','declaracion')
            ->where(DB::raw('ip_id'), '!=', DB::raw('created_by'))
            ->where(DB::raw('date(created_at)'), '>=', DB::raw("date('" . $finicio . "')"))
            ->orderBy('id', 'desc')            
            ->get();
        //dd($bitacora);
        return response()->json(['bitacora' => $bitacora]);
    }
}
