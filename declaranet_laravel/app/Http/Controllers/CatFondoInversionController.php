<?php

namespace App\Http\Controllers;

use App\CatFondoInversion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatFondoInversionController extends Controller
{
    public function index()
    {
        $fondos = CatFondoInversion::orderBy('valor', 'ASC')->get();
        return response()->json(['fondos' => $fondos]);
    }

    public function store(Request $request)
    {
        $fondo = CatFondoInversion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['fondo' => $fondo]);
    }

    public function delete($id)
    {
        $fondo = CatfondoInversion::findOrFail($id);
        $fondo->delete();
    }
}