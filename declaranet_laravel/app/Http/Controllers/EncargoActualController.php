<?php

namespace App\Http\Controllers;

use Auth;

use \App\Token;

use Carbon\Carbon;

use App\CatVialidad;
use App\EncargoActual;
use App\EncargoActualFuncP;
use Illuminate\Http\Request;

class EncargoActualController extends Controller
{

    public function index()
    {
        $encargoActual = EncargoActual::orderBy('id', 'asc')->get();

        return response()->json(['encargoActual' => $encargoActual]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

        $id = $token->uid;

        $informacion =  EncargoActual::where('informacion_personal_id', $id)->first();

        return response()->json(['informacion' => $informacion]);
    }

    public function encargo($id)
    {
        $informacion =  EncargoActual::where('informacion_personal_id', $id)->first();

        return response()->json(['informacion' => $informacion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        // $token = Token::decodeToken($auth);

        // $id = $token->uid;

        // if(Auth::id() != $id){
        //     return response()->json(['errorSession' => 'error']);
        // }

        ////

        $funciones = $request->funcionesp;

        $informacion = $request->except([
            'tipo_vialidad', 'vialidad_nombre', 'vial_id', 'funcionesp',
            'datos_encargo_actual_funciones_principales', 'vialidad', 'ente'
        ]);



        $informacion['fecha_posesion'] = Carbon::parse($request->fecha_posesion);

        $registro = EncargoActual::updateOrCreate(
            ['informacion_personal_id' => $id],
            $informacion
        );

        $registro->save();

        ///////////////////////encargoactural/////////////
        /////primero borrar las que ya estan para no duplicar, no se si haya un método mejor
        $funcionesp = EncargoActualFuncP::where('datos_encargo_actual_id', $registro->id);
        $funcionesp->forceDelete();


        $num = count($funciones);

        for ($i = 0; $i < $num; $i++) {
            $funcion =  new EncargoActualFuncP([
                'datos_encargo_actual_id' =>      $registro->id,
                'funcion_id' =>  $request->input('funcionesp.' . $i . '')
            ]);

            $funcion->save();
        }

        ///////////////////////

        return response()->json(['registro' => $registro->refresh()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
