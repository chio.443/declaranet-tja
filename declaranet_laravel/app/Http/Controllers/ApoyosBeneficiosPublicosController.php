<?php

namespace App\Http\Controllers;

use App\ApoyoBeneficioPublico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ApoyosBeneficiosPublicosController extends Controller
{
    public function index()
    {
        $response = ApoyoBeneficioPublico::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $response = ApoyoBeneficioPublico::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        //$data->fill($request->only(['secuencial','vehiculo','status_id','nombre_ruta','descripcion','dias','hora_salida','salida','destino','max','copy','recurrente','especial','caducidad']));

        $response = ApoyoBeneficioPublico::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'beneficiario' => $request->beneficiario,
                'programa' => $request->programa,
                'institucion_otorgante' => $request->institucion_otorgante,
                'nivel_orden_gobierno_id' => $request->nivel_orden_gobierno_id,
                'tipo_apoyo_id' => $request->tipo_apoyo_id,
                'monto' => $request->monto,
                'observaciones' => $request->observaciones,
                'especificar_apoyo' => $request->especificar_apoyo,
                'forma' => $request->forma,
                'especificar_tipo_apoyo' => $request->especificar_tipo_apoyo
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $response = ApoyoBeneficioPublico::findOrFail($id);
        $response->delete();
    }
}