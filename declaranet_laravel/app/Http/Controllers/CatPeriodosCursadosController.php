<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CatPeriodosCursados;

class CatPeriodosCursadosController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periodos = CatPeriodosCursados::orderBy('id','asc')->get();

        return response()->json(['periodos' =>$periodos]);
    }

    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
