<?php

namespace App\Http\Controllers;

use Auth;
use App\Pais;
use App\Token;
// use DB;
use App\Deudas;
use App\Fiscal;
use App\Moneda;
use App\Ambitos;
use App\CatAfore;
use Carbon\Carbon;
use App\CatTitular;
use App\RegimenMat;
use App\CatBancaria;
use App\CatTipoBien;
use App\Declaracion;
use App\EstadoCivil;
use App\Movimientos;
use App\CatFormaPago;
use App\CatLocalidad;
use App\CatMunicipio;
use App\CatTipoApoyo;
use App\CatTipoAdeudo;
use App\EncargoActual;
use App\NivelGobierno;
use App\PoderJuridico;
use App\CatEntePublico;
use App\CatTipoValores;
use App\EstatusCarrera;
use App\CatBajaInmueble;
use App\CatBeneficiario;
use App\BitacoraConsulta;
use App\CatTipoBeneficio;
use App\CatTipoInversion;
use App\CatTipoOperacion;
use App\CatTitularAdeudo;
use App\CatValorInmueble;
use App\GradoEscolaridad;
use App\CatFondoInversion;
use App\CatTipoBienMueble;
use App\DatosCurriculares;
use App\DocumentoObtenido;
use App\ExperenciaLaboral;
use App\CatOrganizacionpym;
use App\CatSectorIndustria;
use App\CatTipoInstitucion;
use App\CatTipoInstrumento;
use App\CatTipoRelacionDep;
use App\BitacoraDeclaracion;
use App\CatFormaAdquisicion;
use App\CatPeriodosCursados;
use App\CatTipoBienMuebleNr;
use App\CatTipoFideicomisos;
use App\InformacionPersonal;
use Illuminate\Http\Request;
use App\CatEntidadFederativa;
use App\CatTipoMonedaMetales;
use App\CatTipoParticipacion;
use App\DependienteEconomico;
use App\FuncionesPrincipales;
use App\Naturaleza_membresia;
use App\CatRelacionTransmisor;
use App\CatTipoBienEnajenacion;
use App\DeclaracionJustificada;
use App\CatTipoFideicomisosPart;
use App\InversionesCuentasValores;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\UsuarioTipoDecController;

class DeclaracionesReporte extends Controller
{

    public function dec(Request $request)
    {
        //dd($request->dependencia_id);
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $iddep = $token->ente_publico_id;
        $rol = $token->role;
        //dd($rol);
        if ($rol == 5 || $rol == 14 || $rol == 4)
            $dependencia = $iddep;
        else
            $dependencia = $request->dependencia_id ? $request->dependencia_id : 0;

        //dd($dependencia);

        $declarantes = DB::table('declaraciones')
            ->select('informacion_personal.primer_apellido', 'informacion_personal.segundo_apellido', 'informacion_personal.nombres', 'informacion_personal.id as ipid', 'declaraciones.id as decid', 'informacion_personal.rfc', 'cat_ente_publico.valor as ente', 'informacion_personal.correo_electronico_laboral as correo', 'declaraciones.Fecha_Dec as fecha')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('cat_ente_publico', 'datos_encargo_actual.ente_publico_id', '=', 'cat_ente_publico.id')
            ->when($dependencia != 0, function ($query) use ($dependencia) {
                return $query->where('datos_encargo_actual.ente_publico_id', $dependencia);
            })
            //->where('datos_encargo_actual.ente_publico_id', $request->dependencia_id)
            ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
            ->whereYear('declaraciones.created_at', '=', $request->periodo)
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('informacion_personal.deleted_at')
            ->orderBy('informacion_personal.nombres', 'asc')
            ->orderBy('informacion_personal.primer_apellido', 'asc')
            ->orderBy('informacion_personal.segundo_apellido', 'asc')
            ->get();

        return response()->json(['declarantes' => $declarantes]);
    }
    public function decRep(Request $request)
    {
        $dependencia = $request->dependencia_id ? $request->dependencia_id : 0;
        $periodo = $request->periodo;

        $movimientos = new Movimientos;
        $justificion = new DeclaracionJustificada;

        $justificadas = $justificion->where('periodo', $periodo)
            //->where('tipo_declaracion_id', $tipo_dec)
            ->pluck('id_ip')->toArray();

        $licencias = $movimientos->where('tipo_mov', '=', 4) //4 es el id de licencia
            ->where('termina', '>', date('Y-m-d'))
            ->pluck('ip_id')->toArray();

        $bajas = $movimientos->where('tipo_mov', 2) //2 es el id de baja
            ->where('tipo_baja', '!=', 'Normal') //por defuncion
            ->pluck('ip_id')->toArray();

        $excluir = array_merge($bajas, $licencias, $justificadas);

        $declarantes = DB::table('cat_ente_publico as e')
            ->select(DB::raw('count(DISTINCT(t.ip_id)), c.tipo_declaracion'))
            ->leftJoin('datos_encargo_actual as d', function ($join) use ($dependencia, $excluir) {
                $join->on('d.ente_publico_id', '=', 'e.id')
                    ->whereNull('d.deleted_at')
                    ->whereNotIn('d.informacion_personal_id', $excluir)
                    ->when($dependencia > 0, function ($query) use ($dependencia) {
                        $query->where('d.ente_publico_id', $dependencia);
                    });
            })
            ->leftJoin('informacion_personal as i', function ($join)  use ($excluir) {
                $join->on('i.id', '=', 'd.informacion_personal_id')
                    ->whereNotIn('i.id', $excluir)
                    ->whereNull('i.deleted_at');
            })
            ->leftJoin('usuario_tipo_dec as t', function ($join) use ($periodo, $excluir) {
                $join->on('t.ip_id', '=', 'i.id')
                    ->where('t.periodo', $periodo)
                    ->whereNotIn('t.ip_id', $excluir)
                    ->whereNull('t.deleted_at');
            })
            ->join('cat_tipo_declaracion as c', 'c.id', '=', 't.tipo_dec_id')
            ->when($dependencia > 0, function ($query) use ($dependencia) {
                $query->where('e.id', $dependencia);
            })
            ->whereNull('e.deleted_at')
            ->whereNull('t.deleted_at')
            ->whereNotNull('t.declaracion_id')
            ->where('t.periodo', $periodo)
            ->groupBy('c.tipo_declaracion')
            ->get();

        return response()->json(['declarantes' => $declarantes]);
    }
    public function descargaDec(Request $request, $id, $idus)
    {
        $format = 'd/m/Y';

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $idus = $token->uid;



        $dpersonales = array();


        if ($id > 1) {
            $declaracion = BitacoraDeclaracion::where('id', $id)
                ->where('ip_id', $idus)
                ->first(); ///declaracion en tablas
        } else {

            $declaracion = BitacoraDeclaracion::where('ip_id', $idus)
                ->orderBy('id', 'DESC')
                ->first();
        }
        //dd($declaracion);

        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();

        //información personal

        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = Pais::where('id', $func->pais_nacimiento_id)->first()['valor'];
        $dpersonales['entnac'] = CatEntidadFederativa::where('id', $func->entidad_federativa_nacimiento_id)->first()['nom_ent'];
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'];
        $dpersonales['entidad'] = CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'];
        //$dpersonales['localidad'] = CatLocalidad::where('id', $func->domicilio_localidad_id)->first()['nom_loc'];
        $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;

        $dpersonales['edoCivil'] = EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'];
        if ($func->regimen_matrimonial_id)
            $dpersonales['regimen'] = RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'];
        //datos curriculares

        $func = json_decode($declaracion['datos_curriculares']);

        // $func =  (object) $func[0];

        $dcurrc = array();

        foreach ($func as $escuela) {

            if ($escuela->lugar_institucion_ext == 1) {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => 'México',
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            } else {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => Pais::where('id', $escuela->lugar_institucion_educativa_pais_id)->first()['valor'],
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            }
        }
        //experiencia laboral
        //dd($dcurrc);
        $func = json_decode($declaracion['experiencia_laboral']);

        $experiencia = array();

        foreach ($func as $trabajo) {
            $experiencia[] = array(
                'ambito' => Ambitos::where('id', $trabajo->ambito_id)->first()['valor'],
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'municipio' => CatMunicipio::where('id', $trabajo->direccion_municipio_id)->first()['nom_mun'],
                'entidad' => CatEntidadFederativa::where('id', $trabajo->direccion_entidad_federativa_id)->first()['nom_ent'],
                'localidad' => CatLocalidad::where('id', $trabajo->direccion_localidad_id)->first()['nom_loc'],
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => CatSectorIndustria::where('id', $trabajo->sector_industria_id)->first()['valor'],
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format)
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = json_decode($declaracion['dependientes_economicos']);

        $dependientes = array();

        foreach ($func as $dependiente) {
            $dependientes[] = array(
                'tipo_r' =>  CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor'],
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);



        ////datos de otro encargo////////////////////////////////////////////////////////////////////
        $func = json_decode($declaracion['otro_encargo']);
        $oencargo = array();
        if (count($oencargo) > 0) {
            $oencargo['dependencia'] = strtoupper(CatEntePublico::where('id', $func->ente_publico_id)->first()['valor']);
            $oencargo['cargo'] = ($func->empleo_cargo_comision); //cargo
            $oencargo['area'] = ($func->area_adscripcion); //cargo
            $oencargo['ftomaposesion'] =  Carbon::parse($func->fecha_posesion)->format($format);
            $oencargo['baja'] =  Carbon::parse($func->fecha_termino)->format($format);
        }



        // $bitacora = new BitacoraConsulta([
        //     'ip_id' => $func->id,
        //     'created_by' => $idus
        // ]);

        // $bitacora->save();


        ///////////////////////////////////////////intereses //////////////////////////////////////////
        //empresas sociedades asociaciones
        $func = json_decode($declaracion['empresas_sociedades_asociaciones']);

        $empresas = array();
        foreach ($func as $empresa) {
            $empresas[] = array(
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'naturaleza' => $empresa->naturaleza_vinculo_especificar
            );
        }
        //toma de decisiones

        $func = json_decode($declaracion['toma_decisiones']);

        $tomaDecisiones  = array();
        foreach ($func as $toma) {
            $tomaDecisiones[] =
                array(
                    'nombre' => $toma->nombre,
                    'puesto' => $toma->puesto
                );
        }

        //beneficios publicos

        $func = json_decode($declaracion['apoyos_publicos']);

        $beneficiosPubl  = array();
        foreach ($func as $beneficio) {
            $beneficiosPubl[] = array(
                'programa' => $beneficio->programa,
                'institucion' => $beneficio->institucion_otorgante
            );
        }

        //representación activa

        $func = json_decode($declaracion['representacion_activa']);

        $reprActiva  = array();
        foreach ($func as $rep) {
            $reprActiva[] = array(
                'nombre' => $rep->nombre,
                'rfc' => $rep->rfc
            );
        }
        //clientes principales

        $func = json_decode($declaracion['clientes_principales']);

        $clientesPrinc  = array();
        foreach ($func as $cte) {
            $clientesPrinc[] = array(
                'nombre' => $cte->nombre_cliente,
                'rfc' => $cte->rfc_cliente
            );
        }
        //
        //beneficios privados

        $func = json_decode($declaracion['beneficios_privados']);

        $beneficiosPriv  = array();
        foreach ($func as $benef) {
            $beneficiosPriv[] = array(
                'nombre' => $benef->nombre,
                'rfc' => $benef->rfc
            );
        }
        //fideicomisos

        $func = json_decode($declaracion['fideicomisos']);

        $fideicomisos  = array();
        foreach ($func as $fid) {
            $fideicomisos[] = array(
                'nombre' => $fid->nombre_fideicomitente,
                'rfc' => $fid->rfc_fideicomitente
            );
        }



        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array) $func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración mensual neta del declarante por su cargo público ',
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']), 2)
        );


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        );

        ///TOTAL INGRESOS
        $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']) +
            (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
            (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']));

        ///
        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2)
        );


        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        /* $otrossant[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        ); */

        if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        }



        ///////////////////////////////////////activos
        //bienes inmuebles


        $func = json_decode($declaracion['bienes_inmuebles']);


        $binmuebles = array();
        foreach ($func as $inmueble) {
            $binmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'],
                'naturaleza' => CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'],
                'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2)
            );
        }
        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {
            $bmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMueble::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2),
                'domicilio' => $mueble->tipo_operacion_id,

            );
        }
        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            $bmueblesnr[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMuebleNr::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }

        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'],
                'entidad' => CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'],
                'tipoOp' => CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'],
                'tipoInv' => CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'],
                'titular' => CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor']

            );
        }

        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $deuda->cat_tipo_operacion_id)->first()['valor'],
                'tipoAdeudo' => CatTipoAdeudo::where('id', $deuda->cat_tipo_adeudo_id)->first()['valor'],
                'acreedor' => $deuda->nombre_acreedor,
                'identificador' => $deuda->identificador_deuda,
                'original' => '$' . number_format((float)str_replace(',', '', $deuda->monto_original), 2),
                'pendiente' => '$' . number_format((float)str_replace(',', '', $deuda->saldo_pendiente), 2),
                'fadeudo' => Carbon::parse($deuda->fecha_adeudo)->format($format)

            );
        }


        $func = json_decode($declaracion['prestamo_comodato']);

        $prestamo = array();
        foreach ($func as $pr) {
            if ($pr->tipo_inmueble_id) { //si es un inmueble
                $prestamo[] = array(
                    'tipo' => 'Inmueble',
                    'tipoBien' => CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor'],
                    //'desc'=> $pr->vehiculo.' '.$pr->marca.', '.$pr->modelo.' '.$pr->anio
                    'desc' => $pr->vialidad_nombre . ' ' . $pr->colonia . ', ' . $pr->numext

                );
            } else { //si es un vehiculo
                $prestamo[] = array(
                    'tipo' => 'Vehículo',
                    'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor'],
                    //'desc'=> $pr->vialidad_nombre.' '.$pr->colonia.', '.$pr->numext
                    'desc' => $pr->vehiculo . ' ' . $pr->marca . ', ' . $pr->modelo . ' ' . $pr->anio

                );
            }
        }




        $fiscal = json_decode($declaracion['declaracion_fiscal']);

        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';


        $fechaPresenta = Carbon::parse($datosDeclaracion['Fecha_Dec'])->format($format);

        $view =  \View::make('declaracion', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos', 'datosDeclaracion', 'spublicos', 'otross', 'fechaPresenta', 'tomaDecisiones', 'beneficiosPubl', 'reprActiva', 'clientesPrinc', 'beneficiosPriv', 'fideicomisos', 'oencargo', 'prestamo', 'totalingresos'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }

    // public function descargaDec1($id, $idus)
    public function descargaDec1(Request $request, $id, $idus)
    {
        $format = 'd/m/Y';


        //   $dec = DB::table('declaraciones')
        //    ->select('id_ip')
        // ->where('declaraciones.id',$id)->first();

        //$id = $dec->id_ip;

        //  $func = InformacionPersonal::where('id', $id)->first();

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $idus = $token->uid;

        $dpersonales = array();

        $declaracion = BitacoraDeclaracion::where('declaracion_id', $id)
            ->first(); ///declaracion en tablas

        /* if($id>1){
            $declaracion = BitacoraDeclaracion::where('id', $id)
                 ->where('ip_id',$idus)
                ->first(); ///declaracion en tablas
        }
        else{

           $declaracion = BitacoraDeclaracion::where('ip_id', $idus)
                         ->orderBy('id','DESC')
                         ->first();

        } */



        //dd($declaracion);

        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();

        //información personal

        $func = json_decode($declaracion['informacion_personal']);

        $func = (object) $func[0];

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = ($func->pais_nacimiento_id) ? Pais::where('id', $func->pais_nacimiento_id)->first()['valor'] : '';
        $dpersonales['entnac'] = ($func->entidad_federativa_nacimiento_id) ? CatEntidadFederativa::where('id', $func->entidad_federativa_nacimiento_id)->first()['nom_ent'] : '';
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = ($func->domicilio_municipio_id) ? CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'] : '';
        $dpersonales['entidad'] = ($func->domicilio_entidad_federativa_id) ? CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'] : '';
        //$dpersonales['localidad']= CatLocalidad::where('id',$func->domicilio_localidad_id)->first()['nom_loc'];
        $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;

        $dpersonales['edoCivil'] = ($func->estado_civil_id) ? EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'] : '';
        if ($func->regimen_matrimonial_id)
            $dpersonales['regimen'] = ($func->regimen_matrimonial_id) ? RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'] : '';
        //datos curriculares

        $func = json_decode($declaracion['datos_curriculares']);

        // $func =  (object) $func[0];

        $dcurrc = array();

        foreach ($func as $escuela) {

            if ($escuela->lugar_institucion_ext == 1) {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => 'México',
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            } else {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => Pais::where('id', $escuela->lugar_institucion_educativa_pais_id)->first()['valor'],
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            }
        }
        //experiencia laboral
        //dd($dcurrc);
        $func = json_decode($declaracion['experiencia_laboral']);

        $experiencia = array();

        foreach ($func as $trabajo) {
            $experiencia[] = array(
                'ambito' => Ambitos::where('id', $trabajo->ambito_id)->first()['valor'],
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'municipio' => CatMunicipio::where('id', $trabajo->direccion_municipio_id)->first()['nom_mun'],
                'entidad' => CatEntidadFederativa::where('id', $trabajo->direccion_entidad_federativa_id)->first()['nom_ent'],
                'localidad' => CatLocalidad::where('id', $trabajo->direccion_localidad_id)->first()['nom_loc'],
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => CatSectorIndustria::where('id', $trabajo->sector_industria_id)->first()['valor'],
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format)
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = json_decode($declaracion['dependientes_economicos']);

        $dependientes = array();

        foreach ($func as $dependiente) {
            $dependientes[] = array(
                'tipo_r' =>  CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor'],
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);


        $bitacora = new BitacoraConsulta([
            'ip_id' => $func->id,
            'created_by' => $idus
        ]);

        $bitacora->save();

        ////datos de otro encargo////////////////////////////////////////////////////////////////////
        $func = json_decode($declaracion['otro_encargo']);
        $oencargo = array();
        if (count($oencargo) > 0) {
            $oencargo['dependencia'] = strtoupper(CatEntePublico::where('id', $func->ente_publico_id)->first()['valor']);
            $oencargo['cargo'] = ($func->empleo_cargo_comision); //cargo
            $oencargo['area'] = ($func->area_adscripcion); //cargo
            $oencargo['ftomaposesion'] =  Carbon::parse($func->fecha_posesion)->format($format);
            $oencargo['baja'] =  Carbon::parse($func->fecha_termino)->format($format);
        }




        ///////////////////////////////////////////intereses //////////////////////////////////////////
        //empresas sociedades asociaciones
        $func = json_decode($declaracion['empresas_sociedades_asociaciones']);

        $empresas = array();
        foreach ($func as $empresa) {
            $empresas[] = array(
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'naturaleza' => $empresa->naturaleza_vinculo_especificar
            );
        }
        //toma de decisiones

        $func = json_decode($declaracion['toma_decisiones']);

        $tomaDecisiones  = array();
        foreach ($func as $toma) {
            $tomaDecisiones[] =
                array(
                    'nombre' => $toma->nombre,
                    'puesto' => $toma->puesto
                );
        }

        //beneficios publicos

        $func = json_decode($declaracion['apoyos_publicos']);

        $beneficiosPubl  = array();
        foreach ($func as $beneficio) {
            $beneficiosPubl[] = array(
                'programa' => $beneficio->programa,
                'institucion' => $beneficio->institucion_otorgante
            );
        }

        //representación activa

        $func = json_decode($declaracion['representacion_activa']);

        $reprActiva  = array();
        foreach ($func as $rep) {
            $reprActiva[] = array(
                'nombre' => $rep->nombre,
                'rfc' => $rep->rfc
            );
        }
        //clientes principales

        $func = json_decode($declaracion['clientes_principales']);

        $clientesPrinc  = array();
        foreach ($func as $cte) {
            $clientesPrinc[] = array(
                'nombre' => $cte->nombre_cliente,
                'rfc' => $cte->rfc_cliente
            );
        }
        //
        //beneficios privados

        $func = json_decode($declaracion['beneficios_privados']);

        $beneficiosPriv  = array();
        foreach ($func as $benef) {
            $beneficiosPriv[] = array(
                'nombre' => $benef->nombre,
                'rfc' => $benef->rfc
            );
        }
        //fideicomisos

        $func = json_decode($declaracion['fideicomisos']);

        $fideicomisos  = array();
        foreach ($func as $fid) {
            $fideicomisos[] = array(
                'nombre' => $fid->nombre_fideicomitente,
                'rfc' => $fid->rfc_fideicomitente
            );
        }





        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array)$func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público ',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']), 2)
        );


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        );
        ///TOTAL INGRESOS
        $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']) +
            (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
            (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']));

        ///
        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2)
        );


        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        /* $otrossant[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                          'nombre' => $sueldo['ant_arrendamientos'],
                          'ingreso' => '$'.number_format((float)str_replace(',','',$sueldo['ant_arrendamientos_cantidad']),2)
                        ); */
        /* $otrossant[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        ); */
        if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        }
        ///////////////////////////////////////activos
        //bienes inmuebles


        $func = json_decode($declaracion['bienes_inmuebles']);


        $binmuebles = array();
        foreach ($func as $inmueble) {
            $binmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'],
                'naturaleza' => CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'],
                'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2)
            );
        }
        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {
            $bmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMueble::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }
        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            $bmueblesnr[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMuebleNr::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }

        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $deuda->cat_tipo_operacion_id)->first()['valor'],
                'tipoAdeudo' => CatTipoAdeudo::where('id', $deuda->cat_tipo_adeudo_id)->first()['valor'],
                'acreedor' => $deuda->nombre_acreedor,
                'identificador' => $deuda->identificador_deuda,
                'original' => '$' . number_format((float)str_replace(',', '', $deuda->monto_original), 2),
                'pendiente' => '$' . number_format((float)str_replace(',', '', $deuda->saldo_pendiente), 2),
                'fadeudo' => Carbon::parse($deuda->fecha_adeudo)->format($format)

            );
        }


        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'],
                'entidad' => CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'],
                'tipoOp' => CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'],
                'tipoInv' => CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'],
                'titular' => CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor']

            );
        }

        $fiscal = json_decode($declaracion['declaracion_fiscal']);

        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';

        $fechaPresenta = Carbon::parse($datosDeclaracion['Fecha_Dec'])->format($format);



        $func = json_decode($declaracion['prestamo_comodato']);

        $prestamo = array();
        foreach ($func as $pr) {
            if ($pr->tipo_inmueble_id) { //si es un inmueble
                $prestamo[] = array(
                    'tipo' => 'Inmueble',
                    'tipoBien' => CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor'],
                    // 'desc'=> $pr->vehiculo.' '.$pr->marca.', '.$pr->modelo.' '.$pr->anio
                    'desc' => $pr->vialidad_nombre . ' ' . $pr->colonia . ', ' . $pr->numext
                );
            } else { //si es un vehiculo
                $prestamo[] = array(
                    'tipo' => 'Vehículo',
                    'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor'],
                    // 'desc'=> $pr->vialidad_nombre.' '.$pr->colonia.', '.$pr->numext
                    'desc' => $pr->marca . ', ' . $pr->modelo . ' ' . $pr->anio
                    //$pr->vehiculo . ' ' .
                );
            }
        }




        $view =  \View::make('declaracion', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos', 'datosDeclaracion', 'spublicos', 'otross', 'fechaPresenta', 'tomaDecisiones', 'beneficiosPubl', 'reprActiva', 'clientesPrinc', 'beneficiosPriv', 'fideicomisos', 'oencargo', 'prestamo', 'totalingresos'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }


    //reactivar la declaración
    public function reactivar($idDecl, Request $request)
    {
        try {
            DB::beginTransaction();
            $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);

            $id = $token->uid;

            $declaracion = Declaracion::where('id', $idDecl)->first();

            //regresar al tipo de declaracion que presentó
            $encargo = EncargoActual::where('informacion_personal_id', $declaracion['id_ip'])->first();

            $encargo->tipo_dec = $declaracion['tipo_declaracion_id'];

            // Agregar a tabla usuario_tipo_dec
            $ip_id =  $declaracion['id_ip'];
            $declaracion_id =  $idDecl;
            $tipo_dec_id =  $declaracion['tipo_declaracion_id'];
            $ente_publico_id =  $encargo->ente_publico_id;
            $fecha_posesion =  $encargo->fecha_posesion;
            // fin a tabla usuario_tipo_dec
            $periodo = ($declaracion['periodo']) ? $declaracion['periodo'] : Carbon::parse($declaracion['Fecha_Dec'])->format('Y');

            $encargo->save();

            $declaracion = Declaracion::where('id', $idDecl);

            $declaracion->update(array('deleted_by' => $id, 'deleted_at' => date('Ymd')));

            $declaracion->delete();

            // Agregar a tabla usuario_tipo_dec
            $usuario_tipo_dec = new UsuarioTipoDecController;

            $tipo_dec  = new Request;
            $tipo_dec->ip_id = $ip_id;
            $tipo_dec->declaracion_id = $declaracion_id;
            $tipo_dec->tipo_dec_id =  $tipo_dec_id;
            $tipo_dec->ente_publico_id = $ente_publico_id;
            $tipo_dec->fecha_posesion = $fecha_posesion;
            $tipo_dec->periodo = $periodo;

            $tipo_dec_resp = $usuario_tipo_dec->reactivardec($tipo_dec);
            // FIN usuario_tipo_dec
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return response()->json(['declarante' => $declaracion]);
    }

    public function repDec(Request $request)
    {
        //dd($request->dependencia_id);
        $dependencia = $request->dependencia_id1 ? $request->dependencia_id1 : 0;

        $declarantes = DB::table('declaraciones')
            ->select('informacion_personal.primer_apellido', 'informacion_personal.segundo_apellido', 'informacion_personal.nombres', 'informacion_personal.id as ipid', 'declaraciones.id as decid', 'informacion_personal.rfc', 'cat_ente_publico.valor as ente', 'informacion_personal.correo_electronico_laboral as correo', 'declaraciones.Tipo_Dec as tipoDec')
            ->join('informacion_personal', 'informacion_personal.id', '=', 'declaraciones.id_ip')
            ->join('datos_encargo_actual', 'informacion_personal.id', '=', 'datos_encargo_actual.informacion_personal_id')
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->when($dependencia != 0, function ($query) use ($request) {
                return $query->where('declaraciones.ente_publico_id', $request->dependencia_id1);
            })
            //->where('datos_encargo_actual.ente_publico_id', $request->dependencia_id)
            ->where('declaraciones.tipo_declaracion_id', $request->tipo_declaracion)
            ->whereBetween('declaraciones.Fecha_Dec', array($request->fecha_inicio1, $request->fecha_fin1))
            ->whereNull('declaraciones.deleted_at')
            ->whereNull('informacion_personal.deleted_at')
            ->orderBy('informacion_personal.nombres', 'asc')
            ->orderBy('informacion_personal.primer_apellido', 'asc')
            ->orderBy('informacion_personal.segundo_apellido', 'asc')
            ->get();

        return response()->json(['declarantes' => $declarantes]);
    }

    public function repDec2(Request $request)
    {
        $dependencia = $request->dependencia_id ? $request->dependencia_id : 0;

        $declarantes = DB::table('declaraciones')
            ->select(DB::raw(' count(declaraciones.id) as total, cat_ente_publico.valor as ente, cat_tipo_declaracion.tipo_declaracion as tipo'))
            //->select('cat_ente_publico.valor as ente')
            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->groupBy('cat_ente_publico.id', 'cat_tipo_declaracion.tipo_declaracion')
            ->when($dependencia != 0, function ($query) use ($request) {
                return $query->where('declaraciones.ente_publico_id', $request->dependencia_id);
            })
            ->whereBetween('declaraciones.Fecha_Dec', array($request->fecha_inicio, $request->fecha_fin))
            ->orderBy('cat_ente_publico.valor', 'asc')
            ->whereNull('declaraciones.deleted_at')
            ->orderBy('cat_tipo_declaracion.tipo_declaracion', 'asc')
            ->get();

        $dinicial = DB::table('declaraciones')
            ->select(DB::raw(' count(declaraciones.id) as inicial'))
            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->groupBy('cat_ente_publico.id', 'cat_tipo_declaracion.tipo_declaracion')
            ->when($dependencia != 0, function ($query) use ($request) {
                return $query->where('declaraciones.ente_publico_id', $request->dependencia_id);
            })
            ->where('declaraciones.tipo_declaracion_id', '=', 1)
            ->whereBetween('declaraciones.Fecha_Dec', array($request->fecha_inicio, $request->fecha_fin))
            ->whereNull('declaraciones.deleted_at')
            ->get();

        $anual = DB::table('declaraciones')
            ->select(DB::raw(' count(declaraciones.id) as anual'))
            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->groupBy('cat_ente_publico.id', 'cat_tipo_declaracion.tipo_declaracion')
            ->when($dependencia != 0, function ($query) use ($request) {
                return $query->where('declaraciones.ente_publico_id', $request->dependencia_id);
            })
            ->where('declaraciones.tipo_declaracion_id', '=', 2)
            ->whereBetween('declaraciones.Fecha_Dec', array($request->fecha_inicio, $request->fecha_fin))
            ->orderBy('cat_ente_publico.valor', 'asc')
            ->whereNull('declaraciones.deleted_at')
            ->orderBy('cat_tipo_declaracion.tipo_declaracion', 'asc')
            ->get();

        $final = DB::table('declaraciones')
            ->select(DB::raw(' count(declaraciones.id) as final'))
            ->join('cat_tipo_declaracion', 'declaraciones.tipo_declaracion_id', '=', 'cat_tipo_declaracion.id')
            ->join('cat_ente_publico', 'declaraciones.ente_publico_id', '=', 'cat_ente_publico.id')
            ->groupBy('cat_ente_publico.id', 'cat_tipo_declaracion.tipo_declaracion')
            ->when($dependencia != 0, function ($query) use ($request) {
                return $query->where('declaraciones.ente_publico_id', $request->dependencia_id);
            })
            ->where('declaraciones.tipo_declaracion_id', '=', 3)
            ->whereBetween('declaraciones.Fecha_Dec', array($request->fecha_inicio, $request->fecha_fin))
            ->orderBy('cat_ente_publico.valor', 'asc')
            ->whereNull('declaraciones.deleted_at')
            ->orderBy('cat_tipo_declaracion.tipo_declaracion', 'asc')
            ->get();

        return response()->json(['declarantes' => $declarantes, 'dinicial' => $dinicial, 'anual' => $anual, 'final' => $final]);

        //return response()->json(['declarantes' => $declarantes]);
    }

    // public function descarga declaracion completa($id, $idus)
    public function descargaDecComp(Request $request, $id, $idus)
    {
        $format = 'd/m/Y';

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $idus = $token->uid;

        $dpersonales = array();

        $declaracion = BitacoraDeclaracion::where('declaracion_id', $id)
            ->first(); ///declaracion en tablas

        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();

        //información personal

        $func = json_decode($declaracion['informacion_personal']);

        $func = (object) $func[0];
        $nacionalidades = [];
        if (count($func->nac)) {
            foreach ($func->nac as $nac) {
                $nacionalidades[] =  Pais::where('id', $nac->nac_id)->first()['valor'];
            }
            $nacionalidades = implode(",", $nacionalidades);
        } else {
            $nacionalidades = 'México';
        }



        $lugardep = 'En México';
        if ($func->domicilio_pais_id != 150) {
            $lugardep = 'En el Extranjero';
        }
        $pais = ($func->domicilio_pais_id) ? Pais::where('id', $func->domicilio_pais_id)->first()['valor'] : '';
        $entidad = ($func->domicilio_entidad_federativa_id) ? CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'] : '';
        $cp = $func->domicilio_cp;
        $numext = $func->domicilio_numext;
        $numint = $func->domicilio_numint;
        $municipio = ($func->domicilio_municipio_id) ? CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'] : '';
        // $localidad = CatLocalidad::where('id', $dependiente->domicilio_localidad_id)->first()['nom_loc'];
        // $tvialidad = $func->domicilio_vialidad_tipo;
        $calle =  $func->domicilio_vialidad_nombre;
        $colonia = $func->domicilio_colonia;
        //  . $tvialidad . ' '
        $dom_declarante = $lugardep . ', ' . $calle . ' # ' . $numext . ', Int: ' . $numint . ', '
            . $colonia .  ', ' . $municipio . ', ' . $entidad . ', ' . $pais
            . ' CP: ' .  $cp;


        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = ($func->pais_nacimiento_id) ? Pais::where('id', $func->pais_nacimiento_id)->first()['valor'] : '';
        $dpersonales['nacionalidad'] = $nacionalidades;
        $dpersonales['entnac'] = ($func->entidad_federativa_nacimiento_id) ? CatEntidadFederativa::where('id', $func->entidad_federativa_nacimiento_id)->first()['nom_ent'] : '';
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_celular'] = $func->telefono_celular;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['pais'] = ($func->domicilio_pais_id) ? Pais::where('id', $func->domicilio_pais_id)->first()['valor'] : 'México';
        $dpersonales['municipio'] = ($func->domicilio_municipio_id) ? CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'] : '';
        $dpersonales['entidad'] = ($func->domicilio_entidad_federativa_id) ? CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'] : '';
        // $dpersonales['localidad'] = ($func->domicilio_localidad_id) ? CatLocalidad::where('id', $func->domicilio_localidad_id)->first()['nom_loc'] : '';
        // $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;
        // $dpersonales['regimen_matrimonial'] = $func->regimen_matrimonial_id;
        $dpersonales['aclaraciones'] = isset($func->aclaraciones) ?: '';
        $dpersonales['domicilio_observaciones'] = isset($func->domicilio_observaciones) ?: '';

        $dpersonales['edoCivil'] = ($func->estado_civil_id) ? EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'] : '';
        $dpersonales['regimen'] = 'No aplica';
        if ($func->regimen_matrimonial_id && ($func->estado_civil_id == 1 || $func->estado_civil_id == 4)) {
            $dpersonales['regimen'] = ($func->regimen_matrimonial_id) ? RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'] : 'No aplica';
        } //datos curriculares

        $func = json_decode($declaracion['datos_curriculares']);

        // $func =  (object) $func[0];

        $dcurrc = array();

        foreach ($func as $escuela) {

            if ($escuela->lugar_institucion_ext == 1) {
                $dcurrc[] = array(
                    'grado' => ($escuela->grado_maximo_escolaridad_id) ? GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'] : '',
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => ($escuela->lugar_institucion_educativa_entidad_id) ? CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'] : '',
                    'pais' => 'En México',
                    'carrera' => $escuela->carrera,
                    'periodos' => ($escuela->periodos_cursados_id) ? CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'] : '',
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => ($escuela->estatus_id) ? EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'] : '',
                    'doctoOb' => ($escuela->documento_obtenido_id) ? DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'] : '',
                    'cedula' => $escuela->cedula_profesional,
                    'fecha' => isset($escuela->fecha_obtencion) ?: '',
                );
            } else {
                $dcurrc[] = array(
                    'grado' => ($escuela->grado_maximo_escolaridad_id) ? GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'] : '',
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => ($escuela->lugar_institucion_educativa_entidad_id) ? CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'] : '',
                    'pais' => 'En el extranjero',
                    // 'pais' => ($escuela->lugar_institucion_educativa_pais_id) ? Pais::where('id', $escuela->lugar_institucion_educativa_pais_id)->first()['valor'] : '',
                    'carrera' => $escuela->carrera,
                    'periodos' => ($escuela->periodos_cursados_id) ? CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'] : '',
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => ($escuela->estatus_id) ? EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'] : '',
                    'doctoOb' => ($escuela->documento_obtenido_id) ? DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'] : '',
                    'cedula' => $escuela->cedula_profesional,
                    'fecha' => isset($escuela->fecha_obtencion) ?: '',
                );
            }
        }
        //experiencia laboral
        //dd($dcurrc);
        $func = json_decode($declaracion['experiencia_laboral']);

        $experiencia = array();

        foreach ($func as $trabajo) {
            $funciones = [];
            foreach ($trabajo->funciones as $fun) {
                $fun_ = FuncionesPrincipales::where('id', $fun->funcion_id)->first();
                $funciones[] = $fun_['valor'];
            }
            $funciones = implode(', ', $funciones);

            $experiencia[] = array(
                'nivel' => ($trabajo->nivel_gobierno_id) ? NivelGobierno::where('id', $trabajo->nivel_gobierno_id)->first()['valor'] : '',
                'ambito' => ($trabajo->ambito_id) ? Ambitos::where('id', $trabajo->ambito_id)->first()['valor'] : '',
                'ambito_publico' => ($trabajo->poder_ente_id) ? PoderJuridico::where('id', $trabajo->poder_ente_id)->first()['valor'] : '',
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'pais' => ($trabajo->direccion_pais_id) ? Pais::where('id', $trabajo->direccion_pais_id)->first()['valor'] : '',
                'municipio' => ($trabajo->direccion_municipio_id) ? CatMunicipio::where('id', $trabajo->direccion_municipio_id)->first()['nom_mun'] : '',
                'entidad' => ($trabajo->direccion_entidad_federativa_id) ? CatEntidadFederativa::where('id', $trabajo->direccion_entidad_federativa_id)->first()['nom_ent'] : '',
                'localidad' => ($trabajo->direccion_localidad_id) ?  CatLocalidad::where('id', $trabajo->direccion_localidad_id)->first()['nom_loc'] : '',
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => ($trabajo->sector_industria_id) ? CatSectorIndustria::where('id', $trabajo->sector_industria_id)->first()['valor'] : '',
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format),
                'rfc' => isset($trabajo->rfc) ?: '',
                'funcionesp' => $funciones,
                'otras_funciones' => $trabajo->otras_funciones,
                'aclaraciones' => (!empty($trabajo->observaciones)) ? $trabajo->observaciones : '',
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = json_decode($declaracion['dependientes_economicos']);

        $dependientes = array();

        foreach ($func as $dependiente) {
            $domdep = 'Habita en el mismo domicilio';
            $lugar = 'En México';
            if ($dependiente->habita_domicilio_declarante == 2) {
                if ($dependiente->domicilio_pais_id != 150) {
                    $lugar = 'En el Extranjero';
                }
                $pais = ($dependiente->domicilio_pais_id) ? Pais::where('id', $dependiente->domicilio_pais_id)->first()['valor'] : '';
                $entidad = ($dependiente->domicilio_entidad_federativa) ? CatEntidadFederativa::where('id', $dependiente->domicilio_entidad_federativa)->first()['nom_ent'] : '';
                $cp = $dependiente->domicilio_cp;
                $numext = $dependiente->domicilio_numext;
                $numint = $dependiente->domicilio_numint;
                $municipio = ($dependiente->domicilio_municipio_id) ? CatMunicipio::where('id', $dependiente->domicilio_municipio_id)->first()['nom_mun'] : '';
                // $localidad = CatLocalidad::where('id', $dependiente->domicilio_localidad_id)->first()['nom_loc'];
                $tvialidad = $dependiente->domicilio_tipo_vialidad;
                $calle = $dependiente->domicilio_nombre_vialidad;
                $colonia = $dependiente->domicilio_colonia;

                $domdep = $lugar . ', ' . $tvialidad . ' ' . $calle . ' # ' . $numext . ', Int: ' . $numint . ', '
                    . $colonia .  ', ' . $municipio . ', ' . $entidad . ', ' . $pais
                    . ' CP: ' .  $cp;
            } else {
                $domdep = $dom_declarante;
            }
            $ambito = 'Ninguno';
            if (!empty($dependiente->ambito_id)) {
                if ($dependiente->ambito_id == 1) {
                    $ambito = "Público";
                }
                if ($dependiente->ambito_id == 2) {
                    $ambito = "Privado";
                }
                if ($dependiente->ambito_id == 3) {
                    $ambito = "Otro";
                }
                if ($dependiente->ambito_id == 4) {
                    $ambito = "Ninguno";
                }
            }

            $nacionalidades = [];
            $ciudadano = 'Si';
            foreach ($dependiente->nac as $nac) {
                if ($nac->nac_id == 150) {
                    $ciudadano = 'No';
                }
                $nac_ = Pais::where('id', $nac->nac_id)->first();
                $nacionalidades[] = $nac_['valor'];
            }
            $nacionalidades = implode(', ', $nacionalidades);

            // Lugar donde reside: En México, En el Extranjero y Se desconoce
            $dependientes[] = array(
                'tipo_r' => ($dependiente->tipo_relacion_dep_id) ?  CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor'] : '',
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp,
                'rfc' => $dependiente->rfc,
                'fecha_nac' => $dependiente->fecha_nacimiento,
                'ciudadano' => $ciudadano,
                'lugar' => $lugar,
                'paisnac' => $nacionalidades,
                'domicilio' => $domdep,
                'ambito_id' => (!empty($dependiente->ambito_id)) ? $dependiente->ambito_id : 4,
                'ambito' => $ambito,
                // Publico
                'nivel_gobierno' => (!empty($dependiente->nivel_gobierno_id)) ? NivelGobierno::where('id', $dependiente->nivel_gobierno_id)->first()['valor'] : '',
                'poder_ente' => (!empty($dependiente->poder_ente_id)) ? PoderJuridico::where('id', $dependiente->poder_ente_id)->first()['valor'] : '',
                'nombre_dependencia' => (!empty($dependiente->nombre_dependencia)) ? $dependiente->nombre_dependencia : '',
                'area_adscripcion' => (!empty($dependiente->area_adscripcion)) ? $dependiente->area_adscripcion : '',
                'empleo_cargo_comision' => (!empty($dependiente->empleo_cargo_comision)) ? $dependiente->empleo_cargo_comision : '',
                'funcion_principal' => (!empty($dependiente->funcion_principal)) ? $dependiente->funcion_principal : '',
                // Privado
                'nombre_empresa' => (!empty($dependiente->nombre_empresa)) ? $dependiente->nombre_empresa : '',
                'rfcempresa' => (!empty($dependiente->rfcempresa)) ? $dependiente->rfcempresa : '',
                'empleo_cargo' => (!empty($dependiente->empleo_cargo)) ? $dependiente->empleo_cargo : '',
                'salario_mensual' => (!empty($dependiente->salario_mensual)) ? $dependiente->salario_mensual : '',
                'fecha_inicio' => (!empty($dependiente->fecha_inicio)) ? Carbon::parse($dependiente->fecha_inicio)->format($format) : '',
                'proveedor_contratista_gobierno' => ((!empty($dependiente->proveedor_contratista_gobierno)) && $dependiente->proveedor_contratista_gobierno == 1) ? 'Si' : 'No',
                'sector' => (!empty($dependiente->sector_industria_id)) ? CatSectorIndustria::where('id', $dependiente->sector_industria_id)->first()['valor'] : '',
                'observaciones' => (!empty($dependiente->observaciones)) ? $dependiente->observaciones : '',
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        // 'funcion_principal' => $dependiente->funcion_principal,

        $dencargo = array();

        $dencargo['nivel_gobierno'] = ($func->encargo->nivel_gobierno_id) ? strtoupper(NivelGobierno::where('id', $func->encargo->nivel_gobierno_id)->first()['valor']) : '';
        $dencargo['poder_ente'] = ($func->encargo->poder_juridico_id) ? strtoupper(PoderJuridico::where('id', $func->encargo->poder_juridico_id)->first()['valor']) : ''; //dependencia o ente
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);
        $dencargo['honorarios'] =  ($func->encargo->contratado_honorarios == 1 ? 'SI' : 'NO');
        $dencargo['nivel_encargo'] =  ($func->encargo->nivel_encargo);

        $funciones = [];
        foreach ($func->encargo->funcionesp as $fun) {
            $fun_ = FuncionesPrincipales::where('id', $fun->funcion_id)->first();
            $funciones[] = $fun_['valor'];
        }
        $funciones = implode(', ', $funciones);
        // FuncionesPrincipales::
        $dencargo['funcionesp'] =  $funciones;

        $dencargo['otras_funciones'] =  ($func->encargo->otras_funciones);
        $tel =  ($func->encargo->telefono_laboral_numero);
        $ext =  ($func->encargo->telefono_laboral_extension);
        $dencargo['telefono'] = $tel;
        if (!empty($ext)) {
            $dencargo['telefono'] = $tel . ', Ext: ' . $ext;
        }
        // Direccion Encargo
        $encargo_pais = ($func->encargo->direccion_encargo_pais_id) ? Pais::where('id', $func->encargo->direccion_encargo_pais_id)->first()['valor'] : '';
        $encargo_entidad = ($func->encargo->direccion_encargo_entidad_federativa_id) ? CatEntidadFederativa::where('id', $func->encargo->direccion_encargo_entidad_federativa_id)->first()['nom_ent'] : '';
        $encargo_cp = $func->encargo->direccion_encargo_cp;
        $encargo_numext = $func->encargo->direccion_encargo_numext;
        $encargo_numint = $func->encargo->direccion_encargo_numint;
        $encargo_municipio = ($func->encargo->direccion_encargo_municipio_id) ? CatMunicipio::where('id', $func->encargo->direccion_encargo_municipio_id)->first()['nom_mun'] : '';
        $encargo_tvialidad = $func->encargo->direccion_vialidad_tipo;
        $encargo_calle = $func->encargo->direccion_vialidad_nombre;
        $encargo_colonia = $func->encargo->direccion_encargo_colonia;

        $encargo_dom =  $encargo_tvialidad . ' ' . $encargo_calle . ' # ' . $encargo_numext . ', Int: ' . ($encargo_numint) ?: 'S/N' . ', '
            . $encargo_colonia .  ', ' . $encargo_municipio . ', ' . $encargo_entidad . ', ' . $encargo_pais
            . ' CP: ' .  $encargo_cp;

        $dencargo['direccion'] = $encargo_dom;
        $dencargo['domicilio_obs'] = isset($func->encargo->domicilio_obs) ?: '';


        $bitacora = new BitacoraConsulta([
            'ip_id' => $func->id,
            'created_by' => $idus
        ]);

        $bitacora->save();

        ////datos de otro encargo////////////////////////////////////////////////////////////////////
        $func = json_decode($declaracion['otro_encargo']);
        $oencargo = array();
        if (count($oencargo) > 0) {
            $oencargo['dependencia'] = strtoupper(CatEntePublico::where('id', $func->ente_publico_id)->first()['valor']);
            $oencargo['cargo'] = ($func->empleo_cargo_comision); //cargo
            $oencargo['area'] = ($func->area_adscripcion); //cargo
            $oencargo['ftomaposesion'] =  Carbon::parse($func->fecha_posesion)->format($format);
            $oencargo['baja'] =  Carbon::parse($func->fecha_termino)->format($format);

            $oencargo['nivel_gobierno'] = strtoupper(NivelGobierno::where('id', $func->nivel_gobierno_id)->first()['valor']);
            $oencargo['poder_ente'] = strtoupper(PoderJuridico::where('id', $func->poder_juridico_id)->first()['valor']); //dependencia o ente

            $oencargo['honorarios'] =  ($func->contratado_honorarios == 1 ? 'SI' : 'NO');
            $oencargo['nivel_encargo'] =  ($func->nivel_encargo);

            $funciones = [];
            foreach ($func->funciones as $fun) {
                $fun_ = FuncionesPrincipales::where('id', $fun)->first();
                $funciones[] = $fun_['valor'];
            }
            $funciones = implode(', ', $funciones);
            // FuncionesPrincipales::
            $oencargo['funcionesp'] =  $funciones;

            $oencargo['otras_funciones'] =  ($func->otras_funciones);
            $tel =  ($func->telefono_laboral_numero);
            $ext =  ($func->telefono_laboral_extension);
            $oencargo['telefono'] = $tel;
            if (!empty($ext)) {
                $oencargo['telefono'] = $tel . 'Ext: ' . $ext;
            }
            // Direccion Encargo
            $encargo_pais = ($func->direccion_encargo_pais_id) ? Pais::where('id', $func->direccion_encargo_pais_id)->first()['valor'] : '';
            $encargo_entidad = ($func->direccion_encargo_entidad_federativa_id) ? CatEntidadFederativa::where('id', $func->direccion_encargo_entidad_federativa_id)->first()['nom_ent'] : '';
            $encargo_cp = $func->direccion_encargo_cp;
            $encargo_numext = $func->direccion_encargo_numext;
            $encargo_numint = $func->direccion_encargo_numint;
            $encargo_municipio = ($func->direccion_encargo_municipio_id) ? CatMunicipio::where('id', $func->direccion_encargo_municipio_id)->first()['nom_mun'] : '';
            $encargo_tvialidad = $func->direccion_vialidad_tipo;
            $encargo_calle = $func->direccion_vialidad_nombre;
            $encargo_colonia = $func->direccion_encargo_colonia;

            $encargo_dom =  $encargo_tvialidad . ' ' . $encargo_calle . ' # ' . $encargo_numext . ', Int: ' . $encargo_numint . ', '
                . $encargo_colonia .  ', ' . $encargo_municipio . ', ' . $encargo_entidad . ', ' . $encargo_pais
                . ' CP: ' .  $encargo_cp;

            $oencargo['direccion'] = $encargo_dom;
            $oencargo['domicilio_obs'] = $func->domicilio_obs;
        }

        ///////////////////////////////////////////intereses //////////////////////////////////////////
        //empresas sociedades asociaciones
        $func = json_decode($declaracion['empresas_sociedades_asociaciones']);

        $empresas = array();
        foreach ($func as $empresa) {
            $empresas[] = array(
                'participante' => $empresa->participante,
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'rfc' => $empresa->rfc,
                'porcentaje_participacion' => $empresa->porcentaje_participacion,
                'participacion' => ($empresa->participacion) ? CatTipoParticipacion::where('id', $empresa->participacion)->first()['valor'] : '',
                'remuneracion' => $empresa->remuneracion,
                'monto' => $empresa->monto,
                'domicilio_pais_id' => $empresa->domicilio_pais_id,
                'pais' => ($empresa->domicilio_pais_id) ? Pais::where('id', $empresa->domicilio_pais_id)->first()['valor'] : '',
                'domicilio_entidad_federativa_id' => ($empresa->domicilio_entidad_federativa_id) ? CatEntidadFederativa::where('id', $empresa->domicilio_entidad_federativa_id)->first()['nom_ent'] : '',
                'sector_industria_id' => ($empresa->sector_industria_id) ? CatSectorIndustria::where('id', $empresa->sector_industria_id)->first()['valor'] : '',
                'naturaleza' => $empresa->naturaleza_vinculo_especificar,
                'observaciones' => $empresa->observaciones

            );
        }
        //toma de decisiones

        $func = json_decode($declaracion['toma_decisiones']);

        $tomaDecisiones  = array();
        foreach ($func as $toma) {
            $tomaDecisiones[] =
                array(
                    'participante' => $toma->participante,
                    'tipo_institucion' => ($toma->tipon_id) ? CatTipoInstitucion::where('id', $toma->tipon_id)->first()['valor'] : '',
                    'especifique' => $toma->especifique,
                    'nombre' => $toma->nombre,
                    'rfc' => $toma->rfc,
                    'puesto' => $toma->puesto,
                    'fecha' => Carbon::parse($toma->fecha)->format($format),
                    'remuneracion' => $toma->remuneracion,
                    'monto' => $toma->monto,
                    'domicilio_pais_id' => $toma->pais_id,
                    'pais' => ($toma->pais_id) ? Pais::where('id', $toma->pais_id)->first()['valor'] : '',
                    'domicilio_entidad_federativa_id' => ($toma->entidad_federativa_id) ? CatEntidadFederativa::where('id', $toma->entidad_federativa_id)->first()['nom_ent'] : '',
                    'observaciones' => $toma->observaciones
                );
        }

        //beneficios publicos

        $func = json_decode($declaracion['apoyos_publicos']);

        $beneficiosPubl  = array();
        foreach ($func as $beneficio) {
            $beneficiosPubl[] = array(
                'beneficiario' => ($beneficio->beneficiario) ? CatBeneficiario::where('id', $beneficio->beneficiario)->first()['valor'] : '',
                'programa' => $beneficio->programa,
                'institucion' => $beneficio->institucion_otorgante,
                'nivel_gobierno' => ($beneficio->nivel_orden_gobierno_id) ? NivelGobierno::where('id', $beneficio->nivel_orden_gobierno_id)->first()['valor'] : '',
                'tipo_apoyo_id' => ($beneficio->tipo_apoyo_id) ? CatTipoApoyo::where('id', $beneficio->tipo_apoyo_id)->first()['valor'] : '',
                'especificar_tipo_apoyo' => $beneficio->especificar_tipo_apoyo,
                'forma' => $beneficio->forma,
                'monto' => $beneficio->monto,
                'especifique' => $beneficio->especificar_apoyo,
                'observaciones' => $beneficio->observaciones

            );
        }

        //representación activa

        $func = json_decode($declaracion['representacion_activa']);

        $reprActiva  = array();
        foreach ($func as $rep) {
            $reprActiva[] = array(
                'representante' => $rep->representante,
                'tipo_representacion_id' => $rep->tipo_representacion_id,
                'especifique' => $rep->especifique,
                'fecha' =>  Carbon::parse($rep->fecha)->format($format),
                'representado' => $rep->representado,
                'nombre' => $rep->nombre,
                'rfc' => $rep->rfc,
                'remuneracion' => $rep->remuneracion,
                'monto' => $rep->monto,
                'pais_id' => $rep->pais_id,
                'pais' => ($rep->pais_id) ?  Pais::where('id', $rep->pais_id)->first()['valor'] : '',
                'entidad_federativa_id' => ($rep->entidad_federativa_id) ? CatEntidadFederativa::where('id', $rep->entidad_federativa_id)->first()['nom_ent'] : '',
                'sector_industria_id' => ($rep->sector_industria_id) ? CatSectorIndustria::where('id', $rep->sector_industria_id)->first()['valor'] : '',
                'observaciones' => $rep->observaciones
            );
        }
        //clientes principales

        $func = json_decode($declaracion['clientes_principales']);

        $clientesPrinc  = array();
        foreach ($func as $cte) {
            $clientesPrinc[] = array(
                'nombre' => $cte->nombre,
                'rfc' => $cte->rfc,
                'nombre_cliente' => $cte->nombre_cliente,
                'rfc_cliente' => $cte->rfc_cliente,
                'propietario' => $cte->propietario,
                'tipo_persona' => $cte->tipo_persona,
                'sector_industria_id' => ($cte->sector_industria_id) ? CatSectorIndustria::where('id', $cte->sector_industria_id)->first()['valor'] : '',
                'monto' => $cte->monto,
                'pais_id' => $cte->pais_id,
                'pais' => ($cte->pais_id) ? Pais::where('id', $cte->pais_id)->first()['valor'] : '',
                'entidad_federativa_id' => ($cte->entidad_federativa_id) ?  CatEntidadFederativa::where('id', $cte->entidad_federativa_id)->first()['nom_ent'] : '',
                'observaciones' => $cte->monto
            );
        }
        //
        //beneficios privados

        $func = json_decode($declaracion['beneficios_privados']);

        $beneficiosPriv  = array();
        foreach ($func as $benef) {
            $beneficiosPriv[] = array(
                'beneficiario' => $benef->beneficiario,
                'tipo_beneficio' => ($benef->tipo_beneficio) ? CatTipoBeneficio::where('id', $benef->tipo_beneficio)->first()['valor'] : '',
                'especifique' => $benef->especifique,
                'nombre' => $benef->nombre,
                'rfc' => $benef->rfc,
                'otorgante' => $benef->otorgante,
                'forma' => $benef->forma,
                'beneficio' => $benef->beneficio,
                'monto' => $benef->monto,
                'tipo_moneda' => ($benef->tipo_moneda) ? Moneda::where('id', $benef->tipo_moneda)->first()['moneda'] : '',
                'sector_industria_id' => ($benef->sector_industria_id) ? CatSectorIndustria::where('id', $benef->sector_industria_id)->first()['valor'] : '',
                'observaciones' => $benef->observaciones
            );
        }
        //fideicomisos

        $func = json_decode($declaracion['fideicomisos']);

        $fideicomisos  = array();
        foreach ($func as $fid) {
            $fideicomisos[] = array(
                'participante' => $fid->participante,
                'tipo_fideicomiso_id' => ($fid->tipo_fideicomiso_id) ? CatTipoFideicomisos::where('id', $fid->tipo_fideicomiso_id)->first()['valor'] : '',
                'tipo_participacion_id' => ($fid->tipo_participacion_id) ? CatTipoFideicomisosPart::where('id', $fid->tipo_participacion_id)->first()['valor'] : '',
                'rfc_fideicomiso' => $fid->rfc_fideicomiso,
                'fideicomitente' => $fid->fideicomitente,
                'nombre' => $fid->nombre_fideicomitente,
                'rfc' => $fid->rfc_fideicomitente,
                'nombre_fiduciario' => $fid->nombre_fiduciario,
                'rfc_fiduciario' => $fid->rfc_fiduciario,
                'fideicomisario' => $fid->fideicomisario,
                'nombre_fideicomisario' => $fid->nombre_fideicomisario,
                'rfc_fideicomisario' => $fid->rfc_fideicomisario,
                'sector_industria_id' => ($fid->sector_industria_id) ? CatSectorIndustria::where('id', $fid->sector_industria_id)->first()['valor'] : '',
                'pais_id' => $fid->pais_id,
                'pais' => ($fid->pais_id) ? Pais::where('id', $fid->pais_id)->first()['valor'] : '',
                'observaciones' => $fid->observaciones
            );
        }





        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array)$func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público ',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']), 2)
        );

        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'], // RFC
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Por enajenación de bienes (Después de impuestos)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        );
        // $otross[] = array(
        //     'cat' => '5 Otros ingresos no considerados a los anteriores (Después de impuestos)',
        //     'nombre' => $sueldo['arrendamiento'],
        //     'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        // );
        $otross[] = array(
            'cat' => '5 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']), 2)
        );

        // [{"id":19752,"razon_social":null,"razon_social_cantidad":"0","actividad_financiera":"Ventas","actividad_financiera_cantidad":"90000","servicios_profecionales":"Aseorias","servicios_profecionales_cantidad":"30000","arrendamiento":"Consultor\u00eda","arrendamiento_cantidad":"10000","neto_declarante":null,"neto_declarante_cantidad":"0","fecha_fin_periodo":null,"fecha_inicio_periodo":null,"ant_sub_12":"579000","ant_otros_ingresos":null,"ant_otros_ingresos_cantidad":"0","ant_actividad_comercial":"Mi Papeler\u00eda","ant_actividad_comercial_cantidad":"180000","ant_servicios_profecionales":"Consultor\u00eda TI","ant_servicios_profecionales_cantidad":"27000","ant_otros":null,"ant_otros_cantidad":"500999","neto_conyugue":"Serv. Prof.","neto_conyugue_cantidad":"59990","ant_neto_declarante":"0","ant_neto_declarante_cantidad":"0","ant_neto_conyugue":"Servicios profesionales","ant_neto_conyugue_cantidad":"330000","ip_id":25374,"sub_1":"20000","ant_actividad_financiera":"2","ant_actividad_financiera_cantidad":"60000","ant_arrendamientos":null,"ant_arrendamientos_cantidad":"50000","bandera":"1","tiponegocio":"Papeler\u00eda","otro_ant_actividad_financiera":null,"otros_ingresos":"Herencia de tal familiar","tiponegocioactividad":null,"tipo_bien_enajenacion":3}]

        ///TOTAL INGRESOS
        $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']) +
            (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
            (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']));

        ///

        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2),
            'fecha_inicio_periodo' => Carbon::parse($sueldo['fecha_inicio_periodo'])->format($format),
            'fecha_fin_periodo' => Carbon::parse($sueldo['fecha_fin_periodo'])->format($format),
        );

        // $datosDeclaracion['tipo_declaracion_id']

        // if ($datosDeclaracion['tipo_declaracion_id'] == 1) {
        // } else {
        // }

        //////////otros sueldos
        if (!empty($sueldo['tipo_bien_enajenacion'])) {
            $arrendamiento =  CatTipoBienEnajenacion::where('id', $sueldo['tipo_bien_enajenacion'])->first()['valor'];
        } else {
            $arrendamiento = $sueldo['ant_arrendamientos'];
        }

        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );

        $actividad_fin = '';
        if (!empty($sueldo['ant_actividad_financiera'])) {
            if (is_numeric($sueldo['ant_actividad_financiera'])) {
                $ant_a_f = (int)$sueldo['ant_actividad_financiera'];
                if (($ant_a_f < 1) && ($ant_a_f <= 7)) {
                    $actividad_fin = CatTipoInstrumento::where('id', $ant_a_f)->first()['valor'];
                } else {
                    $actividad_fin = $sueldo['ant_actividad_financiera'];
                }
            } else {
                $actividad_fin = $sueldo['ant_actividad_financiera'];
            }
        }
        // ($sueldo['ant_actividad_financiera']) ? CatTipoInstrumento::where('id', $sueldo['ant_actividad_financiera'])->first()['valor'] : ''

        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $actividad_fin,
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '4 Por enajenación de bienes (Después de impuestos)',
            'nombre' => $arrendamiento,
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_arrendamientos_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '5 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        );
        if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '6 Otros ingresos no considerados a los anteriores (Después de impuestos)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        }

        ///TOTAL INGRESOS ANT
        $totalingresosant = array();
        $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']) +
            (float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_arrendamientos_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_otros_cantidad']));

        ///
        ///////////////////////////////////////activos
        //bienes inmuebles

        $func = json_decode($declaracion['bienes_inmuebles']);

        $binmuebles = array();
        foreach ($func as $inmueble) {

            // Direccion inmueble
            $inmueble_pais =  ($inmueble->domicilio_bien_pais_id) ? Pais::where('id', $inmueble->domicilio_bien_pais_id)->first()['valor'] : '';
            $inmueble_entidad =  ($inmueble->domicilio_bien_entidad_federativa_id) ? CatEntidadFederativa::where('id', $inmueble->domicilio_bien_entidad_federativa_id)->first()['nom_ent'] : '';
            $inmueble_cp = $inmueble->domicilio_bien_cp;
            $inmueble_numext = $inmueble->domicilio_bien_numext;
            $inmueble_numint = $inmueble->domicilio_bien_numint;
            $inmueble_municipio =  ($inmueble->domicilio_bien_municipio_id) ? CatMunicipio::where('id', $inmueble->domicilio_bien_municipio_id)->first()['nom_mun'] : '';
            $inmueble_tvialidad = $inmueble->domicilio_bien_vialidad_tipo;
            $inmueble_calle = $inmueble->domicilio_bien_vialidad_nombre;
            $inmueble_colonia = $inmueble->domicilio_bien_colonia;

            $inmueble_dom =  $inmueble_tvialidad . ' ' . $inmueble_calle . ' # ' . $inmueble_numext . ', Int: ' . $inmueble_numint . ', '
                . $inmueble_colonia .  ', ' . $inmueble_municipio . ', ' . $inmueble_entidad . ', ' . $inmueble_pais
                . ' CP: ' .  $inmueble_cp;

            if ($inmueble->precio_adquisicion_moneda_id) {
                $padquisicion_moneda =   Moneda::where('id', $inmueble->precio_adquisicion_moneda_id)->first();
            }
            if (empty($padquisicion_moneda)) {
                $padquisicion_moneda = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }
            $tercero = null;
            if (isset($inmueble->tercero)) {
                $tercero = ($inmueble->tercero == 1) ? 'Física' : 'Moral';
            }
            $binmuebles[] = array(
                'tipoOperacion' => ($inmueble->tipo_operacion_id) ? CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'] : '',
                'naturaleza' => ($inmueble->tipo_bien_id) ? CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'] : '',
                'especifique_inmueble' => isset($inmueble->especifique_inmueble) ?: '',
                'titular' => $inmueble->titular_id,
                'porcentaje_propiedad' => $inmueble->porcentaje_propiedad . ' %',
                'superficie_terreno' => $inmueble->superficie_terreno,
                'superficie_construccion' => $inmueble->superficie_construccion,
                'tercero' =>  $tercero,
                'nombre_tercero' => isset($inmueble->nombre_tercero) ?: '',
                'rfc_tercero' => isset($inmueble->rfc_tercero) ?: '',
                'forma_adquisicion' => ($inmueble->forma_adquisicion_id) ? CatFormaAdquisicion::where('id', $inmueble->forma_adquisicion_id)->first()['valor'] : '',
                'forma_pago' => isset($inmueble->forma_pago) ? CatFormaPago::where('id', $inmueble->forma_pago)->first()['valor'] : '',
                'transmisor' => isset($inmueble->transmisor) ? (($inmueble->transmisor == 1) ? 'Física' : 'Moral') : '',
                'nombre_transmisor' => isset($inmueble->nombre_denominacion_quien_adquirio) ?: '',
                'rfc_transmisor' => isset($inmueble->rfc_quien_adquirio) ?: '',
                'relacion_transmisor' => $inmueble->relacion_persona_adquirio_id ? CatRelacionTransmisor::where('id', $inmueble->relacion_persona_adquirio_id)->first()['valor'] : '',
                'relacion_especificar' => isset($inmueble->relacion_especificar) ?: '',
                'padquisicion' => $padquisicion_moneda['signo']  . ' ' .  number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2) . ' / ' . $padquisicion_moneda['moneda'],
                'valor_conforme' => isset($inmueble->valor_conforme) ? CatValorInmueble::where('id', $inmueble->valor_conforme)->first()['valor'] : '',
                'fecha_adquisicion' =>   Carbon::parse($inmueble->fecha_adquisicion)->format($format),
                'numero_registro_publico' =>  isset($inmueble->numero_registro_publico) ?: '',
                'motivo_baja' => isset($inmueble->motivo_baja) ? CatBajaInmueble::where('id', $inmueble->motivo_baja)->first()['valor'] : '',
                'especifique_baja' =>  isset($inmueble->especifique_baja) ?: '',
                'observaciones' =>  isset($inmueble->observaciones) ?: '',
                'domicilio' => $inmueble_dom,
            );
        }


        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {

            $pais = $mueble->lugar_registro_pais_id;
            $entidad = $mueble->lugar_registro_entidad_id;
            if ($pais == 150) {
                $ubicacion = 'México, ' . CatEntidadFederativa::where('id', $entidad)->first()['nom_ent'];
            } else {
                $ubcacion = Pais::where('id', $pais)->first()['valor'];
            }

            if ($mueble->precio_adquisicion_moneda_id) {
                $padquisicion_moneda =   Moneda::where('id', $mueble->precio_adquisicion_moneda_id)->first();
            }
            if (empty($padquisicion_moneda)) {
                $padquisicion_moneda = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }
            $tercero = null;
            if (!empty($mueble->tercero)) {
                $tercero = ($mueble->tercero == 1) ? 'Física' : 'Moral';
            }
            $bmuebles[] = array(
                // 'tipoOperacion' => ($mueble->tipo_operacion_id) ? CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'] : '',
                'tipoBien' => ($mueble->tipo_bien_id) ? CatTipoBienMueble::where('id', $mueble->tipo_bien_id)->first()['valor'] : '',
                'especifique' => $mueble->tipo_bien_especificar,
                'titular' => $mueble->titular_id,
                'tercero' =>  $tercero,
                'nombre_tercero' => (!empty($mueble->nombre_tercero)) ? $mueble->nombre_tercero : '',
                'rfc_tercero' => (!empty($mueble->rfc_tercero)) ? $mueble->rfc_tercero : '',
                'transmisor' => ((!empty($mueble->transmisor)) && $mueble->transmisor == 1) ? 'Física' : 'Moral',
                'nombre_transmisor' => $mueble->nombre_denominacion_adquirio,
                'rfc_transmisor' => $mueble->rfc_quien_adquirio,
                'relacion_transmisor' => $mueble->relacion_persona_adquirio_id ? CatRelacionTransmisor::where('id', $mueble->relacion_persona_adquirio_id)->first()['valor'] : '',
                'relacion_especificar' => $mueble->relacion_especificar,
                'marca' => $mueble->marca,
                'modelo' => $mueble->modelo,
                'anio' => (!empty($mueble->anio)) ? $mueble->anio : '',
                'numero_serie' => $mueble->numero_serie,
                'forma_adquisicion' => (!empty($mueble->forma_adquisicion)) ? CatFormaAdquisicion::where('id', $mueble->forma_adquisicion)->first()['valor'] : '',
                'forma_pago' => (!empty($mueble->forma_pago)) ? CatFormaPago::where('id', $mueble->forma_pago)->first()['valor'] : '',
                'padquisicion' =>  $padquisicion_moneda['signo']  . ' ' .  number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2) . ' / ' . $padquisicion_moneda['moneda'],
                'fecha_adquisicion' => (!empty($mueble->fecha_adquisicion)) ? Carbon::parse($mueble->fecha_adquisicion)->format($format) : '',
                'motivo_baja' => (!empty($mueble->motivo_baja)) ? CatBajaInmueble::where('id', $mueble->motivo_baja)->first()['valor'] : '',
                'especifique_baja' => (!empty($mueble->especifique_baja)) ? $mueble->especifique_baja : '',
                'ubicacion' =>  $ubicacion,
                'observaciones' => (!empty($mueble->observaciones)) ? $mueble->observaciones : '',
            );
        }
        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            if ($mueble->precio_adquisicion_moneda_id) {
                $padquisicion_moneda =   Moneda::where('id', $mueble->precio_adquisicion_moneda_id)->first();
            }
            if (empty($padquisicion_moneda)) {
                $padquisicion_moneda = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }
            $tercero = null;
            if (!empty($mueble->tercero)) {
                $tercero = ($mueble->tercero == 1) ? 'Física' : 'Moral';
            }

            $bmueblesnr[] = array(
                // 'tipoOperacion' => ($mueble->tipo_operacion_id) ? CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'] : '',
                'tipoBien' => ($mueble->tipo_bien_id) ? CatTipoBienMuebleNr::where('id', $mueble->tipo_bien_id)->first()['valor'] : '',
                'especifique' => (!empty($mueble->especifique_bien)) ? $mueble->especifique_bien : '',
                'descripcion' => (!empty($mueble->descripcion)) ? $mueble->descripcion : '',
                'titular' => (!empty($mueble->titular_id)) ? $mueble->titular_id : '',
                'tercero' => (!empty($mueble->tercero)) ? $tercero : ' ',
                'nombre_tercero' => (!empty($mueble->nombre_tercero)) ? $mueble->nombre_tercero : '',
                'rfc_tercero' => (!empty($mueble->rfc_tercero)) ? $mueble->rfc_tercero : '',
                'transmisor' => (!empty($mueble->transmisor) && $mueble->transmisor == 1) ? 'Física' : 'Moral',
                'nombre_transmisor' => (!empty($mueble->nombre_transmisor)) ? $mueble->nombre_transmisor : '',
                'rfc_transmisor' => (!empty($mueble->rfc_transmisor)) ? $mueble->rfc_transmisor : '',
                'relacion_transmisor' => (!empty($mueble->relacion_persona_adquirio_id)) ? CatRelacionTransmisor::where('id', $mueble->relacion_persona_adquirio_id)->first()['valor'] : '',
                'relacion_especificar' => (!empty($mueble->relacion_especificar)) ? $mueble->relacion_especificar : '',
                'forma_adquisicion' => (!empty($mueble->forma_adquisicion_id)) ? CatFormaAdquisicion::where('id', $mueble->forma_adquisicion_id)->first()['valor'] : '',
                'forma_pago' => (!empty($mueble->forma_pago)) ? CatFormaPago::where('id', $mueble->forma_pago)->first()['valor'] : '',
                'padquisicion' =>  $padquisicion_moneda['signo']  . ' ' .  number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2) . ' / ' . $padquisicion_moneda['moneda'],
                'fecha_adquisicion' => (!empty($mueble->fecha_adquisicion)) ? Carbon::parse($mueble->fecha_adquisicion)->format($format) : '',
                'motivo_baja' => (!empty($mueble->motivo_baja)) ? CatBajaInmueble::where('id', $mueble->motivo_baja)->first()['valor'] : '',
                'especifique_baja' => (!empty($mueble->especifique_baja)) ? $mueble->especifique_baja : '',
                'observaciones' => (!empty($mueble->observaciones)) ?  $mueble->observaciones : '',
            );
        }

        $func = json_decode($declaracion['inversiones_cuentas_valores']);

        $inversiones = array();
        foreach ($func as $dato) {
            $tercero = null;
            if (!empty($dato->tercero)) {
                $tercero = ($dato->tercero == 1) ? 'Física' : 'Moral';
            }

            $inv_desc = "";

            if ($dato->tipo_inversion_id == 1) {
                // $dato->tipo_inversion_id : 1  (Bancaria)
                $inv_desc = (!empty($dato->bancaria_id)) ? CatBancaria::where('id', $dato->bancaria_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 2) {
                // $dato->tipo_inversion_id : 2  (Valores bursátiles)
                $inv_desc = (!empty($dato->valores_id)) ? CatTipoValores::where('id', $dato->valores_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 3) {
                // $dato->tipo_inversion_id : 3  (Fondos de inversión)
                $inv_desc = (!empty($dato->fondo_inversion_id)) ? CatFondoInversion::where('id', $dato->fondo_inversion_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 4) {
                // $dato->tipo_inversion_id : 4  (Organizaciones)
                $inv_desc = (!empty($dato->organizacionespym_id)) ? CatOrganizacionpym::where('id', $dato->organizacionespym_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 5) {
                // $dato->tipo_inversion_id : 5  (Posesión de monedas)
                $inv_desc = (!empty($dato->moneda_metales_id)) ? CatTipoMonedaMetales::where('id', $dato->moneda_metales_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 6) {
                // $dato->tipo_inversion_id : 6  (Seguros)
                $inv_desc = (!empty($dato->seguro_id)) ? CatTipoMonedaMetales::where('id', $dato->seguro_id)->first()['valor'] : '';
            }
            if ($dato->tipo_inversion_id == 7) {
                // $dato->tipo_inversion_id : 7  (Afores y otros)
                $inv_desc = (!empty($dato->afore_id)) ? CatAfore::where('id', $dato->afore_id)->first()['valor'] : '';
            }

            if (!empty($dato->moneda_id)) {
                $moriginal =   Moneda::where('id', $dato->moneda_id)->first();
            }

            if (empty($moriginal)) {
                $moriginal = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }

            $inversiones[] = array(
                'tipoInv' => ($dato->tipo_inversion_id) ? CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'] : '',
                'tipo_id' => $dato->tipo_inversion_id,
                'titular' => ($dato->titular_id) ? CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor'] : '',
                'tercero' => $tercero,
                'nombre_tercero' => (!empty($dato->nombre_tercero)) ? $dato->nombre_tercero : '',
                'rfc_tercero' => (!empty($dato->rfc_tercero)) ? $dato->rfc_tercero : '',
                'inversion' => $inv_desc,
                'numero_cuenta' => $dato->numero_cuenta,
                'monto_original' => $moriginal['signo']  . ' ' .  number_format((float)str_replace(',', '', $dato->monto_original), 2) . ' / ' . $moriginal['moneda'],
                'pais' => ($dato->domicilio_institucion_pais_id) ? Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'] : '',
                'nombre_institucion' => $dato->nombre_institucion,
                'rfc_institucion' => $dato->rfc_institucion,
                'observaciones' => $dato->observaciones,
                // 'entidad' => ($dato->domicilio_institucion_entidad_federativa_id) ? CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'] : '',
                // 'tipoOp' => ($dato->tipo_operacion_id) ? CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'] : '',
            );
        }
        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            if ($deuda->cat_tipo_moneda_id) {
                $original =   Moneda::where('id', $deuda->cat_tipo_moneda_id)->first();
            }

            if (empty($original)) {
                $original = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }

            if ($deuda->saldo_cat_tipo_moneda_id) {
                $pendiente =   Moneda::where('id', $deuda->saldo_cat_tipo_moneda_id)->first();
            }

            if (empty($pendiente)) {
                $pendiente = [
                    'moneda' => 'Peso Mexicano',
                    'signo' => '$',
                ];
            }

            $tercero = null;
            if (!empty($deuda->tercero)) {
                $tercero = ($deuda->tercero == 1) ? 'Física' : 'Moral';
            }
            $pasivos[] = array(
                // 'tipoOperacion' => ($deuda->cat_tipo_operacion_id) ? CatTipoOperacion::where('id', $deuda->cat_tipo_operacion_id)->first()['valor'] : '',
                'titular' => (!empty($deuda->titular)) ? CatTitularAdeudo::where('id', $deuda->titular)->first()['valor'] : '',
                'tercero' => $tercero,
                'nombre_tercero' => (!empty($deuda->nombre_tercero)) ? $deuda->nombre_tercero : '',
                'rfc_tercero' => (!empty($deuda->rfc_tercero)) ? $deuda->rfc_tercero : '',
                'tipo_adeudo' => (!empty($deuda->cat_tipo_adeudo_id)) ?  CatTipoAdeudo::where('id', $deuda->cat_tipo_adeudo_id)->first()['valor'] : '',
                'especifique' => (!empty($deuda->especifique)) ? $deuda->especifique : '',
                'identificador' => (!empty($deuda->identificador_deuda)) ? $deuda->identificador_deuda : '',
                'fecha' => (!empty($deuda->fecha_adeudo)) ? Carbon::parse($deuda->fecha_adeudo)->format($format) : ' ',
                'original' => $original['signo']  . ' ' .  number_format((float)str_replace(',', '', $deuda->monto_original), 2) . ' / ' . $original['moneda'],
                'pendiente' => $pendiente['signo']  . ' ' .  number_format((float)str_replace(',', '',  $deuda->saldo_pendiente), 2) . ' ' . $pendiente['moneda'],
                'otorgante' => (!empty($deuda->otorgante) && $deuda->otorgante == 1) ? 'Física' : 'Moral',
                'nombre_acreedor' => (!empty($deuda->nombre_acreedor)) ? $deuda->nombre_acreedor : '',
                'rfc_acreedor' => (!empty($deuda->rfc_acreedor)) ? $deuda->rfc_acreedor : '',
                'pais' => ($deuda->domicilio_acreedor_cat_pais_id) ? Pais::where('id', $deuda->domicilio_acreedor_cat_pais_id)->first()['valor'] : '',
                'observaciones' => (!empty($deuda->observaciones)) ? $deuda->observaciones : '',
            );
        }

        $fiscal = json_decode($declaracion['declaracion_fiscal']);

        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';

        $fechaPresenta = Carbon::parse($datosDeclaracion['Fecha_Dec'])->format($format);



        $func = json_decode($declaracion['prestamo_comodato']);

        $prestamo = array();
        foreach ($func as $pr) {
            // Direccion inmueble
            $pr_pais =  ($pr->pais_id) ? Pais::where('id', $pr->pais_id)->first()['valor'] : '';
            $pr_entidad =  ($pr->entidad_federativa_id) ? CatEntidadFederativa::where('id', $pr->entidad_federativa_id)->first()['nom_ent'] : '';
            $pr_cp = $pr->cp;
            $pr_numext = $pr->numext;
            $pr_numint = $pr->numint;
            $pr_municipio =  ($pr->municipio_id) ? CatMunicipio::where('id', $pr->municipio_id)->first()['nom_mun'] : '';
            $pr_tvialidad = $pr->vialidad_tipo;
            $pr_calle = $pr->vialidad_nombre;
            $pr_colonia = $pr->colonia;

            $pr_dom =  $pr_tvialidad . ' ' . $pr_calle . ' # ' . $pr_numext . ', Int: ' . $pr_numint . ', '
                . $pr_colonia .  ', ' . $pr_municipio . ', ' . $pr_entidad . ', ' . $pr_pais
                . ' CP: ' .  $pr_cp;

            if ($pr->tipo_bien == 1) { //si es un inmueble
                $prestamo[] = array(
                    'tipo' => 'Inmueble',
                    'tipoBien' => ($pr->tipo_inmueble_id) ? CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor'] : '',
                    'especifique' => $pr->especifique,
                    'ubicacion' => $pr_dom,
                    'aclaraciones' => $pr->aclaraciones,
                );
            } else { //si es un vehiculo

                $prestamo[] = array(
                    'tipo' => 'Vehículo',
                    'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor'],
                    'desc' => $pr->marca . ', ' . $pr->modelo . ' ' . $pr->anio,
                    'numero_serie' => $pr->numero_serie,
                    'titular_tipo' => ($pr->titular_tipo == 1) ? 'Física' : 'Moral',
                    'nombre_titular' => $pr->nombre_titular,
                    'rfc' => $pr->rfc,
                    'relacion_titular' => $pr->relacion_titular,
                    'ubicacion' => $pr_pais . ', ' . $pr_entidad,
                    'aclaraciones' => $pr->aclaraciones,
                );
            }
        }


        $view =  \View::make('declaracionCom', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos', 'datosDeclaracion', 'spublicos', 'otross', 'fechaPresenta', 'tomaDecisiones', 'beneficiosPubl', 'reprActiva', 'clientesPrinc', 'beneficiosPriv', 'fideicomisos', 'oencargo', 'prestamo', 'totalingresos', 'totalingresosant'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }

    //Descarga completa desde el historial del servidor público

    public function descargaDec2(Request $request, $id, $idus)
    {
        $format = 'd/m/Y';

        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);
        $idus = $token->uid;



        $dpersonales = array();


        if ($id > 1) {
            $declaracion = BitacoraDeclaracion::where('id', $id)
                ->where('ip_id', $idus)
                ->first(); ///declaracion en tablas
        } else {

            $declaracion = BitacoraDeclaracion::where('ip_id', $idus)
                ->orderBy('id', 'DESC')
                ->first();
        }
        //dd($declaracion);

        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();

        //información personal

        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = Pais::where('id', $func->pais_nacimiento_id)->first()['valor'];
        $dpersonales['entnac'] = CatEntidadFederativa::where('id', $func->entidad_federativa_nacimiento_id)->first()['nom_ent'];
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'];
        $dpersonales['entidad'] = CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'];
        $dpersonales['localidad'] = CatLocalidad::where('id', $func->domicilio_localidad_id)->first()['nom_loc'];
        $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;

        $dpersonales['edoCivil'] = EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'];
        if ($func->regimen_matrimonial_id)
            $dpersonales['regimen'] = RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'];
        //datos curriculares

        $func = json_decode($declaracion['datos_curriculares']);

        // $func =  (object) $func[0];

        $dcurrc = array();

        foreach ($func as $escuela) {

            if ($escuela->lugar_institucion_ext == 1) {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => 'México',
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            } else {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => Pais::where('id', $escuela->lugar_institucion_educativa_pais_id)->first()['valor'],
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            }
        }
        //experiencia laboral
        //dd($dcurrc);
        $func = json_decode($declaracion['experiencia_laboral']);

        $experiencia = array();

        foreach ($func as $trabajo) {
            $experiencia[] = array(
                'ambito' => Ambitos::where('id', $trabajo->ambito_id)->first()['valor'],
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'municipio' => CatMunicipio::where('id', $trabajo->direccion_municipio_id)->first()['nom_mun'],
                'entidad' => CatEntidadFederativa::where('id', $trabajo->direccion_entidad_federativa_id)->first()['nom_ent'],
                'localidad' => CatLocalidad::where('id', $trabajo->direccion_localidad_id)->first()['nom_loc'],
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => CatSectorIndustria::where('id', $trabajo->sector_industria_id)->first()['valor'],
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format)
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = json_decode($declaracion['dependientes_economicos']);

        $dependientes = array();

        foreach ($func as $dependiente) {
            $dependientes[] = array(
                'tipo_r' =>  CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor'],
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);



        ////datos de otro encargo////////////////////////////////////////////////////////////////////
        $func = json_decode($declaracion['otro_encargo']);
        $oencargo = array();
        if (count($oencargo) > 0) {
            $oencargo['dependencia'] = strtoupper(CatEntePublico::where('id', $func->ente_publico_id)->first()['valor']);
            $oencargo['cargo'] = ($func->empleo_cargo_comision); //cargo
            $oencargo['area'] = ($func->area_adscripcion); //cargo
            $oencargo['ftomaposesion'] =  Carbon::parse($func->fecha_posesion)->format($format);
            $oencargo['baja'] =  Carbon::parse($func->fecha_termino)->format($format);
        }



        // $bitacora = new BitacoraConsulta([
        //     'ip_id' => $func->id,
        //     'created_by' => $idus
        // ]);

        // $bitacora->save();


        ///////////////////////////////////////////intereses //////////////////////////////////////////
        //empresas sociedades asociaciones
        $func = json_decode($declaracion['empresas_sociedades_asociaciones']);

        $empresas = array();
        foreach ($func as $empresa) {
            $empresas[] = array(
                'participante' => $empresa->participante,
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'porcentaje_participacion' => $empresa->porcentaje_participacion,
                'participacion' => CatTipoParticipacion::where('id', $empresa->participacion)->first()['valor'],
                'remuneracion' => $empresa->remuneracion,
                'monto' => $empresa->monto,
                'domicilio_pais_id' => $empresa->domicilio_pais_id,
                'pais' => Pais::where('id', $empresa->domicilio_pais_id)->first()['valor'],
                'domicilio_entidad_federativa_id' => CatEntidadFederativa::where('id', $empresa->domicilio_entidad_federativa_id)->first()['nom_ent'],
                'sector_industria_id' => CatSectorIndustria::where('id', $empresa->sector_industria_id)->first()['valor'],
                'naturaleza' => $empresa->naturaleza_vinculo_especificar,
                'observaciones' => $empresa->observaciones
            );
        }
        //toma de decisiones

        $func = json_decode($declaracion['toma_decisiones']);

        $tomaDecisiones  = array();
        foreach ($func as $toma) {
            $tomaDecisiones[] =
                array(
                    'participante' => $toma->participante,
                    'tipo_institucion' => CatTipoInstitucion::where('id', $toma->tipon_id)->first()['valor'],
                    'nombre' => $toma->nombre,
                    'rfc' => $toma->rfc,
                    'puesto' => $toma->puesto,
                    'fecha' => Carbon::parse($toma->fecha)->format($format),
                    'remuneracion' => $toma->remuneracion,
                    'monto' => $toma->monto,
                    'domicilio_pais_id' => $toma->pais_id,
                    'pais' => Pais::where('id', $toma->pais_id)->first()['valor'],
                    'domicilio_entidad_federativa_id' => CatEntidadFederativa::where('id', $toma->entidad_federativa_id)->first()['nom_ent'],
                    'observaciones' => $toma->observaciones
                );
        }

        //beneficios publicos

        $func = json_decode($declaracion['apoyos_publicos']);

        $beneficiosPubl  = array();
        foreach ($func as $beneficio) {
            $beneficiosPubl[] = array(
                'beneficiario' => CatBeneficiario::where('id', $beneficio->beneficiario)->first()['valor'],
                'programa' => $beneficio->programa,
                'institucion' => $beneficio->institucion_otorgante,
                'nivel_gobierno' => NivelGobierno::where('id', $beneficio->nivel_orden_gobierno_id)->first()['valor'],
                'tipo_apoyo_id' => CatTipoApoyo::where('id', $beneficio->tipo_apoyo_id)->first()['valor'],
                'forma' => $beneficio->forma,
                'monto' => $beneficio->monto,
                'observaciones' => $beneficio->observaciones

            );
        }

        //representación activa

        $func = json_decode($declaracion['representacion_activa']);

        $reprActiva  = array();
        foreach ($func as $rep) {
            $reprActiva[] = array(
                'representante' => $rep->representante,
                'tipo_representacion_id' => $rep->tipo_representacion_id,
                'fecha' =>  Carbon::parse($rep->fecha)->format($format),
                'representado' => $rep->representado,
                'nombre' => $rep->nombre,
                'rfc' => $rep->rfc,
                'remuneracion' => $rep->remuneracion,
                'monto' => $rep->monto,
                'pais_id' => $rep->pais_id,
                'pais' => Pais::where('id', $rep->pais_id)->first()['valor'],
                'entidad_federativa_id' => CatEntidadFederativa::where('id', $rep->entidad_federativa_id)->first()['nom_ent'],
                'sector_industria_id' => CatSectorIndustria::where('id', $rep->sector_industria_id)->first()['valor'],
                'observaciones' => $rep->observaciones
            );
        }
        //clientes principales

        $func = json_decode($declaracion['clientes_principales']);

        $clientesPrinc  = array();
        foreach ($func as $cte) {
            $clientesPrinc[] = array(
                'nombre' => $cte->nombre_cliente,
                'rfc' => $cte->rfc_cliente,
                'propietario' => $cte->propietario,
                'tipo_persona' => $cte->tipo_persona,
                'sector_industria_id' => CatSectorIndustria::where('id', $cte->sector_industria_id)->first()['valor'],
                'monto' => $cte->monto,
                'pais_id' => $cte->pais_id,
                'pais' => Pais::where('id', $cte->pais_id)->first()['valor'],
                'entidad_federativa_id' => CatEntidadFederativa::where('id', $cte->entidad_federativa_id)->first()['nom_ent'],
                'observaciones' => $cte->observaciones
            );
        }
        //
        //beneficios privados

        $func = json_decode($declaracion['beneficios_privados']);

        $beneficiosPriv  = array();
        foreach ($func as $benef) {
            $beneficiosPriv[] = array(
                'nombre' => $benef->nombre,
                'rfc' => $benef->rfc,
                'otorgante' => $benef->otorgante,
                'forma' => $benef->forma,
                'monto' => $benef->monto,
                'tipo_moneda' => Moneda::where('id', $benef->tipo_moneda)->first()['valor'],
                'sector_industria_id' => CatSectorIndustria::where('id', $benef->sector_industria_id)->first()['valor'],
                'observaciones' => $benef->observaciones
            );
        }
        //fideicomisos

        $func = json_decode($declaracion['fideicomisos']);

        $fideicomisos  = array();
        foreach ($func as $fid) {
            $fideicomisos[] = array(
                'participante' => $fid->participante,
                'tipo_fideicomiso_id' => CatTipoFideicomisos::where('id', $fid->tipo_fideicomiso_id)->first()['valor'],
                'tipo_participacion_id' => CatTipoFideicomisosPart::where('id', $fid->tipo_participacion_id)->first()['valor'],
                'rfc_fideicomiso' => $fid->rfc_fideicomiso,
                'fideicomitente' => $fid->fideicomitente,
                'nombre' => $fid->nombre_fideicomitente,
                'rfc' => $fid->rfc_fideicomitente,
                'nombre_fiduciario' => $fid->nombre_fiduciario,
                'rfc_fiduciario' => $fid->rfc_fiduciario,
                'fideicomisario' => $fid->fideicomisario,
                'nombre_fideicomisario' => $fid->nombre_fideicomisario,
                'rfc_fideicomisario' => $fid->rfc_fideicomisario,
                'sector_industria_id' => CatSectorIndustria::where('id', $fid->sector_industria_id)->first()['valor'],
                'pais_id' => $fid->pais_id,
                'pais' => Pais::where('id', $fid->pais_id)->first()['valor'],
                'observaciones' => $fid->observaciones
            );
        }



        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array) $func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración mensual neta del declarante por su cargo público ',
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']), 2)
        );


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['razon_social_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']), 2)
        );
        $otross[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' =>  '$' . number_format((float)str_replace(',', '', $sueldo['arrendamiento_cantidad']), 2)
        );

        ///TOTAL INGRESOS
        $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']) +
            (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
            (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']));

        ///

        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2)
        );


        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
            'nombre' => $sueldo['ant_neto_conyugue'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
        );

        if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        }

        ///TOTAL INGRESOS ANT
        $totalingresosant = array();
        $totalingresosant['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']) +
            (float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['ant_otros_cantidad']));

        ///

        ///////////////////////////////////////activos
        //bienes inmuebles


        $func = json_decode($declaracion['bienes_inmuebles']);


        $binmuebles = array();
        foreach ($func as $inmueble) {
            $binmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'],
                'naturaleza' => CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'],
                'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2)
            );
        }
        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {
            $bmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMueble::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2),
                'marca' => $mueble->marca,
                'modelo' => $mueble->modelo,
            );
        }
        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            $bmueblesnr[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMuebleNr::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }

        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'],
                'entidad' => CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'],
                'tipoOp' => CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'],
                'tipoInv' => CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'],
                'titular' => CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor']

            );
        }

        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $deuda->cat_tipo_operacion_id)->first()['valor'],
                'tipoAdeudo' => CatTipoAdeudo::where('id', $deuda->cat_tipo_adeudo_id)->first()['valor'],
                'acreedor' => $deuda->nombre_acreedor,
                'identificador' => $deuda->identificador_deuda,
                'original' => '$' . number_format((float)str_replace(',', '', $deuda->monto_original), 2),
                'pendiente' => '$' . number_format((float)str_replace(',', '', $deuda->saldo_pendiente), 2),
                'fadeudo' => Carbon::parse($deuda->fecha_adeudo)->format($format)

            );
        }


        $func = json_decode($declaracion['prestamo_comodato']);

        $prestamo = array();
        foreach ($func as $pr) {
            if ($pr->tipo_inmueble_id) { //si es un inmueble
                $prestamo[] = array(
                    'tipo' => 'Inmueble',
                    'tipoBien' => CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor'],
                    //'desc'=> $pr->vehiculo.' '.$pr->marca.', '.$pr->modelo.' '.$pr->anio
                    'desc' => $pr->vialidad_nombre . ' ' . $pr->colonia . ', ' . $pr->numext

                );
            } else { //si es un vehiculo
                $prestamo[] = array(
                    'tipo' => 'Vehículo',
                    'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor'],
                    //'desc'=> $pr->vialidad_nombre.' '.$pr->colonia.', '.$pr->numext
                    'desc' => $pr->marca . ', ' . $pr->modelo . ' ' . $pr->anio
                    //$pr->vehiculo . ' ' .

                );
            }
        }




        $fiscal = json_decode($declaracion['declaracion_fiscal']);

        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';


        $fechaPresenta = Carbon::parse($datosDeclaracion['Fecha_Dec'])->format($format);

        $view =  \View::make('declaracionComPriv', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos', 'datosDeclaracion', 'spublicos', 'otross', 'fechaPresenta', 'tomaDecisiones', 'beneficiosPubl', 'reprActiva', 'clientesPrinc', 'beneficiosPriv', 'fideicomisos', 'oencargo', 'prestamo', 'totalingresos', 'totalingresosant'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }

    // public function descargaDecPublica($id, $idus)
    public function descargaDecPublica(Request $request, $id)
    {
        $format = 'd/m/Y';

        $id1 = base64_decode($id);

        //dd($id1);
        //   $dec = DB::table('declaraciones')
        //    ->select('id_ip')
        // ->where('declaraciones.id',$id)->first();

        //$id = $dec->id_ip;

        //  $func = InformacionPersonal::where('id', $id)->first();

        /* $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
            $token = Token::decodeToken($auth);
            $idus = $token->uid; */

        $dpersonales = array();

        $declaracion = BitacoraDeclaracion::where('declaracion_id', $id1)
            ->first(); ///declaracion en tablas

        /* if($id>1){
                $declaracion = BitacoraDeclaracion::where('id', $id)
                     ->where('ip_id',$idus)
                    ->first(); ///declaracion en tablas
            }
            else{

               $declaracion = BitacoraDeclaracion::where('ip_id', $idus)
                             ->orderBy('id','DESC')
                             ->first();

            } */



        //dd($declaracion);

        $datosDeclaracion = Declaracion::where('id', $declaracion['declaracion_id'])->first();

        //información personal

        $func = json_decode($declaracion['informacion_personal']);

        $func = (object) $func[0];

        $dpersonales['nombres'] = strtoupper($func->nombres . ' ' . $func->primer_apellido . ' ' . $func->segundo_apellido . '');
        $dpersonales['curp'] = $func->curp;
        $dpersonales['rfc'] = $func->rfc;
        $dpersonales['paisnac'] = Pais::where('id', $func->pais_nacimiento_id)->first()['valor'];
        $dpersonales['entnac'] = CatEntidadFederativa::where('id', $func->entidad_federativa_nacimiento_id)->first()['nom_ent'];
        $dpersonales['ce_personal'] = $func->correo_electronico_personal;
        $dpersonales['ce_laboral'] = $func->correo_electronico_laboral;
        $dpersonales['tel_particular'] = $func->telefono_particular;
        $dpersonales['cp'] = $func->domicilio_cp;
        $dpersonales['numext'] = $func->domicilio_numext;
        $dpersonales['numint'] = $func->domicilio_numint;
        $dpersonales['colonia'] = $func->domicilio_colonia;
        $dpersonales['municipio'] = CatMunicipio::where('id', $func->domicilio_municipio_id)->first()['nom_mun'];
        $dpersonales['entidad'] = CatEntidadFederativa::where('id', $func->domicilio_entidad_federativa_id)->first()['nom_ent'];
        //$dpersonales['localidad']= CatLocalidad::where('id',$func->domicilio_localidad_id)->first()['nom_loc'];
        $dpersonales['tvialidad'] = $func->domicilio_vialidad_tipo;
        $dpersonales['calle'] = $func->domicilio_vialidad_nombre;

        $dpersonales['edoCivil'] = EstadoCivil::where('id', $func->estado_civil_id)->first()['valor'];
        if ($func->regimen_matrimonial_id)
            $dpersonales['regimen'] = RegimenMat::where('id', $func->regimen_matrimonial_id)->first()['valor'];
        //datos curriculares

        $func = json_decode($declaracion['datos_curriculares']);

        // $func =  (object) $func[0];

        $dcurrc = array();

        foreach ($func as $escuela) {

            if ($escuela->lugar_institucion_ext == 1) {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => 'México',
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            } else {
                $dcurrc[] = array(
                    'grado' => GradoEscolaridad::where('id', $escuela->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
                    'institucion' => $escuela->institucion_educativa,
                    'entidad' => CatEntidadFederativa::where('id', $escuela->lugar_institucion_educativa_entidad_id)->first()['nom_ent'],
                    'pais' => Pais::where('id', $escuela->lugar_institucion_educativa_pais_id)->first()['valor'],
                    'carrera' => $escuela->carrera,
                    'periodos' => CatPeriodosCursados::where('id', $escuela->periodos_cursados_id)->first()['valor'],
                    'periodosc' => $escuela->periodos_cursados,
                    'estatus' => EstatusCarrera::where('id', $escuela->estatus_id)->first()['estatus'],
                    'doctoOb' => DocumentoObtenido::where('id', $escuela->documento_obtenido_id)->first()['documento'],
                    'cedula' => $escuela->cedula_profesional,
                );
            }
        }
        //experiencia laboral
        //dd($dcurrc);
        $func = json_decode($declaracion['experiencia_laboral']);

        $experiencia = array();

        foreach ($func as $trabajo) {
            $experiencia[] = array(
                'ambito' => Ambitos::where('id', $trabajo->ambito_id)->first()['valor'],
                'nombreInst' => $trabajo->nombre_institucion,
                'area' => $trabajo->unidad_administrativa,
                'cp' => $trabajo->direccion_cp,
                'numext' => $trabajo->direccion_numext,
                'numint' => $trabajo->direccion_numint,
                'municipio' => CatMunicipio::where('id', $trabajo->direccion_municipio_id)->first()['nom_mun'],
                'entidad' => CatEntidadFederativa::where('id', $trabajo->direccion_entidad_federativa_id)->first()['nom_ent'],
                'localidad' => CatLocalidad::where('id', $trabajo->direccion_localidad_id)->first()['nom_loc'],
                'tvialidad' => $trabajo->direccion_vialidad_tipo,
                'calle' => $trabajo->direccion_vialidad_nombre,
                'colonia' => $trabajo->direccion_colonia,
                'sector' => CatSectorIndustria::where('id', $trabajo->sector_industria_id)->first()['valor'],
                'jerarquia' => $trabajo->jerarquia_rango,
                'cargo' => $trabajo->cargo_puesto,
                'fingreso' => Carbon::parse($trabajo->fecha_ingreso)->format($format),
                'fsalida' => Carbon::parse($trabajo->fecha_salida)->format($format)
            );
        }
        //////////////////////////////////dependientes economicos//////////////////////////////////////////

        $func = json_decode($declaracion['dependientes_economicos']);

        $dependientes = array();

        foreach ($func as $dependiente) {
            $dependientes[] = array(
                'tipo_r' =>  CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor'],
                'nombre' => $dependiente->nombres . ' ' . $dependiente->primer_apellido . ' ' . $dependiente->segundo_apellido . '',
                'curp' => $dependiente->curp
            );
        }

        //////////////////////////////////////////////////////////////////////////
        //datos del encargo actual
        $func = json_decode($declaracion['informacion_personal']);

        $func =    (object) $func[0];


        $dencargo = array();
        $dencargo['dependencia'] = strtoupper($func->encargo->ente->valor); //dependencia o ente
        $dencargo['cargo'] = ($func->encargo->empleo_cargo_comision); //cargo
        $dencargo['area'] = ($func->encargo->area_adscripcion); //cargo
        $dencargo['ftomaposesion'] =  Carbon::parse($func->encargo->fecha_posesion)->format($format);
        $dencargo['baja'] =  Carbon::parse($func->encargo->fecha_termino)->format($format);


        /* $bitacora = new BitacoraConsulta([
                'ip_id' => $func->id,
                'created_by' => $idus
            ]);

            $bitacora->save();*/

        ////datos de otro encargo////////////////////////////////////////////////////////////////////
        $func = json_decode($declaracion['otro_encargo']);
        $oencargo = array();
        if (count($oencargo) > 0) {
            $oencargo['dependencia'] = strtoupper(CatEntePublico::where('id', $func->ente_publico_id)->first()['valor']);
            $oencargo['cargo'] = ($func->empleo_cargo_comision); //cargo
            $oencargo['area'] = ($func->area_adscripcion); //cargo
            $oencargo['ftomaposesion'] =  Carbon::parse($func->fecha_posesion)->format($format);
            $oencargo['baja'] =  Carbon::parse($func->fecha_termino)->format($format);
        }




        ///////////////////////////////////////////intereses //////////////////////////////////////////
        //empresas sociedades asociaciones
        $func = json_decode($declaracion['empresas_sociedades_asociaciones']);

        $empresas = array();
        foreach ($func as $empresa) {
            $empresas[] = array(
                'grado' => $empresa->nombre_empresa_sociedad_asociacion,
                'naturaleza' => $empresa->naturaleza_vinculo_especificar
            );
        }
        //toma de decisiones

        $func = json_decode($declaracion['toma_decisiones']);

        $tomaDecisiones  = array();
        foreach ($func as $toma) {
            $tomaDecisiones[] =
                array(
                    'nombre' => $toma->nombre,
                    'puesto' => $toma->puesto
                );
        }

        //beneficios publicos

        $func = json_decode($declaracion['apoyos_publicos']);

        $beneficiosPubl  = array();
        foreach ($func as $beneficio) {
            $beneficiosPubl[] = array(
                'programa' => $beneficio->programa,
                'institucion' => $beneficio->institucion_otorgante
            );
        }

        //representación activa

        $func = json_decode($declaracion['representacion_activa']);

        $reprActiva  = array();
        foreach ($func as $rep) {
            $reprActiva[] = array(
                'nombre' => $rep->nombre,
                'rfc' => $rep->rfc
            );
        }
        //clientes principales

        $func = json_decode($declaracion['clientes_principales']);

        $clientesPrinc  = array();
        foreach ($func as $cte) {
            $clientesPrinc[] = array(
                'nombre' => $cte->nombre_cliente,
                'rfc' => $cte->rfc_cliente
            );
        }
        //
        //beneficios privados

        $func = json_decode($declaracion['beneficios_privados']);

        $beneficiosPriv  = array();
        foreach ($func as $benef) {
            $beneficiosPriv[] = array(
                'nombre' => $benef->nombre,
                'rfc' => $benef->rfc
            );
        }
        //fideicomisos

        $func = json_decode($declaracion['fideicomisos']);

        $fideicomisos  = array();
        foreach ($func as $fid) {
            $fideicomisos[] = array(
                'nombre' => $fid->nombre_fideicomitente,
                'rfc' => $fid->rfc_fideicomitente
            );
        }





        //////////////////////////////////////////ingresos
        //////////sueldos publicos

        $func = json_decode($declaracion['ingresos']);
        //$func =   (object) $func[0];

        $spublicos = array();

        $sueldo = (array)$func[0];

        //dd($sueldo);

        $spublicos[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público ',
            'ingreso' => $sueldo['sub_1']
        );


        //////////otros sueldos
        $otross = array();
        $otross[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifica nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['razon_social'],
            'ingreso' => $sueldo['razon_social_cantidad']
        );
        $otross[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['actividad_financiera'],
            'ingreso' => $sueldo['actividad_financiera_cantidad']
        );
        $otross[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['servicios_profecionales'],
            'ingreso' => $sueldo['servicios_profecionales_cantidad']
        );
        $otross[] = array(
            'cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
            'nombre' => $sueldo['arrendamiento'],
            'ingreso' => $sueldo['arrendamiento_cantidad']
        );

        ///TOTAL INGRESOS
        $totalingresos = array();
        $totalingresos['total'] = '$' . number_format((float)str_replace(',', '', $sueldo['sub_1']) +
            (float)str_replace(',', '', $sueldo['razon_social_cantidad']) +
            (float)str_replace(',', '', $sueldo['actividad_financiera_cantidad']) +
            (float)str_replace(',', '', $sueldo['servicios_profecionales_cantidad']) +
            (float)str_replace(',', '', $sueldo['neto_conyugue_cantidad']) +
            (float)str_replace(',', '', $sueldo['arrendamiento_cantidad']));

        ///
        //////////////////////////////////////////ingresos periodo anterior
        //////////sueldos publicos
        $spublicosant = array();

        $spublicosant[] = array(
            'nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_sub_12']), 2)
        );


        //////////otros sueldos
        $otrossant = array();
        $otrossant[] = array(
            'cat' => '1 Por actividad industrial y/o comercial
              Especifíca nombre o razón social y tipo de negocio',
            'nombre' => $sueldo['ant_actividad_comercial'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_comercial_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
            'nombre' => $sueldo['ant_actividad_financiera'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_actividad_financiera_cantidad']), 2)
        );
        $otrossant[] = array(
            'cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
            'nombre' => $sueldo['ant_servicios_profecionales'],
            'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_servicios_profecionales_cantidad']), 2)
        );
        /* $otrossant[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                              'nombre' => $sueldo['ant_arrendamientos'],
                              'ingreso' => '$'.number_format((float)str_replace(',','',$sueldo['ant_arrendamientos_cantidad']),2)
                            ); */
        /* $otrossant[] = array(
                'cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
                'nombre' => $sueldo['ant_neto_conyugue'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_neto_conyugue_cantidad']), 2)
            ); */
        if ($request->periodo == 2020) {
            $otrossant[] = array(
                'cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
                'nombre' => $sueldo['otros_ingresos'],
                'ingreso' => '$' . number_format((float)str_replace(',', '', $sueldo['ant_otros_cantidad']), 2)
            );
        }


        ///////////////////////////////////////activos
        //bienes inmuebles


        $func = json_decode($declaracion['bienes_inmuebles']);


        $binmuebles = array();
        foreach ($func as $inmueble) {
            $binmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $inmueble->tipo_operacion_id)->first()['valor'],
                'naturaleza' => CatTipoBien::where('id', $inmueble->tipo_bien_id)->first()['valor'],
                'domicilio' => $inmueble->domicilio_bien_vialidad_nombre . ' #' . $inmueble->domicilio_bien_numext . ', ' . $inmueble->domicilio_bien_colonia,
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $inmueble->precio_adquisicion_valor), 2)
            );
        }
        // //bienes inmuebles reg
        $func = json_decode($declaracion['bienes_muebles_registrables']);
        $bmuebles = array();
        foreach ($func as $mueble) {
            $bmuebles[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMueble::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }
        //bienes inmuebles no reg
        $func = json_decode($declaracion['bienes_muebles_no_registrables']);
        $bmueblesnr = array();
        foreach ($func as $mueble) {
            $bmueblesnr[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $mueble->tipo_operacion_id)->first()['valor'],
                'tipoBien' => CatTipoBienMuebleNr::where('id', $mueble->tipo_bien_id)->first()['valor'],
                'padquisicion' =>  '$' . number_format((float)str_replace(',', '', $mueble->precio_adquisicion_valor), 2)
            );
        }

        /////////////////////////////////////// pasivos


        $func = json_decode($declaracion['pasivos_deudas']);

        $pasivos = array();
        foreach ($func as $deuda) {
            $pasivos[] = array(
                'tipoOperacion' => CatTipoOperacion::where('id', $deuda->cat_tipo_operacion_id)->first()['valor'],
                'tipoAdeudo' => CatTipoAdeudo::where('id', $deuda->cat_tipo_adeudo_id)->first()['valor'],
                'acreedor' => $deuda->nombre_acreedor,
                'identificador' => $deuda->identificador_deuda,
                'original' => '$' . number_format((float)str_replace(',', '', $deuda->monto_original), 2),
                'pendiente' => '$' . number_format((float)str_replace(',', '', $deuda->saldo_pendiente), 2),
                'fadeudo' => Carbon::parse($deuda->fecha_adeudo)->format($format)

            );
        }


        $func = json_decode($declaracion['inversiones_cuentas_valores']);


        $inversiones = array();
        foreach ($func as $dato) {
            $inversiones[] = array(
                'numero_cuenta' => $dato->numero_cuenta,
                'nombre_institucion' => $dato->nombre_institucion,
                'monto_original' => '$' . number_format((float)str_replace(',', '', $dato->monto_original), 2),
                'pais' => Pais::where('id', $dato->domicilio_institucion_pais_id)->first()['valor'],
                'entidad' => CatEntidadFederativa::where('id', $dato->domicilio_institucion_entidad_federativa_id)->first()['nom_ent'],
                'tipoOp' => CatTipoOperacion::where('id', $dato->tipo_operacion_id)->first()['valor'],
                'tipoInv' => CatTipoInversion::where('id', $dato->tipo_inversion_id)->first()['valor'],
                'titular' => CatTitularAdeudo::where('id', $dato->titular_id)->first()['valor']

            );
        }

        $fiscal = json_decode($declaracion['declaracion_fiscal']);

        if ($fiscal)
            $resFis = 'Si';
        else
            $resFis = 'No';

        $fechaPresenta = Carbon::parse($datosDeclaracion['Fecha_Dec'])->format($format);



        $func = json_decode($declaracion['prestamo_comodato']);

        $prestamo = array();
        foreach ($func as $pr) {
            if ($pr->tipo_inmueble_id) { //si es un inmueble
                $prestamo[] = array(
                    'tipo' => 'Inmueble',
                    'tipoBien' => CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor'],
                    // 'desc'=> $pr->vehiculo.' '.$pr->marca.', '.$pr->modelo.' '.$pr->anio
                    'desc' => $pr->vialidad_nombre . ' ' . $pr->colonia . ', ' . $pr->numext
                );
            } else { //si es un vehiculo
                $prestamo[] = array(
                    'tipo' => 'Vehículo',
                    'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor'],
                    // 'desc'=> $pr->vialidad_nombre.' '.$pr->colonia.', '.$pr->numext
                    'desc' => $pr->vehiculo . ' ' . $pr->marca . ', ' . $pr->modelo . ' ' . $pr->anio
                );
            }
        }




        $view =  \View::make('declaracion', compact('dpersonales', 'dcurrc', 'experiencia', 'dependientes', 'dencargo',   'empresas', 'spublicosant', 'inversiones', 'otrossant', 'binmuebles', 'bmuebles', 'bmueblesnr', 'resFis', 'pasivos', 'datosDeclaracion', 'spublicos', 'otross', 'fechaPresenta', 'tomaDecisiones', 'beneficiosPubl', 'reprActiva', 'clientesPrinc', 'beneficiosPriv', 'fideicomisos', 'oencargo', 'prestamo', 'totalingresos'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        // return Response::download($pdf->download('Declaracion'.$dpersonales['rfc'].'.pdf'), 'Declaracion'.$dpersonales['rfc'].'.pdf');

        return $pdf->download('Declaracion' . $dpersonales['rfc'] . '.pdf');

        // return $pdf->stream('resumen.pdf');
    }
}
