<?php

namespace App\Http\Controllers;

use App\CatSeguro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatSegurosController extends Controller
{
    public function index()
    {
        $seguros = CatSeguro::orderBy('valor', 'ASC')->get();
        return response()->json(['seguros' => $seguros]);
    }

    public function store(Request $request)
    {
        $seguro = CatSeguro::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['seguro' => $seguro]);
    }

    public function delete($id)
    {
        $seguro = CatSeguro::findOrFail($id);
        $seguro->delete();
    }
}