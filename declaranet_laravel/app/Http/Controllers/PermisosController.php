<?php

namespace App\Http\Controllers;

use App\Permisos;
use App\CatPermisos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;



class PermisosController extends Controller
{
    public function index()
    {
        $permisos = Permisos::orderBy('Rol', 'ASC')->get();
        return response()->json(['permisos' => $permisos]);
    }

    public function permisos()
    {
        $permisos = CatPermisos::orderBy('permiso', 'ASC')->get();
        return response()->json(['catpermisos' => $permisos]);
    }

    public function item($id)
    {
        $permisos = Permisos::where('id', $id)->get();
        return response()->json(['permisos' => $permisos]);
    }

    public function store(Request $request)
    {
        $permiso = Permisos::updateOrCreate(
            ['id' => $request->id],
            [
                'Rol' => $request->Rol,
                'Permisos' => $request->Permisos,
            ]
        );

        return response()->json(['permiso' => $permiso]);
    }

    public function delete($id)
    {
        $permiso = Permisos::findOrFail($id);
        $permiso->delete();
    }
}