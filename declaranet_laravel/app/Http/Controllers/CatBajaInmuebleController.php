<?php

namespace App\Http\Controllers;

use App\CatBajaInmueble;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatBajaInmuebleController extends Controller
{
    public function index()
    {
        $response = CatBajaInmueble::orderBy('valor', 'ASC')->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = CatBajaInmueble::updateOrCreate(
            ['id' => $request->id],
            [
                'codigo' => $request->codigo,
                'valor' => $request->valor,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $tipo = CatBajaInmueble::findOrFail($id);
        $tipo->delete();
    }
}