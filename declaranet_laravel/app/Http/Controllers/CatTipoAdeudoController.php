<?php

namespace App\Http\Controllers;

use App\CatTipoAdeudo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoAdeudoController extends Controller
{
    public function index()
    {
        $tipos = CatTipoAdeudo::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoAdeudo::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoAdeudo::findOrFail($id);
        $tipo->delete();
    }
}