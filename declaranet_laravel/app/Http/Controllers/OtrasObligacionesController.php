<?php

namespace App\Http\Controllers;

use App\OtrasObligaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OtrasObligacionesController extends Controller
{
   public function index()
    {
        $bienes = OtrasObligaciones::orderBy('id', 'DESC')->get();
        return response()->json(['bienes' => $bienes]);
    }

    public function item($id)
    {
        $obligaciones = OtrasObligaciones::where('ip_id', $id)->get();
        return response()->json(['obligaciones' => $obligaciones]);
    }

    public function store(Request $request)
    {
       $obligacion   = OtrasObligaciones::updateOrCreate(
            ['id' => $request->id],
            ['ip_id' => $request->ip_id,
                'cat_tipo_operacion_id' => $request->cat_tipo_operacion_id,
                'cat_tipo_acreedor_id' => $request->cat_tipo_acreedor_id,
                'cat_tipo_obligacion_id' => $request->cat_tipo_obligacion_id,
                'identificador_obligacion' => $request->identificador_obligacion,
                'nacional_extranjero_cat_pais_id' => $request->nacional_extranjero_cat_pais_id,
                'nombre_acreedor' => $request->nombre_acreedor,
                'rfc_acreedor' => $request->rfc_acreedor,
                'cat_sector_industrial_id' => $request->cat_sector_industrial_id,
                'domicilio_acreedor_cat_pais_id' => $request->domicilio_acreedor_cat_pais_id,
                'domicilio_acreedor_cat_entidad_federativa_id' => $request->domicilio_acreedor_cat_entidad_federativa_id,
                'domicilio_acreedor_cat_municipio_id' => $request->domicilio_acreedor_cat_municipio_id,
                'domicilio_acreedor_cp' => $request->domicilio_acreedor_cp,
                'domicilio_acreedor_cat_localidad_id' => $request->domicilio_acreedor_cat_localidad_id,
                'domicilio_acreedor_cat_vialidad_id' => $request->domicilio_acreedor_cat_vialidad_id,
                'domicilio_acreedor_numExt' => $request->domicilio_acreedor_numExt,
                'domicilio_acreedor_numInt' => $request->domicilio_acreedor_numInt,
                'fecha_obligacion' => $request->fecha_obligacion,
                'monto_original' => $request->monto_original,
                'cat_tipo_moneda_id' => $request->cat_tipo_moneda_id,
                'tasa_interes' => $request->tasa_interes,
                'saldo_pendiente' => $request->saldo_pendiente,
                'montos_abonados' => $request->montos_abonados,
                'plazo_obligacion' => $request->plazo_obligacion,
                'unidad_medida_adeudo_cat_unidad_medida_plazo_id' => $request->unidad_medida_adeudo_cat_unidad_medida_plazo_id,
                'cat_titular_id' => $request->cat_titular_id,
                'porcentaje_obligacion_titular' => $request->porcentaje_obligacion_titular,
                'garantia' => $request->garantia,
                'observaciones' => $request->observaciones,
                'created_at' => date('Ymd'),
                'created_by' => 1,
                'domicilio_acreedor_colonia' => $request->domicilio_acreedor_colonia,
                'domicilio_acreedor_vialidad_tipo' => $request->domicilio_acreedor_vialidad_tipo,
                'domicilio_acreedor_vialidad_nombre' => $request->domicilio_acreedor_vialidad_nombre
            ]
        );

        return response()->json(['obligacion' => $obligacion]);
    }

    public function delete($id)
    {
        $bien = OtrasObligaciones::findOrFail($id);
        $bien->delete();
    }
}
