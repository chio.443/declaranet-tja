<?php

namespace App\Http\Controllers;

use App\CatTipoObligacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoObligacionesController extends Controller
{
  public function index()
    {
        $tipos = CatTipoObligacion::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoObligacion::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoObligacion::findOrFail($id);
        $tipo->delete();
    }
}
