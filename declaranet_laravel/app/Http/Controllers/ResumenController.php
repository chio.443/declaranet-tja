<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DependienteEconomico;
use App\CatTipoRelacionDep;
use App\InformacionPersonal;


use App\CatTipoBien;
use App\CatTipoBienMueble;
use App\CatTipoParticipacion;
use App\GradoEscolaridad;
use App\Ambitos;


use PDF;
use App\Fiscal;


class ResumenController extends Controller
{

   public function money_format($format, $number) {
        $regex = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?' .
                '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
        if (setlocale(LC_MONETARY, 0) == 'C') {
            setlocale(LC_MONETARY, '');
        }
        $locale = localeconv();
        preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
        foreach ($matches as $fmatch) {
            $value = floatval($number);
            $flags = array(
                'fillchar' => preg_match('/\=(.)/', $fmatch[1], $match) ?
                        $match[1] : ' ',
                'nogroup' => preg_match('/\^/', $fmatch[1]) > 0,
                'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                        $match[0] : '+',
                'nosimbol' => preg_match('/\!/', $fmatch[1]) > 0,
                'isleft' => preg_match('/\-/', $fmatch[1]) > 0
            );
            $width = trim($fmatch[2]) ? (int) $fmatch[2] : 0;
            $left = trim($fmatch[3]) ? (int) $fmatch[3] : 0;
            $right = trim($fmatch[4]) ? (int) $fmatch[4] : $locale['int_frac_digits'];
            $conversion = $fmatch[5];

            $positive = true;
            if ($value < 0) {
                $positive = false;
                $value *= -1;
            }
            $letter = $positive ? 'p' : 'n';

            $prefix = $suffix = $cprefix = $csuffix = $signal = '';

            $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
            switch (true) {
                case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                    $prefix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                    $suffix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                    $cprefix = $signal;
                    break;
                case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                    $csuffix = $signal;
                    break;
                case $flags['usesignal'] == '(':
                case $locale["{$letter}_sign_posn"] == 0:
                    $prefix = '(';
                    $suffix = ')';
                    break;
            }
            if (!$flags['nosimbol']) {
                $currency = $cprefix .
                        ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
                        $csuffix;
            } else {
                $currency = $cprefix .$csuffix;
            }
            $space = $locale["{$letter}_sep_by_space"] ? ' ' : '';

            $value = number_format($value, $right, $locale['mon_decimal_point'], $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
            $value = @explode($locale['mon_decimal_point'], $value);

            $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
            if ($left > 0 && $left > $n) {
                $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
            }
            $value = implode($locale['mon_decimal_point'], $value);
            if ($locale["{$letter}_cs_precedes"]) {
                $value = $prefix . $currency . $space . $value . $suffix;
            } else {
                $value = $prefix . $value . $space . $currency . $suffix;
            }
            if ($width > 0) {
                $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                                STR_PAD_RIGHT : STR_PAD_LEFT);
            }

            $format = str_replace($fmatch[0], $value, $format);
        }
        return $format;
    }
    public function generaResumen2($id){
    	$auth = trim(str_replace("Bearer", "", $request->header("Authorization")));
        $token = Token::decodeToken($auth);

		$anioact    =  date("Y");


        $id = $token->uid;


        $func = InformacionPersonal::where('id', $id)
                  ->first();



         $dpersonales = array();

         //creo el arreglo en el orden en el que se van a mostrar los datos en la lista


         $dpersonales['nombres']=$func->nombres.' '.$func->primer_apellido.' '.$func->segundo_apellido.'';
		 $dpersonales['curp']= $func->curp;
		 $dpersonales['rfc']= $func->rfc;
		 //datos del encargo actual
		 $dencargo = array();
		 $dencargo['dependencia']= $func->encargo->ente->valor;//dependencia o ente
		 $dencargo['cargo']= $func->encargo->empleo_cargo_comision;//cargo
		 ///////////////////////////////////////////intereses
		 //empresas
		 $empresas = array();
		 foreach ($func->empresas as $empresa) {
				 $empresas[] = array('nombreEmpresa' => $empresa->nombre_empresa_sociedad_asociacion,
				   					 'naturaleza' => $empresa->naturaleza->valor
									);
				}
		//////////////////////////////////////////ingresos
		//////////sueldos publicos
		$spublicos = array();
		  foreach ($func->sueldosPublicos as $sueldo) {
		 		 	$spublicos[] = array('ente' => $sueldo->ente->valor,
		 		   					 	 'ingreso' => $sueldo->ingreso_bruto_anual
		 							);
		 		}

		//////////otros sueldos
		$otross = array();
		  foreach ($func->sueldosOtros as $sueldo) {
		 		 	$otross[] = array('nombre' => $sueldo->nombre_denominacion_razon_social,
		 		   					  'ingreso' => $sueldo->ingreso_bruto_anual
		 							);
		 		}

		///////////////////////////////////////activos
		//bienes inmuebles
		  $binmuebles = array();
		  foreach ($func->binmuebles as $inmueble) {
		 		 $binmuebles[] = array(	'tipoOperacion' => $inmueble->tipoOperacion->valor,
		 		   					 	'naturaleza' => $inmueble->tipoBien->valor,
				   					 	'domicilio' => $inmueble->domicilio_bien_vialidad_nombre.' #'.$inmueble->domicilio_bien_numext.', '.$inmueble->domicilio_bien_colonia,
		 		   					 	'padquisicion' => $inmueble->precio_adquisicion_valor
		 							);
		 		}
		// //bienes inmuebles reg
		 $bmuebles = array();
		  foreach ($func->bmuebles as $mueble) {
		 		 $bmuebles[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor ,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
		 		}
		 //bienes inmuebles no reg
		  $bmueblesnr = array();
		  foreach ($func->bmueblesnr as $mueble) {
		 		 $bmueblesnr[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
	 		}

		///////////////////////////////////////
                 $fiscal = Fiscal::where('ip_id', $id)
             					->where('created_at','>=', $anioact.'-01-01')
             					->first();
                 if($fiscal)
                     $resFis='Si';
                 else
                     $resFis='No';

		 $resumen = ["dpersonales"=>$dpersonales,
		 			 "dencargo"   =>$dencargo,
		 			 "intereses"=>$empresas,
                     "fiscal"=>$resFis,
		 			 "ingresos"   => array("spublicos" => $spublicos,
		 								    "otros" =>$otross),
		 			 "activos"    => array("inmuebles"=>$binmuebles,
							 			   "bmuebles"   =>$bmuebles,
							 			   "mueblesnr"=>$bmueblesnr

		 			 					   )];


		 return response()->json(['resumen' =>$resumen]);
		// return response()->json($func->bmueblesnr);




    }
     public function export_pdf2(Request $request, $id)
      {

        $func = InformacionPersonal::where('id', $id)
                  ->first();

         $dpersonales = array();
         //creo el arreglo en el orden en el que se van a mostrar los datos en la lista
         $dpersonales['nombres']=$func->nombres.' '.$func->primer_apellido.' '.$func->segundo_apellido.'';
		 $dpersonales['curp']= $func->curp;
		 $dpersonales['rfc']= $func->rfc;
		 //datos del encargo actual
		 $dencargo = array();
		 $dencargo['dependencia']= $func->encargo->ente->valor;//dependencia o ente
		 $dencargo['cargo']= $func->encargo->empleo_cargo_comision;//cargo
		 ///////////////////////////////////////////intereses
		 //empresas
		 $empresas = array();
		 foreach ($func->empresas as $empresa) {
				 $empresas[] = array('nombreEmpresa' => $empresa->nombre_empresa_sociedad_asociacion,
				   					 'naturaleza' => $empresa->naturaleza->valor
									);
				}
		//////////////////////////////////////////ingresos
		//////////sueldos publicos
		$spublicos = array();
		  foreach ($func->sueldosPublicos as $sueldo) {
		 		 	$spublicos[] = array('ente' => $sueldo->ente->valor,
		 		   					 	 'ingreso' => $sueldo->ingreso_bruto_anual
		 							);
		 		}

		//////////otros sueldos
		$otross = array();
		  foreach ($func->sueldosOtros as $sueldo) {
		 		 	$otross[] = array('nombre' => $sueldo->nombre_denominacion_razon_social,
		 		   					  'ingreso' => $sueldo->ingreso_bruto_anual
		 							);
		 		}

		///////////////////////////////////////activos
		//bienes inmuebles
		  $binmuebles = array();
		  foreach ($func->binmuebles as $inmueble) {
		 		 $binmuebles[] = array(	'tipoOperacion' => $inmueble->tipoOperacion->valor,
		 		   					 	'naturaleza' => $inmueble->tipoBien->valor,
				   					 	'domicilio' => $inmueble->domicilio_bien_vialidad_nombre.' #'.$inmueble->domicilio_bien_numext.', '.$inmueble->domicilio_bien_colonia,
		 		   					 	'padquisicion' => $inmueble->precio_adquisicion_valor
		 							);
		 		}
		// //bienes inmuebles reg
		 $bmuebles = array();
		  foreach ($func->bmuebles as $mueble) {
		 		 $bmuebles[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor ,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
		 		}
		 //bienes inmuebles no reg
		  $bmueblesnr = array();
		  foreach ($func->bmueblesnr as $mueble) {
		 		 $bmueblesnr[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
	 		}
		///////////////////////////////////////
                 $resFis = Fiscal::where('ip_id', $id)->first();
                 if($resFis)
                     $fiscal='Si';
                 else
                     $fiscal='No';


        $view =  \View::make('reporte', compact('dpersonales', 'dencargo','empresas','spublicos','otross','binmuebles','bmuebles','bmueblesnr','fiscal'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->download('Resumen'.$dpersonales['rfc'].'.pdf');

//        return $pdf->stream('resumen.pdf');
      }

 	public function export_pdf(Request $request, $id)
      {
      	setlocale(LC_MONETARY, 'es_MX');

        $func = InformacionPersonal::where('id', $id)->first();

		$dpersonales = array();

         //creo el arreglo en el orden en el que se van a mostrar los datos en la lista

        $dependientes = DependienteEconomico::where('informacion_personal_id',$id)->orderBy('id','asc')->get();


         $dpersonales['nombres']=strtoupper($func->nombres.' '.$func->primer_apellido.' '.$func->segundo_apellido.'');
		 $dpersonales['curp']= $func->curp;
		 $dpersonales['rfc']= $func->rfc;
		 //datos del encargo actual
		 $dencargo = array();
		 $dencargo['dependencia']= strtoupper($func->encargo->ente->valor);//dependencia o ente
		 $dencargo['cargo']= strtoupper($func->encargo->empleo_cargo_comision);//cargo
		 ///////////////////////////////////////////intereses
		 //empresas
		 $empresas = array();
		 foreach ($func->empresas as $empresa) {
                    $empresas[] = array(
                        'nombreEmpresa' => $empresa->nombre_empresa_sociedad_asociacion,
                        'naturaleza' => $empresa->naturaleza_vinculo_especificar );
               }



		//////////////////////////////////////////ingresos
		//////////sueldos publicos
		$spublicos = array();

		$sueldo = $func->ingresos;

	 	$spublicos[] = array('nombre' => 'I. Remuneración mensual neta del declarante por su cargo público ',
	   					 	 'ingreso' => $sueldo['sub_1']
						);


		//////////otros sueldos
		$otross = array();
	 	$otross[] = array('cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
	 					  'nombre' => $sueldo['razon_social'],
	   					  'ingreso' => $sueldo['razon_social_cantidad']
						);
	 	$otross[] = array('cat' => '',
	   					  'nombre' => $sueldo['ant_actividad_comercial'],
	   					  'ingreso' => $sueldo['ant_actividad_comercial_cantidad']
						);
	 	$otross[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 					  'nombre' => $sueldo['actividad_financiera'],
	   					  'ingreso' => $sueldo['actividad_financiera_cantidad']
						);
	 	$otross[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 					  'nombre' => $sueldo['servicios_profecionales'],
	   					  'ingreso' => $sueldo['servicios_profecionales_cantidad']
						);
	 	$otross[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 					  'nombre' => $sueldo['arrendamiento'],
	   					  'ingreso' => $sueldo['arrendamiento_cantidad']
						);
		 	//////////////////////////////////////////ingresos periodo anterior
		//////////sueldos publicos
		$spublicosant = array();

	 	$spublicosant[] = array('nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
	   					 	 'ingreso' => $this->money_format('%(#10n', $sueldo['ant_sub_12'])
						);


		//////////otros sueldos
		$otrossant = array();
	 	$otrossant[] = array('cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
	 					   'nombre' => $sueldo['ant_actividad_comercial'],
	   					  'ingreso' => $this->money_format('%(#10n', $sueldo['ant_actividad_comercial_cantidad'])
						);
	 	$otrossant[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 					  'nombre' => $sueldo['ant_actividad_financiera'],
	   					  'ingreso' => $this-> money_format('%(#10n', $sueldo['ant_actividad_financiera_cantidad'])
						);
	 	$otrossant[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 					  'nombre' => $sueldo['ant_servicios_profecionales'],
	   					  'ingreso' => $this->money_format('%(#10n', $sueldo['ant_servicios_profecionales_cantidad'])
						);
	 	$otrossant[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 					  'nombre' => $sueldo['ant_arrendamientos'],
	   					  'ingreso' => $this->money_format('%(#10n', $sueldo['ant_arrendamientos_cantidad'])
						);

	 	$otrossant[] = array('cat' => '5 Otros ingresos no considerados a los anteriores (Después de impuestos)',
	 					  'nombre' => '',
	   					  'ingreso' => $this->money_format('%(#10n', $sueldo['ant_otros_cantidad'])
						);



		///////////////////////////////////////activos
		//bienes inmuebles
		  $binmuebles = array();
		  foreach ($func->binmuebles as $inmueble) {
		 		 $binmuebles[] = array(	'tipoOperacion' => $inmueble->tipoOperacion->valor,
		 		   					 	'naturaleza' => $inmueble->tipoBien->valor,
				   					 	'domicilio' => $inmueble->domicilio_bien_vialidad_nombre.' #'.$inmueble->domicilio_bien_numext.', '.$inmueble->domicilio_bien_colonia,
		 		   					 	'padquisicion' =>  $this->money_format('%(#10n', $inmueble->precio_adquisicion_valor)
		 							);
		 		}
		// //bienes inmuebles reg
		 $bmuebles = array();
		  foreach ($func->bmuebles as $mueble) {
		 		 $bmuebles[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor ,
		 		   					 	'padquisicion' =>  $this->money_format('%(#10n', $mueble->precio_adquisicion_valor)
		 							);
		 		}
		 //bienes inmuebles no reg
		  $bmueblesnr = array();
		  foreach ($func->bmueblesnr as $mueble) {
		 		 $bmueblesnr[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor,
		 		   					 	'padquisicion' =>  $this->money_format('%(#10n', $mueble->precio_adquisicion_valor)
		 							);
	 		}

		///////////////////////////////////////
                 $fiscal = Fiscal::where('ip_id', $id)->first();
                 if($fiscal)
                     $resFis='Si';
                 else
                     $resFis='No';


                 //dd($dependientes);
        $view =  \View::make('reporte', compact('dpersonales', 'dencargo','empresas','spublicosant','otrossant','binmuebles','bmuebles','bmueblesnr','resFis','dependientes'))->render();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->download('Resumen'.$dpersonales['rfc'].'.pdf');

//        return $pdf->stream('resumen.pdf');
      }

      public function generaResumen($id){

		$anioact    =  date("Y");


        $func = InformacionPersonal::where('id', $id)
        		  ->with('tomaDecisiones','beneficiosPubl','reprActiva','clientesPrinc','beneficiosPriv','fideicomisos','inversiones','deudas','prestamo','curriculares','experiencia')
                  ->first();



         $dpersonales = array();

         //creo el arreglo en el orden en el que se van a mostrar los datos en la lista

         $dpersonales['nombres']=$func->nombres.' '.$func->primer_apellido.' '.$func->segundo_apellido.'';
		 $dpersonales['curp']= $func->curp;
		 $dpersonales['rfc']= $func->rfc;
		 //datos del encargo actual
		 $dencargo = array();
		 $dencargo['dependencia']= $func->encargo->ente->valor;//dependencia o ente
		 $dencargo['cargo']= $func->encargo->empleo_cargo_comision;//cargo

	    //dependientes
        $dependientes = DependienteEconomico::where('informacion_personal_id',$id)
        				->orderBy('id','asc')
        				->get();

        $dep = array();


		foreach ($dependientes as $dependiente) {
	 		$dep[] = array(
	 		 			'nombre' => $dependiente->nombres.' '. $dependiente->primer_apellido.' '. $dependiente->segundo_apellido,
	 		 			'relacion' => CatTipoRelacionDep::where('id', $dependiente->tipo_relacion_dep_id)->first()['valor']
	 				);
		}


        //datos curriculares

        $curriculares = array();


		foreach ($func->curriculares as $curricular) {
	 		$curriculares[] = array(
	 		 			'grado' => GradoEscolaridad::where('id', $curricular->grado_maximo_escolaridad_id)->first()['grado_escolaridad'],
	 		 			'institucion' => $curricular->institucion_educativa,
	 		 			'carrera' => $curricular->carrera,
	 				);
		}

        //experiencia laboral
        $experiencia = array();


		foreach ($func->experiencia as $exp) {
	 		$experiencia[] = array(
	 		 			'ambito' => Ambitos::where('id', $exp->ambito_id)->first()['valor'],
	 		 			'institucion' => $exp->nombre_institucion,
	 		 			'unidad' => $exp->unidad_administrativa
	 				);
		}




		 ///////////////////////////////////////////intereses //////////////////////////////////////////
		 //empresas sociedades asociaciones

			 $empresas = array();
			 foreach ($func->empresas as $empresa) {
					 $empresas[] = array('nombreEmpresa' => $empresa->nombre_empresa_sociedad_asociacion,
					   					 'naturaleza' =>  CatTipoParticipacion::where('id', $empresa->naturaleza_vinculo)->first()['valor']
										);
					}
			//toma de decisiones
			$tomaDecisiones  = array();
			 foreach ($func->tomaDecisiones as $toma) {
					 $tomaDecisiones[] =
					 array(		'nombre' => $toma->nombre,
					   			'puesto' => $toma->puesto
						);
			}

			//beneficios publicos
			$beneficiosPubl  = array();
			 foreach ($func->beneficiosPubl as $beneficio) {
					 $beneficiosPubl[] = array('programa' => $beneficio->programa,
					   				  			'institucion' => $beneficio->institucion_otorgante
									);
			}

			//representación activa
			$reprActiva  = array();
			 foreach ($func->reprActiva as $rep) {
					 $reprActiva[] = array('nombre' => $rep->nombre,
					   				 	 	'rfc' => $rep->rfc
									);
			}
			//clientes principales
			$clientesPrinc  = array();
			 foreach ($func->clientesPrinc as $cte) {
					 $clientesPrinc[] = array('nombre' => $cte->nombre_cliente,
					   				 		  'rfc' => $cte->rfc_cliente
									);
			}
			//
			//beneficios privados
			$beneficiosPriv  = array();
			 foreach ($func->beneficiosPriv as $benef) {
					 $beneficiosPriv[] = array('nombre' => $benef->nombre,
							   				  'rfc' => $benef->rfc
											);
			}
			//fideicomisos
			$fideicomisos  = array();
			 foreach ($func->fideicomisos as $fid) {
					 $fideicomisos[] = array('nombre' => $fid->nombre_fideicomitente,
						   				  'rfc' => $fid->rfc_fideicomitente
										);
			}




		//////////////////////////////////////////ingresos
		//////////sueldos publicos
		$spublicos = array();

		$sueldo = $func->ingresos;

	 	$spublicos[] = array('nombre' => 'I. Remuneración mensual neta del declarante por su cargo público ',
	   					 	 'ingreso' => $sueldo['sub_1']
						);


		//////////otros sueldos
		$otross = array();
	 	$otross[] = array('cat' => '1 Por actividad industrial y/o comercial
          Especifica nombre o razón social y tipo de negocio',
	 					   'nombre' => $sueldo['razon_social'],
	   					  'ingreso' => $sueldo['razon_social_cantidad']
						);
	 	$otross[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 					  'nombre' => $sueldo['actividad_financiera'],
	   					  'ingreso' => $sueldo['actividad_financiera_cantidad']
						);
	 	$otross[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 					  'nombre' => $sueldo['servicios_profecionales'],
	   					  'ingreso' => $sueldo['servicios_profecionales_cantidad']
						);
	 	$otross[] = array('cat' => '4 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 					  'nombre' => $sueldo['arrendamiento'],
	   					  'ingreso' => $sueldo['arrendamiento_cantidad']
						);

		 	//////////////////////////////////////////ingresos periodo anterior
		//////////sueldos publicos
		$spublicosant = array();

	 	$spublicosant[] = array('nombre' => 'I. Remuneración anual neta del declarante por su cargo público (deduzca impuestos) (por concepto de sueldos, honorarios, compensaciones, bonos y otras prestaciones)',
	   					 	 'ingreso' => $sueldo['ant_sub_12']
						);


		//////////otros sueldos
		$otrossant = array();
	 	$otrossant[] = array('cat' => '1 Por actividad industrial y/o comercial
          Especifíca nombre o razón social y tipo de negocio',
	 					   'nombre' => $sueldo['ant_actividad_comercial'],
	   					  'ingreso' => $sueldo['ant_actividad_comercial_cantidad']
						);
	 	$otrossant[] = array('cat' => '2 Por actividad financiera (rendimientos de contratos bancarios o de valores)',
	 					  'nombre' => $sueldo['ant_actividad_financiera'],
	   					  'ingreso' => $sueldo['ant_actividad_financiera_cantidad']
						);
	 	$otrossant[] = array('cat' => '3 Por servicios profesionales, participación en consejos, consultorías o asesorías Especifica el tipo de servicio y el contratante ',
	 					  'nombre' => $sueldo['ant_servicios_profecionales'],
	   					  'ingreso' => $sueldo['ant_servicios_profecionales_cantidad']
						);

	 	$otrossant[] = array('cat' => '4 Ingreso anual neto del cónyuge, concubina o concubinario y/o dependientes económicos',
	 					  'nombre' => $sueldo['ant_neto_conyugue'],
	   					  'ingreso' => $sueldo['ant_neto_conyugue_cantidad']
						);

	 	$otrossant[] = array('cat' => '5 Otros (arrendamientos, regalías, sorteos, concursos, donaciones, etc.)',
	 					  'nombre' => $sueldo['ant_arrendamientos'],
	   					  'ingreso' => $sueldo['ant_arrendamientos_cantidad']
						);
	 	$otrossant[] = array('cat' => '6 Otros ingresos no considerados a los anteriores (Después de impuestos)',
	 					  'nombre' => '',
	   					  'ingreso' => $sueldo['ant_otros_cantidad']
						);

		///////////////////////////////////////activos
		//bienes inmuebles
		  $binmuebles = array();
		  foreach ($func->binmuebles as $inmueble) {
		 		 $binmuebles[] = array(	'tipoOperacion' => $inmueble->tipoOperacion->valor,
		 		   					 	'naturaleza' => $inmueble->tipoBien->valor,
				   					 	'domicilio' => $inmueble->domicilio_bien_vialidad_nombre.' #'.$inmueble->domicilio_bien_numext.', '.$inmueble->domicilio_bien_colonia,
		 		   					 	'padquisicion' => $inmueble->precio_adquisicion_valor
		 							);
		 		}
		// //bienes inmuebles reg
		 $bmuebles = array();
		  foreach ($func->bmuebles as $mueble) {
		 		 $bmuebles[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor ,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
		 		}
		 //bienes inmuebles no reg
		  $bmueblesnr = array();
		  foreach ($func->bmueblesnr as $mueble) {
		 		 $bmueblesnr[] = array(	'tipoOperacion' => $mueble->tipoOperacion->valor,
		 		   					 	'tipoBien' => $mueble->tipoBien->valor,
		 		   					 	'padquisicion' => $mueble->precio_adquisicion_valor
		 							);
	 		}
	 	 //inversiones
		  $inversiones = array();
		  foreach ($func->inversiones as $inversion) {
		 		 $inversiones[] = array('institucion' => $inversion->nombre_institucion,
		 		   					 	'monto' => $inversion->monto_original,
		 							);
	 		}

	 	///////////////////////////////////////pasivos
		//adeudos pasivos
		  $deudas = array();
		  foreach ($func->deudas as $deuda) {
		 		 $deudas[] = array(	'identificador' => $deuda->identificador_deuda,
		 		   					 'acreedor' => $deuda->nombre_acreedor,
				   					 'saldo' => $deuda->saldo_pendiente
		 							);
		 		}

		// //prestamo comodato
		 $prestamo = array();
		  foreach ($func->prestamo as $pr) {
		 		if($pr->tipo_inmueble_id){//si es un inmueble
		 			 $prestamo[] = array(	'tipo' => 'Inmueble',
		 		   					 	'tipoBien' => CatTipoBien::where('id', $pr->tipo_inmueble_id)->first()['valor']
		 							);
		 		}else{//si es un vehiculo
		 			 $prestamo[] = array(	'tipo' => 'Vehículo',
		 		   					 	'tipoBien' => CatTipoBienMueble::where('id', $pr->tipo_vehiculo_id)->first()['valor']
		 							);

		 		}

		 }




		///////////////////////////////////////	fiscal

                 /* $fiscal = Fiscal::where('ip_id', $id)->first();
                 if($fiscal)
                     $resFis='Si';
                 else
                     $resFis='No'; */

				 $fiscal = Fiscal::where('ip_id', $id)
					 ->where('created_at','>=', $anioact.'-01-01')
					 ->first();
	 			if($fiscal)
		 			$resFis='Si';
	 			else
		 			$resFis='No';

		 $resumen = ["dpersonales"=>$dpersonales,
		 			 "dencargo"   =>$dencargo,
		 			 "dependientes"   =>$dep,
		 			 "intereses"   => array("empresas" => $empresas,
		 								    "tomaDecisiones" =>$tomaDecisiones,
		 								    "beneficiosPubl" => $beneficiosPubl,
		 								    "reprActiva" =>$reprActiva,
		 								    "clientesPrinc" =>$clientesPrinc,
		 									"beneficiosPriv" =>$beneficiosPriv,
		 									"fideicomisos" =>$fideicomisos
		 							),
		 			 "experiencia"=>$experiencia,
		 			 "curriculares"=>$curriculares,
                     "fiscal"=>$resFis,
		 			 "ingresos"   => array("spublicos" => $spublicos,
		 								    "otros" =>$otross,
		 								    "spublicosant" => $spublicosant,
		 								    "otrosant" =>$otrossant),
		 			 "activos"    => array("inmuebles"=>$binmuebles,
							 			   "bmuebles"   =>$bmuebles,
							 			   "mueblesnr"=>$bmueblesnr,
							 			   "inversiones" =>$inversiones),
		 			 "pasivos"    => array("deudas"=>$deudas,
							 			   "prestamo"   =>$prestamo
							 			 ),
		 			];


		 return response()->json(['resumen' =>$resumen]);
		// return response()->json($func->bmueblesnr);




    }
}
