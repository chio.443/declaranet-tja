<?php

namespace App\Http\Controllers;

use App\CatTipoMetal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CatTipoMetalController extends Controller
{
    public function index()
    {
        $tipos = CatTipoMetal::orderBy('valor', 'ASC')->get();
        return response()->json(['tipos' => $tipos]);
    }

    public function store(Request $request)
    {
        $tipo = CatTipoMetal::updateOrCreate(
            ['id' => $request->id],
            ['codigo' => $request->codigo,
            'valor' => $request->valor,
            ]
        );

        return response()->json(['tipo' => $tipo]);
    }

    public function delete($id)
    {
        $tipo = CatTipoMetal::findOrFail($id);
        $tipo->delete();
    }
}