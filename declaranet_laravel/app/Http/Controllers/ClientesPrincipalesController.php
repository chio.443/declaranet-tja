<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Clientes_principales;

class ClientesPrincipalesController extends Controller
{
    public function index()
    {
        $response = Clientes_principales::orderBy('id', 'DESC')->get();
        return response()->json(['response' => $response]);
    }

    public function item($id)
    {
        $response = Clientes_principales::where('ip_id', $id)->get();
        return response()->json(['response' => $response]);
    }

    public function store(Request $request)
    {
        $response = Clientes_principales::updateOrCreate(
            ['id' => $request->id],
            [
                'ip_id' => $request->ip_id,
                'propietario' => $request->propietario,
                'nombre' => $request->nombre,
                'rfc' => $request->rfc,
                'nombre_cliente' => $request->nombre_cliente,
                'rfc_cliente' => $request->rfc_cliente,
                'pais_id' => $request->pais_id,
                'entidad_federativa_id' => $request->entidad_federativa_id,
                'tipo_persona' => $request->tipo_persona,
                'monto' => $request->monto,
                'sector_industria_id' => $request->sector_industria_id,
                'observaciones' => $request->observaciones,
            ]
        );

        return response()->json(['response' => $response]);
    }

    public function delete($id)
    {
        $cliente = Clientes_principales::findOrFail($id);
        $cliente->delete();
    }
}