<?php

namespace App\Http\Middleware;

use Closure;
use App\Token;

class TokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = trim(str_replace("Bearer", "", $request->header("Authorization")));

        if ($user = Token::checkToken($auth)) {

            auth()->loginUsingId($user);

            return $next($request);
        }

        return response()->json(['message' => "Acceso no autorizado"], 401);
    }
}