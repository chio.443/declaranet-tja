<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosCurriculares extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'datos_curriculares';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $casts = ['fecha_obtencion' => 'date:d-m-Y'];


    public function pais()
    {
        return $this->belongsTo('App\Pais', 'pais_nacimiento_id', 'id')->withDefault();
    }
    public function ent()
    {
        return $this->belongsTo('App\CatEntidadFederativa', 'entidad_federativa_nacimiento_id', 'id')->withDefault();
    }
    public function gradoEsc()
    {
        return $this->belongsTo('App\GradoEscolaridad', 'grado_maximo_escolaridad_id', 'id')->withDefault();
    }
    public function periodos()
    {
        return $this->belongsTo('App\CatPeriodosCursados', 'periodos_cursados_id', 'id')->withDefault();
    }
    public function docto()
    {
        return $this->belongsTo('App\DocumentoObtenido', 'documento_obtenido_id', 'id')->withDefault();
    }
    public function estatus()
    {
        return $this->belongsTo('App\EstatusCarrera', 'estatus_id', 'id')->withDefault();
    }
}