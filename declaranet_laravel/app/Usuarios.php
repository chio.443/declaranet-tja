<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "usuarios";
    protected $guarded = ['id'];
    //protected $dateFormat = 'Y-m-d H:i:sP';

    //protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    //protected $with = ['encargo', 'ente']; // Consultar datos de las tablas relacionadas


    public function encargo()
    {
        return $this->belongsTo('App\EncargoActual', 'id', 'informacion_personal_id')->withDefault();
    }

    public function ente()
    {
        return $this->belongsTo('App\Ente_publico', 'ID_Dependencia', 'id')->withDefault();
    }
    public function roles()
    {
        return $this->belongsTo('App\Roles', 'tipo_id', 'id')->withDefault();
    }
}