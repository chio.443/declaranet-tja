<?php

namespace App;

use Firebase\JWT\JWT;
use Illuminate\Support\Carbon;

class Token
{
    const KEY = "my-secret-fucking-key";


    public static function getToken($params)
    {
        // dd($params);
        $token = array(
            "uid"   => $params->id,
            "status_dec" => $params->status_dec,
            "ente_publico_id" => $params->ente_publico_id,
            "fecha_termino" => $params->fecha_termino,
            "email" => $params->correo_electronico_laboral,
            "role"  => $params->rol_id,
            "grupo"  => $params->grupo,
            "simplificada"  => $params->simplificada,
            "nombre"  => $params->nombre,
            "ente"  => $params->ente,
            "periodo"  => $params->periodo,
            "tipo_dec"  => $params->tipo_dec,
            "tipo_dec1"  => $params->tipo_dec1,
            "tipo_dec_usuario"  => $params->tipo_dec_usuario,
            "tipo_dec_list"  => $params->tipo_dec_list,
            "expires" => now()->addDays(1),
            "permisos" => $params->permisos,
            "movimiento" => $params->movimiento,
        );
        $jwt = JWT::encode($token, "my-secret-fucking-key");
        return $jwt;
    }

    public static function decodeToken($jwt)
    {
        //dd($jwt);
        //exit();

        $decoded = JWT::decode($jwt, "my-secret-fucking-key", array('HS256'));

        return $decoded;
    }

    public static function checkToken($token)
    {
        // return false;
        $decode = Token::decodeToken($token);

        if ($decode && is_numeric($decode->uid)) {
            if (!empty($decode->expires->date)) {
                $expires = Carbon::parse($decode->expires->date);
            } else {
                $expires = Carbon::parse($decode->expires);
            }
            if (now() < $expires)
                return $decode->uid;
        }

        return false;
    }
}
