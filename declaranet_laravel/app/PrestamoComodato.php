<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestamoComodato extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded	= ['id'];
    protected $table 	= 'prestamo_comodato';
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
    protected $with = ['tipoInm','tipoVeh'];

	public function tipoInm()
	  {
	    return $this->belongsTo('App\CatTipoBien', 'subtipo_id', 'id')->withDefault();
	  }
	public function tipoVeh()
	  {
	    return $this->belongsTo('App\CatTipoBienMueble', 'subtipo_id', 'id')->withDefault();
	  }
	  

    
}
