<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad_temporal extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'cat_unidad_temporal';
}