<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatTipoDeclaracion extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "cat_tipo_declaracion";
    protected $guarded = ['id'];
}