<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitacoraDeclaracion extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "bitacora_declaraciones";
    protected $guarded = ['id'];
    protected $hidden = array('updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');


}