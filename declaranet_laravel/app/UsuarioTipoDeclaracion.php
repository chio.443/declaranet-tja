<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioTipoDeclaracion extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'usuario_tipo_dec';
    // protected $with = ['declaracion'];
    protected $appends = ['ente'];

    public function getEnteAttribute()
    {
        if ($this->enteutd) {
            return $this->enteutd->valor;
        }
        return '';
    }

    public function declarante()
    {
        return $this->hasMany('App\InformacionPersonal', 'id', 'ip_id');
    }

    public function bitacora()
    {
        return $this->hasOne('App\BitacoraDeclaracion', 'declaracion_id', 'declaracion_id');
    }

    public function declaracion()
    {
        return $this->hasOne('App\Declaracion', 'id', 'declaracion_id');
    }

    public function enteutd()
    {
        return $this->hasOne('App\Ente_publico', 'id', 'ente_publico_id');
    }
}
