<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EncargoActualFuncP extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'datos_encargo_actual_funciones_principales';


    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $appends = ['funcion'];

    public function getFuncionAttribute()
    {
        return $this->funcionesp->valor;
    }

    public function funcionesp()
    {
        return $this->hasOne('App\FuncionesPrincipales', 'id', 'funcion_id');
    }
}
