<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deudas extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    // protected $table = "pasivos_adeudos";
    protected $table = "pasivos_deudas";
    protected $guarded = ['id'];
    // protected $dates = ['fecha_adeudo'];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
    protected $casts = ['fecha_adeudo' => 'date:d-m-Y'];

    public function tipoOperacion()
    {
        return $this->belongsTo('App\CatTipoOperacion', 'cat_tipo_operacion_id', 'id')->withDefault();
    }
    public function tipoAdeudo()
    {
        return $this->belongsTo('App\CatTipoAdeudo', 'cat_tipo_adeudo_id', 'id')->withDefault();
    }
}