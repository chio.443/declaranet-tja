<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InversionesCuentasValores extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "inversiones_cuentas_valores";
    protected $guarded = ['id'];

    protected $casts = [
        'fecha_inicio'=> 'date:d-m-Y', 
        'fecha_venta'=>'date:d-m-Y'
    ];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
	
	public function tipoOperacion()
	  {
	    return $this->belongsTo('App\CatTipoOperacion', 'tipo_operacion_id', 'id')->withDefault();
	  }
	public function tipoInv()
	  {
	    return $this->belongsTo('App\CatTipoInversion', 'tipo_inversion_id', 'id')->withDefault();
	  }
	public function pais()
	  {
	    return $this->belongsTo('App\Pais', 'domicilio_institucion_pais_id', 'id')->withDefault();
	  }
	public function entidad()
	  {
	    return $this->belongsTo('App\CatEntidadFederativa', 'domicilio_institucion_entidad_federativa_id', 'id')->withDefault();
	  }
	public function titular()
	  {
	    return $this->belongsTo('App\CatTitular', 'titular_id', 'id')->withDefault();
	  }
}