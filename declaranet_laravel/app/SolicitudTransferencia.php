<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudTransferencia extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'solicitudes_transferencia';

    protected $with = ['declarante', 'usuario', 'dependenciaOrigen', 'dependenciaDestino'];

    protected $casts = ['datos_cambio' => 'array', 'datos_actuales' => 'array'];

    public function declarante()
    {
        return $this->hasOne('App\InformacionPersonal', 'id', 'ip_id');
    }
    public function usuario()
    {
        return $this->hasOne('App\Usuarios', 'id', 'created_by')->withTrashed();
    }
    public function dependenciaOrigen()
    {
        return $this->hasOne('App\CatEntePublico', 'id', 'ente_publico_origen');
    }
    public function dependenciaDestino()
    {
        return $this->hasOne('App\CatEntePublico', 'id', 'ente_publico_destino');
    }
}