<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimientos extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;


    protected $table = "movimientos";
    protected $guarded = ['id'];
    protected $with = ['servidor'];
    //protected $casts = ['actividad_economica' => 'integer'];
    protected $casts = ['origen' => 'array', 'cambio' => 'array'];
    //protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    public function usuarios()
    {
        return $this->belongsTo('App\InformacionPersonal', 'ip_id', 'id')->withTrashed();
    }
    public function servidor()
    {
        return $this->belongsTo('App\InformacionPersonal', 'ip_id', 'id')->withDefault();
    }
    public function usuario()
    {
        return $this->belongsTo('App\Usuarios', 'created_by', 'id')->withTrashed();
    }
    public function aprobacion()
    {
        return $this->belongsTo('App\Usuarios', 'aprobado', 'id')->withTrashed();
    }
    public function encargo()
    {
        return $this->belongsTo('App\EncargoActual', 'ip_id', 'informacion_personal_id')->withDefault();
    }
}
