<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class InformacionPersonal extends Authenticatable
{
    use Notifiable;
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'informacion_personal';

    protected $hidden = array('updated_at', 'deleted_at', 'updated_by', 'deleted_by');

    protected $with = ['nac', 'vialidad', 'encargo', 'declaracion'];


    public function vialidad()
    {
        return $this->belongsTo('App\CatVialidad', 'domicilio_vialidad_id', 'id')->withDefault();
    }

    public function nac()
    {
        return $this->hasMany('App\IGNacionalidades', 'ip_id', 'id'); //->select(array('id', 'nac_id'));;

    }
    public function roles()
    {
        return $this->belongsTo('App\Roles', 'rol_id', 'id')->withDefault();
    }

    public function encargo()
    {
        return $this->belongsTo('App\EncargoActual', 'id', 'informacion_personal_id')->withDefault();
    }

    public function pendientes()
    {
        return $this->hasMany('App\UsuarioTipoDeclaracion', 'ip_id', 'id')
            ->whereNull('declaracion_id');
    }

    public function utdeclaraciones()
    {
        return $this->hasMany('App\UsuarioTipoDeclaracion', 'ip_id', 'id');
    }
    public function empresas()
    {
        return $this->hasMany('App\Empresas', 'ip_id', 'id');
    }

    public function experiencia()
    {
        return $this->hasMany('App\ExperenciaLaboral', 'informacion_personal_id', 'id');
    }

    public function binmuebles()
    {
        return $this->hasMany('App\BienesInmuebles', 'ip_id', 'id');
    }

    public function bmuebles()
    {
        return $this->hasMany('App\BienesMueblesRegistrables', 'ip_id', 'id');
    }

    public function bmueblesnr()
    {
        return $this->hasMany('App\BienesMueblesNoRegistrables', 'ip_id', 'id');
    }

    public function sueldosPublicos()
    {
        return $this->hasMany('App\SueldosSalariosPublicos', 'ip_id', 'id');
    }

    public function sueldosOtros()
    {
        return $this->hasMany('App\SueldosSalariosOtrosEmpleos', 'ip_id', 'id');
    }

    public function ingresos()
    {
        return $this->hasOne('App\Ingresos', 'ip_id', 'id');
    }

    public function ente()
    {
        return $this->belongsTo('App\Ente_publico', 'ente_publico_id', 'id')->withDefault();
    }

    public function entidadFedD()
    {
        return $this->belongsTo('App\CatEntidadFederativa', 'domicilio_entidad_federativa_id', 'id')->withDefault();
    }
    public function municipioD()
    {
        return $this->belongsTo('App\CatMunicipio', 'domicilio_municipio_id', 'id')->withDefault();
    }
    public function localidadD()
    {
        return $this->belongsTo('App\CatLocalidad', 'domicilio_localidad_id', 'id')->withDefault();
    }
    public function paisNac()
    {
        return $this->belongsTo('App\Pais', 'pais_nacimiento_id', 'id')->withDefault();
    }
    public function entNac()
    {
        return $this->belongsTo('App\CatEntidadFederativa', 'entidad_federativa_nacimiento_id', 'id')->withDefault();
    }
    public function edoCivil()
    {
        return $this->belongsTo('App\EstadoCivil', 'estado_civil_id', 'id')->withDefault();
    }
    public function regimenMat()
    {
        return $this->belongsTo('App\RegimenMat', 'regimen_matrimonial_id', 'id')->withDefault();
    }

    public function tomaDecisiones()
    {
        return $this->hasMany('App\TomaDecisiones', 'ip_id', 'id');
    }
    public function beneficiosPubl()
    {
        return $this->hasMany('App\ApoyoBeneficioPublico', 'ip_id', 'id');
    }
    public function reprActiva()
    {
        return $this->hasMany('App\Representacion_activa', 'ip_id', 'id');
    }
    public function clientesPrinc()
    {
        return $this->hasMany('App\Clientes_principales', 'ip_id', 'id');
    }
    public function beneficiosPriv()
    {
        return $this->hasMany('App\BeneficiosPrivados', 'ip_id', 'id');
    }
    public function fideicomisos()
    {
        return $this->hasMany('App\Fideicomiso', 'ip_id', 'id');
    }
    public function inversiones()
    {
        return $this->hasMany('App\InversionesCuentasValores', 'ip_id', 'id');
    }
    public function deudas()
    {
        return $this->hasMany('App\Deudas', 'ip_id', 'id');
    }
    public function prestamo()
    {
        return $this->hasMany('App\PrestamoComodato', 'ip_id', 'id');
    }
    public function curriculares()
    {
        return $this->hasMany('App\DatosCurriculares', 'informacion_personal_id', 'id');
    }
    public function declaracion()
    {
        return $this->belongsTo('App\Declaracion', 'id', 'id_ip')->withDefault();
    }

    public function declaraciones()
    {
        return $this->hasMany('App\Declaracion', 'id_ip', 'id');
    }

    public function movimientos()
    {
        return $this->hasMany('App\Movimientos', 'ip_id', 'id');
    }

    public function transferencias()
    {
        return $this->hasMany('App\SolicitudTransferencia', 'ip_id', 'id');
    }

    public function tipodeclaracion()
    {
        return $this->hasMany('App\UsuarioTipoDeclaracion', 'ip_id', 'id');
    }

    public function tipodeclaracionl()
    {
        return $this->hasOne('App\UsuarioTipoDeclaracion', 'ip_id', 'id');
    }
}
