<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BitacoraConsultaBalanza extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "bitacora_consultas_balanza";
    protected $guarded = ['id'];
    protected $hidden = array('updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    public function usuario()
	  {
	    return $this->belongsTo('App\Usuarios', 'created_by', 'id')->withDefault();
	  }
	 public function declarante()
	  {
	    return $this->belongsTo('App\InformacionPersonal', 'ip_id', 'id')->withDefault();
	  }
	  public function declaracion()
	  {
	    return $this->belongsTo('App\Declaracion', 'declaracion_id', 'id')->withDefault();
	  }

}