<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otra_parte extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'otras_partes';
}