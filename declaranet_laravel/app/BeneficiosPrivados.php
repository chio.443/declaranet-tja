<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class BeneficiosPrivados extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'beneficios_privados';
}