<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BienesInmuebles extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "bienes_inmuebles";
    protected $guarded = ['id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
    protected $casts = [
        'fecha_adquisicion' => 'date:d-m-Y',
        'identificacion_bien_fecha_contrato' => 'date:d-m-Y',
        'fecha_obra' => 'date:d-m-Y',
        'fecha_venta' => 'date:d-m-Y',
    ];
    /* protected $dateFormat = 'd/m/Y'; */

    protected $with = ['tipoOperacion', 'tipoBien'];

    public function tipoOperacion()
    {
        return $this->belongsTo('App\CatTipoOperacion', 'tipo_operacion_id', 'id')->withDefault();
    }
    public function tipoBien()
    {
        return $this->belongsTo('App\CatTipoBien', 'tipo_bien_id', 'id')->withDefault();
    }
}