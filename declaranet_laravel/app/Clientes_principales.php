<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes_principales extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'clientes_principales';
}