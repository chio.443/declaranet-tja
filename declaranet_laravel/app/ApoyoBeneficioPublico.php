<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApoyoBeneficioPublico extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'apoyos_beneficios_publicos';
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}