<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $table = "empresas_sociedades_asociaciones";
    protected $guarded = ['id'];
    protected $casts = ['actividad_economica' => 'integer'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $with =['naturaleza'];

    public function naturaleza(){
       return $this->belongsTo('App\CatNaturalezas', 'naturaleza_vinculo', 'id')->withDefault();
    }

}