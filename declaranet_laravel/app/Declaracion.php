<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Declaracion extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'declaraciones';
    protected $primaryKey = 'id';

    public function declarante()
    {
        return $this->hasMany('App\InformacionPersonal', 'id', 'id_ip'); //->select(array('id', 'nac_id'));;
    }

    public function repdeclarante()
    {
        return $this->hasOne('App\InformacionPersonal', 'id', 'id_ip'); //->select(array('id', 'nac_id'));;
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuarios', 'deleted_by', 'id')->withDefault();
    }

    public function tipoDec()
    {
        return $this->belongsTo('App\CatTipoDeclaracion', 'tipo_declaracion_id', 'id')->withDefault();
    }
}
