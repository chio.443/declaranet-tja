<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivel_orden_gobierno extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'cat_nivel_orden_gobierno';
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}