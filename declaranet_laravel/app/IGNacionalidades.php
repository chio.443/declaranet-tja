<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IGNacionalidades extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'rel_informacion_general_nacionalidades';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    //  protected $with =['pais'];

    public function pais()
    {
        return $this->belongsTo('App\Pais', 'nac_id', 'id')->withDefault();
    }
}