<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DependienteEconomico extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $table     = 'dependientes_economicos';

    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    protected $casts = ['fecha_nacimiento' => 'date:d-m-Y', 'fecha_inicio' => 'date:d-m-Y'];

    protected $with = ['vialidad', 'nac', 'programa', 'entidad'];


    public function vialidad()
    {
        return $this->belongsTo('App\CatVialidad', 'domicilio_vialidad_id', 'id')->withDefault();
    }
    public function nac()
    {
        return $this->hasMany('App\DependienteNacionalidad', 'depec_id', 'id');
    }
    public function programa()
    {
        return $this->hasOne('App\DependienteBeneficiario', 'depec_id', 'id')->withDefault();
    }
    public function entidad()
    {
        return $this->hasOne('App\DependenciaEntDep', 'depec_id', 'id')->withDefault();
    }
    public function tipoRel()
    {
        return $this->belongsTo('App\CatTipoRelacionDep', 'tipo_relacion_dep_id', 'id')->withDefault();
    }
}