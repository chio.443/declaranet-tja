<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representacion_activa extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'representacion_activa';
    protected $casts = ['fecha' => 'date:d-m-Y']; //, 'pagado' => 'integer'
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
    /* protected $casts = [

        'identificacion_bien_fecha_contrato' => 'date:d-m-Y',
        'fecha_obra' => 'date:d-m-Y',
        'fecha_venta' => 'date:d-m-Y',
    ]; */
}