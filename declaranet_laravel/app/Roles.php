<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded    = ['id'];
    protected $casts = ['Permisos' => 'array'];
    protected $table     = 'cat_roles';
    protected $hidden     = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');
}