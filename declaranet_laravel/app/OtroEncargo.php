<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtroEncargo extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded       = ['id'];
    protected $table     = 'otro_encargo';
    protected $casts = ['fecha_posesion' => 'date:d-m-Y','funciones' => 'array'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

}