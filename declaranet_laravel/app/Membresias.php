<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membresias extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Wildside\Userstamps\Userstamps;

    protected $guarded = ['id'];
    protected $table = 'membresias';
    protected $casts = ['fecha_inicio' => 'date:d-m-Y', 'pagado' => 'integer'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by');

    public function tipoSociedad()
    {
        return $this->belongsTo('App\CatTipoSociedad', 'tipo_sociedad_id', 'id')->withDefault();
    }
    public function tipoOperacion()
    {
        return $this->belongsTo('App\CatTipoOperacion', 'tipo_operacion_id', 'id')->withDefault();
    }
    public function titular()
    {
        return $this->belongsTo('App\CatTitular', 'tipo_responsable_id', 'id')->withDefault();
    }
}